// require('../css/compiled.css');
// var $ = require('jquery');
// require('jquery-ui');
// var qtip = require('qtip2');
// global.$ = global.jQuery = $;
var Event = function (event_type_id, minute, event) {
    var self = this;

    self.event_type_id = event_type_id;
    self.minute = minute;
    self.event = event;
};

var Formation = function (position, quantity) {
    var self = this;

    self.position = ko.observable(position);
    self.quantity = ko.observable(quantity);
};

var playerperteam = function (id, starting_11) {
    var self = this;

    self.player_id = id;
    self.starting_11 = starting_11;
};

var Player = function (id, name, matchday_id, played_from, played_to,
                       member_played_from, member_played_to, is_playing,
                       position_id, kit_mm, sub_of, info, events, is_offline) {

    if (is_offline == undefined) is_offline = false;

    var self = this;

    var _played_from = played_from;
    var _played_to = played_to;
    var member_played_from = member_played_from;
    var _member_played_to = member_played_to;
    var sub_of = sub_of;
    var events = events ? events : [];
    var _kit_mm = kit_mm;
    var red_card_at = null;

    self.player_id = id;
    self.name = ko.observable(name);
    self.info = info;
    self.matchday_id = matchday_id;
    self.is_playing = is_playing;
    self.position_id = ko.observable(position_id);

    self.kit_mm = ko.computed({
        read: function () {
            return ko.observable(_kit_mm);
        },
        write: function (value) {
            self._kit_mm = value;
        },
        owner: this
    });

    self.sub_of = sub_of;
    self.minute_from = member_played_from;
    self.minute_to = member_played_to;
    self.is_default = ko.observable(false);
    self.is_completed = ko.observable(false);
    self.is_offline = ko.observable(is_offline);

    self.played_from = ko.computed(function () {
        return _played_from;
    });

    self.played_to = ko.computed({
        read: function () {
            return ko.observable(_played_to);
        }
        ,
        write: function (value) {
            self._played_to = value;
        }
        ,
        owner: this
    });

    self.member_played_from = ko.computed(function () {
        return member_played_from;
    });
    self.member_played_to = ko.computed({
        read: function () {
            return ko.observable(_member_played_to);
        },
        write: function (value) {
            self._member_played_to = value;
        },
        owner: this
    });

    self.red_card_at = ko.observable(function () {
        return red_card_at;
    });

    self.score = ko.observable(null);
    self.minute = ko.observable(0);

    self.events = ko.observableArray(events);

    self.status = ko.computed(function () {
        var sub_out = false;
        var red_card = false;
        var straight_red = false;

        if (self.events()) {

            for (var i = 0; i < self.events().length; i++) {
                sub_out = (self.events()[i].event_type_id == 5);
                red_card = (self.events()[i].event_type_id == 3);
                straight_red = (self.events()[i].event_type_id == 9);
                if (sub_out || red_card || straight_red) {
                    self.member_played_to(self.events()[i].minute);
                    if (red_card || straight_red) {
                        red_card_at = self.events()[i].minute;
                    }
                    break;
                }
            }
        }
        if (self.is_offline()) {
            return 'finished';
        } else if (self.is_completed() || (self.member_played_to()() < 1000 && self.member_played_to()() < self.played_to())) {
            return 'complete';
        } else if (self.minute() == 1000 || self.sub_of || sub_out || red_card || straight_red || self.played_to() == 1000) {
            return 'finished';
        } else if (self.minute() < self.member_played_from()) {
            return 'idle';
        } else if (self.score() == null) {
            return 'noscore';
        } else {
            return 'inplay';
        }
    });
};

var Match = function (match_datetime, match_id, matchday_id, home_team_id, away_team_id, home_team_ids, away_team_ids) {
    var self = this;

    var match_id = match_id;
    var matchday_id = matchday_id;
    var home_team_id = home_team_id;
    var away_team_id = away_team_id;

    self.match_datetime = match_datetime;
    self.match_id = match_id;
    self.matchday_id = matchday_id;
    self.home_team_id = home_team_id;
    self.away_team_id = away_team_id;
    self.home_team_ids = home_team_ids;
    self.away_team_ids = away_team_ids;
    self.is_completed = ko.observable(false);
    self.is_inplay = ko.observable(false);
    self.home_team_goals = ko.observable();
    self.away_team_goals = ko.observable();

    self.home_starters = ko.observableArray([]);
    self.home_subs = ko.observableArray([]);

    self.away_starters = ko.observableArray([]);
    self.away_subs = ko.observableArray([]);

    self.home_match_events = ko.observableArray([]);
    self.away_match_events = ko.observableArray([]);
    self.full_match_events = ko.observableArray([]);

    self.minute = ko.observable();

    self.status = ko.computed(function () {
        if (self.is_completed()) {
            return 'finished';
        } else if (self.is_inplay()) {
            return 'inplay';
        } else {
            return 'noscore';
        }
    });
};

var MatchViewModel = function (matches, positions) {
    var self = this;

    self.matches = matches;
    self.positions = positions;

    this.position = function (pos) {
        return ko.computed({
            read: function () {
                if (self.positions[pos]) {
                    return self.positions[pos];
                }
                else {
                    return 'N/A';
                }
            },
            write: function (value) {

            }
        }, self);
    };

    this.minute = function (pos) {
        return ko.computed({

            read: function () {
                if (!self.matches[pos].minute()) {
                    return '0';
                } else {
                    return self.matches[pos].minute();
                }
            },
            write: function (value) {

            }
        }, self);
    };

    this.status = function (pos) {
        return ko.computed({
            read: function () {
                return self.matches[pos].status();
            },
            write: function (value) {

            }
        }, self);
    };

    this.home_team_goals = function (pos) {
        return ko.computed({
            read: function () {
                return self.matches[pos].home_team_goals();
            },
            write: function (value) {

            }
        }, self);
    };

    this.away_team_goals = function (pos) {
        return ko.computed({
            read: function () {
                return self.matches[pos].away_team_goals();
            },
            write: function (value) {

            }
        }, self);
    };

    this.home_starters = function (pos) {
        return ko.computed({
            read: function () {
                return self.matches[pos].home_starters();
            },
            write: function (value) {

            }
        }, self);
    };

    this.away_starters = function (pos) {
        return ko.computed({
            read: function () {
                return self.matches[pos].away_starters();
            },
            write: function (value) {

            }
        }, self);
    };

    this.home_subs = function (pos) {
        return ko.computed({
            read: function () {
                return self.matches[pos].home_subs();
            },
            write: function (value) {

            }
        }, self);
    };

    this.away_subs = function (pos) {
        return ko.computed({
            read: function () {
                return self.matches[pos].away_subs();
            },
            write: function (value) {

            }
        }, self);
    };

    this.home_match_events = function (pos) {
        return ko.computed({
            read: function () {
                return self.matches[pos].home_match_events().sort(this.sortFunction);
            },
            write: function (value) {

            }
        }, self);
    };

    this.away_match_events = function (pos) {
        return ko.computed({
            read: function () {
                return self.matches[pos].away_match_events().sort(this.sortFunction);
            },
            write: function (value) {

            }
        }, self);
    };

    this.full_match_events = function (pos) {
        return ko.computed({
            read: function () {
                return self.matches[pos].full_match_events().sort(this.sortFunction);
            },
            write: function (value) {

            }
        }, self);
    };

    this.sortFunction = function (a, b) {
        return parseInt(a.minute) > parseInt(b.minute) ? 1 : -1;
    };
};

var Timeline = function () {
    var self = this;

    self.inplay = ko.observableArray([]);

    self.subs = ko.observableArray([]);
};

function updateStyles($style) {
    var ajaxURL = $('#ajax_url').val();
    $.ajax({
        type: "POST",
        url: ajaxURL,
        data: {
            action: 'updateThemeStyles',
            style: $style,
        },
        success: function (data, textStatus, jqXHR) {
            window.location.reload();

        },
        error: function (data, textStatus, jqXHR) {
        },
        dataType: 'html'
    });
}

/*Team-Builder*/

var disableSelected = function () {

    $(".table.searchbox > tbody > tr").removeClass("player-add-sub-tr");
    $(".table.searchbox > tbody > tr").removeClass("player-add-tr");

    $(".field .player, .subs .player").not(".default").each(function () {
        var player_id = $(this).attr("data-player-id");
        var $disabled = $(".table.searchbox > tbody > tr[data-player-id='" + player_id + "']");

        if ($(this).hasClass('for-field')) {
            $disabled.addClass("player-add-tr");
        } else if ($(this).hasClass('for-sub')) {
            $disabled.addClass("player-add-sub-tr");
        }
    });
    if ($('.teamnotification .problem').hasClass('valid')) {
        $('.builder-option.join-league').addClass('active');
    } else {
        $('.builder-option.join-league').removeClass('active');
    }
}


function ajaxForm(data, container,successFunction, errorFunction, action) {
    $.ajax({
        type: "POST",
        url: action,
        data: data,
        success: function (data, textStatus, jqXHR) {
            $(container).html(data);
            if (successFunction) successFunction(data);
        },
        error: function (data, textStatus, jqXHR) {
            if (errorFunction) errorFunction(data);
        },
        dataType: 'html'
    });
}

function updateMemberSchedule() {
    var days = $('input[name="days"]').val();
    var matches = $('input[name="matches"]').val();
    var leagues = $("select#league-list option").map(function () {
        return $(this).val();
    }).get().join(",");
    var lang = $('#local_l').val();

    $.ajax({
        type: "POST",
        url: lang + '/squad/memberSchedule',
        async: false,
        data: {
            leagueid: memberleagueid,
            teamid: teamid,
            currentteamid: currentteamid,
        },
        success: function (data, textStatus, jqXHR) {

            $('.substitution-guide-placeholder').html(data);
        },
        error: function (data, textStatus, jqXHR) {
        },
        dataType: 'html'
    });
}

function updateHomeMemberSchedule() {
    var days = $('input[name="days"]').val();
    var matches = $('input[name="matches"]').val();
    var leagues = $("select#league-list option").map(function () {
        return $(this).val();
    }).get().join(",");
    var lang = $('#local_l').val();

    $.ajax({
        type: "POST",
        url: lang + '/home/memberSchedule',
        async: false,
        data: {
            leagueid: memberleagueid,
            teamid: teamid,
            currentteamid: currentteamid,
        },
        success: function (data, textStatus, jqXHR) {

            $('.substitution-guide-placeholder').html(data);
        },
        error: function (data, textStatus, jqXHR) {
        },
        dataType: 'html'
    });
}

function performAction(ajaxURL, data, cb) {
    var lang = $('#local_l').val();
    $(".player-mobile-icon").off();

    $.post(lang + '/' + ajaxURL, data, function (strHTML) {
        var dom = $(strHTML);
        // console.log(dom.find("#pitch-content").html());
        $(".pitch-placeholder").html(dom.find("#pitch-content").html());
        //$(".substitution-guide-placeholder").html(dom.find("#substitution-guide-content").html());

        // Evaluate SCRIPTS inside the content
        dom.filter('script').each(function () {
            $.globalEval(this.text || this.textContent || this.innerHTML || '');
        });
        if (!isSaved) {
            updateMemberSchedule();
        }else if(isHome){
            updateHomeMemberSchedule();
        }
        if(!data.refresh){
            submitSearchForm();
        }

        $(".player-mobile-icon").on();
        if (cb != null && cb) cb();
    });
}

function swipePlayer(player_id, position_id, type, isSaved) {
    var route = 'squad';
    if(isSaved==2){
        route='home';
    }
    else if (isSaved) {
        route = 'savedsquad';
    }

    var starting = 0;
    if (type == "sub") {
        starting = 1;
    } else if (type == "starter") {
        starting = 0;
    }
    var days = $('input[name="days"]').val();
    var matches = $('input[name="matches"]').val();
    var leagues = $("select#league-list option").map(function () {
        return $(this).val();
    }).get().join(",");

    showLoadingScreen();
    //showMiniSearchLoadingScreen();
    performAction(route + '/swapPlayer', {
        playerid: player_id,
        position_id: position_id,
        formation: $("#formations").val(),
        withid: null,
        savedTeam: isSaved,
        starting: starting,
        leagueid: memberleagueid,
        teamid: teamid,
        currentteamid: currentteamid,
        loadingteam: loadingteam,
        days: days,
        matches: matches,
        league: leagues,
    });
}
var isHome=false;
function removePlayer(player_id, isSaved) {
    if (player_id) {
        var days = $('input[name="days"]').val();
        var matches = $('input[name="matches"]').val();
        var leagues = $("select#league-list option").map(function () {
            return $(this).val();
        }).get().join(",");

        var route = 'squad';
        if(isSaved==2){
            route='home';
            isHome=true;
        }
        else if (isSaved) {
            route = 'savedsquad';
        }

        showLoadingScreen();
        performAction(route + '/removePlayer', {
            leagueid: memberleagueid,
            playerid: player_id,
            formation: $("#formations").val(),
            teamid: teamid,
            savedTeam: isSaved,
            currentteamid: currentteamid,
            loadingteam: loadingteam,
            days: days,
            matches: matches,
            league: leagues,
        });
    }
}

function addPlayerToField(player_id, isSaved, position_id, $this) {

    var tr = $($this).closest("tr");
    var days = $('input[name="days"]').val();
    var matches = $('input[name="matches"]').val();
    var teamName = $('#teamname').val();

    var leagues = $("select#league-list option").map(function () {
        return $(this).val();
    }).get().join(",");

    var route = 'squad';
    if(isSaved==2){
        route='home';
        isHome=true;
    }
    else if (isSaved) {
        route = 'savedsquad';
    }

    $(tr).toggleClass('player-add-tr');
    if ($(tr).hasClass('player-add-sub-tr')) {
        showLoadingScreen();
        showMiniSearchLoadingScreen();

        performAction(route + '/swapPlayer', {
            playerid: player_id,
            position_id: position_id,
            starting: 1,
            leagueid: memberleagueid,
            teamid: teamid,
            formation: $("#formations").val(),
            savedTeam: isSaved,
            currentteamid: currentteamid,
            loadingteam: loadingteam,
            days: days,
            matches: matches,
            league: leagues,
            teamName: teamName,
        });
        $(tr).removeClass('player-add-sub-tr');
    } else {

        if ($(tr).hasClass("player-add-tr")) {
            showLoadingScreen();

            //showMiniSearchLoadingScreen();
            performAction(route + '/addPlayer', {
                playerid: player_id,
                position_id: position_id,
                starting: 1,
                formation: $("#formations").val(),
                leagueid: memberleagueid,
                teamid: teamid,
                savedTeam: isSaved,
                currentteamid: currentteamid,
                loadingteam: loadingteam,
                days: days,
                matches: matches,
                league: leagues,
                teamName: teamName,
            });
        } else {
            $(tr).removeClass('player-add-tr');
            $(".team-builder .player[data-player-id=" + player_id + "]").find(".remove").trigger('click');
        }
    }
}

function addPlayerToSub(player_id, isSaved, position_id, $this) {

    var tr = $($this).closest("tr");
    $(tr).toggleClass('player-add-sub-tr');
    var days = $('input[name="days"]').val();
    var matches = $('input[name="matches"]').val();
    var leagues = $("select#league-list option").map(function () {
        return $(this).val();
    }).get().join(",");

    var route = 'squad';
    if(isSaved==2){
        route='home';
        isHome=true;
    }
    else if (isSaved) {
        route = 'savedsquad';
    }

    if ($(tr).hasClass('player-add-tr')) {
        showLoadingScreen();
        //showMiniSearchLoadingScreen();
        performAction(route + '/swapPlayer', {
            playerid: player_id,
            position_id: position_id,
            formation: $("#formations").val(),
            starting: 0,
            leagueid: memberleagueid,
            teamid: teamid,
            savedTeam: isSaved,
            currentteamid: currentteamid,
            loadingteam: loadingteam,
            days: days,
            matches: matches,
            league: leagues,
        });
        $(tr).removeClass('player-add-tr');
    } else {
        if ($(tr).hasClass("player-add-sub-tr")) {
            showLoadingScreen();
            //showMiniSearchLoadingScreen();
            performAction(route + '/addPlayer', {
                playerid: player_id,
                position_id: position_id,
                formation: $("#formations").val(),
                starting: 0,
                leagueid: memberleagueid,
                teamid: teamid,
                savedTeam: isSaved,
                currentteamid: currentteamid,
                loadingteam: loadingteam,
                days: days,
                matches: matches,
                league: leagues,
            });
        } else {
            $(tr).removeClass('player-add-sub-tr');
            $(".team-builder .player[data-player-id=" + player_id + "]").find(".remove").trigger('click');
        }
    }
}


var team_lists = [];

function updateSideSearchTeam(leagueID) {
    var selected_team_id = $('select[name="team"]').val();
    $('select[name="team"]').children().remove();

    if (!parseInt(leagueID)) {

        for (var i = 0; i < team_lists.length; i++) {
            $('select[name="team"]')
                .append($("<option></option>")
                    .attr("value", team_lists[i].value)
                    .attr("data-thumb", team_lists[i].thumb)
                    .text(team_lists[i].text));
        }
    } else {
        // create new options
        var found = false;
        for (var i = 0; i < team_lists.length; i++) {
            if (team_lists[i].leagueid == leagueID || !team_lists[i].leagueid) {
                $('select[name="team"]')
                    .append($("<option></option>")
                        .attr("value", team_lists[i].value)
                        .attr("data-thumb", team_lists[i].thumb)
                        .text(team_lists[i].text));
                if (team_lists[i].value == selected_team_id) {
                    found = true;
                }
            }
        }

        if (!found) {
            selected_team_id = "";
        }
    }
    $('select[name="team"]').val(selected_team_id);
}

$(document).delegate('.favoriteTeam #club-list', 'change', function () {

    var thumb = $(this).find(':selected').data('thumb');
    $('.teamFlag').html('<img src="' + thumb + '">');
})

$(document).delegate('.favoriteTeam #league-list', 'change', function () {

    var thumb = $(this).find(':selected').data('thumb');
    $('.teamFlag').html('<img src="/bundles/images/kits/default-kit.png">');
})

var typingTimer;

function showMiniSearchLoadingScreen() {
    if (typeof(showSideLoadingScreen) == "function") {
        showSideLoadingScreen();
    }
}


function submitSearchForm() {
    showMiniSearchLoadingScreen();

    $('.playersearch  >form').submit();
}

function doTypingSearch() {
    clearTimeout(typingTimer);

    // Set the timer - 1000ms seems to be a good value if the player's name
    // field is being locked during search. If it was not, the value can be lower, around 500ms.
    typingTimer = setTimeout('submitSearchForm()', 500);

    return false;
}

function changeSearchElementLock(action) {
    var form = $('.playersearch form');

    switch (action) {
        case 'lock':
            form.find('input, select').prop('disabled', 'disabled');
            $('#value-range').slider('disable');
            break;

        case 'unlock':
            form.find('input, select').prop('disabled', false);
            $('#value-range').slider('enable');

            break;
    }
}

$(document).ready(function () {

    $(".playersearch .player-position").click(function () {

        if (!$(this).hasClass('frozen')) {
            $('input[name="page"]').val(1);
            if ($(this).attr('data-pos-id') != "" && $(this).attr('data-pos-id') != "undefined" && $(this).attr('data-pos-id') != null) {
                $(".player-positions .player-position").removeClass('active');
                $(this).toggleClass('active');
                /*var positions = "";
				$(".player-positions .player-position.active").each(function(){
					positions += $(this).attr('data-pos-id')+",";
				});*/
                $('#member_position').val($(this).attr('data-pos-id'));
            }
            if ($(this).attr('data-league-id') != "" && $(this).attr('data-league-id') != "undefined" && $(this).attr('data-league-id') != null) {
                $(this).toggleClass('active');
                var leagues = "";
                $(".player-leagues .player-position.active").each(function () {
                    leagues += $(this).attr('data-league-id') + ",";
                });
                $('#member_league').val(leagues);
            }
            if ($(this).attr('data-team-id') != "" && $(this).attr('data-team-id') != "undefined" && $(this).attr('data-team-id') != null) {
                $('#member_team').val($(this).attr('data-team-id'));
            }
            submitSearchForm();
        }
    });
    $('#league-list,#club-list').change(function () {
        setPagination(0);
    });
    $('.playersearch input[name="field"]').bind("keyup", function () {
        $('.playersearch input[name="page"]').val(1);
        doTypingSearch();
    });

    $('.leagues-available-search input[name="field"]').bind("keyup", function () {
        $('.leagues-available-search input[name="page"]').val(1);
        doTypingSearch();
    });

    $('select[name="team"]').children().each(function () {

        team_lists.push({
            text: $(this).text(),
            value: $(this).val(),
            leagueid: $(this).data("leagueid"),
            thumb: $(this).data("thumb")
        });
    });

});

var showHelp = false;

$(document).ready(function () {
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || $(window).width() < 768) {
        console.log('tooltip not availabel on mobile devices for teambuilder');
    } else {
        $(".btn-help").click(function () {
            showHelp = !showHelp;

            $('.team-builder-tip').qtip({
                content: {
                    text: function (api) {
                        return $('#' + $(this).data("help-content")).clone();
                    }
                }, style: {
                    classes: 'qtip-shadow qtip-help'
                },
                position: {
                    my: 'top center',
                    at: 'bottom center'
                },
                show: false,
                hide: false
            });

            if (showHelp) {
                $('.team-builder-tip').qtip('show');
            } else {
                $('.team-builder-tip').qtip('hide');
            }
        });

        $('.btn-action').qtip({
            content: {
                text: function (api) {
                    return $('#' + $(this).data("help-content")).clone();
                }
            }, style: {
                classes: 'qtip-shadow qtip-help'
            },
            position: {
                my: 'bottom right',
                at: 'top center'
            }
        });

        // qtip for leagues-available, changed position of the tooltip to keep it within the page container
        $('.btn-action-2').qtip({
            content: {
                text: function (api) {
                    return $('#' + $(this).data("help-content")).clone();
                }
            }, style: {
                classes: 'qtip-shadow qtip-help'
            },
            position: {
                my: 'bottom left',
                at: 'top center'
            }
        });

        // qtip for leagues-available, changed position of the tooltip to keep it within the page container
        $('.btn-action-3').qtip({
            content: {
                text: function (api) {
                    return $('#' + $(this).data("help-content")).clone();
                }
            }, style: {
                classes: 'qtip-shadow qtip-help'
            },
            position: {
                my: 'bottom left',
                at: 'top center'
            }
        });
    }
});

/*Custom*/
function startTimer(container, timeDifference) {
    $(container).countDown({
        'diffSecs': timeDifference,
        'omitWeeks': true,
        'onComplete': function () {
            location.reload();
        }
    });
}

function showDialog(data, action) {

    var dialog = $("#dialog-temp");

    if (dialog.length === 0) {
        $('body').prepend('<div id="dialog-temp" style="display:none"></div>');
    }

    closeAllDialogs();
    $.ajax({
        type: "POST",
        url: action,
        data: data,
        success: function (data, textStatus, jqXHR) {
            var dialog = $("#dialog-temp");
            dialog.empty();
            dialog.html(data);
            $('.circle-chat-reply>input').removeAttr('disabled');
        },
        dataType: 'html'
    });
}

function closeAllDialogs() {
    $(".ui-dialog-content").dialog("destroy");
}

function setUpSocket(cb) {

    if (typeof io != 'undefined') {

        var socket = io.connect(nodeUrl);

        socket.on('disconnect', function (reason) {
            showErrorDialog(354); // disconncted
        });

        socket.on('error', function () {
            showErrorDialog(352); // generic error
        });

        socket.on('connect', function () {
            closeAllDialogs();
            cb(socket);
        });
    } else {
        /*showErrorDialog(353); // could not connect*/
    }
}

function setupSockets(Players, PlayerIDs, socketpod, home, matchIndex) {
    for (var i = 0; i < Players.length; i++) {
        socketpod.emit('register_player', Players[i]);
        socketpod.on("score_data_" + Players[i].player_id + "_" + Players[i].matchday_id + "_" + Players[i].member_played_from() +  "_" +  Players[i].member_played_to()(), function(data) {
            if (data.result) {
                var id = data.result.player_id;
                var index = $.inArray(parseInt(id), PlayerIDs);
                if (index != -1) {
                    //updating score
                    if (data.result.status == 'noscore') {
                        Players[index].score(null);
                    } else {
                        Players[index].score(data.result.score);
                    }

                    //updating events
                    if (Players[index].events != undefined) {
                        Players[index].events(data.result.events);
                    }

                    //updating match events
                    if (data.result.events) {
                        events = [];
                        for (var k=0; k < data.result.events.length; k++) {
                            //Dont add assists
                            if (data.result.events[k].event_type_id != "7") {
                                var name = Players[index].name().split(' ');
                                if ( name[0] && name[1] ){
                                    var lastname = name[1];
                                    var firstname = name[0].substring(0, 1);
                                    data.result.events[k].player_name = lastname + ',' + firstname;
                                }else{
                                    data.result.events[k].player_name = Players[index].name();
                                }

                                if (home) {
                                    matches[matchIndex].home_match_events.push(data.result.events[k]);
                                    data.result.events[k].team = "home";
                                } else {
                                    matches[matchIndex].away_match_events.push(data.result.events[k]);
                                    data.result.events[k].team = "away";
                                }
                                matches[matchIndex].full_match_events.push(data.result.events[k]);
                            }
                        }
                    }

                }
            }
        });
    }
}

function setUpSocketToURL(url, cb) {
    if (typeof io != 'undefined') {
        var socketToURL = io.connect(url);

        socketToURL.on('disconnect', function (reason) {

            showErrorDialog(354); // disconncted
        });

        socketToURL.on('error', function (e) {

            showErrorDialog(352); // generic error
        });

        socketToURL.on('connect', function () {
            closeAllDialogs();
            cb(socketToURL);
        });

        if (socketToURL.connected) {
            cb(socketToURL);
        }
    } else {
        showErrorDialog(353); // could not connect
    }
}

function bindDialogPlayerHover(id) {
    if (!$.support.touch) {
        $(".dialog-player .player.p" + id).hover(function () {
            $(".player-dialog-selection").stop(true, true).show();

            $(".player-dialog-selection").position({
                of: $(this),
                my: 'center center+8',
                at: 'center center'
            });
        }, function () {
            $(".player-dialog-selection").stop(true, true).hide();
        });
    }
}

function bindPlayerHover(id) {
    if (!$.support.touch) {
        $(".field .player.p" + id).unbind('mouseenter mouseleave');

        $(".field .player.p" + id).hover(function () {
            var self = this;

            $(".player-selection").stop(true, true).show();

            $(".player-selection").position({
                of: $(self),
                my: 'center center',
                at: 'center center'
            });
        }, function () {
            $(".player-selection").stop(true, true).hide();
        });
    }
}

function bindInplayDialogs(url, competitionweekid) {
    $('.scoringbreakdown').bind('click', function (e) {
        var player_id = $(this).parents(".player").data("id");
        if (!player_id) {
            player_id = $(this).data("player-id");
        }

        var matchday_id = $(this).parents(".player").data("matchday-id");
        openPlayerLivePopup(url, player_id, matchday_id, competitionweekid);
    });
}

function loadSidebarActivityFeed(container) {
    $.ajax({
        type: "POST",
        url: '/system/controls/ajaxControls.php',
        data: {action: 'loadCommunityActivityFeed'},
        success: function (data) {
            $(container).html(data);
        },
        dataType: 'html'
    });
}

function openPlayerLivePopup(url, player_id, matchday_id, week_id) {
    var dialog = $("#dialog-temp");

    if (dialog.length === 0) {
        $('body').prepend('<div id="dialog-temp" style="display:none"></div>');
    }

    closeAllDialogs();
    var dialog = $("#dialog-temp");

    dialog.empty();
    dialog.html('<iframe frameborder="0" height="700" id="iframe" src="' + url + '?pid=' + player_id + '&matchday=' + matchday_id + '&week=' + week_id + '" width="100%"></iframe>');

    dialog.dialog({
        resizable: false,
        height: 480,
        width: 750,
        dialogClass: 'player-state-dialog fixed-dialog',
        draggable: false,
        modal: true,
        open: function (event, ui) {
            $('.ui-widget-overlay').bind('click', function () {
                dialog.dialog('close');
            });
        }
    });
}

function openCompare() {
    $(".stats > .sub-heading.category").each(function () {
        var selector = '.category-' + $(this).data('category');

        var $arrow = $(this).children('.row').children('.title').children('.arrow');

        $arrow.removeClass('closed');

        $arrow.html('&#9660;');

        $(selector).removeClass('hidden');
    });
}

function closeCompare() {
    $(".stats > .sub-heading.category").each(function () {
        var selector = '.category-' + $(this).data('category');

        var $arrow = $(this).children('.row').children('.title').children('.arrow');

        $arrow.addClass('closed');

        $arrow.html('&#9654;');

        $(selector).addClass('hidden');
    });
}

function validateEmail(emails) {
    emails = emails.split(";");
    var valid_emails = 0;
    var invalid_emails = 0;
    $.each(emails, function (index, email) {
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
            valid_emails++;
        } else {
            invalid_emails++;
        }
    });
    if (invalid_emails > 0) {
        return (false);
    } else {
        return (true)
    }
}

function showLeagueInformation(memberleagueid, memberleagueslug, activetab, pageid, action) {
    showDialog({
        leagueId: memberleagueid,
        leagueslug: memberleagueslug,
        activetab: activetab,
        showJoinButton: pageid
    }, action);
    $('.looking-for-league').blur();
}

function showPlayerProfile(playerid, action, from) {
    if (playerid) {
        showDialog({playerid: playerid, from: from}, action);
        $('.bodycontainer').blur();
    }
    else {
        return false;
    }
}

function setPagination(pageID) {
    $('.playerSearchResultsPage').val(pageID).change();
}

function showOrHide(selector) {
    $(selector).toggleClass('hidden');
}

// DOM ready
$(document).ready(function () {

    $(".stats > .sub-heading.category").click(function () {
        var selector = '.category-' + $(this).data('category');

        var $arrow = $(this).children('.row').children('.title').children('.arrow');

        $arrow.toggleClass('closed');

        //$arrow.html($arrow.hasClass('closed') ? '&#9654;' : '&#9660;');

        showOrHide(selector);
    });

    $(".fixtures-custom .sub-heading").click(function () {
        var $arrow = $(this).children('.arrow');

        $arrow.toggleClass('closed');

        $arrow.html($arrow.hasClass('closed') ? '&#9654;' : '&#9660;');
    });
    $('.btn-lang-header.dropdown-toggle').click(function () {
        $('.header-language').toggleClass('active');
    });
    $('.btn-lang-footer.dropdown-toggle').click(function () {
        $('.footer-language').toggleClass('active');
    });
    $(document).click(function(event) {
        //if you click on anything except the modal itself or the "open modal" link, close the modal
        if (!$(event.target).closest(".dropdown-menu").length) {
            $("body").find(".dropdown-menu").removeClass("active");
        }
    });
    /***userprofile.html about user: toggle div***/
    $('.profile-name .editProfile').click(function () {
        $('.aboutUser').toggle();
        $('.profile-bio').toggle();
    });
    $('.profile-name .acceptButtonsCancel ').click(function () {
        $('.aboutUser').toggle();
        $('.profile-bio').toggle();
    });

    /** aboutUser textarea: max char 140 converse counting **/
    $('.aboutUser .aboutYourself').keyup(function () {
        var max = 140;
        var len = $(this).val().length;
        $(this).next('span').text(max - len);
        if (len >= max) {
            $(this).next('span').text(0);
        }
    });
});

function generateReadMore() {
    $('.text.ellipsis').each(function () {
        if (!($(this).next().hasClass('read-more'))) {
            var originalHeight = $(this).height();
            var newHeight = $(this).css('white-space', 'nowrap', 'important').height();
            $(this).css('overflow', 'hidden', 'important');
            if (originalHeight > newHeight) {
                var div = $('<div>').addClass('read-more');
                $(this).after(div);
                // $(this).width($(this).width() - 40);
            } else {
                $(this).width($(this).width());
            }
            $(this).css('display', 'inline-block');
            $(this).css('float', 'left');
            $(this).css('white-space', 'nowrap', 'important');

        }
    });
}
function setCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}
function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}
function eraseCookie(name) {
    document.cookie = name+'=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;';
}