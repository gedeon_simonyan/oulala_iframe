<?php
namespace OulalaiFrameBundle\ApiManager;

use Buzz\Client\Curl;
use Symfony\Component\HttpFoundation\Request;
use Buzz\Browser;

/**
 * Class Manager
 * This class implements API calls.
 */
class Manager
{
    /**
     * Method: POST, PUT, GET etc, default GET
     * Data: array("param" => "value") ==> index.php?param=value
     * @param string $method
     * @param string $url
     * @param string $access_token
     * @param array $data
     * @return mixed
     */
    public static function APIRequest($method = 'GET', $url = '', $access_token = '', $data = '')
    {
        $browser = new Browser(new Curl());
        $browser->getClient()->setTimeout(500);
        $response = array();
        $headers = array();
        if ($access_token) {
            $headers[] = 'authorization: Bearer ' . $access_token;
            $headers[] = 'cache-control: no-cache';
            $headers[] = 'content-type: application/json';
        }
        switch ($method) {

            case "POST":
                if ($data) {
                    $response = $browser->post($url, $headers, $data);
                }
                break;
            case "PUT":
                $response = $browser->put($url, $headers, $data);
                break;
            case "GET":
                $response = $browser->get($url, $headers);
                break;
        }

        if (empty($response)) {
            echo "API is not available. ";
            die;
        }

        return json_decode($response->getContent());
    }
}
