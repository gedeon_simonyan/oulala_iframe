<?php
namespace OulalaiFrameBundle\Command;

use Symfony\Component\Filesystem\LockHandler;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CurrentLeaguesCommand extends Command
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('oulalai_frame:update_current_leagues')
            // the short description shown while running "php bin/console list"
            ->setDescription('Update current leagues data')
            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("This command allows update current leagues data...");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $lockHandler = new LockHandler('current-leagues.lock');
        $globalLockHandler = new LockHandler('start-update.lock');
        if (!$lockHandler->lock() or  !$globalLockHandler->lock()) {
            return 0;
        }
        $globalLockHandler->release();
        $access_token = $this->getApplication()->getKernel()->getContainer()->get('OulalaiFrame.repository.token')->getToken();

        $output->writeln(["----------------",'Token',$access_token,"============"," "]);

        $output->writeln([
            '',
            ' Start update Leagues data...',
            '',
        ]);
        $this->getApplication()->getKernel()->getContainer()->get('OulalaiFrame.repository.leagues')->updateLeaguesData($access_token);

        $output->writeln([
            '',
            ' End update Leagues data. \n Start update Match and User data in League...',
            '',
        ]);
        $this->getApplication()->getKernel()->getContainer()->get('OulalaiFrame.repository.matches')->updateLeagueMatchesAndUserData($access_token);

        $output->writeln([
            '',
            'End update Leagues data matches and user data. ',
            '',
        ]);
    }
}
