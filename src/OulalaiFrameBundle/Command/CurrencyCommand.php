<?php
namespace OulalaiFrameBundle\Command;

use OulalaiFrameBundle\Controller\LeagueMatchesController;
use OulalaiFrameBundle\Controller\LeaguePlayersController;
use OulalaiFrameBundle\Controller\LeaguesController;
use OulalaiFrameBundle\Controller\MatchesController;
use OulalaiFrameBundle\Controller\PlayersController;
use OulalaiFrameBundle\Controller\SquadsController;
use OulalaiFrameBundle\Controller\TeamsController;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CurrencyCommand extends Command
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('oulalai_frame:update_currency')
            // the short description shown while running "php bin/console list"
            ->setDescription('Update currency data')
            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("This command allows update currency data...");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            '',
            ' Start update currency data...',
            '',
        ]);
        $this->getApplication()->getKernel()->getContainer()->get('OulalaiFrame.repository.currency')->currencyConverter();
        $output->writeln([
            ' End update currency data.',
        ]);
    }
}
