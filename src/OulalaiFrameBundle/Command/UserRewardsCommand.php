<?php
namespace OulalaiFrameBundle\Command;

use OulalaiFrameBundle\Controller\LeagueMatchesController;
use OulalaiFrameBundle\Controller\LeaguePlayersController;
use OulalaiFrameBundle\Controller\LeaguesController;
use OulalaiFrameBundle\Controller\MatchesController;
use OulalaiFrameBundle\Controller\PlayersController;
use OulalaiFrameBundle\Controller\SquadsController;
use OulalaiFrameBundle\Controller\TeamsController;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UserRewardsCommand extends Command
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('oulalai_frame:update_user_rewards')
            // the short description shown while running "php bin/console list"
            ->setDescription('Update user info  rewards')
            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("This command allows update user rewards  data...");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $access_token = $this->getApplication()->getKernel()->getContainer()->get('OulalaiFrame.repository.token')->getToken();

        $output->writeln(["----------------",'Token',$access_token,"============"," "]);

        $output->writeln([
            '',
            ' Start update user rewards data...',
            '',
        ]);
        $this->getApplication()->getKernel()->getContainer()->get('OulalaiFrame.repository.profile')->updateUserRewards($access_token);
        $output->writeln([
            ' End update user rewards  data.',
        ]);
    }
}
