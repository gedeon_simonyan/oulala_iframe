<?php
namespace OulalaiFrameBundle\Command;

use Symfony\Component\Filesystem\LockHandler;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GlobalCommand extends Command
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('oulalai_frame:start_update')
            // the short description shown while running "php bin/console list"
            ->setDescription('Update data')
            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("This command allows update all data...");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $lockHandler = new LockHandler('start-update.lock');
        if (!$lockHandler->lock()) {
            return 0;
        }
        $access_token = $this->getApplication()->getKernel()->getContainer()->get('OulalaiFrame.repository.token')->getToken();

        $output->writeln(["----------------",'Token',$access_token,"============"," "]);

        $output->writeln([
            'Start update database',
            '============',
            '',
        ]);

        $output->writeln([
            ' Start update Teams data...',
            '',
        ]);
        $this->getApplication()->getKernel()->getContainer()->get('OulalaiFrame.repository.teams')->updateTeamsData($access_token);

        $output->writeln([
            '',
            'End update Teams data. \n Start update Matches data...',
            '',
        ]);
        $this->getApplication()->getKernel()->getContainer()->get('OulalaiFrame.repository.matches')->updateUpcomingMatchesData($access_token);

        $output->writeln([
            '',
            'End update Matches data.\n Start update Players data...',
            '',
        ]);

        $this->getApplication()->getKernel()->getContainer()->get('OulalaiFrame.repository.players')->updatePlayersData($access_token);

        $output->writeln([
            '',
            'End update Players data.\n Start update Leagues data...',
            '',
        ]);
        $this->getApplication()->getKernel()->getContainer()->get('OulalaiFrame.repository.leagues')->updateLeaguesData($access_token);


        $output->writeln([
            '',
            ' End update Leagues data. \n Start update Match and User data in League...',
            '',
        ]);
        $this->getApplication()->getKernel()->getContainer()->get('OulalaiFrame.repository.matches')->updateLeagueMatchesAndUserData($access_token);

        $output->writeln([
            '',
            'End update Leagues data. \n Start update formation data...',
            '',
        ]);
        $this->getApplication()->getKernel()->getContainer()->get('OulalaiFrame.repository.squad')->updateSquadFormation($access_token);
        $output->writeln([
            '',
            'End update formation data.\n Start update events data...',
            '',
        ]);
        $this->getApplication()->getKernel()->getContainer()->get('OulalaiFrame.repository.players')->updateEvents($access_token);

        $output->writeln([
            '',
            'End update events data.\n Start update Real League data...',
            '',
        ]);

        $this->getApplication()->getKernel()->getContainer()->get('OulalaiFrame.repository.leagues')->updateRealLeague($access_token);

        $output->writeln([
            '',
            'End update Real League data.\n Start update players stats...',
            '',
        ]);

        $this->getApplication()->getKernel()->getContainer()->get('OulalaiFrame.repository.players')->updatePlayerStats($access_token);


        $output->writeln([
            '',
            'End update Players stats.\n Start update prizes data...',
            '',
        ]);

        $this->getApplication()->getKernel()->getContainer()->get('OulalaiFrame.repository.prizes')->updatePrizesData($access_token);
        $output->writeln([
            ' End update prizes data. \n Start update rewards data...',
        ]);

        $this->getApplication()->getKernel()->getContainer()->get('OulalaiFrame.repository.profile')->updateUserInfo($access_token);
        $output->writeln([
            ' End update user level info.  \n Start update followers data...',
        ]);

        $this->getApplication()->getKernel()->getContainer()->get('OulalaiFrame.repository.profile')->updateAllFollowers($access_token);
        $output->writeln([
            ' End update followers data. \n Start update Squads data...',
        ]);
        $this->getApplication()->getKernel()->getContainer()->get('OulalaiFrame.repository.squad')->updateSquadsData($access_token);
        $output->writeln([
            ' End update Squads data. Start update rewards data...',
        ]);

//        $this->getApplication()->getKernel()->getContainer()->get('OulalaiFrame.repository.profile')->updateUserRewards($access_token);
        $output->writeln([
            ' End update rewards data.',
        ]);
    }
}
