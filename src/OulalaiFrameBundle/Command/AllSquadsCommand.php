<?php
namespace OulalaiFrameBundle\Command;

use OulalaiFrameBundle\Controller\LeagueMatchesController;
use OulalaiFrameBundle\Controller\LeaguePlayersController;
use OulalaiFrameBundle\Controller\LeaguesController;
use OulalaiFrameBundle\Controller\MatchesController;
use OulalaiFrameBundle\Controller\NotificationController;
use OulalaiFrameBundle\Controller\PlayersController;
use OulalaiFrameBundle\Controller\SquadsController;
use OulalaiFrameBundle\Controller\TeamsController;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AllSquadsCommand extends Command
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('oulalai_frame:all_squads')
            // the short description shown while running "php bin/console list"
            ->setDescription('Updateing all squads data.')
            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("Updateing all squads data.");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $access_token = $this->getApplication()->getKernel()->getContainer()->get('OulalaiFrame.repository.token')->getToken();

        $output->writeln(["----------------",'Token',$access_token,"============"," "]);

        $output->writeln([
            '',
            ' Start update squads data...',
            '',
        ]);
        
        $this->getApplication()->getKernel()->getContainer()->get('OulalaiFrame.repository.squad')->updateSquadsData($access_token);
        $output->writeln([
            ' End update squads data.',
        ]);
    }
}
