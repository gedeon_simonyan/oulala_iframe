<?php
namespace OulalaiFrameBundle\Command;

use Symfony\Component\Filesystem\LockHandler;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class EmailCommand extends Command
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('oulalai_frame:send_email')
            // the short description shown while running "php bin/console list"
            ->setDescription('Checking if the league finished sending emails about winning or refunding.')
            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("Checking if the league finished sending emails about winning or refunding.");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $lockHandler = new LockHandler('send-email.lock');
        $globalLockHandler = new LockHandler('start-update.lock');
        if (!$lockHandler->lock() or  !$globalLockHandler->lock()) {
            return 0;
        }
        $globalLockHandler->release();
        $output->writeln([
            '',
            ' Start check transactions...',
            '',
        ]);
        
        $this->getApplication()->getKernel()->getContainer()->get('OulalaiFrame.repository.notification')->sendPayouts();
        $output->writeln([
            ' End check transactions.',
        ]);
    }
}
