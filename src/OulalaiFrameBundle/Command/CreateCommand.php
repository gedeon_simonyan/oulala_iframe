<?php
namespace OulalaiFrameBundle\Command;

use OulalaiFrameBundle\Controller\LeagueMatchesController;
use OulalaiFrameBundle\Controller\LeaguesController;
use OulalaiFrameBundle\Controller\MatchesController;
use OulalaiFrameBundle\Controller\PlayersController;
use OulalaiFrameBundle\Controller\TeamsController;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateCommand extends Command
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('oulalai_frame:start_update')
            // the short description shown while running "php bin/console list"
            ->setDescription('Update data')
            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("This command allows update all data...");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Start update database',
            '============',
            '',
        ]);
        $output->writeln([
            ' Start update Teams data...',
            '',
        ]);
        $teams = new TeamsController();
        $teams->updateTeamsData();
        $output->writeln([
            ' End update Teams data.',
        ]);
        $output->writeln([
            '',
            ' Start update Players data...',
            '',
        ]);
        $players = new PlayersController();
        $players->updatePlayersData();
        $output->writeln([
            ' End update Players data.',
        ]);
        $output->writeln([
            '',
            ' Start update Matches data...',
            '',
        ]);
        $matchs = new MatchesController();
        $matchs->updateMatchesData();
        $output->writeln([
            ' End update Matches data.',
        ]);

        $output->writeln([
            '',
            ' Start update Leagues data...',
            '',
        ]);
        $leagues = new LeaguesController();
        $leagues->updateLeaguesData();
        $output->writeln([
            ' End update Leagues data.',
        ]);

        $output->writeln([
            '',
            ' Start update Match and User data in League...',
            '',
        ]);
        $leagues = new LeagueMatchesController();
        $leagues->updateLeagueMatchesAndUserData();
        $output->writeln([
            ' End update Leagues data.',
        ]);
    }
}
