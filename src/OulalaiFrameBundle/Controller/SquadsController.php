<?php

namespace OulalaiFrameBundle\Controller;

use OulalaiFrameBundle\ApiManager\ApiUrls;
use OulalaiFrameBundle\OulalaiFrameBundle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use OulalaiFrameBundle\ApiManager\Manager;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class SquadsController extends Controller
{

    /**
     * @var iPage
     */
    private $iPage = 1;
    /**
     * @var iStart
     */
    private $iStart = 0;
    /**
     * @var iLimit
     */
    private $iLimit = 10;
    /**
     * @var aPlayers
     */
    private $aPlayers = null;
    /**
     * @var IsValid
     */
    private $bIsValid = false;
    /**
     * @var fAmountSpent
     */
    private $fAmountSpent = 0;
    private $fMemberLeagueBudget = 100;
    private $sTeamName = "FC OULALA";
    private $iCurrentTeamId = null;
    private $aFormationSlots = null;
    private $leagueId = null;
    private $formationList = null;

    private $aSchedulePlayers = array();
    private $aBudget = array();
    private $aSlots = 1;
    private $aValid = array();

    private $sRef = null;
    private $oSquadRepository;
    private $iLang = 1;

    public function __construct($leagueId = null)
    {
        $request = Request::createFromGlobals();

        $this->aValid['Goalkeeper'] = true;
        $this->aValid['Defender'] = true;
        $this->aValid['Midfielder'] = true;
        $this->aValid['Striker'] = true;
        $this->aValid['bench'] = true;
        if (empty($this->aPlayers['lineup'])) {
            $this->aPlayers['lineup'] = array();
            $this->aPlayers['lineup']['Midfielder'] = array();
            $this->aPlayers['lineup']['Defender'] = array();
            $this->aPlayers['lineup']['Striker'] = array();
            $this->aPlayers['lineup']['Goalkeeper'] = array();
        }
        if (empty($this->aPlayers['bench'])) {
            $this->aPlayers['bench'] = array();
        }

        $this->leagueId = $request->request->get('leagueid');

        $squadFormation = false;

        $formation = $request->request->get('formation');
        $fID = ($formation) ? $formation : (($squadFormation) ? $squadFormation : 1);

        $this->aSlots = $fID;

        if ($request->request->get('name')) {
            $this->sTeamName = $request->request->get('name');
        }
        $lang = $request->getPathInfo();
        $lang = explode('/', trim($lang, '/'));
        if ($lang[0] == 'fr') {
            $this->iLang = 2;
        }
    }

    public function indexAction($page)
    {
        $request = Request::createFromGlobals();
        $client_data = $this->get('OulalaiFrame.repository.config')->getClientData($request->query->all());
        $session = new Session();

        if (!$session->has('user_ref')) {
            $referer = $request->headers->get('referer');
            if (!$referer) {
                $referer='/'.$request->getLocale();
            }
            return new RedirectResponse($referer);
        }

        $this->sRef = $session->get('user_ref');
        $savedTeamId=isset($request->cookies->all()['teamId'])?$request->cookies->all()['teamId']:0;

        if($savedTeamId && $this->sRef){
            $this->get('OulalaiFrame.repository.savedSquad')->updateSquadUserRef($savedTeamId, $this->sRef);
        }

        $clientId = $session->get('client');
        $style = $this->get('OulalaiFrame.repository.config')->getUserStyle($clientId);
        $inviteFb = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('invite_fb_friend', $clientId);
        $css_version = $this->getParameter('css_version');
        $js_version = $this->getParameter('js_version');

        $menu = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('show_menu', $clientId);

        $sOrderBy = "fromDate";
        $pagetitle = "My Teams";
        $sortOrder = '';
        if ($request->isXmlHttpRequest()) {
            $orderBy = $request->request->get('orderby');
            $sortOrder = $orderBy;
            if (!empty($orderBy)) {
                if (!empty($orderBy)) {
                    $sort = explode("-", $orderBy);
                }
                $orderBy = empty($sort[0]) ? "fromDate" : $sort[0];
                $order = empty($sort[1]) ? "ASC" : $sort[1];
                $sOrderBy = $orderBy." ".$order;
            } else {
                $sOrderBy = $sOrderBy." DESC";
            }
        } else {
            $sortOrder = $sOrderBy.'-DESC';
            $sOrderBy = $sOrderBy." DESC";
        }

        if ($page == 'current') {
            $squads = $this->get('OulalaiFrame.repository.squad')->squadsList($this->sRef, $sOrderBy, false, true);
        } elseif ($page == 'past') {
            $squads = $this->get('OulalaiFrame.repository.squad')->squadsList($this->sRef, $sOrderBy, true);
        } elseif ($page == 'winning') {
            $squads = $this->get('OulalaiFrame.repository.squad')->squadsList(
                $this->sRef,
                $sOrderBy,
                false,
                false,
                true
            );
        } elseif ($page == 'upcoming') {
            $squads = $this->get('OulalaiFrame.repository.squad')->squadsList($this->sRef, $sOrderBy);
        } else {
            $pagetitle = "Saved Teams";
            $confDate = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('league_date', 0);
            $availableMatchDates = $this->get('OulalaiFrame.repository.matches')->getMatchesDate(3, $confDate);

            return $this->render(
                'templates/saved-teams-container.html.twig',
                array(
                    'saved_team' => array(),
                    'dates' => $availableMatchDates,
                    'pagetitle' => $pagetitle,
                    'active' => $page,
                    'loadingteam' => false,
                    'style' => $style.$css_version,
                    'css_version' => $css_version,
                    'js_version' => $js_version,
                    'menu' => $menu,
                    'inviteFb' => $inviteFb,
                )
            );
        }
        if ($request->isXmlHttpRequest()) {
            return $this->render(
                'templates/controls/my-teams-list.html.twig',
                array("leagues" => $squads, 'active' => $page, 'sort' => $sortOrder)
            );
        }
        $cookies = $request->cookies;
        $source = $cookies->get('currency');
        if (!$source) {
            $source = 'EUR';
        }
        if (!empty($source)) {
            $exchangeValue = $this->get('OulalaiFrame.repository.currency')->exchangeRate($source);
            $entryPrice = round($exchangeValue['currency_value'], 2);
            $prize = round($exchangeValue['currency_value'], 3);
        }

        foreach ($squads as $key => $value) {
            $squads[$key]['class'] = strtolower($exchangeValue['currency_key']);
            if ($value['entryFee'] != 0 && $value['prize'] != 0) {
                $squads[$key]['entryFee'] = $value['entryFee'] * $entryPrice;
                $squads[$key]['prize'] = $value['prize'] * $prize;
            }
        }

        return $this->render(
            'templates/teams.html.twig',
            array(
                "leagues" => $squads,
                'pagetitle' => $pagetitle,
                'active' => $page,
                'style' => $style.$css_version,
                'css_version' => $css_version,
                'js_version' => $js_version,
                'menu' => $menu,
                'userref' => $this->sRef,
                'sort' => $sortOrder,
                'inviteFb' => $inviteFb,
            )
        );
    }

    public function createSquadsAction($leagueId, $iCurrentTeamId = null)
    {
        $session = new Session();
        $request = Request::createFromGlobals();

        $queryData=$request->query->all();
        $client_data = $this->get('OulalaiFrame.repository.config')->getClientData($queryData);

        if (!$session->has('user_ref')) {
            $referer = $request->headers->get('referer');
            return new RedirectResponse($referer);
        }

        $clientId = $session->get('client');
        if(!$clientId){
            $clientId= $this->get('OulalaiFrame.repository.config')->getClientByAlias($request->query->all()['operator_id']);
        }
        $style = $this->get('OulalaiFrame.repository.config')->getUserStyle($clientId);

        $css_version = $this->getParameter('css_version');
        $js_version = $this->getParameter('js_version');

        $menu = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('show_menu', $clientId);
        $clientName = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('client_name', $clientId);
        $allowSubs = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('allow_subs', $clientId);
        $shareFb = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('fb_share', $clientId);
        $this->leagueId = $leagueId;

        $squadFormation = false;

        $this->iCurrentTeamId = $iCurrentTeamId;
        $teamName = '';
        if ($this->iCurrentTeamId) {
            $squadFormation = $this->get('OulalaiFrame.repository.squad')->getSquadDetailsById($this->iCurrentTeamId);

            if ($squadFormation) {
                $teamName = $squadFormation['name'];
                $squadFormation = $squadFormation['formation'];
            } else {
                //redirect to squads list if no squad with the requested squad ID
                //return $this->redirectToRoute('squads', array('_locale'=>$request->getLocale(),'page' => 'upcoming'), 301);
                return new RedirectResponse('/'.$request->getLocale()."/squads-list/upcoming", 301);
            }
            $this->assignPlayers($this->iCurrentTeamId, true);
            $showJoinButton = false;
        } else {
            $showJoinButton = true;
        }

        $formation = $request->request->get('formation');
        $type = $request->request->get('type');
        $iPage = $request->request->get('page');

        $aLeagues = $this->get('OulalaiFrame.repository.leagues')->getLeaguesByMemberLeagueID($leagueId);
        $aLeagueTeams = $this->get('OulalaiFrame.repository.teams')->getTeamsByMemberLeagueID($leagueId);

        $aLeagueDetails = $this->get('OulalaiFrame.repository.leagues')->getMemberLeagueDetailsByID($leagueId);
        $memberteamschedule = $this->getSubstitutionGuide(true, $leagueId);
        $positions = $this->get('OulalaiFrame.repository.players')->getAllPlayerPositions();
        $errors = [];

        $fID = ($squadFormation) ? $squadFormation : (($formation) ? $formation : 1);
        $this->aFormationSlots = $this->get('OulalaiFrame.repository.teams')->assignFormationDetails($fID);
        $this->aSlots = $fID;
        $this->formationList = $this->get('OulalaiFrame.repository.teams')->getFormation();

        $this->get('OulalaiFrame.repository.players')->generateTeamImage($iCurrentTeamId?$iCurrentTeamId:$queryData['tmpteam'],$this->aPlayers,$this->aSlots,$this->formationList);

        return $this->render(
            'templates/teambuilder.html.twig',
            array(
                'leagues' => $aLeagues,
                'teams' => $aLeagueTeams,
                'team_name' => $teamName,
                'leagueId' => $this->leagueId,
                'formationList' => $this->formationList,
                'playervalue' => 45,
                'budget' => $this->aBudget,
                'positions' => $positions,
                'league' => $aLeagueDetails,
                'teamid' => ($iCurrentTeamId) ? $iCurrentTeamId : null,
                'isSaved' => 0,
                'matches' => null,
                'days' => null,
                'loadingteam' => 0,
                'fieldFormation' => $this->aFormationSlots,
                'formation' => $this->aSlots,
                'memberteamschedule' => $memberteamschedule,
                'playerslist' => $this->aPlayers,
                'players' => array(),
                'valid_team' => $this->bIsValid,
                'showJoinButton' => $showJoinButton,
                'page_offset' => $iPage,
                'budgetleft' => $this->getBudgetLeft(),
                'client_name' => $clientName,
                'style' => $style.$css_version,
                'css_version' => $css_version,
                'js_version' => $js_version,
                'menu' => $menu,
                'allowSubs'=>$allowSubs,
                'shareFb'=>$shareFb,
            )
        );
    }

    public function uploadTeamImageAction(Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            $uploadDir=$this->getParameter('team_image_path');
            $image=$request->request->get('image');
            $teamId=$request->request->get('teamId');
            $leagueId=$request->request->get('leagueId');
            $imgname = "image-".$teamId;
//            if(file_exists($request->server->get('REQUEST_SCHEME').'://'.$request->server->get(
//                    'HTTP_HOST'
//                ).'/bundles/images/team/'.$imgname .'.jpg')){
//                $resp = ['re'=>$request->server->get('REQUEST_SCHEME').'://'.$request->server->get(
//                        'HTTP_HOST'
//                    ).'/bundles/images/team/'.$imgname .'.jpg'];
//
//                return new Response(json_encode($resp));
//            }
            //get properly base64 image data passed via post in 'cnvimg'
            $cnvimg = trim(strip_tags($image));
            $cnvimg = str_replace(['data:image/png;base64,', ' '], ['', '+'], $cnvimg);


            //get image data from base64 and save it on server
            $data = base64_decode($cnvimg);
            $file = $uploadDir .'/'. $imgname .'.jpg';
            $save = file_put_contents($file, $data);

            //set and output response
            $resp = $save ?['re'=>$request->server->get('REQUEST_SCHEME').'://'.$request->server->get(
                    'HTTP_HOST'
                ).'/bundles/images/team/'.$imgname .'.jpg'] :['er'=>'Unable to save: '. $file];

            return new Response(json_encode($resp));
        }
    }

    public function joinLeagueConfirmAction()
    {
        $session = new Session();
        $request = Request::createFromGlobals();
        if (!$session->has('user_ref')) {
            return new RedirectResponse('/'.$request->getLocale());
        }

        if ($request->isXmlHttpRequest()) {
            $teamId = $request->request->get('teamid');
            $loadTeam = $request->request->get('loadTeam');
            $totalCount = $request->request->get('totalCount');

            $aLeagueDetails = $this->get('OulalaiFrame.repository.leagues')->getMemberLeagueDetailsByID(
                $this->leagueId
            );

            $result = array(
                'leagueId' => $this->leagueId,
                'teamid' => $teamId,
                'formation' => $this->aSlots,
                'isSaved' => 0,
                'loadingteam' => 0,
                'league' => $aLeagueDetails,
                'loadTeam' => $loadTeam,
                'totalCount' => $totalCount,
            );

            return $this->render('templates/controls/dialog-confirm-join-league.html.twig', $result);
        }
    }

    public function joinLeagueAction()
    {
        $session = new Session();
        $request = Request::createFromGlobals();
        $clientId = $session->get('client');
        $username = $session->get('username');
        if (!$session->has('user_ref')) {
            return new RedirectResponse('/'.$request->getLocale());
        }
        $this->sRef = $session->get('user_ref');

        if ($request->isXmlHttpRequest()) {
            $teamId = $request->request->get('teamid');
            $totalCount = $request->request->get('totalCount');

            $fEntryFee = $request->request->get('entryFee');
            if ($teamId && $this->leagueId && $this->aSlots && $this->sTeamName) {
                $teamPlayers = $this->get('OulalaiFrame.repository.squad')->getTeamPlayers($teamId);

                $form_params = [
                    'user' => $this->sRef, //test user param
                    'league' => (int)$this->leagueId,
                    'name' => $this->sTeamName,
                    'formation' => (int)$this->aSlots,
                ];
                for($i=0;$i<$totalCount;$i++) {
                    $result = $this->sendDataToApiToJoin($teamPlayers, $form_params, $teamId, false);
                }
                if (empty($result['errors'])) {
                    $id = $result['teamid'];
                    $this->get('OulalaiFrame.repository.config')
                        ->tid(
                            "insert",
                            "join",
                            $result['tid'],
                            $clientId,
                            $fEntryFee,
                            $username,
                            null,
                            $result['oulalaTid'],
                            null,
                            $id
                        );

                    return new Response("success");
                } else {
                    return new Response(json_encode($result));
                }
            }
        }
    }

    public function sendDataToApiToJoin($teamPlayers, $form_params, $teamId = 0, $bIsSquadeEdit)
    {
        $request = Request::createFromGlobals();
        $tid = uniqid();
        $oulalaTid = null;
        $session = new Session();
        $username = $session->get('username');
        $clientId = $session->get('client');
        $access_token = $this->get('OulalaiFrame.repository.token')->getClientToken($clientId)->access_token;
        $aPlayersCommon = new \stdClass();
        $aPlayersCommon->lineup = new \stdClass();
        $aPlayersCommon->bench = new \stdClass();

        //adding the keywords in bench with empty list
        $aPlayersCommon->bench->goalkeeper = [];
        $aPlayersCommon->bench->defenders = [];
        $aPlayersCommon->bench->midfielders = [];
        $aPlayersCommon->bench->strikers = [];
        //adding the keywords in linup with empty list
        $aPlayersCommon->lineup->goalkeeper = [];
        $aPlayersCommon->lineup->defenders = [];
        $aPlayersCommon->lineup->midfielders = [];
        $aPlayersCommon->lineup->strikers = [];
        $linup_count = 0;

        if (!empty($teamPlayers)) {
            foreach ($teamPlayers as $key => $value) {
                switch ($value['player_position_id']) {
                    case 1:
                        if ($value['starting_11']) {
                            $aPlayersCommon->lineup->goalkeeper[] = intval($value['player_id']);
                        } else {
                            $aPlayersCommon->bench->goalkeeper[] = intval($value['player_id']);
                        }
                        break;
                    case 2:
                        if ($value['starting_11']) {
                            $aPlayersCommon->lineup->defenders[] = intval($value['player_id']);
                        } else {
                            $aPlayersCommon->bench->defenders[] = intval($value['player_id']);
                        }
                        break;
                    case 3:
                        if ($value['starting_11']) {
                            $aPlayersCommon->lineup->midfielders[] = intval($value['player_id']);
                        } else {
                            $aPlayersCommon->bench->midfielders[] = intval($value['player_id']);
                        }
                        break;
                    case 4:
                        if ($value['starting_11']) {
                            $aPlayersCommon->lineup->strikers[] = intval($value['player_id']);
                        } else {
                            $aPlayersCommon->bench->strikers[] = intval($value['player_id']);
                        }
                        break;
                }
                if ($value['starting_11']) {
                    $linup_count++;
                }
            }
        }

        if ($linup_count >= 11) {
            $this->bIsValid = true;
        }
        $date = new \DateTime();
        $curdate = $date->format('Y-m-d h:m:i');
        if ($this->bIsValid) {
            $form_params['players'] = $aPlayersCommon;

            $league = $this->get('OulalaiFrame.repository.leagues')->getLeaguesById((int)$this->leagueId);
            $fEntryFee = $league['entryFee'] ? $league['entryFee'] : '0';
            if($bIsSquadeEdit){
                $squad = $this->get("OulalaiFrame.apiservice.api")->APIRequest(
                    'PUT',
                    $this->container->getParameter('api_base_url') . 'squads',
                    $access_token,
                    $form_params
                );

                $this->get('OulalaiFrame.repository.squad')->updateOneSquadData($access_token, $squad->data->teamId);
                $this->get('OulalaiFrame.repository.leagues')->updateOneLeagueData($access_token, (int)$this->leagueId, 1);
                $this->get('OulalaiFrame.repository.matches')->updateOneLeagueMatchesAndUserData($access_token, (int)$this->leagueId);
               if(isset($squad->status) && $squad->status == 'success'){
                   return 'Your team is updated';
               }else{
                   foreach ($squad->data->violations as $value) {
                       $errors['errors'] = $value;
                   }

                   $errors['status'] = $squad->status;

                   return array('errors' => $errors);
               }

            }else{
                    $squad = $this->get("OulalaiFrame.apiservice.api")->APIRequest(
                        'POST',
                        $this->container->getParameter('api_base_url') . 'squads',
                        $access_token,
                        $form_params
                    );

            }

            if (isset($squad->status) && $squad->status == 'success') {
                $oulalaTid = $squad->data->transaction;
                /* league is NOT free */
                $bPayed = false;

                $sLeaguePayUrl = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName(
                    'client_money_out',
                    $clientId
                );
                $betData = [
                    "token" => $session->get('token'),
                    "amount" => $fEntryFee,
                    "bet_transaction" => $tid,
                    "extra_params1" => $teamId,
                    "extra_params2" => (int)$this->leagueId,

                ];

                $logger = $this->get('monolog.logger.client_api');
                $json = json_encode($betData);
                if (bccomp($fEntryFee, 0, 2) == 1) {

//                    if ($this->getParameter('oulalatest')) {
//                        $sLeaguePayUrl = "http://deviframejulien.oulala.com/bet";
//                    }

                    $logger->info('SendTo: '.$sLeaguePayUrl.' data: '.$json);

                    $res = $this->get('OulalaiFrame.repository.leagues')->moneyOut($sLeaguePayUrl, $fEntryFee, $squad->data->teamId, (int)$this->leagueId, $tid);
                    $logger->debug('Response: '.json_encode($res));
                    if (isset($res->ok) && $res->ok === true) {
                        $bPayed = true;
                    }
                } else {
                    $logger->info('SendTo: '.$sLeaguePayUrl.' data: '.$json);
                    $res = $this->get('OulalaiFrame.repository.leagues')->moneyOut(
                        $sLeaguePayUrl,
                        $fEntryFee,
                        $squad->data->teamId,
                        (int)$this->leagueId,
                        $tid
                    );
                    $logger->debug('Response: '.json_encode($res));
                    $bPayed = true;
                }

                if ($bPayed) {
                    $updateParams = ["wager_state" => 3];
                    $updateSquad = $this->get("OulalaiFrame.apiservice.api")->APIRequest('PUT', $this->container->getParameter('api_base_url') . 'squads' . '/update/'.$squad->data->teamId, $access_token, $updateParams);
                    $this->get('OulalaiFrame.repository.squad')->updateOneSquadData($access_token, $squad->data->teamId);
                    $this->get('OulalaiFrame.repository.leagues')->updateOneLeagueData($access_token, (int)$this->leagueId, 1);
                    $this->get('OulalaiFrame.repository.matches')->updateOneLeagueMatchesAndUserData($access_token, (int)$this->leagueId);
                    $this->get('OulalaiFrame.repository.squad')->clearTeamQuery($teamId);

                    $leagues = $this->get('OulalaiFrame.repository.leagues')->getLeaguesById($this->leagueId);
                    $aRewardOption = $this->get('OulalaiFrame.repository.prizes')->getRewardDetails(
                        $this->leagueId,
                        $this->iLang
                    );

                    $params = [
                        'league_name' => $leagues['name'],
                        'league_id' => $this->leagueId,
                        'entry_fee' => $leagues['entryFee'],
                        'date_and_time' => $curdate,
                        'prize_distribution' => $aRewardOption['title'],
                        'team_id' => $squad->data->teamId,
                        'team_name' => $form_params['name'],
                        'league_starting_date_and_time' => $leagues['fromDate'],
                        'league_settlement_date_and_time' => $leagues['toDate'],
                        'user_ref' => $form_params['user'],
                    ];


                    //                    $email = $this->get("OulalaiFrame.apiservice.api")->APIRequest('POST',  $request->cookies->get(md5('website')).'/league', $request->cookies->get('access_token'), json_encode($params));

                    return array(
                        'leagueId' => $this->leagueId,
                        'teamid' => $squad->data->teamId,
                        'formation' => $this->aSlots,
                        'isSaved' => 0,
                        'loadingteam' => 0,
                        'tid' => $tid,
                        'oulalaTid' => $oulalaTid,
                    );
                } else {
                    if (isset($res)) {
                        $errors['errors'] = "code ".$res->errorCode." - ".$res->errorDescription;
                        $this->get('OulalaiFrame.repository.config')
                            ->tid(
                                "insert",
                                "join",
                                $tid,
                                $clientId,
                                $fEntryFee,
                                $username,
                                "code ".$res->errorCode." - ".$res->errorDescription,
                                null,
                                null,
                                $squad->data->teamId
                            );
                    } else {
                        $errors['errors'] = "Client API UNREACHABLE";
                        $this->get('OulalaiFrame.repository.config')
                            ->tid(
                                "insert",
                                "join",
                                $tid,
                                $clientId,
                                $fEntryFee,
                                $username,
                                "Client API UNREACHABLE",
                                null,
                                null,
                                $squad->data->teamId
                            );
                    }

                    return array('errors' => $errors);
                }
            }else {
                $errors['errors'] = implode(',<br>',$squad->data->violations);
//                foreach ($squad->data->violations as $value) {
//                    $errors['errors'] = $value;
//                }

                $errors['status'] = $squad->status;
                $this->get('OulalaiFrame.repository.config')
                    ->tid("insert", "join", $tid, $clientId, $fEntryFee, $username, json_encode($errors));


                return array('errors' => $errors);
            }
        }
    }

    public function searchPlayersBuildTeamAction()
    {
        $session = new Session();
        $request = Request::createFromGlobals();
        if (!$session->has('user_ref')) {
            return new RedirectResponse('/'.$request->getLocale());
        }
        if ($request->isXmlHttpRequest()) {
            $valueFrom = $request->request->get('valuefrom');
            $valueTo = $request->request->get('valueto');
            $inbudget = $request->request->get('inbudget');
            $iBudgetleft = $request->request->get('budgetleft');
            $orderBy = $request->request->get('sort');
            $iTeamID = $request->request->get('team');
            $iRealLeagueID = $request->request->get('league');
            $iPosition = $request->request->get('position');
            $sPlayerName = $request->request->get('field');
            $iPage = $request->request->get('page');
            $this->leagueId = $request->request->get('leagueid');

            $sOrdering = '';
            if (!empty($orderBy)) {
                $sort = explode("-", $orderBy);
                $orderBy = empty($sort[0]) ? "p.`value`" : $sort[0];
                $order = empty($sort[1]) ? "ASC" : $sort[1];
                $sOrdering = $orderBy." ".$order;
            }

            if (!empty($inbudget)) {
                $valueTo = $iBudgetleft;
            }

            $this->enablePaging($iPage);
            $limit = $this->iLimit;
            $offset = $this->iStart;

            $league_player = $this->get('OulalaiFrame.repository.leagues')->getPlayersById(
                $this->leagueId,
                $iRealLeagueID,
                trim($iTeamID, ','),
                $iPosition,
                $sOrdering,
                $offset,
                $limit,
                $valueFrom!=null?$valueFrom:0,
                $valueTo!=null?$valueTo:45,
                $sPlayerName
            );
            $aAllPlayers = $this->get('OulalaiFrame.repository.leagues')->getPlayersById(
                $this->leagueId,
                $iRealLeagueID,
                trim($iTeamID, ','),
                $iPosition,
                $sOrdering,
                0,
                '',
                $valueFrom!=null?$valueFrom:0,
                $valueTo!=null?$valueTo:45,
                $sPlayerName
            );

            $iTotalRows = count($aAllPlayers);
            $pagination = $this->assignPagination($iTotalRows, $iPage);
            $result = array(
                'players' => $league_player,
                'page_offset' => $iPage,
                'pagination' => $pagination,
            );

            return $this->render('templates/controls/search-players-result.html.twig', $result);
        }
    }

    public function setFormationAction()
    {
        $session = new Session();
        $request = Request::createFromGlobals();
        if (!$session->has('user_ref')) {
            return new RedirectResponse('/'.$request->getLocale());
        }
        if ($request->isXmlHttpRequest()) {
            $teamId = $request->request->get('teamid');
            $bIsSquadeEdit = false;
            $currentteamid = $request->request->get('currentteamid');

            if ($currentteamid) {
                $bIsSquadeEdit = true;
                $this->get('OulalaiFrame.repository.squad')->updateSquadFromation($this->aSlots, $currentteamid);
            }

            $this->formationList = $this->get('OulalaiFrame.repository.teams')->getFormation();
            $this->aFormationSlots = $this->get('OulalaiFrame.repository.teams')->assignFormationDetails($this->aSlots);
            $this->chackPlayersInFormation($this->aSlots, $teamId, $bIsSquadeEdit, true);

            $result = array(
                'leagueId' => $this->leagueId,
                'formationList' => $this->formationList,
                'playervalue' => 45,
                'budget' => $this->aBudget,
                'teamid' => ($this->iCurrentTeamId) ? $this->iCurrentTeamId : null,
                'loadingteam' => 0,
                'fieldFormation' => $this->aFormationSlots,
                'formation' => $this->aSlots,
                'playerslist' => $this->aPlayers,
                'valid_team' => $this->bIsValid,
                'budgetleft' => $this->getBudgetLeft(),
            );

            return $this->render('templates/controls/teambuilder-pitch.html.twig', $result);
        }
    }

    public function memberScheduleAction()
    {
        $session = new Session();
        $request = Request::createFromGlobals();
//        if (!$session->has('user_ref')) {
//            return new RedirectResponse('/'.$request->getLocale());
//        }
        if ($request->isXmlHttpRequest()) {
            $iTeamID = $request->request->get('teamid');
            $currentteamid = $request->request->get('currentteamid');
            $positions = $this->get('OulalaiFrame.repository.players')->getAllPlayerPositions();

            $bIsSquadeEdit = false;
            if ($currentteamid) {
                $bIsSquadeEdit = true;
            }

            $this->assignPlayers($iTeamID, $bIsSquadeEdit);
            $memberteamschedule = $this->getSubstitutionGuide(true, $this->leagueId);
            $result = array(
                'memberteamschedule' => $memberteamschedule,
                'positions' => $positions,
            );

            return $this->render('templates/controls/memberteam-schedule.html.twig', $result);
        }
    }

    public function clearTeamAction()
    {
        $session = new Session();
        $request = Request::createFromGlobals();
        if (!$session->has('user_ref')) {
            return new RedirectResponse('/'.$request->getLocale());
        }
        if ($request->isXmlHttpRequest()) {
            $teamId = $request->request->get('teamid');
            $bIsSquadeEdit = false;
            $currentteamid = $request->request->get('currentteamid');

            if ($currentteamid) {
                $bIsSquadeEdit = true;
            }
            $this->get('OulalaiFrame.repository.squad')->clearTeamQuery($teamId, $bIsSquadeEdit);
            $this->assignPlayers($teamId, $bIsSquadeEdit);
            $this->formationList = $this->get('OulalaiFrame.repository.teams')->getFormation();
            $this->aFormationSlots = $this->get('OulalaiFrame.repository.teams')->assignFormationDetails($this->aSlots);

            $result = array(
                'leagueId' => $this->leagueId,
                'formationList' => $this->formationList,
                'playervalue' => 45,
                'budget' => $this->aBudget,
                'teamid' => ($this->iCurrentTeamId) ? $this->iCurrentTeamId : null,
                'loadingteam' => 0,
                'fieldFormation' => $this->aFormationSlots,
                'formation' => $this->aSlots,
                'playerslist' => $this->aPlayers,
                'valid_team' => $this->bIsValid,
                'budgetleft' => $this->getBudgetLeft(),
                'isSaved' => 0,
            );

            return $this->render('templates/controls/teambuilder-pitch.html.twig', $result);
        }
    }

    public function clearTeamConfirmAction()
    {
        $session = new Session();
        $request = Request::createFromGlobals();
        if (!$session->has('user_ref')) {
            return new RedirectResponse('/'.$request->getLocale());
        }
        if ($request->isXmlHttpRequest()) {
            $teamId = $request->request->get('teamid');
            $currentteamid = $request->request->get('currentteamid');

            $result = array(
                'leagueId' => $this->leagueId,
                'teamid' => $teamId,
                'isSaved' => 0,
                'currentteamid' => $currentteamid,
            );

            return $this->render('templates/controls/dialog-confirm-clear-team.html.twig', $result);
        }
    }

    public function removePlayerAction()
    {
        $session = new Session();
        $request = Request::createFromGlobals();
        if (!$session->has('user_ref')) {
            return new RedirectResponse('/'.$request->getLocale());
        }
        if ($request->isXmlHttpRequest()) {
            $teamId = $request->request->get('teamid');
            $player_id = $request->request->get('playerid');
            $currentteamid = $request->request->get('currentteamid');
            $clientId=$session->get('client');
            $allowSubs = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('allow_subs', $clientId);

            $bIsSquadeEdit = false;
            if ($currentteamid) {
                $bIsSquadeEdit = true;
            }
            $this->get('OulalaiFrame.repository.squad')->removePlayerQuery($player_id, $teamId, $bIsSquadeEdit);
            $this->formationList = $this->get('OulalaiFrame.repository.teams')->getFormation();
            $this->aFormationSlots = $this->get('OulalaiFrame.repository.teams')->assignFormationDetails($this->aSlots);
            $this->assignPlayers($teamId, $bIsSquadeEdit);
            if($bIsSquadeEdit){
                $form_params = [
                    'user' => $session->get('user_ref'), //test user param
                    'league' => (int)$this->leagueId,
                    'name' => $this->sTeamName,
                    'formation' => (int)$this->aSlots,
                    'squadId'=>intval($teamId)
                ];
                $teamPlayers = $this->get('OulalaiFrame.repository.squad')->getSquadePlayers($teamId);
                $this->sendDataToApiToJoin($teamPlayers, $form_params, $teamId, $bIsSquadeEdit,1);
            }
            $this->get('OulalaiFrame.repository.players')->generateTeamImage($teamId,$this->aPlayers,$this->aSlots,$this->formationList);

            $result = array(
                'leagueId' => $this->leagueId,
                'formationList' => $this->formationList,
                'playervalue' => 45,
                'budget' => $this->aBudget,
                'teamid' => ($this->iCurrentTeamId) ? $this->iCurrentTeamId : null,
                'loadingteam' => 0,
                'fieldFormation' => $this->aFormationSlots,
                'formation' => $this->aSlots,
                'playerslist' => $this->aPlayers,
                'valid_team' => $this->bIsValid,
                'budgetleft' => $this->getBudgetLeft(),
                'allowSubs' => $allowSubs,
            );

            return $this->render('templates/controls/teambuilder-pitch.html.twig', $result);
        }
    }

    public function swapPlayerAction()
    {
        $session = new Session();
        $request = Request::createFromGlobals();
        if (!$session->has('user_ref')) {
            return new RedirectResponse('/'.$request->getLocale());
        }
        if ($request->isXmlHttpRequest()) {
            $starting = $request->request->get('starting');
            $player_id = $request->request->get('playerid');
            $teamId = $request->request->get('teamid');
            $player_position_id = $request->request->get('position_id');
            $sPositionTitle = $this->get('OulalaiFrame.repository.squad')->getPositionById(intval($player_position_id));
            $currentteamid = $request->request->get('currentteamid');
            $clientId=$session->get('client');
            $allowSubs = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('allow_subs', $clientId);

            $valid = true;
            $bIsSquadeEdit = ($currentteamid ? true : false);
            $swapLastPlayer = false;

            $this->chackPlayersInFormation($this->aSlots, $teamId, $bIsSquadeEdit);
            if ($this->aValid[$sPositionTitle['comment']]) {
                $swapLastPlayer = false;
            } else {
                if ($starting) {
                    $valid = false;
                }
            }

            if ($starting == 0) {
                if (!$this->aValid['bench']) {
                    $valid = false;
                }
            } else {
                $swapLastPlayer = $player_position_id;
            }

            if ($valid) {
                $this->get('OulalaiFrame.repository.squad')->swapPlayerQuery(
                    $player_id,
                    $teamId,
                    $starting,
                    $bIsSquadeEdit,
                    $swapLastPlayer
                );
            }
            $this->assignPlayers($teamId, $bIsSquadeEdit);
            $this->formationList = $this->get('OulalaiFrame.repository.teams')->getFormation();
            $this->aFormationSlots = $this->get('OulalaiFrame.repository.teams')->assignFormationDetails($this->aSlots);
            if($bIsSquadeEdit){
                $form_params = [
                    'user' => $session->get('user_ref'), //test user param
                    'league' => (int)$this->leagueId,
                    'name' => $this->sTeamName,
                    'formation' => (int)$this->aSlots,
                    'squadId'=>intval($teamId)
                ];
                $teamPlayers = $this->get('OulalaiFrame.repository.squad')->getSquadePlayers($teamId);
                $this->sendDataToApiToJoin($teamPlayers, $form_params, $teamId, $bIsSquadeEdit,1);
            }
//            if($this->bIsValid){
                $this->get('OulalaiFrame.repository.players')->generateTeamImage($teamId,$this->aPlayers,$this->aSlots, $this->formationList);

//            }
            $result = array(
                'leagueId' => $this->leagueId,
                'formationList' => $this->formationList,
                'playervalue' => 45,
                'budget' => $this->aBudget,
                'teamid' => ($this->iCurrentTeamId) ? $this->iCurrentTeamId : null,
                'loadingteam' => 0,
                'fieldFormation' => $this->aFormationSlots,
                'formation' => $this->aSlots,
                'playerslist' => $this->aPlayers,
                'valid_team' => $this->bIsValid,
                'budgetleft' => $this->getBudgetLeft(),
                'allowSubs' => $allowSubs,
            );

            return $this->render('templates/controls/teambuilder-pitch.html.twig', $result);
        }
    }

    public function addPlayerAction()
    {
        $session = new Session();
        $request = Request::createFromGlobals();
        if (!$session->has('user_ref')) {
            return new RedirectResponse('/'.$request->getLocale());
        }
        if ($request->isXmlHttpRequest()) {
            $starting = $request->request->get('starting');
            $player_id = $request->request->get('playerid');
            $teamId = $request->request->get('teamid');
            $currentteamid = $request->request->get('currentteamid');
            $player_position_id = $request->request->get('position_id');
            $leagueId = $request->request->get('leagueId');
            $clientId=$session->get('client');
            $allowSubs = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('allow_subs', $clientId);

            $sPositionTitle = $this->get('OulalaiFrame.repository.squad')->getPositionById(intval($player_position_id));
            $bIsSquadeEdit = ($currentteamid ? true : false);
            $this->chackPlayersInFormation($this->aSlots, $teamId, $bIsSquadeEdit);

            if ($this->aValid[$sPositionTitle['comment']]) {
                if ($currentteamid) {
                    $bIsSquadeEdit = true;
                    $this->putIntoSquadPlayers(
                        $player_id,
                        $teamId,
                        $starting,
                        strtolower($sPositionTitle['comment']),
                        $player_position_id
                    );
                } else {
                    $bIsSquadeEdit = false;
                    $this->putIntoTeamPlayers($player_id, $teamId, $starting, $player_position_id);
                }
            } else {
                if ($this->aValid['bench']) {
                    if ($currentteamid) {
                        $bIsSquadeEdit = true;
                        $this->putIntoSquadPlayers(
                            $player_id,
                            $teamId,
                            0,
                            strtolower($sPositionTitle['comment']),
                            $player_position_id
                        );
                    } else {
                        $bIsSquadeEdit = false;
                        $this->putIntoTeamPlayers($player_id, $teamId, 0, $player_position_id);
                    }
                }
            }
            $this->assignPlayers($teamId, $bIsSquadeEdit);
            $this->formationList = $this->get('OulalaiFrame.repository.teams')->getFormation();
            $this->aFormationSlots = $this->get('OulalaiFrame.repository.teams')->assignFormationDetails($this->aSlots);
            if($bIsSquadeEdit){
                $form_params = [
                    'user' => $session->get('user_ref'), //test user param
                    'league' => (int)$this->leagueId,
                    'name' => $this->sTeamName,
                    'formation' => (int)$this->aSlots,
                    'squadId'=>intval($teamId)
                ];
                $teamPlayers = $this->get('OulalaiFrame.repository.squad')->getSquadePlayers($teamId);
                $this->sendDataToApiToJoin($teamPlayers, $form_params, $teamId, $bIsSquadeEdit,1);
            }

//            if($this->bIsValid){
                $this->get('OulalaiFrame.repository.players')->generateTeamImage($teamId,$this->aPlayers,$this->aSlots,$this->formationList);

//            }
            $result = array(
                'leagueId' => $this->leagueId,
                'formationList' => $this->formationList,
                'playervalue' => 45,
                'budget' => $this->aBudget,
                'teamid' => ($this->iCurrentTeamId) ? $this->iCurrentTeamId : null,
                'loadingteam' => 0,
                'fieldFormation' => $this->aFormationSlots,
                'formation' => $this->aSlots,
                'playerslist' => $this->aPlayers,
                'valid_team' => $this->bIsValid,
                'budgetleft' => $this->getBudgetLeft(),
                'allowSubs' => $allowSubs
            );

            return $this->render('templates/controls/teambuilder-pitch.html.twig', $result);
        }
    }

    public function loadSavedTeamAction()
    {
        $session = new Session();
        $request = Request::createFromGlobals();
        if (!$session->has('user_ref')) {
            return new RedirectResponse('/'.$request->getLocale());
        }
        if ($request->isXmlHttpRequest()) {
            $bIsLoaded = $request->request->get('loadingteam');
            $currentTeamId = $request->request->get('currentteamid');
            $loadingTeamID = $request->request->get('loadteamid');
            $teamid = $request->request->get('teamid');
            $positions = $this->get('OulalaiFrame.repository.players')->getAllPlayerPositions();
            $memberteamschedule = $this->getSubstitutionGuide(true, $this->leagueId);
            $oSavedSquad = $this->get('OulalaiFrame.repository.savedsquad');

            if (!is_null($currentTeamId) && $currentTeamId != "" && $currentTeamId != "saveloaded" && $bIsLoaded) {
                if (!empty($loadingTeamID)) {
                    $players = $oSavedSquad->getNotDraftPlayers($loadingTeamID);
                }
                if ($currentTeamId == 0) {
                    $currentTeamId = $teamid;
                } else {
                    $changed = $oSavedSquad->changePlayersDraftStatus($currentTeamId, 1, 1);

                    if ($changed) {
                        foreach ($players as $aPlayer):
                            $sPositionTitle = $this->get('OulalaiFrame.repository.squad')->getPositionById(
                                intval($aPlayer['fk_player_position_id'])
                            );
                        $this->putIntoSquadPlayers(
                                $aPlayer['fk_player_id'],
                                $currentTeamId,
                                $aPlayer['starting_11'],
                                strtolower($sPositionTitle['comment']),
                                $aPlayer['fk_player_position_id']
                            );
                        endforeach;
                    }
                }
                $bEditTeam = 1;
            } else {
                if ($currentTeamId == "saveloaded") {
                    $bIsLoaded = true;
                }
                if ((is_null($currentTeamId) || $currentTeamId == "") && $bIsLoaded) {
                    if (!empty($loadingTeamID)) {
                        $players = $oSavedSquad->getNotDraftPlayers($loadingTeamID);
                    }
                    $this->get('OulalaiFrame.repository.squad')->clearTeamQuery($teamid);
                    $leaguePlayers=$this->get('OulalaiFrame.repository.leagues')->getPlayersById($this->leagueId, null, null, null, 'value', 0, 0);

                    foreach($leaguePlayers as $leaguePlayer){
                        foreach ($players as $aPlayer) {
                            if($leaguePlayer['id']==$aPlayer['fk_player_id']){
                                $sPositionTitle = $this->get('OulalaiFrame.repository.squad')->getPositionById(
                                    intval($aPlayer['fk_player_position_id'])
                                );
                                $this->putIntoTeamPlayers(
                                    $aPlayer['fk_player_id'],
                                    $teamid,
                                    $aPlayer['starting_11'],
                                    $aPlayer['fk_player_position_id']
                                );
                            }

                        }
                    }

                }
                $bEditTeam = 0;
            }
            $this->assignPlayers($teamid, $bEditTeam);
            $this->formationList = $this->get('OulalaiFrame.repository.teams')->getFormation();
            $this->aFormationSlots = $this->get('OulalaiFrame.repository.teams')->assignFormationDetails($this->aSlots);

            $result = array(
                'leagueId' => $this->leagueId,
                'formationList' => $this->formationList,
                'playervalue' => 45,
                'budget' => $this->aBudget,
                'positions' => $positions,
                'teamid' => $teamid,
                'currentteamid' => $currentTeamId,
                'isSaved' => 0,
                'matches' => null,
                'days' => null,
                'loadingteam' => ($bIsLoaded ? true : false),
                'fieldFormation' => $this->aFormationSlots,
                'formation' => $this->aSlots,
                'memberteamschedule' => $memberteamschedule,
                'playerslist' => $this->aPlayers,
                'players' => array(),
                'valid_team' => $this->bIsValid,
                'showJoinButton' => false,
                'budgetleft' => $this->getBudgetLeft(),
            );

            return $this->render('templates/controls/teambuilder-pitch.html.twig', $result);
        }
    }

    /**
     * Assings pagination to players search result
     *
     * @return array
     * @param int iTotalRows total count of search result
     * @param int iPage current page number
     */
    private function assignPagination($iTotalRows, $iPage)
    {
        $this->enablePaging($iPage);
        $aPages = array();
        $aPages['current'] = $this->iPage;
        $iEnd = (($this->iLimit + $this->iStart) < $iTotalRows) ? ($this->iLimit + $this->iStart) : $iTotalRows;
        if ($this->iStart > 0):
            $aPages['previous'] = $this->iPage - 1;
        endif;
        if ($iEnd < $iTotalRows):
            $aPages['next'] = $this->iPage + 1;
        endif;

        $dPageCount = round($iTotalRows / $this->iLimit, 0, PHP_ROUND_HALF_UP);

        if ($iTotalRows % $this->iLimit != 0) {
            $dPageCount = $dPageCount + 1;
        }

        $aPages['last'] = $dPageCount - 1;
        $aPages['totalresults'] = $iTotalRows;
        $aPages['shownresults'] = array('start' => $this->iStart + 1, 'end' => $iEnd, 'count' => $this->iLimit);

        return $aPages;
    }

    /**
     * Assings players search result pagination start and current indexed
     *
     * @param int iPage current page number
     */
    private function enablePaging($iPage)
    {
        if ($iPage) {
            $this->iStart = ($this->iLimit * ($iPage - 1));
            $this->iPage = $iPage;
        }
    }

    /**
     * Assings linup and bench players
     *
     * @param int iTeamId
     * @param bollean bIsSquadeEdit to check if editing existing squade or creating new one
     */
    private function assignPlayers($iTeamId, $bIsSquadeEdit = false)
    {
        $this->aPlayers['lineup']['Midfielder'] = array();
        $this->aPlayers['lineup']['Defender'] = array();
        $this->aPlayers['lineup']['Striker'] = array();
        $this->aPlayers['lineup']['Goalkeeper'] = array();
        $this->aPlayers['bench'] = array();
        $this->fAmountSpent = 0;
        if ($bIsSquadeEdit) {
            $teamPlayers = $this->get('OulalaiFrame.repository.squad')->getSquadePlayers($iTeamId);
        } else {
            $teamPlayers = $this->get('OulalaiFrame.repository.squad')->getTeamPlayers($iTeamId);
        }

        $iTeamPlayersCount = 0;
        if (!empty($teamPlayers)) {
            foreach ($teamPlayers as $player) {
                $playerDetails = $this->get('OulalaiFrame.repository.players')->getPlayerProfileDetails(
                    $player['player_id']
                );

                if (!empty($playerDetails)) {
                    $this->fAmountSpent += $playerDetails['value'];
                    if ($this->getBudgetLeft() >= 0) {
                        if ($player['starting_11'] == 1) {
                            $pos = $playerDetails['position'];

                            $playerDetails['starting_11'] = $player['starting_11'];
                            array_push($this->aPlayers['lineup'][$pos], $playerDetails);
                            $iTeamPlayersCount++;
                        } else {
                            if (count($this->aPlayers['bench']) < 7) {
                                $playerDetails['starting_11'] = $player['starting_11'];
                                array_push($this->aPlayers['bench'], $playerDetails);
                            }
                        }
                        $this->aSchedulePlayers[$player['player_id']] = $playerDetails;
                        //array_push($this->aSchedulePlayers[$player['player_id']],$playerDetails);


                    } else {
                        $this->aBadget['error'] = true;
                    }
                }
                if ($iTeamPlayersCount == 11) {
                    $this->bIsValid = true;
                } else {
                    $this->bIsValid = false;
                }
            }
        }
        $this->aBudget['available'] = $this->fMemberLeagueBudget;
        $this->aBudget['used'] = $this->fAmountSpent;
        if ($this->fAmountSpent > $this->fMemberLeagueBudget) {
            $this->aBudget['error'] = true;
        } else {
            $this->aBudget['left'] = $this->getBudgetLeft();
        }
    }

    /**
     * Calidates that a change in formation has not resulted in an illegal team being created
     * @param $iFormationID
     * @return bool
     */
    private function chackPlayersInFormation($iFormationID, $iTeamID, $bIsSquadeEdit = false, $bChangeFormation = false)
    {
        $positionSlots = $this->get('OulalaiFrame.repository.teams')->getFormationPositions($iFormationID);
        $this->assignPlayers($iTeamID, $bIsSquadeEdit);
        if (!empty($positionSlots)) {
            foreach ($positionSlots as $slot) {
                $pos = $slot['position_title'];

                $pos_count = count($this->aPlayers['lineup'][$pos]);

                if ($pos_count >= $slot['slots']) {
                    $this->aValid[$pos] = false;
                }
                if ($bChangeFormation) {
                    $count = count($this->aPlayers['lineup'][$pos]);
                    if ($pos_count < $slot['slots']) {
                        if (count($this->aPlayers['bench']) > 0) {
                            foreach ($this->aPlayers['bench'] as $sub_player) {
                                // put players to field from substitute
                                if ($count < $slot['slots']) {
                                    if ($sub_player['position'] == $pos && $sub_player['starting_11'] == 0) {
                                        $this->get('OulalaiFrame.repository.squad')->swapPlayerQuery(
                                            $sub_player['id'],
                                            $iTeamID,
                                            1,
                                            $bIsSquadeEdit,
                                            false
                                        );

                                        $this->assignPlayers($iTeamID);
                                        $count++;
                                    }
                                }
                            }
                        }
                    } else {
                        //                        if (count($this->aPlayers['bench']) < 7) {
                        foreach ($this->aPlayers['lineup'][$pos] as $start_player) {
                            // put players to sub from field
                            if ($count > $slot['slots']) {
                                $this->get('OulalaiFrame.repository.squad')->swapPlayerQuery(
                                    $start_player['id'],
                                    $iTeamID,
                                    0,
                                    $bIsSquadeEdit,
                                    false
                                );
                                $this->assignPlayers($iTeamID);
                                $count--;
                            }
                        }
//                        }
                    }
                    $this->assignPlayers($iTeamID, $bIsSquadeEdit);
                }
            }
        }
        if (count($this->aPlayers['bench']) >= 6) {
            $this->aValid['bench'] = false;
        }

        return $this->aValid;
    }

    /**
     * Returns the amount of money a member has left to spend.
     *
     * @return int
     */
    private function getBudgetLeft()
    {
        return $this->fMemberLeagueBudget - $this->fAmountSpent;
    }

    /**
     * Creates a table-like array representing the times of players' matches and marks substitute players as well as
     * players that play after substitute players and thus cannot be substituted.
     *
     * @param bool $bUseFillers whether to include a dummy element after every time group
     *
     * @return array|null table of values
     */
    private function getSubstitutionGuide($bUseFillers = true, $iMemberLeagueID)
    {
        $aPlayerTimes = $this->getPlayersWithTimes($iMemberLeagueID);
        if (!is_array($aPlayerTimes) || count($aPlayerTimes) < 1) {
            return null;
        }

        $aPlayerTimes = $this->groupPlayersByMatchDatetime($aPlayerTimes);

        $aOutput = array(
            'days' => array(),
            'times' => array(),
        );
        $aSubstitueFlags = array();

        // Get all the positions
        $aPositions = $this->get('OulalaiFrame.repository.players')->getAllPlayerPositions();
        $aPositionIndex = array();

        foreach ($aPositions as $iKey => $aValue) {
            $aPositionIndex[$aValue['player_position_id']] = $iKey;
            $aOutput['position'.$iKey] = array();
            $aSubstitueFlags['position'.$iKey] = false;
        }

        // Process each day
        foreach ($aPlayerTimes as $iDayStamp => $aDay) {
            $aOutput['days'][$iDayStamp] = array(
                'value' => date('l, M j', $iDayStamp),
                'day_colspan' => count($aDay),
            );

            // Process time of the day
            $iTotalPlayerPositionsInGroup = 0;
            $iPointer = 1;

            foreach ($aDay as $iTimestamp => $aTimeGroup) {
                $aMarkers = array();
                if ($iPointer == 1) {
                    $aMarkers['first'] = 'first';
                }
                if ($iPointer == count($aDay)) {
                    $aMarkers['last'] = 'last';
                }
                if ($iPointer != count($aDay)) {
                    $aTimeGroupKeys = array_keys($aDay);
                    $aCurrentKey = array_search($iTimestamp, $aTimeGroupKeys);
                    $iNextTimestamp = $aTimeGroupKeys[($aCurrentKey + 1)];

                    $aMarkers['hour-diff'] = 'hour-diff-'.round(($iNextTimestamp - $iTimestamp) / 3600);
                }

                $aOutput['times'][$iTimestamp] = array(
                    'value' => date('G:i', $iTimestamp),
                    'column_type' => 'time',
                    'markers' => implode(' ', $aMarkers),
                );

                $aPlayerPositions = array();
                $aPlayerPositionsCount = array();
                foreach ($aPositions as $iKey => $aValue) {
                    $aPlayerPositions['position'.$iKey] = array();
                    $aPlayerPositionsCount['position'.$iKey] = 0;
                }

                foreach ($aTimeGroup as $iPlayerKey => $aPlayer) {
                    $aPlayerMarkers = array();
                    $sPosition = 'position'.$aPositionIndex[$aPlayer['positionId']];
                    $aPlayerPositionsCount[$sPosition]++;
                    $aPlayerPositions[$sPosition][$iPlayerKey] = $aPlayer;
                    $aPlayerPositions[$sPosition][$iPlayerKey]['column_type'] = 'player';

                    if ($aPlayer['starting_11'] == 0) {
                        $aSubstitueFlags[$sPosition] = true;
                        $aPlayerMarkers[] = 'substitute';
                    } elseif ($aSubstitueFlags[$sPosition] == true) {
                        $aPlayerMarkers[] = 'plays-after-substitute';
                    }

                    $aPlayerPositions[$sPosition][$iPlayerKey]['markers'] = implode(' ', $aPlayerMarkers);
                }
                $iPlayerPositions = max($aPlayerPositionsCount);
                $iTotalPlayerPositionsInGroup += $iPlayerPositions;

                // Pad each group so that the amount of player columns is the same in each group
                foreach ($aPositions as $iKey => $aValue) {
                    $aPlayerPositions['position'.$iKey] = array_pad(
                        $aPlayerPositions['position'.$iKey],
                        $iPlayerPositions,
                        array('column_type' => 'empty_player')
                    );
                }

                // Add fillers at the end of the time group
                if ($bUseFillers) {
                    $aFillerMarkers = $aMarkers;
                    unset($aFillerMarkers['first']);

                    $aOutput['times'][] = array('column_type' => 'filler', 'markers' => implode(' ', $aFillerMarkers));

                    foreach ($aPositions as $iKey => $aValue) {
                        $aPlayerPositions['position'.$iKey][] = array(
                            'column_type' => 'filler',
                            'markers' => implode(' ', $aFillerMarkers),
                        );
                    }
                }

                // Set how many columns the time group spans
                $aOutput['times'][$iTimestamp]['colspan'] = $iPlayerPositions;

                // Merge time groups
                foreach ($aPositions as $iKey => $aValue) {
                    $aOutput['position'.$iKey] = array_merge(
                        $aOutput['position'.$iKey],
                        $aPlayerPositions['position'.$iKey]
                    );
                }

                $iPointer++;
            }

            // Set how many columns the day spans
            if ($bUseFillers) {
                $aOutput['days'][$iDayStamp]['day_colspan'] = (count($aDay) * 2);
                $aOutput['days'][$iDayStamp]['total_colspan'] = $iTotalPlayerPositionsInGroup + count($aDay);
            } else {
                $aOutput['days'][$iDayStamp]['day_colspan'] = count($aDay);
                $aOutput['days'][$iDayStamp]['total_colspan'] = $iTotalPlayerPositionsInGroup;
            }
        }

        return $aOutput;
    }

    /**
     * Groups players into a multidimensional array of days and times depending on the 'match_date' value.
     *
     * @param array $aPlayerTimes array of players together with their match times
     * @return array players grouped by day (represented by timestamp) and match time (represented by timestamp)
     */
    private function groupPlayersByMatchDatetime($aPlayerTimes)
    {
        $aOutput = array();

        foreach ($aPlayerTimes as $iPlayerId => $aPlayer) {
            if (!array_key_exists('match_date', $aPlayer)) {
                continue;
            }

            $iTimestamp = strtotime($aPlayer['match_date']);
            $iDay = mktime(0, 0, 0, date('m', $iTimestamp), date('d', $iTimestamp), date('Y', $iTimestamp));
            $iTime = mktime(
                date('H', $iTimestamp),
                date('i', $iTimestamp),
                0,
                date('m', $iTimestamp),
                date('d', $iTimestamp),
                date('Y', $iTimestamp)
            );

            if (!array_key_exists($iDay, $aOutput)) {
                $aOutput[$iDay] = array();
                ksort($aOutput);
            }

            if (!array_key_exists($iTimestamp, $aOutput[$iDay])) {
                $aOutput[$iDay][$iTime] = array();
                ksort($aOutput[$iDay]);
            }

            $aOutput[$iDay][$iTime][$iPlayerId] = $aPlayer;
        }

        return $aOutput;
    }

    /**
     * Store player in temporary table
     *
     * @param int iPlayerId
     * @param int iSquadId
     * @param int starting get 0 or 1 to check if player is for lineup or bench
     * @param int sPositionTitle player position title
     */
    public function putIntoSquadPlayers($iPlayerId, $iSquadId, $starting, $sPositionTitle, $positionId)
    {
        $player = $this->get('OulalaiFrame.repository.players')->getPlayerProfileDetails($iPlayerId);
        if ($player['value'] > $this->getBudgetLeft()) {
            $this->aBudget['error'] = true;

            return false;
        }
        $teamPlayers = $this->get('OulalaiFrame.repository.squad')->getSquadePlayers($iSquadId);
        if (!empty($teamPlayers)) {
            foreach ($teamPlayers as $players) {
                if ($players['player_id'] == $iPlayerId) {
                    return false;
                }
            }
        }

        try {
            $this->get('OulalaiFrame.repository.squad')->insertSquadeplayers(
                $iSquadId,
                $iPlayerId,
                $starting,
                $sPositionTitle,
                $positionId
            );
        } catch (Exception $e) {
        }
    }

    /**
     * Store player in temporary table
     *
     * @param int iPlayerId
     * @param int iTeamId
     * @param int starting get 0 or 1 to check if player is for lineup or bench
     * @param int iPositionId player position id
     */
    private function putIntoTeamPlayers($iPlayerId, $iTeamId, $starting, $iPositionId)
    {
        $player = $this->get('OulalaiFrame.repository.players')->getPlayerProfileDetails($iPlayerId);
        if ($player['value'] > $this->getBudgetLeft()) {
            $this->aBudget['error'] = true;

            return false;
        }
        $teamPlayers = $this->get('OulalaiFrame.repository.squad')->getTeamPlayers($iTeamId);

        if (!empty($teamPlayers)) {
            foreach ($teamPlayers as $players) {
                if ($players['player_id'] == $iPlayerId) {
                    return false;
                }
            }
        }
        try {
            $this->get('OulalaiFrame.repository.squad')->insertTempPlayers(
                $iTeamId,
                $iPlayerId,
                $starting,
                $iPositionId
            );
        } catch (Exception $e) {
        }
    }

    /**
     * Returns players data together with the date & time of the first match
     * of the current competition week for each player of the member's team.
     *
     * @return array|null players' times
     * @todo It would be better if this method could be avoided and we could always rely on $this->aSchedulePlayers containing
     * the current competition week match time.
     */
    private function getPlayersWithTimes($iMemberLeagueID)
    {

        if (!is_array($this->aSchedulePlayers) || count($this->aSchedulePlayers) < 1) {
            return null;
        }

        $aResult = $this->get('OulalaiFrame.repository.squad')->getPlayersWithTimes(
            $iMemberLeagueID,
            $this->aSchedulePlayers
        );

        //$this->assignPlayers($iTeamID);
        // Add players' match dates
        $aOutput = $this->aSchedulePlayers;
        foreach ($aResult as $aValue) {
            $aOutput[$aValue['player_id']]['match_date'] = $aValue['match_date'];
        }

        // Sort players so that the substitutes come last
        $aSortFlags = array();
        foreach ($this->aSchedulePlayers as $iPlayerKey => $aPlayer) {
            $aSortFlags[$iPlayerKey] = $aPlayer['starting_11'];
        }

        array_multisort($aSortFlags, SORT_DESC, $aOutput);

        return $aOutput;
    }

    public function openDialogForMemberInvitationAction(Request $request)
    {
        $session = new Session();
        $clientId = $session->get('client');
        $sSorting = "";
        $sWhere = "WHERE lm.`match_id`=m.`match_id`";
        $leaguesList = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('client_leagues', $clientId);
        $minCost=$this->get('OulalaiFrame.repository.config')->getConfigByKeyName('min_join_cost', $clientId);
        $leagues = $this->get('OulalaiFrame.repository.leagues')->availableLeagues($sSorting, $sWhere, null, $minCost, $leaguesList);
        $member = $request->get('m');

        return $this->render(
            'templates/controls/dialog-invite-members-to-league.html.twig',
            array(
                'available_leagues' => $leagues,
                'member' => $member,
            )
        );
    }

    public function changeTeamNameAction(Request $request)
    {
        $session = new Session();
        $clientId = $session->get('client');
        $access_token = $this->get('OulalaiFrame.repository.token')->getClientToken($clientId)->access_token;
        $teamName = $request->request->get('teamName');
        $teamId = $request->request->get('teamId');
        $params = json_encode(['team_name' => $teamName]);
        $savedTeamName = $this->get('OulalaiFrame.repository.squad')->getTeamName($teamId);
        if ($teamName != trim($savedTeamName[0]['name'], " ")) {
            $changedName = $this->get("OulalaiFrame.apiservice.api")->APIRequest(
                'PUT',
                $this->container->getParameter('api_base_url') . 'squads' . "/update/".$teamId,
                $access_token,
                $params
            );

            if (isset($changedName->status) && $changedName->status == "success") {
                $this->get('OulalaiFrame.repository.squad')->updateTeamName($teamId, $teamName);

                return new Response(json_encode("success"));
            } else {
                return new Response(json_encode("error"));
            }
        }
    }
}
