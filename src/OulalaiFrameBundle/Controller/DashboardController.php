<?php

namespace OulalaiFrameBundle\Controller;

use OulalaiFrameBundle\ApiManager\ApiUrls;
use OulalaiFrameBundle\OulalaiFrameBundle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use OulalaiFrameBundle\ApiManager\Manager;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class DashboardController extends Controller
{
    protected $experience_half = 100;
    protected $experience_full = 250;
    private $sRef = '';

    public function indexAction($member = null, Request $request)
    {
        $this->get('OulalaiFrame.repository.config')->getClientData($request->query->all());

        $session = new Session();

        $request = Request::createFromGlobals();

        if (!$session->has('user_ref')) {
//            $referer = $request->headers->get('referer');
            return new RedirectResponse('/'.$request->getLocale());
        }

        $currentMemberRef = $session->get('user_ref');
        $clientId = $session->get('client');
        $style = $this->get('OulalaiFrame.repository.config')->getUserStyle($clientId);

        $css_version = $this->getParameter('css_version');
        $js_version = $this->getParameter('js_version');

        $menu = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('show_menu', $clientId);
        $allowRedirect = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('allow_redirect', $clientId);
        $client_name = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('client_name', $clientId);
        $profileuserid = $currentMemberRef;
        $access_token = $this->get('OulalaiFrame.repository.token')->getClientToken($clientId)->access_token;
        if ($member) {
            $username = $this->get('OulalaiFrame.repository.profile')->getUserRefByUsername($member, $clientId);
            $this->sRef = $username['userref'];
            $profileuserid = $this->sRef;
        } else {
            $this->sRef = $currentMemberRef;
        }
        $this->get('OulalaiFrame.repository.profile')->updateOneUserReward($this->sRef, $client_name, $access_token);
        $this->get('OulalaiFrame.repository.profile')->updateOneUserInfo($access_token, $this->sRef);
        $aMemberInfos = $this->get('OulalaiFrame.repository.profile')->getMemberLevelInfo($this->sRef);
        $aMemberTeam = null;
        if (isset($aMemberInfos['team_id']) && $aMemberInfos['team_id']) {
            $aMemberTeam = $this->get('OulalaiFrame.repository.teams')->getTeamById($aMemberInfos['team_id']);
        }
        $leaguesList = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('client_leagues', $clientId);
        $realLeagues = $this->get('OulalaiFrame.repository.leagues')->getCountryLeague($leaguesList);
        $allTeams = $this->get('OulalaiFrame.repository.teams')->getAllTeams();
        $currentMemberUsername = $this->get('OulalaiFrame.repository.profile')->getMemberLevelInfo($currentMemberRef);
        $aLevels = $this->get('OulalaiFrame.repository.profile')->getAllLevels();
        $expValue = array();
        $cup = false;
        $status = '';
        $mRewardsPoint = 0;
        $lang = explode('/', trim($request->getPathInfo(), '/'));

        if (!empty($aMemberInfos)) {
            $cup = $aMemberInfos['membertrophy'];
            $userExp = $aMemberInfos['memberexperience'] ?: 0;
            if ($userExp >= $this->experience_half && $userExp < $this->experience_full) {
                $expValue['icon'] = 'star-half-full';
            } elseif ($userExp >= $this->experience_full) {
                $expValue['icon'] = 'star';
            } else {
                $expValue['icon'] = 'star-o';
            }
            $expValue['value'] = $userExp;

            $aMemberInfos['memberexperience'] = $expValue;
            $aMemberInfos['membertrophy'] = $cup;
        }
        $aMemberInfos['following'] = $this->get('OulalaiFrame.repository.profile')->getMemberFollowings($this->sRef);
        $aMemberInfos['followers'] = $this->get('OulalaiFrame.repository.profile')->getMemberFollowers($this->sRef);
        $aMemberInfos['badges'] = $this->assignMemberBadges($this->sRef);
        $memberNotificaions = $this->get('OulalaiFrame.repository.profile')->getMemberNotifications($this->sRef);
        $aMemberInfos['membersrewards'] = $this->get('OulalaiFrame.repository.profile')->getMemberAllAchievedRewardsIds($this->sRef);
        $currentMember['currentMemberID'] = $currentMemberRef;
        $currentMember['currentMemberUsername'] = $currentMemberUsername['username'];

        return $this->render('templates/userprofile.html.twig',
            array(
                'profileuserid' => $profileuserid,
                'currentMemberURL' => $lang[0] . '/userprofilehome',
                'memberinfo' => $aMemberInfos,
                'memberlevels' => $aLevels,
                'bio' => $aMemberInfos['bio'] ? $aMemberInfos['bio'] : '',
                'memberNotification' => $memberNotificaions,
                'aMemberTeam' => $aMemberTeam,
                'leagues' => $realLeagues,
                'teams' => $allTeams,
                'userfollowing' => $this->get('OulalaiFrame.repository.profile')->getFollowingIDs($currentMemberRef),
                'userfollowers' => $this->get('OulalaiFrame.repository.profile')->getFollowerIDs($currentMemberRef),
                'currentMember' => $currentMember,
                'follows' => $this->get('OulalaiFrame.repository.profile')->follwingExists($currentMemberRef, $profileuserid),
                'style' => $style . $css_version,
                'css_version' => $css_version,
                'js_version' => $js_version,
                'client_name' => $client_name,
                'menu' => $menu,
                'allowRedirect' => $allowRedirect,
            ));
    }

    private function assignMemberBadges($sRef)
    {
        $badges = array();
        $rRules = $this->get('OulalaiFrame.repository.profile')->getMemberBadges($sRef);

        if (!empty($rRules)) {
            foreach ($rRules as $key => $rule) {
                $badges[$key]['reward_rule_id'] = $rule['reward_rule_id'];
                $badges[$key]['reward'] = array_merge($this->get('OulalaiFrame.repository.profile')->getRewardsDataByRule($rule['reward_rule_id']));
                $badges[$key]['memberLastReward'] = unserialize($rule['memberLastReward']);
                $badges[$key]['memberNextReward'] = unserialize($rule['memberNextReward']);
            }
        }
        return $badges;
    }

    public function searchMembersToFollowAction(Request $request)
    {
        $session = new Session();
        $request = Request::createFromGlobals();

        if (!$session->has('user_ref')) {
            return new RedirectResponse('/' . $request->getLocale());
        }
        $this->sRef = $session->get('user_ref');

        if ($request->isXmlHttpRequest()) {
            $search = $request->get('search');
            $is_invite = $request->get('invite');
            $clientId = $session->get('client');

            $members = $this->get('OulalaiFrame.repository.profile')->getMembersToFollow($this->sRef, $search, $clientId, $is_invite);
            $json = json_encode($members);
            $response = new Response($json, 200);
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
    }

    public function followMemberAction(Request $request)
    {
        $session = new Session();
        $request = Request::createFromGlobals();

        if (!$session->has('user_ref')) {
            return new RedirectResponse('/' . $request->getLocale());
        }
        $this->sRef = $session->get('user_ref');
        $access_token = $this->get('OulalaiFrame.repository.token')->getToken();
        $clientId = $session->get('client');

        if ($request->isXmlHttpRequest()) {
            $follower_username = trim($request->get('follower'));
            $follower = $this->get('OulalaiFrame.repository.profile')->getUserRefByUsername($follower_username, $clientId);
            $follower = $follower['userref'];

            if ($follower == $this->sRef) {
                $following_username = trim($request->get('following'));
                $following = $this->get('OulalaiFrame.repository.profile')->getUserRefByUsername($following_username, $clientId);
                $following = $following['userref'];

                $status = $request->get('status');
                $status = $status ?: 'follow';

                $form_params = array(
                    'follower_ref' => $follower,
                    'Following_ref' => $following,
                    'followstatus' => $status
                );
                $params = $form_params;

                if (in_array($status, array('follow', 'unfollow'))) {
                    $data = $this->get("OulalaiFrame.apiservice.api")->APIRequest('POST', $this->container->getParameter('api_base_url') . 'followers', $access_token, $params);

                    if (!empty($data->status) && ($data->status == 'success')) {
                        $this->get('OulalaiFrame.repository.profile')->updateUserFollowers($access_token, $this->sRef);
                        $json = json_encode(array('success'));
                        $response = new Response($json, 200);
                        $response->headers->set('Content-Type', 'application/json');
                    } else {
                        $response = new Response(json_encode(array($data)), 200);
                        $response->headers->set('Content-Type', 'application/json');
                    }
                }

                return $response;
            }
        }
    }

    public function showMemberInviteBoxAction(Request $request)
    {
        $session = new Session();
        $sSorting = "";
        $sWhere = "WHERE lm.`match_id`=m.`match_id`";
        $clientId = $session->get('client');
        $leaguesList = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('client_leagues', $clientId);
        $leagues = $this->get('OulalaiFrame.repository.leagues')->availableLeagues($sSorting, $sWhere, null, null, $leaguesList);
        $member_username = trim($request->get('member'));
        $member = $this->get('OulalaiFrame.repository.profile')->getUserRefByUsername($member_username, $clientId);
        $member = $member['userref'];

        return $this->render('templates/controls/dialog-invite-member-to-league.html.twig', array(
            'available_leagues' => $leagues,
            'member' => $member
        ));
    }


    public function addMemberBioAction(Request $request)
    {
        $session = new Session();
        $user_ref = $session->get('user_ref');
        if ($request->isXmlHttpRequest()) {
            $bio = $request->request->all()['bio'];
            $team_id = $request->request->all()['teamid'];
            $access_token = $this->get('OulalaiFrame.repository.token')->getToken();
            if ($team_id=="Choose team") {
                $team_id=0;
            }
            $form_params = array(
                'user_ref' => $user_ref,
                'bio' => $bio,
                'team_id' => $team_id
            );

            $params = $form_params;
            $data = $this->get("OulalaiFrame.apiservice.api")->APIRequest('PUT', $this->container->getParameter('api_base_url') . 'users', $access_token, $params);

            if (isset($data->status) && ($data->status == 'success')) {
                $this->get('OulalaiFrame.repository.profile')->updateMemberBio($access_token, $user_ref);
                $json = json_encode(array('success' => true));
                $response = new Response($json, 200);
                $response->headers->set('Content-Type', 'application/json');
            } else {
                $response = new Response(json_encode(array($data)), 200);
                $response->headers->set('Content-Type', 'application/json');
            }

            return $response;
        }
    }

    public function rulesAction()
    {
        $session = new Session();
        $clientId = $session->get('client');
        $style = $this->get('OulalaiFrame.repository.config')->getUserStyle($clientId);

        $css_version = $this->getParameter('css_version');
        $js_version = $this->getParameter('js_version');


        $menu = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('show_menu', $clientId);
        $client_name = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('client_name', $clientId);

        return $this->render(':templates:rules.html.twig', [
            'style' => $style .$css_version,
            'css_version' => $css_version,
            'js_version' => $js_version,
            'name' => $client_name,
            'menu' => $menu,
        ]);
    }

    public function howItWorksAction()
    {
        $session = new Session();
        $clientId = $session->get('client');
        $style = $this->get('OulalaiFrame.repository.config')->getUserStyle($clientId);
        $css_version = $this->getParameter('css_version');
        $js_version = $this->getParameter('js_version');

        $menu = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('show_menu', $clientId);
        $client_name = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('client_name', $clientId);

        $style = $this->get('OulalaiFrame.repository.config')->getUserStyle($clientId);

        return $this->render(':templates:how-it-works.html.twig', [
            'style' => $style .$css_version,
            'css_version' => $css_version,
            'js_version' => $js_version,
            'name' => $client_name,
            'menu' => $menu,
        ]);
    }
}
