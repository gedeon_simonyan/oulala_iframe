<?php

namespace OulalaiFrameBundle\Controller;

use OulalaiFrameBundle\Entity\ForumReplies;
use OulalaiFrameBundle\Entity\ForumTopics;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class ForumController extends Controller
{
    public function indexAction(Request $request)
    {
        $session = new Session();
        $isLogged = false;

        $operator_id = isset($request->query->all()['operator_id']) ? $request->query->all()['operator_id'] : null;
        $client_token = isset($request->query->all()['token']) ? $request->query->all()['token'] : null;

        if ($client_token) {
            $this->get('OulalaiFrame.repository.config')->getClientData($request->query->all());
            $clientId = $session->get('client');
        } elseif ($operator_id) {
            $clientId = $this->get('OulalaiFrame.repository.config')->getClientByAlias($operator_id);
            $session->set('client', $clientId);
        } else {
            $clientId = $session->get('client');
        }

        $limit=0;
        if (isset($request->query->all()['p'])) {
            $limit=$request->query->all()['p'];
        }

        $style = $this->get('OulalaiFrame.repository.config')->getUserStyle($clientId);
        $css_version = $this->getParameter('css_version');
        $js_version = $this->getParameter('js_version');
        $menu = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('show_menu', $clientId);
        $contact_page = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('contact_page', $clientId);
        if ($session->has('user_ref')) {
            $isLogged = true;
        }

        $offset = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('forum_pages_count', $clientId);
        $topicsPostsCount = $this->get('OulalaiFrame.repository.forum')->getTopicCount($clientId);
        $offset=$offset?$offset:10;
        if ($limit==0 ||$limit==1) {
            $countFrom=1;
            $countTo=$offset;
            $nextPage=2;
        } else {
            $nextPage=$limit+1;
            $countTo=$limit*$offset-1;
            $countFrom=($limit-1)*$offset+1;
        }

        $lastPage=ceil($topicsPostsCount/$offset);
        $prevPage=$limit-1;

        /*** get topics ***/
        $topics = $this->get("OulalaiFrame.repository.forum")->getTopics($clientId, $limit, $offset);
        foreach ($topics as $key => $value) {
            if ($value['reply_id']) {
                /*** get topics first level chldren ***/
                $topics[$key]['subItems'] = $this->get('OulalaiFrame.repository.forum')->getTopicChildren($value['topic_id'], true, 0);
            }
        }

        /*** get item children items ***/
        if ($request->isXmlHttpRequest()) {
            $topicId = intval($request->get('topicId'));
            $level = intval($request->get('level'));
            if ($topicId) {
                $items = $this->get("OulalaiFrame.repository.forum")->getTopicChildren($topicId, false, $level);

                foreach ($items as $key => $value) {
                    if ($value['reply_id']) {
                        $reply_count = $this->get("OulalaiFrame.repository.forum")->getRepliesChildren($value['reply_id']);
                        $items[$key]['reply_count'] = $reply_count;
                    }
                }
                return new Response(json_encode($items));
            }
        }
        return $this->render('forum/forum.html.twig',
            [
                'topics' => $topics,
                'topicsPostsCount' => $topicsPostsCount,
                'countFrom' => $countFrom,
                'countTo' => $countTo,
                'nextPage' => $nextPage,
                'lastPage' => $lastPage,
                'prevPage' => $prevPage,
                'offset' => $offset,
                'isLogged' => $isLogged,
                'style' => $style . $css_version,
                'css_version' => $css_version,
                'js_version' => $js_version,
                'menu' => $menu,
                'contact_page' => $contact_page
            ]
        );
    }

    public function replyForumAction(Request $request)
    {
        $session = new Session();
        $sUserRef = $session->get('user_ref');
        $userId = intval($this->get('OulalaiFrame.repository.notification')->getUidByRef($sUserRef));
        /*** return reply dialog ***/
        $chatId = intval($request->get('chatId'));

        if (!empty($chatId)) {
            return $this->render(':forum:dialog-forum-replay.html.twig');
        }

        /*** insert reply ***/
        $data = $request->get('data');
        $lastInsertedItem = "";
        $data['reply_by'] = $userId;
        if (!empty($data['text']) && isset($data['level']) && !empty($data['forumId'])) {
            $success = $this->get('OulalaiFrame.repository.forum')->insertReply($data);
            if ($success && $data['level'] == 0 && $data['topicId']) {
                $this->get('OulalaiFrame.repository.forum')->updateTopicDate($data['topicId']);
            }
            if ($success) {
                $lastInsertedItem = $this->get('OulalaiFrame.repository.forum')->getLastInsertedItem($data);
            }
            return new Response(json_encode($lastInsertedItem));
        }
        return new Response('Empty data');
    }

    public function insertItemAction(Request $request)
    {
        $session = new Session();
        $userRef = trim($request->get('userRef'), "/");
        $userId = intval($this->get('OulalaiFrame.repository.notification')->getUidByRef($userRef));
        $subject = $request->get('message');
        $locale = trim(strtoupper($request->get('locale')), "/");
        $topic_operator = $session->get('client');
        $is_admin = 0;
        $response = "Error";

        if (is_numeric($userId) && !empty($subject)) {
            $data = ['topic_subject' => $subject, 'topic_by' => $userId, 'topic_locale' => $locale,
                'topic_operator' => $topic_operator, 'is_admin' => $is_admin
            ];
            $success = $this->get('OulalaiFrame.repository.forum')->insertPost($data);
            if ($success) {
                $response = "Success";
            }
            return new Response($response);
        }
        return new Response($response);
    }

    public function newAction(Request $request)
    {
        $usr= $this->get('security.token_storage')->getToken()->getUser();
        $role = $usr->getRoles()['0'];
        $topic = new ForumTopics();
        $form = $this->createForm('OulalaiFrameBundle\Form\ForumTopicsType', $topic);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $date = new \DateTime();
            $topic->setUpdateTime($date);
            $topic->setCreateTime($date);
            $alias = $this->getUser()->getUsername();
            $operatorId = $this->get('OulalaiFrame.repository.config')->getClientByAlias($alias);
            $topic->setTopicBy($operatorId);
            $topic->setTopicOperator($operatorId);
            $topic->setIsAdmin(1);
            $topic->setTopicLevel(0);
            $em = $this->getDoctrine()->getManager();
            $em->persist($topic);
            $em->flush();

            return $this->redirectToRoute('admin_forum');
        }
        return $this->render(
            'admin/forum/new.html.twig',
            array(
                'topic' => $topic,
                'form' => $form->createView(),
                'role' => $role
            )
        );
    }

    public function newReplyAction($replyId, Request $request)
    {
        $usr= $this->get('security.token_storage')->getToken()->getUser();
        $role = $usr->getRoles()['0'];
        $alias = $this->getUser()->getUsername();
        $operatorId = $this->get('OulalaiFrame.repository.config')->getClientByAlias($alias);
        /*** get reply by id ***/
        $item = $this->get('OulalaiFrame.repository.forum')->getReplies($operatorId, $replyId);
        $reply = new ForumReplies();
        $form = $this->createForm('OulalaiFrameBundle\Form\ForumRepliesType', $reply);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $date = new \DateTime();
            $reply->setHasReply(0);
            $reply->setCreateTime($date);
            $reply->setUpdateTime($date);
            $alias = $this->getUser()->getUsername();
            $operatorId = $this->get('OulalaiFrame.repository.config')->getClientByAlias($alias);
            $reply->setReplyBy($operatorId);
            $reply->setReplyTopic($item['reply_topic']);
            $reply->setReplyTo($replyId);
            $reply->setIsAdmin(1);
            $reply->setForumLevel(intval(++$item['forum_level']));
            $em = $this->getDoctrine()->getManager();
            $em->persist($reply);
            $em->flush();

            $this->get('OulalaiFrame.repository.forum')->updateReplyDate($replyId);
            $this->get('OulalaiFrame.repository.forum')->hasReply($replyId);
            return $this->redirectToRoute('admin_reply');
        }
        return $this->render(
            'admin/forum/new_reply.html.twig',
            array(
                'reply' => $reply,
                'form' => $form->createView(),
                'id' => $item['reply_id'],
                'role' => $role
            )
        );
    }
    
    public function newTopicReplyAction($topicId, Request $request)
    {
        $usr= $this->get('security.token_storage')->getToken()->getUser();
        $role = $usr->getRoles()['0'];
        /*** get reply by id ***/
//        $item = $this->get('OulalaiFrame.repository.forum')->getTopics($operatorId, $topicId);
        $reply = new ForumReplies();
        $form = $this->createForm('OulalaiFrameBundle\Form\ForumRepliesType', $reply);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $date = new \DateTime();
            $reply->setHasReply(0);
            $reply->setCreateTime($date);
            $reply->setUpdateTime($date);
            $alias = $this->getUser()->getUsername();
            $operatorId = $this->get('OulalaiFrame.repository.config')->getClientByAlias($alias);
            $reply->setReplyBy($operatorId);
            $reply->setReplyTopic($topicId);
            $reply->setReplyTo(0);
            $reply->setIsAdmin(1);
            $reply->setForumLevel(1);
            $em = $this->getDoctrine()->getManager();
            $em->persist($reply);
            $em->flush();

            $this->get('OulalaiFrame.repository.forum')->updateTopicDate($topicId);

            return $this->redirectToRoute('admin_reply');
        }
        return $this->render(
            'admin/forum/new_topic_reply.html.twig',
            array(
                'reply' => $reply,
                'form' => $form->createView(),
                'id' => $topicId,
                'role' => $role
            )
        );
    }

    public function editAction($topicId, Request $request)
    {
        $usr= $this->get('security.token_storage')->getToken()->getUser();
        $role = $usr->getRoles()['0'];
        $em = $this->getDoctrine()->getManager();
        $topic = $this->getDoctrine()->getRepository(ForumTopics::class)->find($topicId);

        $editForm = $this->createForm('OulalaiFrameBundle\Form\ForumTopicsType', $topic);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $data = $editForm->getData();
            $em->persist($data);
            $em->flush();

            return $this->redirectToRoute('topic_show', array('topicId' => $topicId));
        }
        return $this->render(
            ':admin/forum:edit.html.twig',
            array(
                'form' => $editForm->createView(),
                'id' => $topicId,
                'role' => $role
            )
        );
    }

    public function showTopicAction($topicId, Request $request)
    {
        $usr= $this->get('security.token_storage')->getToken()->getUser();
        $role = $usr->getRoles()['0'];
        $alias = $this->getUser()->getUsername();
        $operatorId = $this->get('OulalaiFrame.repository.config')->getClientByAlias($alias);
        $forumTopic = $this->getDoctrine()
            ->getRepository(ForumTopics::class)
            ->find($topicId);
        $deleteForm = $this->createDeleteForm($forumTopic);
        $forumTopics = $this->get('OulalaiFrame.repository.forum')->getForums($operatorId, $topicId);

        return $this->render(
            'admin/forum/show.html.twig',
            array(
                'forumTopic' => $forumTopics,
                'delete_form' => $deleteForm->createView(),
                'role' => $role
            )
        );
    }

    public function deleteAction($topicId, Request $request)
    {
        $blogPost = $this->getDoctrine()
            ->getRepository(ForumTopics::class)
            ->find($topicId);
        $deleteForm = $this->createDeleteForm($blogPost);
        $deleteForm->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        if ($deleteForm->isSubmitted() && $deleteForm->isValid()) {
            $em->remove($blogPost);
            $em->flush();
        } else {
            $em->remove($blogPost);
            $em->flush();
        }

        return $this->redirectToRoute('admin_forum');
    }

    public function showReplyAction($replyId, Request $request)
    {
        $usr= $this->get('security.token_storage')->getToken()->getUser();
        $role = $usr->getRoles()['0'];
        $alias = $this->getUser()->getUsername();
        $operatorId = $this->get('OulalaiFrame.repository.config')->getClientByAlias($alias);
        $forumTopic = $this->getDoctrine()
            ->getRepository(ForumReplies::class)
            ->find($replyId);
        $deleteForm = $this->createReplyDeleteForm($forumTopic);
        $forumTopics = $this->get('OulalaiFrame.repository.forum')->getReplies($operatorId, $replyId);

        return $this->render(
            'admin/forum/show_reply.html.twig',
            array(
                'forumTopic' => $forumTopics,
                'delete_form' => $deleteForm->createView(),
                'role' => $role
            )
        );
    }


    public function editReplyAction($replyId, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $topic = $this->getDoctrine()->getRepository(ForumReplies::class)->find($replyId);
        $usr= $this->get('security.token_storage')->getToken()->getUser();
        $role = $usr->getRoles()['0'];
        $editForm = $this->createForm('OulalaiFrameBundle\Form\ForumRepliesType', $topic);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $data = $editForm->getData();
            $em->persist($data);
            $em->flush();

            return $this->redirectToRoute('reply_show', array('replyId' => $replyId));
        }
        return $this->render(
            ':admin/forum:edit_reply.html.twig',
            array(
                'form' => $editForm->createView(),
                'id' => $replyId,
                'role' => $role
            )
        );
    }

    public function deleteReplyAction($replyId, Request $request)
    {
        $reply = $this->getDoctrine()
            ->getRepository(ForumReplies::class)
            ->find($replyId);
        $deleteForm = $this->createReplyDeleteForm($reply);
        $deleteForm->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        if ($deleteForm->isSubmitted() && $deleteForm->isValid()) {
            $em->remove($reply);
            $em->flush();
        } else {
            $em->remove($reply);
            $em->flush();
        }

        return $this->redirectToRoute('admin_reply');
    }

    private function createDeleteForm(ForumTopics $forumTopic)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('topic_delete', array('topicId' => $forumTopic->getTopicId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    private function createReplyDeleteForm(ForumReplies $forumReply)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('reply_delete', array('replyId' => $forumReply->getReplyId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
