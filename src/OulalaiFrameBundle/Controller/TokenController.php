<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 14-Dec-16
 * Time: 18:34
 */

namespace OulalaiFrameBundle\Controller;

use OulalaiFrameBundle\ApiManager\ApiUrls;
use OulalaiFrameBundle\ApiManager\Manager;
use OulalaiFrameBundle\OulalaiFrameBundle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TokenController extends Controller
{
    /**
     * Required parameters for getting access_token
     */
    private $content;


    /**
     *  Building Token
     */
    public function buildTokenAction(Request $request)
    {
        $response = new Response();
        if (empty($request->cookies->get('access_token'))) {
            $access_token = $this->getToken();
            if (isset($access_token)) {
                $cookie = new Cookie('access_token', $access_token, time() + 3600);
                $response->headers->setCookie($cookie);
                $response->send();
            }
        }
        return $this->render('OulalaiFrameBundle:templates:matches.html.twig', array("data" => ''));
    }

    public function getToken()
    {
        $this->getClientData('api');
        $access_token = $this->get("OulalaiFrame.apiservice.api")->APIRequest("POST", $this->container->getParameter('api_auth_url'), '', $this->content['content']);

        if (isset($access_token->error)) {
            return "Invalid token";
        }
        return $access_token->access_token;
    }

    public function getAllTokens()
    {
        $access_token=[];
        $contents=$this->getClientData('api');

        foreach ($contents as $key=>$content) {
            $token=$this->get("OulalaiFrame.apiservice.api")->APIRequest("POST", $this->container->getParameter('api_auth_url'), '', $content['content']);
            if (!isset($token->error)) {
                $access_token[$key]['token'] = $token->access_token;
                $access_token[$key]['client_id'] = $content['client_id'];
            }
        }

        return $access_token;
    }


    public function getClientToken($client_id)
    {
        $contents=$this->getClientData('api', $client_id);
        $token=$this->get("OulalaiFrame.apiservice.api")->APIRequest("POST", $this->container->getParameter('api_auth_url'), '', $contents[0]['content']);
        return $token;
    }

    public function getIframeTokens($client_id)
    {
        $access_token=[];
        $contents=$this->getClientData('iframe', $client_id);

        foreach ($contents as $key=>$content) {
            $url=$this->getIframeTokenUrl($client_id);
            $client_website=$this->getIframeUrl($client_id);
            $token=$this->get("OulalaiFrame.apiservice.api")->APIRequest("POST", $url['url'], '', $content['content']);

            if (!isset($token->error)) {
                $access_token['token'] = $token->access_token;
                $access_token['url'] = $client_website['url'];
            }
        }

        return $access_token;
    }

    private function getIframeTokenUrl($client_id)
    {
        $connection = OulalaiFrameBundle::getContainer()->get('doctrine')->getManager()->getConnection();
        $trans = $connection->prepare('SELECT `config_value` as url  FROM `config` WHERE 
                          `config_key` = "iframe_toke_url" AND `client_id`='.$client_id);
        $trans->execute();
        $url = $trans->fetch();
        return $url;
    }

    private function getIframeUrl($client_id)
    {
        $connection = OulalaiFrameBundle::getContainer()->get('doctrine')->getManager()->getConnection();
        $trans = $connection->prepare('SELECT `config_value` as url  FROM `config` WHERE 
                          `config_key` = "client_website" AND `client_id`='.$client_id);
        $trans->execute();
        $url = $trans->fetch();
        return $url;
    }

    private function getClientData($type, $id=0)
    {
        $content = [];
        $where='';
        if ($id) {
            $where=' AND `client_id`='.$id;
        }
        $connection = OulalaiFrameBundle::getContainer()->get('doctrine')->getManager()->getConnection();
        $trans = $connection->prepare('SELECT `config_key` ,`config_value`  FROM `config` WHERE 
                          `config_key` IN ("'.$type.'_client_secret", "'.$type.'_client_id")');
        $trans->execute();
        $secrets = $trans->fetchAll();
        if ($secrets) {
            foreach ($secrets as $secret) {
                if ($secret['config_key'] == $type.'_client_secret') {
                    $client_secret = $secret['config_value'];
                } elseif ($secret['config_key'] == $type.'_client_id') {
                    $client_id = $secret['config_value'];
                }
            }
        }

        $em1 = $connection->prepare('SELECT `client_id` FROM `config` 
                      WHERE `config_key`="'.$type.'_username"'.$where.' GROUP BY `client_id`');
        $em1->execute();
        $clients = $em1->fetchALL();

        if ($clients) {
            foreach ($clients as $key=>$client) {
                $em2 = $connection->prepare('SELECT `config_key`, `config_value`  FROM `config` 
                      WHERE `config_key` IN("'.$type.'_username","'.$type.'_password") AND `client_id`=:id ORDER BY `config_key` ASC');
                $em2->bindValue(':id', $client['client_id']);
                $em2->execute();
                $client_datas = $em2->fetchALL();

                if ($client_datas) {
                    foreach ($client_datas as $client_data) {
                        if ($client_data['config_key'] == $type.'_password') {
                            $client_password = $client_data['config_value'];
                        } elseif ($client_data['config_key'] == $type.'_username') {
                            $client_username = $client_data['config_value'];
                        }
                    }
                    if ($client_id && $client_secret && $client_username && $client_password) {
                        $content[$key]['content'] = 'grant_type=password&client_id=' . $client_id . '&client_secret=' . $client_secret . '&username=' . $client_username . '&password=' . $client_password;
                        $content[$key]['client_id'] = $client['client_id'];
                        $this->content=$content[0];
                    } else {
                        $content=[];
                    }
                }
            }
        }

        return $content;
    }
}
