<?php

namespace OulalaiFrameBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class OulalaHomeController extends Controller
{
    protected $iMemberTeamID = null;

    private $iCurrentTeamId = null;
    private $aFormationSlots = null;
    private $leagueId = null;
    private $formationList = null;

    private $sMatches = null;
    private $sDays = null;
    private $sSaveLeagues = null;
    private $aSchedulePlayers = array();
    private $aBudget = array();
    private $aSlots = 1;
    private $aValid = array();

    private $sLeagues = null;
    private $aLeagueTeams = null;
    private $sRef = null;

    /**
     * @var iPage
     */
    private $iPage = 1;
    /**
     * @var iStart
     */
    private $iStart = 0;
    /**
     * @var iLimit
     */
    private $iLimit = 6;
    /**
     * @var aPlayers
     */
    private $aPlayers = null;
    /**
     * @var IsValid
     */
    private $bIsValid = false;
    /**
     * @var fAmountSpent
     */
    private $fAmountSpent = 0;
    private $fMemberLeagueBudget = 100;
    private $sTeamName = "FC OULALA";

    public function __construct($iCurrentTeamId = null)
    {
        $request = Request::createFromGlobals();

        $this->aValid['Goalkeeper'] = true;
        $this->aValid['Defender'] = true;
        $this->aValid['Midfielder'] = true;
        $this->aValid['Striker'] = true;
        $this->aValid['bench'] = true;
        if (empty($this->aPlayers['lineup'])) {
            $this->aPlayers['lineup'] = array();
        }
        if (empty($this->aPlayers['bench'])) {
            $this->aPlayers['bench'] = array();
        }

        $this->leagueId = $request->request->get('leagueid');
        $squadFormation = false;

        $iTeamId = $request->request->get('teamid');

        if ($iCurrentTeamId) {
            $this->iMemberTeamID = $iCurrentTeamId;
        } elseif (!empty($iTeamId)) {
            $this->iMemberTeamID = $iTeamId;
        }
        if (!empty($this->iMemberTeamID)) {
            $formation = $request->request->get('formation');
            $fID = ($formation) ? $formation : (($squadFormation) ? $squadFormation : 1);
            $this->leagueId = 0;
            $this->aSlots = $fID;
            if ($request->request->get('name')) {
                $this->sTeamName = $request->request->get('name');
            }
        }
    }

    public function indexAction($iCurrentTeamId = null)
    {
        $request = Request::createFromGlobals();
        $session = new Session();
        $session->clear();
        $clientId = $session->get('client');
        if (!$clientId) {
            $clientId = $this->get('OulalaiFrame.repository.config')->getClientByAlias(
                $request->query->all()['operator_id']
            );
            if (!$clientId) {
                $clientId = 1;
            }
            $session->set('client', $clientId);
        }

        $style = $this->get('OulalaiFrame.repository.config')->getUserStyle($clientId);

        $css_version = $this->getParameter('css_version');
        $js_version = $this->getParameter('js_version');

        $menu = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('show_menu', $clientId);
        $clientName = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('client_name', $clientId);
        $leagueListUrl = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('league_list_url', $clientId);
        $forumUrl = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('forum_url', $clientId);
        $blogUrl = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('blog_share_url', $clientId);
        $leaguesList = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('client_leagues', $clientId);
        $this->leagueId = 2;

        $formation = $request->request->get('formation');
        $type = $request->request->get('type');
        $iPage = $request->request->get('page');
        $loadingteam = $request->request->get('loadingteam');

        $aLeagueDetails = $this->get('OulalaiFrame.repository.leagues')->getMemberLeagueDetailsByID($this->leagueId);

        $memberteamschedule = array();//$this->getSubstitutionGuide(true,$this->leagueId);
        $positions = $this->get('OulalaiFrame.repository.players')->getAllPlayerPositions();
        $squadFormation = false;

        $this->iCurrentTeamId = $iCurrentTeamId;
        $this->iMemberTeamID = $iCurrentTeamId;

        if ($this->iCurrentTeamId) {
            $squadFormation = $this->get('OulalaiFrame.repository.savedsquad')->getDraftTeamDetailsById(
                $this->iCurrentTeamId
            );
            if ($squadFormation) {
                $squadFormation = $squadFormation['fk_formation_id'];
            } else {
                //redirect to squads list if no squad with the requested squad ID
                return new RedirectResponse('/'.$request->getLocale().'/squads-list/upcoming', 301);
            }
            $this->assignPlayers($this->iCurrentTeamId, true);
        }

        $userIP = str_replace('.', '', explode(',', $this->get_ip())[0]);
        $userCountryCode = $this->get('OulalaiFrame.repository.config')->getUserCountrCode($userIP);


        $formation = $request->request->get('formation');
        $matchDate = isset($request->query->all()['date']) ? $request->query->all()['date'] : 0;
        $fID = ($squadFormation) ? $squadFormation : (($formation) ? $formation : 1);
        $this->aFormationSlots = $this->get('OulalaiFrame.repository.teams')->assignFormationDetails($fID);
        $this->aSlots = $fID;
        $this->formationList = $this->get('OulalaiFrame.repository.teams')->getFormation();
        $country_leagues = $this->get('OulalaiFrame.repository.leagues')->getCountryLeague($leaguesList);
        $confDate = '00:30:00';
        $availableMatchDates = $this->get('OulalaiFrame.repository.matches')->getMatchesDate(3, $confDate);

        if ($request->isXmlHttpRequest()) {
            $action = $request->request->get('action');
            $league_id = $request->request->get('leagueid');
            switch ($action) {

                case "loadAvailableMatches":
                    if (is_array($request->request->get('date'))) {
                        $availableMatches = array();
                        foreach ($request->request->get('date') as $sDate) {
                            $availableMatche = $this->get('OulalaiFrame.repository.matches')->getFromDate(
                                $sDate,
                                $confDate,
                                $league_id
                            );
                            $availableMatches = array_merge($availableMatches, $availableMatche);
                        }
                    } else {
                        $availableMatches = $this->get('OulalaiFrame.repository.matches')->getFromDate(
                            $request->request->get('date'),
                            $confDate,
                            $league_id
                        );
                    }

                    $availableMatchesFixtures = array();
                    foreach ($availableMatches as $key => $value) {
                        $availableMatchesFixtures[$value['league_dates']][$key] = $value;
                    }

                    return $this->render(
                        'templates/controls/dialog-create-team-available-matches.html.twig',
                        array("availableMatches" => $availableMatchesFixtures)
                    );
                    break;


            }
        }
        $blogPost = $this->get('OulalaiFrame.repository.blog')->getBlogPosts($clientId);
        $lastPost = '';
        if ($blogPost) {
            $lastPost = $blogPost[0];
        }
        $league_exists = $this->in_array_r($userCountryCode, $this->sLeagues);

        if (!$league_exists) {
            $userCountryCode = '';
        }
        if ($matchDate) {
            $mDateStr = explode('-', $matchDate);
            $mDateArr = ['Year' => $mDateStr[0], 'Month' => $mDateStr[1], 'Day' => $mDateStr[2]];
            $availableMatches = $this->getMatches($mDateArr, $userCountryCode);
        } else {
            $availableMatches = $this->getMatches($availableMatchDates[0], $userCountryCode);
        }

        $matchStartTime = $this->get('OulalaiFrame.repository.matches')->getFirstMatchStartTime(
            implode(',', $availableMatches)
        );
        $userCountryCode = 'EN';
        if(!$matchDate){
            $date = new \DateTime();
            $date->setDate($availableMatchDates[0]['Year'],$availableMatchDates[0]['Month'],$availableMatchDates[0]['Day']);
            $matchDate = $date->format('Y-m-d');
        }

        return $this->render(
            'templates/homepage/oulalaHome.html.twig',
            array(
                'leagues' => $this->sLeagues,
                'teams' => $this->aLeagueTeams,
                'blogPost' => $lastPost,
                'timeToStart' => $matchStartTime ? $matchStartTime : 0,
                'countries' => $country_leagues,
                'dates' => $availableMatchDates,
                'leagueId' => $this->leagueId,
                'formationList' => $this->formationList,
                'playervalue' => 45,
                'budget' => $this->aBudget,
                'positions' => $positions,
                'league' => $aLeagueDetails,
                'teamid' => $this->iMemberTeamID,
                'isSaved' => 1,
                'matches' => implode(',', $this->sMatches),
                'days' => implode(',', $this->sDays),
                'loadingteam' => ($loadingteam ? 1 : 0),
                'fieldFormation' => $this->aFormationSlots,
                'formation' => $this->aSlots,
                'memberteamschedule' => $memberteamschedule,
                'playerslist' => $this->aPlayers,
                'players' => array(),
                'valid_team' => $this->bIsValid,
                'showJoinButton' => false,
                'page_offset' => $iPage,
                'budgetleft' => $this->getBudgetLeft(),
                'client_name' => 'Oulala',
                'style' => $style.$css_version,
                'css_version' => $css_version,
                'js_version' => $js_version,
                'menu' => $menu,
                'countryCode' => $userCountryCode,
                'leagueUrl' => $leagueListUrl,
                'forumUrl' => $forumUrl,
                'blogUrl' => $blogUrl,
                'matchDate' => $matchDate,
            )
        );
    }

    private function in_array_r($needle, $haystack, $strict = false)
    {
        foreach ($haystack as $item) {
            if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->in_array_r(
                        $needle,
                        $item,
                        $strict
                    ))) {
                return true;
            }
        }

        return false;
    }

    public function resetTimerAction(Request $request)
    {
        if ($request->isXmlHttpRequest()) {

            $countryCode = $request->request->get('code');

            $matchDate = '2018-03-13';
            $confDate = '00:30:00';
            $availableMatchDates = $this->get('OulalaiFrame.repository.matches')->getMatchesDate(3, $confDate);
            if ($matchDate) {
                $mDateStr = explode('-', $matchDate);
                $mDateArr = ['Year' => $mDateStr[0], 'Month' => $mDateStr[1], 'Day' => $mDateStr[2]];
                $availableMatches = $this->getMatches($mDateArr, $countryCode);
            } else {
                $availableMatches = $this->getMatches($availableMatchDates[0], $countryCode);
            }
            $matchStartTime = $this->get('OulalaiFrame.repository.matches')->getFirstMatchStartTime(
                implode(',', $availableMatches)
            );

            return $this->render(
                'templates/homepage/home-timer.html.twig',
                array(
                    'countryCode' => $countryCode,
                    'timeToStart' => $matchStartTime ? $matchStartTime : 0,
                )
            );
        }
    }

    private function get_ip()
    {
        // IP si internet partagé
        if (isset($_SERVER['HTTP_CLIENT_IP'])) {
            return $_SERVER['HTTP_CLIENT_IP'];
        } // IP derrière un proxy
        elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
        } // Sinon : IP normale
        else {
            return (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '');
        }
    }

    private function getMatches($mDate, $leagueCode)
    {
        $confDate = '00:30:00';
        $date = new \DateTime();
        $now=$date->format('Y-m-d');
        $date->setdate($mDate['Year'], $mDate['Month'], $mDate['Day']);
        $day=$date->format('Y-m-d');

       if($day==$now){
           $time = $date->format('Y-m-d H:i:s');
       }else{
           $time=$day;
       }

        $availableMatches = $this->get('OulalaiFrame.repository.matches')->getFromDate($time, $confDate, $leagueCode);

        $availableMatchesFixtures = array();
        foreach ($availableMatches as $key => $value) {
            $availableMatchesFixtures[$value['league_dates']][$key] = $value;
        }
        $matches = array();
        $days = array();
        $leagues = array();

        foreach ($availableMatchesFixtures as $matcheDate) {
            foreach ($matcheDate as $match) {
                $days[] = $match['matchday'];
                $matches[] = $match['match_id'];
                $leagues[] = $match['league_id'];
            }
        }

        $this->sDays = array_unique($days);
        $this->sMatches = !empty($matches)?array_unique($matches):0;
        $this->sSaveLeagues = array_unique($leagues);

        if (!empty($leagues) && !empty($this->sMatches)) {
            $this->sLeagues = $this->get('OulalaiFrame.repository.leagues')->getLeaguesByRealLeagueIDs(
                implode(',', $leagues)
            );
            $this->aLeagueTeams = $this->get('OulalaiFrame.repository.teams')->getTeamsByMatchesID(
                implode(',', $this->sMatches)
            );
        }

        return $this->sMatches;
    }

    public function loadAvailableMatcesAction(Request $request)
    {
        $confDate = '00:30:00';
        $date = new \DateTime();
        $time = $date->format('H:i:s');

        if (is_array($request->request->get('date'))) {
            $availableMatches = array();
            foreach ($request->request->get('date') as $sDate) {
                $availableMatche = $this->get('OulalaiFrame.repository.matches')->getFromDate(
                    $sDate.' '.$time,
                    $confDate
                );
                $availableMatches = array_merge($availableMatches, $availableMatche);
            }
        } else {
            $availableMatches = $this->get('OulalaiFrame.repository.matches')->getFromDate(
                $request->request->get('date').' '.$time,
                $confDate
            );
        }

        $availableMatchesFixtures = array();
        foreach ($availableMatches as $key => $value) {
            $availableMatchesFixtures[$value['league_dates']][$key] = $value;
        }
        $matches = array();
        $days = array();
        $leagues = array();
        foreach ($availableMatchesFixtures as $matcheDate) {
            foreach ($matcheDate as $match) {
                $days[] = $match['matchday'];
                $matches[] = $match['match_id'];
                $leagues[] = $match['league_id'];
            }
        }

        $this->sDays = array_unique($days);
        $this->sMatches = array_unique($matches);
        $this->sSaveLeagues = array_unique($leagues);
//        return new Response(json_encode(['matches'=>implode(',', $this->sMatches),'days'=>implode(',',$this->sDays)]));
        if (!empty($leagues) && !empty($this->sMatches)) {
            $this->sLeagues = $this->get('OulalaiFrame.repository.leagues')->getLeaguesByRealLeagueIDs(
                implode(',', $leagues)
            );
            $this->aLeagueTeams = $this->get('OulalaiFrame.repository.teams')->getTeamsByMatchesID(
                implode(',', $this->sMatches)
            );
        }
        $valueFrom = $request->request->get('valuefrom') ? $request->request->get('valuefrom') : 0;
        $valueTo = $request->request->get('valueto') ? $request->request->get('valueto') : 45;
        $inbudget = $request->request->get('inbudget');
        $iBudgetleft = $request->request->get('budgetleft');
        $orderBy = $request->request->get('sort');
        $iTeamID = $request->request->get('team');
        $iRealLeagueID = $request->request->get('league');
        $iPosition = $request->request->get('position');
        $sPlayerName = $request->request->get('field');
        $iPage = $request->request->get('page');
        $country_leagues = $this->get('OulalaiFrame.repository.leagues')->getCountryLeague('0');
        $positions = $this->get('OulalaiFrame.repository.players')->getAllPlayerPositions();
        $sOrdering = '';
        if (!empty($orderBy)) {
            $sort = explode("-", $orderBy);
            $orderBy = empty($sort[0]) ? "p.`value`" : $sort[0];
            $order = empty($sort[1]) ? "ASC" : $sort[1];
            $sOrdering = $orderBy." ".$order;
        }

        $budgetleft = $this->getBudgetLeft();

        if (!empty($inbudget)) {
            $valueTo = $iBudgetleft;
        }

        $this->enablePaging($iPage);
        $limit = $this->iLimit;
        $offset = $this->iStart;
        $league_player = $this->get('OulalaiFrame.repository.leagues')->getPlayersByMatches(
            $iRealLeagueID,
            implode(',', $this->sMatches),
            implode(',', $this->sDays),
            $iTeamID,
            $iPosition,
            $sOrdering,
            $offset,
            $limit,
            $valueFrom,
            $valueTo,
            $sPlayerName
        );
        $aAllPlayers = $this->get('OulalaiFrame.repository.leagues')->getPlayersByMatches(
            $iRealLeagueID,
            implode(',', $this->sMatches),
            implode(',', $this->sDays),
            $iTeamID,
            $iPosition,
            $sOrdering,
            0,
            '',
            $valueFrom,
            $valueTo,
            $sPlayerName
        );
        $iTotalRows = count($aAllPlayers);
        $pagination = $this->assignPagination($iTotalRows, $iPage);

        return $this->render(
            'templates/homepage/players-block.html.twig',
            array(
                "availableMatches" => $availableMatchesFixtures,
                'leagues' => $this->sLeagues,
                'teams' => $this->aLeagueTeams,
                'countries' => $country_leagues,
                'leagueId' => $this->leagueId,
                'formationList' => $this->formationList,
                'playervalue' => 45,
                'budget' => $this->aBudget,
                'positions' => $positions,
                'teamid' => $this->iMemberTeamID,
                'isSaved' => 1,
                'matches' => implode(',', $this->sMatches),
                'days' => implode(',', $this->sDays),
                'fieldFormation' => $this->aFormationSlots,
                'formation' => $this->aSlots,
                'playerslist' => $this->aPlayers,
                'players' => $league_player,
                'valid_team' => $this->bIsValid,
                'showJoinButton' => false,
                'budgetleft' => $this->getBudgetLeft(),
                'page_offset' => $iPage,
                'pagination' => $pagination,
            )
        );
    }

    private function setMatchesDays($request)
    {
        $session = new Session();
        $this->sRef = $session->get('user_ref');
        $matches = $request->request->get('matches');
        $matches = trim($matches, ",");
        $matches = implode(',', array_unique(explode(',', $matches)));

        $days = $request->request->get('days');
        $days = trim($days, ",");
        $days = implode(',', array_unique(explode(',', $days)));

        $leagues = $request->request->get('league');
        $leagues = trim($leagues, ",");
        $leagues = implode(',', array_unique(explode(',', $leagues)));

        $this->sDays = $days;
        $this->sMatches = $matches;
        $this->sSaveLeagues = $leagues;

        if (!empty($leagues) && !empty($this->sMatches)) {
            $this->sLeagues = $this->get('OulalaiFrame.repository.leagues')->getLeaguesByRealLeagueIDs($leagues);
            $this->aLeagueTeams = $this->get('OulalaiFrame.repository.teams')->getTeamsByMatchesID($this->sMatches);
        } elseif (empty($this->sMatches) && empty($this->sDays) && empty($leagues)) {

            $aTeamDetails = $this->get('OulalaiFrame.repository.savedsquad')->loadTeamInfo(
                $this->sRef,
                $this->iMemberTeamID
            );
            $this->sLeagues = $this->get('OulalaiFrame.repository.leagues')->getLeaguesByRealLeagueIDs(
                $aTeamDetails['team_leagues']
            );
            $this->aLeagueTeams = $this->get('OulalaiFrame.repository.teams')->getTeamsByMatchesID(
                $aTeamDetails['team_matches']
            );

            $this->sSaveLeagues = $aTeamDetails['team_leagues'];
            $this->sDays = $aTeamDetails['team_matches_days'];
            $this->sMatches = $aTeamDetails['team_matches'];

        }
    }

    public function searchPlayersBuildTeamAction()
    {
        $request = Request::createFromGlobals();
        if ($request->isXmlHttpRequest()) {
            $valueFrom = $request->request->get('valuefrom');
            $valueTo = $request->request->get('valueto');
            $inbudget = $request->request->get('inbudget');
            $iBudgetleft = $request->request->get('budgetleft');
            $orderBy = $request->request->get('sort');
            $iTeamID = $request->request->get('team');
            $iRealLeagueID = $request->request->get('league');
            $iPosition = $request->request->get('position');
            $sPlayerName = $request->request->get('field');
            $iPage = $request->request->get('page');

            $this->setMatchesDays($request);
            $sOrdering = '';
            if (!empty($orderBy)) {
                $sort = explode("-", $orderBy);
                $orderBy = empty($sort[0]) ? "p.`value`" : $sort[0];
                $order = empty($sort[1]) ? "ASC" : $sort[1];
                $sOrdering = $orderBy." ".$order;
            }

            $budgetleft = $this->getBudgetLeft();
            //echo $budgetleft;
            if (!empty($inbudget)) {
                $valueTo = $iBudgetleft;
            }
            $this->enablePaging($iPage);
            $limit = $this->iLimit;
            $offset = $this->iStart;
            if ($iRealLeagueID && !$iTeamID) {
                $this->sMatches = $this->get('OulalaiFrame.repository.matches')->getMatchByLeagueId(
                    $iRealLeagueID,
                    $this->sDays
                );
                $iTeamID = $this->get('OulalaiFrame.repository.matches')->getTeamByMatch($this->sMatches);
                $iRealLeagueID = null;
            }
//            print_r($iTeamID);die;
            $league_player = $this->get('OulalaiFrame.repository.leagues')->getPlayersByMatches(
                $iRealLeagueID,
                $this->sMatches,
                $this->sDays,
                $iTeamID,
                $iPosition,
                $sOrdering,
                $offset,
                $limit,
                $valueFrom,
                $valueTo,
                $sPlayerName
            );
            $aAllPlayers = $this->get('OulalaiFrame.repository.leagues')->getPlayersByMatches(
                $iRealLeagueID,
                $this->sMatches,
                $this->sDays,
                $iTeamID,
                $iPosition,
                $sOrdering,
                0,
                '',
                $valueFrom,
                $valueTo,
                $sPlayerName
            );
            $iTotalRows = count($aAllPlayers);
            $pagination = $this->assignPagination($iTotalRows, $iPage);

            $result = array(
                'players' => $league_player,
                'page_offset' => $iPage,
                'pagination' => $pagination,
                'isSaved' => 1,
            );

            return $this->render('templates/homepage/search-players-result-home.html.twig', $result);
        }
    }

    private function getAllMySavedTeam($bShowButtons = true)
    {
        $allTeams = array();
        $session = new Session();
        $user_ref = $session->get('user_ref');

        $saved_teams = $this->get('OulalaiFrame.repository.savedsquad')->searchMySavedTeams(
            $user_ref,
            $this->iMemberTeamID
        );

        if (!empty($saved_teams)) {
            $index = 0;
            foreach ($saved_teams as $key => $team) {
                $allTeams[$index] = $team;
                if ($bShowButtons) {
                    $allTeams[$index]['edit_url'] = $this->get('router')->generate(
                        'saved_teams_edit',
                        array('iCurrentTeamId' => $team['member_draftteam_id'])
                    );
                    $allTeams[$index]['join_url'] = $this->get('router')->generate(
                        'join-with-saved-team',
                        array('iTeamId' => $team['member_draftteam_id'])
                    );
                    $allTeams[$index]['join_new_league_url'] = $this->get('router')->generate(
                        'create_leagues_with_saved_team',
                        array('iSavedTeamId' => (int)$team['member_draftteam_id'])
                    );
                }
                $allTeams[$index]['formation'] = $this->get('OulalaiFrame.repository.teams')->getFormationPositions(
                    $team['fk_formation_id']
                );
                foreach ($allTeams[$index]['formation'] as $key => $formation) {
                    $allTeams[$index]['formation'][$key]['players'] = $this->get(
                        'OulalaiFrame.repository.savedsquad'
                    )->getDraftTeamPlayers($team['member_draftteam_id'], $formation['player_position_id']);
                }
                $allTeams[$index]['subs'] = $this->get('OulalaiFrame.repository.savedsquad')->getDraftTeamPlayers(
                    $team['member_draftteam_id'],
                    "sub"
                );
                $index++;
            }
        }

        return $allTeams;
    }

    public function joinWithSavedTeamAction($iTeamId)
    {
        $request = Request::createFromGlobals();
        $session = new Session();
        $this->iMemberTeamID = $iTeamId;
        $clientId = $session->get('client');
        $style = $this->get('OulalaiFrame.repository.config')->getUserStyle($clientId);

        $css_version = $this->getParameter('css_version');
        $js_version = $this->getParameter('js_version');

        $menu = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('show_menu', $clientId);
        $minCost = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('min_join_cost', $clientId);
        $sOrdering = "ORDER BY fromDate ASC";
        $sSorting = "";
        $sWhere = "WHERE lm.`match_id`=m.`match_id`";
        $leaguesList = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('client_leagues', $clientId);
        $availableLeagues = $this->get('OulalaiFrame.repository.leagues')->availableLeagues(
            $sSorting,
            $sWhere,
            $sOrdering,
            $minCost,
            $leaguesList
        );
        $leagues = $this->get('OulalaiFrame.repository.leagues')->getCountryLeague($leaguesList);

        if ($request->isXmlHttpRequest()) {
            $action = $request->request->get('action');

            switch ($action) {
                case "searchAvailableLeagues":
                    $league_ids = $request->request->get('league');
                    $orderBy = $request->request->get('sort');
                    $entryFee_from = $request->request->get('entryFee_from');
                    $entryFee_to = $request->request->get('entryFee_to');
                    $sOrdering = "";
                    $sSorting = "";
                    $sWhere = "WHERE lm.`match_id`=m.`match_id`";

                    if (!empty($orderBy) || !empty($mobileFilterByType)) {
                        if (!empty($orderBy)) {
                            $sort = explode("-", $orderBy);
                        }
                        if (!empty($mobileFilterByType)) {
                            //$sort = explode("-", $mobileFilterByType);
                        }

                        $orderBy = empty($sort[0]) ? "timeToStart" : $sort[0];
                        $order = empty($sort[1]) ? "ASC" : $sort[1];
                        $sOrdering = " ORDER BY ".$orderBy." ".$order;
                    }
                    if (!empty($entryFee_from) && !empty($entryFee_to)) {
                        $sWhere .= " AND leagues.entryFee BETWEEN {$entryFee_from} AND {$entryFee_to} ";
                    }
                    if (!empty($league_ids)) {
                        $list = implode(',', $league_ids);
                        $sWhere .= ' AND m.`league_id` IN ('.$list.')';
                    }

                    $sSorting .= $sOrdering.' LIMIT 0, 20';
                    $availableLeagues = $this->get('OulalaiFrame.repository.leagues')->availableLeagues(
                        $sSorting,
                        $sWhere,
                        '',
                        $minCost,
                        $leaguesList
                    );

                    return $this->render(
                        'templates/controls/leagues-available-search-saved-teams.html.twig',
                        array(
                            'teamid' => $this->iMemberTeamID,
                            'available_leagues' => $availableLeagues,
                            'serverurl' => $this->get('router')->generate(
                                'join-with-saved-team',
                                array('iTeamId' => $this->iMemberTeamID)
                            ),
                        )
                    );
                    break;
                case "openDialogJoinLeagueWithSavedTeam":
                    $teamid = $request->request->get('teamid');
                    $totalfee = $request->request->get('totalfee');
                    $totalcount = $request->request->get('totalcount');
                    $selectedleagues = $request->request->get('selectedleagues');
                    $teamname = "";
                    if (!empty($teamid)) {
                        $teamname = $this->get('OulalaiFrame.repository.savedsquad')->getDraftTeamDetailsById(
                            $teamid
                        )['comment'];
                    }

                    return $this->render(
                        'templates/controls/dialog-confirm-join-league-with-saved-team.html.twig',
                        array(
                            'savedteamname' => $teamname,
                            'teamid' => $this->iMemberTeamID,
                            'leagueentryfee' => $totalfee,
                            'memberleagueids' => $selectedleagues,
                            'selectedleaguecount' => $totalcount,
                            'serverurl' => $this->get('router')->generate('savedsquad_joinLeague'),
                        )
                    );
                    break;
            }
        }
        $teamname = "";
        if (!empty($this->iMemberTeamID)) {
            $teamname = $this->get('OulalaiFrame.repository.savedsquad')->getDraftTeamDetailsById(
                $this->iMemberTeamID
            )['comment'];
        }

        return $this->render(
            'templates/leagues-available-search-for-saved-teams.html.twig',
            array(
                'leagues' => $leagues,
                'teamname' => $teamname,
                'teamid' => $this->iMemberTeamID,
                'available_leagues' => $availableLeagues,
                'style' => $style.$css_version,
                'css_version' => $css_version,
                'js_version' => $js_version,
                'menu' => $menu,
                'serverurl' => $this->get('router')->generate(
                    'join-with-saved-team',
                    array('iTeamId' => $this->iMemberTeamID)
                ),
            )
        );
    }


    public function saveTeamAction()
    {
        $request = Request::createFromGlobals();
        $iCurrentTeamID = $request->request->get('currentteamid');
        $bIsLoaded = $request->request->get('loadingteam');
        $aJoinedLeagues = $request->request->get('joinedLeagues');
        $teamName = $request->request->get('teamName');
        $this->setMatchesDays($request);

        $session = new Session();
        $clientId = $session->get('client');

        if ($teamName) {
            $this->sTeamName = $teamName;
        } else {
            $this->sTeamName = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName(
                    'client_name',
                    $clientId
                ).' FC';
        }
        $bSaved = $this->get('OulalaiFrame.repository.savedsquad')->updateDraftTeamData(
            $this->iMemberTeamID,
            $this->aSlots,
            $this->sTeamName,
            $this->sMatches,
            $this->sDays,
            $this->sSaveLeagues
        );
        if (!$bSaved) {
            return new Response('Error');
        }
        if ($session->has('user_ref')) {
            $url = $this->get('router')->generate('saved_teams');
        } else {
            $response = new Response();
            $cookie = $response->headers->setCookie(new Cookie('teamId', $this->iMemberTeamID));
            $response->send();
            $url = $this->get('router')->generate('saved_teams');
        }

        return new Response($url);

    }

    /**
     * This method first checks whether the team is not breaking any rules. The team is then saved to the database.
     *
     * @return bool
     */
    private function saveEditedTeam($aJoinedLeagues = null)
    {
        $query = $this->saveToDB();
        if ($query) {
            $session = new Session();
            $clientId = $session->get('client');
            $this->sTeamName = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName(
                    'client_name',
                    $clientId
                ).' FC';

            $this->get('OulalaiFrame.repository.savedsquad')->updateDraftTeamData(
                $this->iMemberTeamID,
                $this->aSlots,
                $this->sTeamName,
                $this->sMatches,
                $this->sDays,
                $this->sSaveLeagues
            );
            $this->get('OulalaiFrame.repository.savedsquad')->deleteDraftTeamPlayers($this->iMemberTeamID);

            if (!empty($aJoinedLeagues)) {
                $this->editJoinedTeamsWithSavedTeams($aJoinedLeagues);
            }

            return $query;
        }
    }

    public function loadSavedTeamAction()
    {
        $request = Request::createFromGlobals();

        $bIsLoaded = $request->request->get('loadingteam');
        $currentTeamId = $request->request->get('currentteamid');
        $loadingTeamID = $request->request->get('loadteamid');
        $teamId = $request->request->get('teamid');
        $positions = $this->get('OulalaiFrame.repository.players')->getAllPlayerPositions();
        $this->formationList = $this->get('OulalaiFrame.repository.teams')->getFormation();
        $this->aFormationSlots = $this->get('OulalaiFrame.repository.teams')->assignFormationDetails($this->aSlots);
        $this->setMatchesDays($request);

        if (!is_null($currentTeamId) && $currentTeamId != "" && $currentTeamId != "saveloaded" && $bIsLoaded) {
            if (!empty($loadingTeamID)) {
                $players = $this->get('OulalaiFrame.repository.savedsquad')->getNotDraftPlayers($loadingTeamID);
            }

            if ($currentTeamId == 0 || !$currentTeamId) {
                $currentTeamId = $teamId;
            } else {
                $changed = $this->get('OulalaiFrame.repository.savedsquad')->changePlayersDraftStatus(
                    $currentTeamId,
                    1
                );

                if ($changed) {
                    foreach ($players as $aPlayer):
                        $this->putIntoTeamPlayers(
                            $aPlayer['fk_player_id'],
                            $currentTeamId,
                            $aPlayer['starting_11'],
                            $aPlayer['fk_player_position_id']
                        );
                    endforeach;
                    $this->assignPlayers($currentTeamId);
                }
            }
        } else {
            if ($currentTeamId == "saveloaded") {
                $bIsLoaded = true;
            }

            $teamId = $this->iMemberTeamID;

            $this->assignPlayers($loadingTeamID);
        }

        $result = array(
            'leagueId' => $this->leagueId,
            'formationList' => $this->formationList,
            'playervalue' => 45,
            'budget' => $this->aBudget,
            'positions' => $positions,
            'teamid' => $teamId,
            'currentteamid' => $currentTeamId,
            'isSaved' => 1,
            'matches' => $this->sMatches,
            'days' => $this->sDays,
            'loadingteam' => ($bIsLoaded ? true : false),
            'fieldFormation' => $this->aFormationSlots,
            'formation' => $this->aSlots,
            'memberteamschedule' => array(),
            'playerslist' => $this->aPlayers,
            'players' => array(),
            'valid_team' => $this->bIsValid,
            'showJoinButton' => false,
            'budgetleft' => $this->getBudgetLeft(),
        );

        return $this->render('templates/homepage/teambuilder-pitch.html.twig', $result);
    }

    public function chooseLeaguesTeamToChangeAction()
    {
        $request = Request::createFromGlobals();
        if ($request->isXmlHttpRequest()) {
            $session = new Session();
            $clientId = $session->get('client');
            $loadingteam = $request->request->get('loadingteam');
            $minCost = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('min_join_cost', $clientId);
            $sOrdering = "ORDER BY fromDate ASC";
            $sSorting = "";
            $sWhere = "WHERE lm.`match_id`=m.`match_id`";
            $leaguesList = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName(
                'client_leagues',
                $clientId
            );
            $leagues = $this->get('OulalaiFrame.repository.leagues')->availableLeagues(
                $sSorting,
                $sWhere,
                $sOrdering,
                $minCost,
                $leaguesList
            );
            $this->formationList = $this->get('OulalaiFrame.repository.teams')->getFormation();
            $this->aFormationSlots = $this->get('OulalaiFrame.repository.teams')->assignFormationDetails($this->aSlots);

            $result = array(
                'leagues' => $leagues,
                'leagueId' => $this->leagueId,
                'formationList' => $this->formationList,
                'playervalue' => 45,
                'budget' => $this->aBudget,
                'teamid' => $this->iMemberTeamID,
                'loadingteam' => ($loadingteam ? 1 : 0),
                'fieldFormation' => $this->aFormationSlots,
                'formation' => $this->aSlots,
                'playerslist' => $this->aPlayers,
                'isSaved' => 1,
                'valid_team' => $this->bIsValid,
                'budgetleft' => $this->getBudgetLeft(),
            );

            return $this->render('templates/controls/dialog-choose-leagues-to-edit-team.html.twig', $result);
        }
    }

    public function saveTeamConfirmAction()
    {
        $request = Request::createFromGlobals();
        if ($request->isXmlHttpRequest()) {
            $loadingteam = $request->request->get('loadingteam');
            $this->formationList = $this->get('OulalaiFrame.repository.teams')->getFormation();
            $this->aFormationSlots = $this->get('OulalaiFrame.repository.teams')->assignFormationDetails($this->aSlots);
            $session = new Session();
            $clientId = $session->get('client');

            $loginUrl = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('login_url', $clientId);
            $result = array(
                'leagueId' => $this->leagueId,
                'formationList' => $this->formationList,
                'playervalue' => 45,
                'budget' => $this->aBudget,
                'teamid' => $this->iMemberTeamID,
                'loadingteam' => ($loadingteam ? 1 : 0),
                'fieldFormation' => $this->aFormationSlots,
                'formation' => $this->aSlots,
                'playerslist' => $this->aPlayers,
                'isSaved' => 1,
                'valid_team' => $this->bIsValid,
                'budgetleft' => $this->getBudgetLeft(),
                'login_url' => $loginUrl,
            );

            return $this->render('templates/homepage/dialog-confirm-save-team.html.twig', $result);
        }
    }

    public function joinLeagueConfirmAction()
    {
        $request = Request::createFromGlobals();
        if ($request->isXmlHttpRequest()) {
            $name = $request->request->get('name');
            $loadingteam = $request->request->get('loadingteam');
            $aLeagueDetails = $this->get('OulalaiFrame.repository.leagues')->getMemberLeagueDetailsByID(
                $this->leagueId
            );

            $result = array(
                'leagueId' => $this->leagueId,
                'teamid' => $this->iMemberTeamID,
                'formation' => $this->aSlots,
                'isSaved' => 1,
                'loadingteam' => ($loadingteam ? 1 : 0),
                'league' => $aLeagueDetails,
            );

            return $this->render('templates/controls/dialog-confirm-join-league.html.twig', $result);
        }
    }

    public function setFormationAction()
    {
        $request = Request::createFromGlobals();
        if ($request->isXmlHttpRequest()) {
            $bIsSquadeEdit = false;
            $currentteamid = $request->request->get('currentteamid');
            $loadingteam = $request->request->get('loadingteam');

            if ($currentteamid) {
                $bIsSquadeEdit = true;
                $this->get('OulalaiFrame.repository.savedsquad')->updateDraftTeamFromation(
                    $this->aSlots,
                    $currentteamid
                );
            }

            $this->chackPlayersInFormation($this->aSlots, $this->iMemberTeamID, $bIsSquadeEdit, true);
            $this->formationList = $this->get('OulalaiFrame.repository.teams')->getFormation();
            $this->aFormationSlots = $this->get('OulalaiFrame.repository.teams')->assignFormationDetails($this->aSlots);

            $result = array(
                'leagueId' => $this->leagueId,
                'formationList' => $this->formationList,
                'playervalue' => 45,
                'budget' => $this->aBudget,
                'teamid' => $this->iMemberTeamID,
                'loadingteam' => ($loadingteam ? 1 : 0),
                'fieldFormation' => $this->aFormationSlots,
                'formation' => $this->aSlots,
                'playerslist' => $this->aPlayers,
                'isSaved' => 1,
                'valid_team' => $this->bIsValid,
                'budgetleft' => $this->getBudgetLeft(),
            );

            return $this->render('templates/homepage/teambuilder-pitch.html.twig', $result);
        }
    }

    public function memberScheduleAction()
    {
        $request = Request::createFromGlobals();
        if ($request->isXmlHttpRequest()) {
            $currentteamid = $request->request->get('teamid');
            $positions = $this->get('OulalaiFrame.repository.players')->getAllPlayerPositions();

            $bIsSquadeEdit = false;
            if ($currentteamid) {
                $bIsSquadeEdit = true;
            }

            $this->assignPlayers($currentteamid, $bIsSquadeEdit);
            $memberteamschedule = $this->getSubstitutionGuide(true, $this->leagueId);

            $result = array(
                'memberteamschedule' => $memberteamschedule,
                'positions' => $positions,
            );

            return $this->render('templates/homepage/memberteam-schedule.html.twig', $result);
        }
    }

    private function getSubstitutionGuide($bUseFillers = true, $iMemberLeagueID)
    {
        $aPlayerTimes = $this->getPlayersWithTimes($iMemberLeagueID);
        if (!is_array($aPlayerTimes) || count($aPlayerTimes) < 1) {
            return null;
        }

        $aPlayerTimes = $this->groupPlayersByMatchDatetime($aPlayerTimes);

        $aOutput = array(
            'days' => array(),
            'times' => array(),
        );
        $aSubstitueFlags = array();

        // Get all the positions
        $aPositions = $this->get('OulalaiFrame.repository.players')->getAllPlayerPositions();
        $aPositionIndex = array();

        foreach ($aPositions as $iKey => $aValue) {
            $aPositionIndex[$aValue['player_position_id']] = $iKey;
            $aOutput['position'.$iKey] = array();
            $aSubstitueFlags['position'.$iKey] = false;
        }

        // Process each day
        foreach ($aPlayerTimes as $iDayStamp => $aDay) {
            $aOutput['days'][$iDayStamp] = array(
                'value' => date('l, M j', $iDayStamp),
                'day_colspan' => count($aDay),
            );

            // Process time of the day
            $iTotalPlayerPositionsInGroup = 0;
            $iPointer = 1;

            foreach ($aDay as $iTimestamp => $aTimeGroup) {
                $aMarkers = array();
                if ($iPointer == 1) {
                    $aMarkers['first'] = 'first';
                }
                if ($iPointer == count($aDay)) {
                    $aMarkers['last'] = 'last';
                }
                if ($iPointer != count($aDay)) {
                    $aTimeGroupKeys = array_keys($aDay);
                    $aCurrentKey = array_search($iTimestamp, $aTimeGroupKeys);
                    $iNextTimestamp = $aTimeGroupKeys[($aCurrentKey + 1)];

                    $aMarkers['hour-diff'] = 'hour-diff-'.round(($iNextTimestamp - $iTimestamp) / 3600);
                }

                $aOutput['times'][$iTimestamp] = array(
                    'value' => date('G:i', $iTimestamp),
                    'column_type' => 'time',
                    'markers' => implode(' ', $aMarkers),
                );

                $aPlayerPositions = array();
                $aPlayerPositionsCount = array();
                foreach ($aPositions as $iKey => $aValue) {
                    $aPlayerPositions['position'.$iKey] = array();
                    $aPlayerPositionsCount['position'.$iKey] = 0;
                }

                foreach ($aTimeGroup as $iPlayerKey => $aPlayer) {
                    $aPlayerMarkers = array();
                    $sPosition = 'position'.$aPositionIndex[$aPlayer['positionId']];
                    $aPlayerPositionsCount[$sPosition]++;
                    $aPlayerPositions[$sPosition][$iPlayerKey] = $aPlayer;
                    $aPlayerPositions[$sPosition][$iPlayerKey]['column_type'] = 'player';

                    if ($aPlayer['starting_11'] == 0) {
                        $aSubstitueFlags[$sPosition] = true;
                        $aPlayerMarkers[] = 'substitute';
                    } elseif ($aSubstitueFlags[$sPosition] == true) {
                        $aPlayerMarkers[] = 'plays-after-substitute';
                    }

                    $aPlayerPositions[$sPosition][$iPlayerKey]['markers'] = implode(' ', $aPlayerMarkers);
                }
                $iPlayerPositions = max($aPlayerPositionsCount);
                $iTotalPlayerPositionsInGroup += $iPlayerPositions;

                // Pad each group so that the amount of player columns is the same in each group
                foreach ($aPositions as $iKey => $aValue) {
                    $aPlayerPositions['position'.$iKey] = array_pad(
                        $aPlayerPositions['position'.$iKey],
                        $iPlayerPositions,
                        array('column_type' => 'empty_player')
                    );
                }

                // Add fillers at the end of the time group
                if ($bUseFillers) {
                    $aFillerMarkers = $aMarkers;
                    unset($aFillerMarkers['first']);

                    $aOutput['times'][] = array('column_type' => 'filler', 'markers' => implode(' ', $aFillerMarkers));

                    foreach ($aPositions as $iKey => $aValue) {
                        $aPlayerPositions['position'.$iKey][] = array(
                            'column_type' => 'filler',
                            'markers' => implode(' ', $aFillerMarkers),
                        );
                    }
                }

                // Set how many columns the time group spans
                $aOutput['times'][$iTimestamp]['colspan'] = $iPlayerPositions;

                // Merge time groups
                foreach ($aPositions as $iKey => $aValue) {
                    $aOutput['position'.$iKey] = array_merge(
                        $aOutput['position'.$iKey],
                        $aPlayerPositions['position'.$iKey]
                    );
                }

                $iPointer++;
            }

            // Set how many columns the day spans
            if ($bUseFillers) {
                $aOutput['days'][$iDayStamp]['day_colspan'] = (count($aDay) * 2);
                $aOutput['days'][$iDayStamp]['total_colspan'] = $iTotalPlayerPositionsInGroup + count($aDay);
            } else {
                $aOutput['days'][$iDayStamp]['day_colspan'] = count($aDay);
                $aOutput['days'][$iDayStamp]['total_colspan'] = $iTotalPlayerPositionsInGroup;
            }
        }

        return $aOutput;
    }

    private function groupPlayersByMatchDatetime($aPlayerTimes)
    {
        $aOutput = array();

        foreach ($aPlayerTimes as $iPlayerId => $aPlayer) {
            if (!array_key_exists('match_date', $aPlayer)) {
                continue;
            }

            $iTimestamp = strtotime($aPlayer['match_date']);
            $iDay = mktime(0, 0, 0, date('m', $iTimestamp), date('d', $iTimestamp), date('Y', $iTimestamp));
            $iTime = mktime(
                date('H', $iTimestamp),
                date('i', $iTimestamp),
                0,
                date('m', $iTimestamp),
                date('d', $iTimestamp),
                date('Y', $iTimestamp)
            );

            if (!array_key_exists($iDay, $aOutput)) {
                $aOutput[$iDay] = array();
                ksort($aOutput);
            }

            if (!array_key_exists($iTimestamp, $aOutput[$iDay])) {
                $aOutput[$iDay][$iTime] = array();
                ksort($aOutput[$iDay]);
            }

            $aOutput[$iDay][$iTime][$iPlayerId] = $aPlayer;
        }

        return $aOutput;
    }

    private function getPlayersWithTimes($iMemberLeagueID)
    {

        if (!is_array($this->aSchedulePlayers) || count($this->aSchedulePlayers) < 1) {
            return null;
        }

        $aResult = $this->get('OulalaiFrame.repository.squad')->getPlayersWithTimes(
            $iMemberLeagueID,
            $this->aSchedulePlayers
        );

        //$this->assignPlayers($iTeamID);
        // Add players' match dates
        $aOutput = $this->aSchedulePlayers;
        foreach ($aResult as $aValue) {
            $aOutput[$aValue['player_id']]['match_date'] = $aValue['match_date'];
        }

        // Sort players so that the substitutes come last
        $aSortFlags = array();
        foreach ($this->aSchedulePlayers as $iPlayerKey => $aPlayer) {
            $aSortFlags[$iPlayerKey] = $aPlayer['starting_11'];
        }

        array_multisort($aSortFlags, SORT_DESC, $aOutput);

        return $aOutput;
    }
    public function clearTeamAction()
    {
        $request = Request::createFromGlobals();
        if ($request->isXmlHttpRequest()) {
            $bIsSquadeEdit = false;
            $currentteamid = $request->request->get('teamid');
            $loadingteam = $request->request->get('loadingteam');

            if ($currentteamid) {
                $bIsSquadeEdit = true;
            }

            $this->get('OulalaiFrame.repository.savedsquad')->clearTeamQuery($currentteamid, $bIsSquadeEdit);
            $this->formationList = $this->get('OulalaiFrame.repository.teams')->getFormation();
            $this->aFormationSlots = $this->get('OulalaiFrame.repository.teams')->assignFormationDetails($this->aSlots);
            $this->assignPlayers($currentteamid, $bIsSquadeEdit);

            $result = array(
                'leagueId' => $this->leagueId,
                'formationList' => $this->formationList,
                'playervalue' => 45,
                'budget' => $this->aBudget,
                'teamid' => $currentteamid,
                'loadingteam' => ($loadingteam ? 1 : 0),
                'fieldFormation' => $this->aFormationSlots,
                'formation' => $this->aSlots,
                'playerslist' => $this->aPlayers,
                'valid_team' => $this->bIsValid,
                'budgetleft' => $this->getBudgetLeft(),
                'isSaved' => 1,
            );

            return $this->render('templates/homepage/teambuilder-pitch.html.twig', $result);
        }
    }

    public function clearTeamConfirmAction()
    {
        $request = Request::createFromGlobals();
        if ($request->isXmlHttpRequest()) {
            $currentteamid = $request->request->get('currentteamid');

            $result = array(
                'leagueId' => $this->leagueId,
                'teamid' => $this->iMemberTeamID,
                'isSaved' => 1,
                'currentteamid' => $currentteamid,
            );

            return $this->render('templates/controls/dialog-confirm-clear-team.html.twig', $result);
        }
    }

    public function removePlayerAction()
    {
        $request = Request::createFromGlobals();
        if ($request->isXmlHttpRequest()) {
            $player_id = $request->request->get('playerid');
            $currentteamid = $request->request->get('currentteamid');
            $loadingteam = $request->request->get('loadingteam');

            $bIsSquadeEdit = false;
            if ($currentteamid) {
                $bIsSquadeEdit = true;
            }
            $this->get('OulalaiFrame.repository.savedsquad')->removePlayerQuery(
                $player_id,
                $this->iMemberTeamID,
                $bIsSquadeEdit
            );
            $this->formationList = $this->get('OulalaiFrame.repository.teams')->getFormation();
            $this->aFormationSlots = $this->get('OulalaiFrame.repository.teams')->assignFormationDetails($this->aSlots);
            $this->assignPlayers($this->iMemberTeamID, $bIsSquadeEdit);

            $result = array(
                'leagueId' => $this->leagueId,
                'formationList' => $this->formationList,
                'playervalue' => 45,
                'budget' => $this->aBudget,
                'teamid' => $this->iMemberTeamID,
                'loadingteam' => ($loadingteam ? 1 : 0),
                'fieldFormation' => $this->aFormationSlots,
                'formation' => $this->aSlots,
                'playerslist' => $this->aPlayers,
                'valid_team' => $this->bIsValid,
                'budgetleft' => $this->getBudgetLeft(),
                'isSaved' => 1,
            );

            return $this->render('templates/homepage/teambuilder-pitch.html.twig', $result);
        }
    }

    public function swapPlayerAction()
    {
        $request = Request::createFromGlobals();
        if ($request->isXmlHttpRequest()) {
            $starting = $request->request->get('starting');
            $player_id = $request->request->get('playerid');
            $player_position_id = $request->request->get('position_id');
            $sPositionTitle = $this->get('OulalaiFrame.repository.savedsquad')->getPositionById(
                intval($player_position_id)
            );
            $currentteamid = $request->request->get('currentteamid');
            $loadingteam = $request->request->get('loadingteam');

            $valid = true;
            $bIsSquadeEdit = ($currentteamid ? true : false);
            $swapLastPlayer = false;

            $this->chackPlayersInFormation($this->aSlots, $this->iMemberTeamID, $bIsSquadeEdit);
            if ($this->aValid[$sPositionTitle['comment']]) {
                $swapLastPlayer = false;
            } else {
                if ($starting) {
                    $valid = false;
                }
            }

            if ($starting == 0) {
                if (!$this->aValid['bench']) {
                    $valid = false;
                }
            } else {
                $swapLastPlayer = $player_position_id;
            }

            if ($valid) {
                $this->get('OulalaiFrame.repository.savedsquad')->swapPlayerQuery(
                    $player_id,
                    $this->iMemberTeamID,
                    $starting,
                    $bIsSquadeEdit,
                    $swapLastPlayer
                );
            }

            $this->formationList = $this->get('OulalaiFrame.repository.teams')->getFormation();
            $this->aFormationSlots = $this->get('OulalaiFrame.repository.teams')->assignFormationDetails($this->aSlots);
            $this->assignPlayers($this->iMemberTeamID, $bIsSquadeEdit);

            $result = array(
                'leagueId' => $this->leagueId,
                'formationList' => $this->formationList,
                'playervalue' => 45,
                'budget' => $this->aBudget,
                'teamid' => $this->iMemberTeamID,
                'loadingteam' => ($loadingteam ? 1 : 0),
                'fieldFormation' => $this->aFormationSlots,
                'formation' => $this->aSlots,
                'playerslist' => $this->aPlayers,
                'valid_team' => $this->bIsValid,
                'budgetleft' => $this->getBudgetLeft(),
                'isSaved' => 1,
            );

            return $this->render('templates/homepage/teambuilder-pitch.html.twig', $result);
        }
    }

    public function addPlayerAction()
    {
        $request = Request::createFromGlobals();
        $session = new Session();
        $this->sRef = $session->get('user_ref');

        if ($request->isXmlHttpRequest()) {
            $starting = $request->request->get('starting');
            $player_id = $request->request->get('playerid');
            $currentteamid = $request->request->get('currentteamid');
            $player_position_id = $request->request->get('position_id');
            $leagueId = $request->request->get('leagueId');
            $loadingteam = $request->request->get('loadingteam');
            $teamName = $request->request->get('teamName');

            $iPlayerTeamId = 0;
            if (!empty($player_id)) {
                $iPlayerTeamId = $this->get('OulalaiFrame.repository.players')->getPlayerProfileDetails(
                    $player_id
                )['team_id'];
            }
            $sPositionTitle = $this->get('OulalaiFrame.repository.savedsquad')->getPositionById(
                intval($player_position_id)
            );
            $bIsSquadeEdit = ($currentteamid ? true : false);

            $clientId = $session->get('client');

            if ($teamName) {
                $this->sTeamName = $teamName;
            } else {
                $this->sTeamName = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName(
                        'client_name',
                        $clientId
                    ).' FC';
            }
            $this->get('OulalaiFrame.repository.savedsquad')->setCurrentDraftTeam(
                $this->iMemberTeamID,
                $this->sRef,
                $this->aSlots,
                $this->sTeamName,
                $this->bIsValid,
                $this->sMatches,
                $this->sDays,
                $this->sSaveLeagues
            );
            $this->chackPlayersInFormation($this->aSlots, $this->iMemberTeamID, $bIsSquadeEdit);
            $this->assignPlayers($this->iMemberTeamID, $bIsSquadeEdit);

            if ($this->aValid[$sPositionTitle['comment']]) {
                $this->putIntoDraftTeamPlayers(
                    $player_id,
                    $this->iMemberTeamID,
                    $starting,
                    $player_position_id,
                    $iPlayerTeamId
                );
            } else {
                if ($this->aValid['bench']) {
                    $this->putIntoDraftTeamPlayers(
                        $player_id,
                        $this->iMemberTeamID,
                        0,
                        $player_position_id,
                        $iPlayerTeamId
                    );
                }
            }
            $this->formationList = $this->get('OulalaiFrame.repository.teams')->getFormation();
            $this->aFormationSlots = $this->get('OulalaiFrame.repository.teams')->assignFormationDetails($this->aSlots);
            $this->assignPlayers($this->iMemberTeamID, $bIsSquadeEdit);

            $result = array(
                'leagueId' => $this->leagueId,
                'formationList' => $this->formationList,
                'playervalue' => 45,
                'budget' => $this->aBudget,
                'teamid' => $this->iMemberTeamID,
                'loadingteam' => ($loadingteam ? 1 : 0),
                'fieldFormation' => $this->aFormationSlots,
                'formation' => $this->aSlots,
                'playerslist' => $this->aPlayers,
                'valid_team' => $this->bIsValid,
                'budgetleft' => $this->getBudgetLeft(),
                'isSaved' => 1,
            );

            return $this->render('templates/homepage/teambuilder-pitch.html.twig', $result);
        }
    }

    /**
     * Assings pagination to players search result
     *
     * @return array
     * @param int iTotalRows total count of search result
     * @param int iPage current page number
     */
    private function assignPagination($iTotalRows, $iPage)
    {
        $this->enablePaging($iPage);
        $aPages = array();
        $aPages['current'] = $this->iPage;
        $iEnd = (($this->iLimit + $this->iStart) < $iTotalRows) ? ($this->iLimit + $this->iStart) : $iTotalRows;
        if ($this->iStart > 0):
            $aPages['previous'] = $this->iPage - 1;
        endif;
        if ($iEnd < $iTotalRows):
            $aPages['next'] = $this->iPage + 1;
        endif;

        $dPageCount = round($iTotalRows / $this->iLimit, 0, PHP_ROUND_HALF_UP);

        if ($iTotalRows % $this->iLimit != 0) {
            $dPageCount = $dPageCount + 1;
        }

        $aPages['last'] = $dPageCount - 1;
        $aPages['totalresults'] = $iTotalRows;
        $aPages['shownresults'] = array('start' => $this->iStart + 1, 'end' => $iEnd, 'count' => $this->iLimit);

        return $aPages;
    }

    /**
     * Assings players search result pagination start and current indexed
     *
     * @param int iPage current page number
     */
    private function enablePaging($iPage)
    {
        if ($iPage) {
            $this->iStart = ($this->iLimit * ($iPage - 1));
            $this->iPage = $iPage;
        }
    }

    /**
     * Assings linup and bench players
     *
     * @param int iTeamId
     * @param bollean bIsSquadeEdit to check if editing existing squade or creating new one
     */
    private function assignPlayers($iTeamId, $bIsSquadeEdit = false)
    {
        $this->aPlayers['lineup']['Midfielder'] = array();
        $this->aPlayers['lineup']['Defender'] = array();
        $this->aPlayers['lineup']['Striker'] = array();
        $this->aPlayers['lineup']['Goalkeeper'] = array();
        $this->aPlayers['bench'] = array();
        $this->fAmountSpent = 0;
        $teamPlayers = $this->get('OulalaiFrame.repository.savedsquad')->getDraftTeamPlayers($iTeamId);

        $iTeamPlayersCount = 0;
        if (!empty($teamPlayers)) {
            foreach ($teamPlayers as $player) {
                $playerDetails = $this->get('OulalaiFrame.repository.players')->getPlayerProfileDetails(
                    $player['player_id']
                );

                if (!empty($playerDetails)) {
                    $this->fAmountSpent += $playerDetails['value'];
                    if ($this->getBudgetLeft() >= 0) {
                        if ($player['starting_11'] == 1) {
                            $pos = $playerDetails['position'];

                            $playerDetails['starting_11'] = $player['starting_11'];
                            array_push($this->aPlayers['lineup'][$pos], $playerDetails);
                            $iTeamPlayersCount++;
                        } else {
                            if (count($this->aPlayers['bench']) < 7) {
                                $playerDetails['starting_11'] = $player['starting_11'];
                                array_push($this->aPlayers['bench'], $playerDetails);
                            }
                        }
                        $this->aSchedulePlayers[$player['player_id']] = $playerDetails;
                    } else {
                        $this->aBadget['error'] = true;
                    }
                }
                if ($iTeamPlayersCount == 11) {
                    $this->bIsValid = true;
                } else {
                    $this->bIsValid = false;
                }
            }
        }

        $this->aBudget['available'] = $this->fMemberLeagueBudget;
        $this->aBudget['used'] = $this->fAmountSpent;
        if ($this->fAmountSpent > $this->fMemberLeagueBudget) {
            $this->aBudget['error'] = true;
        } else {
            $this->aBudget['left'] = $this->getBudgetLeft();
        }
    }

    /**
     * Calidates that a change in formation has not resulted in an illegal team being created
     * @param $iFormationID
     * @return bool
     */
    private function chackPlayersInFormation($iFormationID, $iTeamID, $bIsSquadeEdit = false, $bChangeFormation = false)
    {
        $positionSlots = $this->get('OulalaiFrame.repository.teams')->getFormationPositions($iFormationID);
        $this->assignPlayers($iTeamID, $bIsSquadeEdit);
        if (!empty($positionSlots)) {
            foreach ($positionSlots as $slot) {
                $pos = $slot['position_title'];

                $pos_count = count($this->aPlayers['lineup'][$pos]);

                if ($pos_count >= $slot['slots']) {
                    $this->aValid[$pos] = false;
                }
                if ($bChangeFormation) {
                    $count = count($this->aPlayers['lineup'][$pos]);

                    if ($pos_count < $slot['slots']) {
                        if (count($this->aPlayers['bench']) > 0) {
                            foreach ($this->aPlayers['bench'] as $sub_player) {
                                // put players to field from substitute
                                if ($count < $slot['slots']) {
                                    if ($sub_player['position'] == $pos && $sub_player['starting_11'] == 0) {
                                        $this->get('OulalaiFrame.repository.savedsquad')->swapPlayerQuery(
                                            $sub_player['id'],
                                            $iTeamID,
                                            1,
                                            $bIsSquadeEdit,
                                            false
                                        );

                                        $this->assignPlayers($iTeamID);
                                        $count++;
                                    }
                                }
                            }
                        }
                    } else {
                        //                        if (count($this->aPlayers['bench']) < 7) {
                        foreach ($this->aPlayers['lineup'][$pos] as $start_player) {
                            // put players to sub from field
                            if ($count > $slot['slots']) {
                                $this->get('OulalaiFrame.repository.savedsquad')->swapPlayerQuery(
                                    $start_player['id'],
                                    $iTeamID,
                                    0,
                                    $bIsSquadeEdit,
                                    false
                                );
                                $this->assignPlayers($iTeamID);
                                $count--;
                            }
                        }
//                        }
                    }
                    $this->assignPlayers($iTeamID, $bIsSquadeEdit);
                }
            }
        }
        if (count($this->aPlayers['bench']) >= 6) {
            $this->aValid['bench'] = false;
        }

        return $this->aValid;
    }

    /**
     * Returns the amount of money a member has left to spend.
     *
     * @return int
     */
    private function getBudgetLeft()
    {
        return $this->fMemberLeagueBudget - $this->fAmountSpent;
    }

    public function loadMySavedTeamsAction()
    {
        $session = new Session();
        $request = Request::createFromGlobals();
        if (!$session->has('user_ref')) {
            return $this->redirectToRoute('oulalai_frame', array(), 301);
        }
        if ($request->isXmlHttpRequest()) {
            $allTeams = $this->getAllMySavedTeam(false);
            $isSaved = $request->request->get('savedTeam');
            $leagueId = $request->request->get('leagueid');
            $currentteamid = $request->request->get('currentteamid');
            $result = array(
                'saved_team' => $allTeams,
                'loadingteam' => true,
                'savedTeam' => $isSaved,
                'leagueId' => $leagueId,
                'currentteamid' => $currentteamid,
            );

            return $this->render('templates/controls/my-saved-teams.html.twig', $result);
        }
    }

    public function cancelTeamSaveAction()
    {
        $request = Request::createFromGlobals();
        if ($request->isXmlHttpRequest()) {
            $iTeamID = $request->request->get('teamid');
            $loadingteam = $request->request->get('loadingteam');
            $currentteamid = $request->request->get('currentteamid');
            $this->get('OulalaiFrame.repository.savedsquad')->changePlayersDraftStatus($iTeamID, 0, 1);
            $bIsSquadeEdit = ($currentteamid ? true : false);
            $this->formationList = $this->get('OulalaiFrame.repository.teams')->getFormation();
            $this->aFormationSlots = $this->get('OulalaiFrame.repository.teams')->assignFormationDetails($this->aSlots);
            $this->assignPlayers($this->iTeamID, $bIsSquadeEdit);

            $result = array(
                'leagueId' => $this->leagueId,
                'formationList' => $this->formationList,
                'playervalue' => 45,
                'budget' => $this->aBudget,
                'teamid' => $this->iMemberTeamID,
                'loadingteam' => ($loadingteam ? 1 : 0),
                'fieldFormation' => $this->aFormationSlots,
                'formation' => $this->aSlots,
                'playerslist' => $this->aPlayers,
                'valid_team' => $this->bIsValid,
                'budgetleft' => $this->getBudgetLeft(),
                'isSaved' => 1,
            );

            return $this->render('templates/homepage/teambuilder-pitch.html.twig', $result);
        }
    }

    private function editJoinedTeamsWithSavedTeams($aJoinedLeagues)
    {
        $joinedLeagues = substr(trim($aJoinedLeagues), 0, -1);
        $aPlayers = $this->get('OulalaiFrame.repository.savedsquad')->getDraftPlayersByJoinedTeamIDs(
            $this->iMemberTeamID,
            $joinedLeagues
        );

        $oSquads = new SquadsController();

        if (!empty($aJoinedTeamsIDs)) {
            $joinedTeamsIDs = implode(',', $aJoinedTeamsIDs);

            $this->get('OulalaiFrame.repository.savedsquad')->deleteSquadPlayers();

            foreach ($aJoinedTeamsIDs as $iSquadId) {
                if (!empty($aPlayers)) {
                    foreach ($aPlayers as $iPlayer) {
                        $oSquads->putIntoSquadPlayers(
                            $iPlayer['fk_player_id'],
                            $iSquadId,
                            $iPlayer['starting_11'],
                            $iPlayer['position_title'],
                            $iPlayer['positionId']
                        );
                    }
                }
            }
        }
    }

    /**
     * Store player in temporary table
     *
     * @param int iPlayerId
     * @param int iSquadId
     * @param int starting get 0 or 1 to check if player is for lineup or bench
     * @param int sPositionTitle player position title
     */
    private function putIntoDraftTeamPlayers($iPlayerId, $iSquadId, $starting, $sPositionId, $iPlayerTeamId)
    {
        $player = $this->get('OulalaiFrame.repository.players')->getPlayerProfileDetails($iPlayerId);
        if ($player['value'] > $this->getBudgetLeft()) {
            $this->aBudget['error'] = true;

            return false;
        }
        $teamPlayers = $this->get('OulalaiFrame.repository.savedsquad')->getDraftTeamPlayers($iSquadId);
        if (!empty($teamPlayers)) {
            foreach ($teamPlayers as $players) {
                if ($players['player_id'] == $iPlayerId) {
                    return false;
                }
            }
        }

        $this->get('OulalaiFrame.repository.savedsquad')->insertDraftTeamPlayers(
            $iSquadId,
            $iPlayerId,
            $iPlayerTeamId,
            $starting,
            $sPositionId
        );
    }

    private function putIntoTeamPlayers($iPlayerId, $iTeamId, $starting, $iPositionId)
    {
        $player = $this->get('OulalaiFrame.repository.players')->getPlayerProfileDetails($iPlayerId);
        if ($player['value'] > $this->getBudgetLeft()) {
            $this->aBudget['error'] = true;

            return false;
        }
        $teamPlayers = $this->get('OulalaiFrame.repository.squad')->getTeamPlayers($iTeamId);
        if (!empty($teamPlayers)) {
            foreach ($teamPlayers as $players) {
                if ($players['player_id'] == $iPlayerId) {
                    return false;
                }
            }
        }
        try {
            $this->get('OulalaiFrame.repository.squad')->insertTempPlayers(
                $iTeamId,
                $iPlayerId,
                $starting,
                $iPositionId
            );
        } catch (Exception $e) {
        }
    }

    public function joinToLeagueWithSavedTeamAction()
    {
        $request = Request::createFromGlobals();
        $session = new Session();
        $this->sRef = $session->get('user_ref');
        $selectedleagues = $request->request->get('leagueids');
        $selectedleagues = explode(",", $selectedleagues);

        if (is_array($selectedleagues) && !empty($selectedleagues)) {
            $iSquadId = $request->request->get('teamid');
            $join = false;
            foreach ($selectedleagues as $iLeagueId) {
                if (!empty($iLeagueId)) {
                    $aTeam = $this->get('OulalaiFrame.repository.savedsquad')->getDraftTeamDetailsById($iSquadId);
                    $aPlayers = $this->get('OulalaiFrame.repository.savedsquad')->getDraftTeamPlayersData($iSquadId);

                    $form_params = [
                        'user' => $this->sRef,//test user param
                        'league' => (int)$iLeagueId,
                        'name' => $aTeam['comment'],
                        'formation' => (int)$aTeam['fk_formation_id'],

                    ];
                    $result = $this->get('OulalaiFrame.repository.savedsquad')->sendDataToApiToJoin(
                        $aPlayers,
                        $form_params
                    );
                }
            }

            if (empty($result['errors'])) {
                return new Response(json_encode(["url" => "/".$request->getLocale()."/squads-list/upcoming"]));
            } else {
                return new Response(json_encode($result));
            }
        }
    }


    public function changeSavedTeamNameAction(Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            $currentteamid = $request->request->get('currentteamid');
            $teamName = $request->request->get('teamName');
            if ($currentteamid && $teamName) {
                $sTeamExists = $this->get('OulalaiFrame.repository.savedsquad')->getDraftTeamDetailsById(
                    $currentteamid
                );
                if ($sTeamExists) {
                    $updateSquadName = $this->get('OulalaiFrame.repository.savedsquad')->updateDraftTeamName(
                        $currentteamid,
                        $teamName
                    );
                    if ($updateSquadName) {
                        return new Response('Success');
                    } else {
                        return new Response('Error', 404);
                    }
                }

                return new Response('Success');
            }

            return new Response(json_encode(['Error' => 'Missing team data']), 404);
        }
    }
}
