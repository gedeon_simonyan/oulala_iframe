<?php
/**
 * Created by PhpStorm.
 * User: conta
 * Date: 03/10/2017
 * Time: 9:08 AM
 */

namespace OulalaiFrameBundle\Controller;

use OulalaiFrameBundle\Entity\BlogCategories;
use OulalaiFrameBundle\Entity\BlogPosts;
use OulalaiFrameBundle\Entity\Config;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Validator\Constraints\DateTime;

class BlogController extends Controller
{
    public function indexAction(Request $request)
    {
        $session = new Session();
        $operator_id = isset($request->query->all()['operator_id']) ? $request->query->all()['operator_id'] : null;

        if ($operator_id) {
            $client_id = $this->get('OulalaiFrame.repository.config')->getClientByAlias($operator_id);
            $session->set('client', $client_id);
        } else {
            $client_id = $session->get('client');
        }

        if (!$client_id) {
            exit("Invalid Client");
        }
        $limit=1;
        if (isset($request->query->all()['p'])) {
            $limit=$request->query->all()['p'];
        }
        if (isset($request->query->all()['s'])) {
            $search=$request->query->all()['s'];
        } else {
            $search=null;
        }

        $locale=$request->getLocale();
        $style = $this->get('OulalaiFrame.repository.config')->getUserStyle($client_id);
        $css_version = $this->getParameter('css_version');
        $js_version = $this->getParameter('js_version');
        $menu = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('show_menu', $client_id);
        $offset = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('blog_pages_count', $client_id);
        $blogCategory = $this->get('OulalaiFrame.repository.blog')->getBlogCategories($client_id);
        $blogPosts = $this->get('OulalaiFrame.repository.blog')->getBlogPosts($client_id, null, $locale, $limit, $offset, $search);
        $blogPostsCount = $this->get('OulalaiFrame.repository.blog')->getPostCount($client_id, '', $search, $locale);
        $client_alias = $this->get('OulalaiFrame.repository.config')->getAliasByClient($client_id);
        if ($limit==0 ||$limit==1) {
            $countFrom=1;
            $countTo=$offset;
            $nextPage=2;
        } else {
            $nextPage=$limit+1;
            $countTo=$limit*$offset;
            $countFrom=($limit-1)*$offset+1;
        }

        $lastPage=ceil($blogPostsCount/$offset);
        $prevPage=$limit-1;

        return $this->render(
            'blog/news.html.twig',
            array(
                'blogcategories' => $blogCategory,
                'blogPostsCount' => $blogPostsCount,
                'blogitems' => $blogPosts,
                'countFrom' => $countFrom,
                'countTo' => $countTo,
                'nextPage' => $nextPage,
                'lastPage' => $lastPage,
                'prevPage' => $prevPage,
                'offset' => $offset,
                'style' => $style.$css_version,
                'css_version' => $css_version,
                'js_version' => $js_version,
                'menu' => $menu,
                'clientAlias' => $client_alias,
            )
        );
    }

    public function getBlogPostAction($postId, Request $request)
    {
        $session = new Session();
        $operator_id = isset($request->query->all()['operator_id']) ? $request->query->all()['operator_id'] : null;

        if ($operator_id) {
            $client_id = $this->get('OulalaiFrame.repository.config')->getClientByAlias($operator_id);
            $session->set('client', $client_id);
        } else {
            $client_id = $session->get('client');
        }
        $locale=$request->getLocale();
        $style = $this->get('OulalaiFrame.repository.config')->getUserStyle($client_id);
        $css_version = $this->getParameter('css_version');
        $js_version = $this->getParameter('js_version');
        $menu = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('show_menu', $client_id);
        $blogUrl = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('blog_share_url', $client_id);
        $blogPosts = $this->get('OulalaiFrame.repository.blog')->getBlogPosts($client_id, $postId, $locale);
        $blogFeaturedPosts = $this->get('OulalaiFrame.repository.blog')->getBlogFeaturedPost($client_id, $locale);
        $limit=3;

        if(!empty($blogFeaturedPosts)){
            $limit=2;
        }
        $blogLastPosts = $this->get('OulalaiFrame.repository.blog')->getBlogLastPosts($client_id, $locale,$limit);

        return $this->render(
            'blog/news-item.html.twig',
            array(
                'metatitle' => $blogPosts['post_subject'],
                'post_image' => $request->server->get('REQUEST_SCHEME').'://'.$request->server->get(
                        'HTTP_HOST'
                    ).'/bundles/images/blog/'.$blogPosts['post_illustration_link'],
                'post_content' => substr(strip_tags($blogPosts['post_content']), 0, 60),
                'blogitems' => $blogPosts,
                'style' => $style.$css_version,
                'css_version' => $css_version,
                'js_version' => $js_version,
                'menu' => $menu,
                'blogUrl' => $blogUrl,
                'lastPosts'=>$blogLastPosts,
                'featuredPost'=>$blogFeaturedPosts,
            )
        );
    }

    public function getPostsByCategoryAction($catName, Request $request)
    {
        $session = new Session();
        $client_id = $session->get('client');
        $style = $this->get('OulalaiFrame.repository.config')->getUserStyle($client_id);
        $limit=0;
        if (isset($request->query->all()['p'])) {
            $limit=$request->query->all()['p'];
        }
        $css_version = $this->getParameter('css_version');
        $js_version = $this->getParameter('js_version');
        $menu = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('show_menu', $client_id);
        $offset = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('blog_pages_count', $client_id);
        $blogPosts = $this->get('OulalaiFrame.repository.blog')->getBlogPostsByCategory($client_id, $catName, $limit, $offset);
        $blogCategory = $this->get('OulalaiFrame.repository.blog')->getBlogCategories($client_id);
        $blogPostsCount = $this->get('OulalaiFrame.repository.blog')->getPostCount($client_id, $catName);
        $client_alias = $this->get('OulalaiFrame.repository.config')->getAliasByClient($client_id);
        if ($limit==0 ||$limit==1) {
            $countFrom=1;
            $countTo=$offset;
            $nextPage=2;
        } else {
            $nextPage=$limit+1;
            $countTo=$limit*$offset-1;
            $countFrom=($limit-1)*$offset+1;
        }

        $lastPage=ceil($blogPostsCount/$offset);
        $prevPage=$limit-1;


        return $this->render(
            'blog/news.html.twig',
            array(
                'blogcategories' => $blogCategory,
                'blogPostsCount' => $blogPostsCount,
                'blogitems' => $blogPosts,
                'countFrom' => $countFrom,
                'countTo' => $countTo,
                'nextPage' => $nextPage,
                'lastPage' => $lastPage,
                'prevPage' => $prevPage,
                'offset' => $offset,
                'style' => $style.$css_version,
                'css_version' => $css_version,
                'js_version' => $js_version,
                'menu' => $menu,
                'clientAlias' => $client_alias,
            )
        );
    }

    public function newAction(Request $request)
    {
        $blogPost = new BlogPosts();
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm('OulalaiFrameBundle\Form\BlogPostsType', $blogPost, array('entity_manager'=>$em,'translation_domain'=>$request->getLocale()));
        $form->handleRequest($request);

        $usr= $this->get('security.token_storage')->getToken()->getUser();
        $role = $usr->getRoles()['0'];
        $alias = $this->getUser()->getUsername();

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $file */
            $file = $blogPost->getPostIllustrationLink();
            if ($file) {
                // Generate a unique name for the file before saving it
                $fileName = md5(uniqid()).'.'.$file->guessExtension();

                // Move the file to the directory where brochures are stored
                $file->move(
                    $this->getParameter('image_path'),
                    $fileName
                );

                // Update the 'brochure' property to store the PDF file name
                // instead of its contents
                $blogPost->setPostIllustrationLink($fileName);
            }
            $date = new \DateTime();
            $blogPost->setCreateDate($date);
            $blogPost->setUpdateDate($date);
            $alias = $this->getUser()->getUsername();
            $operatorId = $this->get('OulalaiFrame.repository.config')->getClientByAlias($alias);
            $blogPost->setPostOperator($operatorId);

            $blogPost->setCategoryId($blogPost->getCategoryId()->getCategoryId());
            $em = $this->getDoctrine()->getManager();
            $em->persist($blogPost);
            $em->flush();

            return $this->redirectToRoute('blog_show', array('postId' => $blogPost->getPostid()));
        }

        return $this->render(
            'admin/blog/new.html.twig',
            array(
                'blogPost' => $blogPost,
                'form' => $form->createView(),
                'role' => $role
            )
        );
    }

    public function editAction($postId, Request $request)
    {
        $session = new Session();

        $usr= $this->get('security.token_storage')->getToken()->getUser();
        $role = $usr->getRoles()['0'];
        $alias = $this->getUser()->getUsername();

        if ($role != "ROLE_SUPER_ADMIN") {
            $operatorId = $this->get('OulalaiFrame.repository.config')->getClientByAlias($alias);
            $session->set('admin_operator_id', $operatorId);
        } else {
            $operatorId = null;
        }

        $blogPosts = $this->get('OulalaiFrame.repository.blog')->getBlogPosts($operatorId, $postId);
        $languages = ['en' => 'EN', 'fr' => 'FR', 'tr' => 'TR', 'gr' => 'GR', 'pt' => 'PT'];
        $em = $this->getDoctrine()->getManager();
        $session->set('admin_operator_id', $operatorId);
        $blogPost = $this->getDoctrine()->getRepository(BlogPosts::class)->find($postId);

        if ($blogPost->getCategoryId()) {
            $categories = $this->getDoctrine()->getRepository(BlogCategories::class)->find($blogPost->getCategoryId());
            $blogPost->setCategoryId($categories->getCategoryId());
        }

        $post_image = $blogPost->getPostIllustrationLink();
        $editForm = $this->createForm('OulalaiFrameBundle\Form\BlogPostsType', $blogPost, array('entity_manager'=>$em,'translation_domain'=>$request->getLocale()));
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $data = $editForm->getData();

            /** @var UploadedFile $file */
            $file = $blogPost->getPostIllustrationLink();
            $category_id = $blogPost->getCategoryId();

            if ($file) {
                // Generate a unique name for the file before saving it
                $fileName = md5(uniqid()).'.'.$file->guessExtension();

                // Move the file to the directory where brochures are stored
                $file->move(
                    $this->getParameter('image_path'),
                    $fileName
                );
            } else {
//                if($post_image){
                /** @var UploadedFile $fileName */
                $fileName = $post_image;
            }

            $date = new \DateTime();
            // Update the 'brochure' property to store the PDF file name
            // instead of its contents
            $blogPost->setPostIllustrationLink($fileName);
            if ($category_id) {
                $blogPost->setCategoryId($category_id->getCategoryId());
            }

            $blogPost->setUpdateDate($date);

            $em->persist($data);
            $em->flush();

            return $this->redirectToRoute('blog_show', array('postId' => $postId));
        }

        return $this->render(
            'admin/blog/edit.html.twig',
            array(
                'form' => $editForm->createView(),
                'blogPost' => $blogPosts,
                'languages' => $languages,
                'postImage' => $post_image,
                'role' => $role
            )
        );
    }

    public function showAction($postId)
    {
        $session = new Session();
        $usr= $this->get('security.token_storage')->getToken()->getUser();
        $role = $usr->getRoles()['0'];
        $alias = $this->getUser()->getUsername();

        $blogPost = $this->getDoctrine()
            ->getRepository(BlogPosts::class)
            ->find($postId);
        $deleteForm = $this->createDeleteForm($blogPost);
        if ($role != "ROLE_SUPER_ADMIN") {
            $operatorId = $this->get('OulalaiFrame.repository.config')->getClientByAlias($alias);
            $session->set('admin_operator_id', $operatorId);
        } else {
            $operatorId = null;
        }
        $blogPosts = $this->get('OulalaiFrame.repository.blog')->getBlogPosts($operatorId, $postId);

        return $this->render(
            'admin/blog/show.html.twig',
            array(
                'blogPost' => $blogPosts,
                'delete_form' => $deleteForm->createView(),
                'role' => $role
            )
        );
    }

    /**
     * Deletes a blogPost entity.
     *
     */
    public function deleteAction(Request $request, $postId)
    {
        $blogPost = $this->getDoctrine()
            ->getRepository(BlogPosts::class)
            ->find($postId);
        $deleteForm = $this->createDeleteForm($blogPost);
        $deleteForm->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        if ($deleteForm->isSubmitted() && $deleteForm->isValid()) {
            $em->remove($blogPost);
            $em->flush();
        } else {
            $em->remove($blogPost);
            $em->flush();
        }

        return $this->redirectToRoute('admin_blog');
    }

    private function createDeleteForm(BlogPosts $blogPost)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('blog_delete', array('postId' => $blogPost->getPostid())))
            ->setMethod('DELETE')
            ->getForm();
    }

    public function newCategoryAction(Request $request)
    {
        $usr= $this->get('security.token_storage')->getToken()->getUser();
        $role = $usr->getRoles()['0'];
        $blogCategory = new BlogCategories();
        $form = $this->createForm('OulalaiFrameBundle\Form\BlogCategoryType', $blogCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $alias = $this->getUser()->getUsername();
            $operatorId = $this->get('OulalaiFrame.repository.config')->getClientByAlias($alias);
            $blogCategory->setOperatorId($operatorId);
            if (!$blogCategory->getCategorySlug()) {
                $catTitle=$blogCategory->getCategoryTitle();
                $catSlug=preg_replace('/\W+/', '-', strtolower($catTitle));
                $blogCategory->setCategorySlug($catSlug);
            }
            $em->persist($blogCategory);
            $em->flush();

            return $this->redirectToRoute('admin_blog_category', array('postId' => $blogCategory->getCategoryId()));
        }

        return $this->render('admin/blog/category/new-category.html.twig', array(
            'blogPost' => $blogCategory,
            'form' => $form->createView(),
            'role' => $role
        ));
    }

    public function editCategoryAction(Request $request, BlogCategories $categoryId)
    {
        $deleteForm = $this->createCategoryDeleteForm($categoryId);
        $usr= $this->get('security.token_storage')->getToken()->getUser();
        $role = $usr->getRoles()['0'];
        $editForm = $this->createForm('OulalaiFrameBundle\Form\BlogCategoryType', $categoryId);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            if (!$categoryId->getCategorySlug()) {
                $catTitle=$categoryId->getCategoryTitle();
                $catSlug=preg_replace('/\W+/', '-', strtolower($catTitle));
                $categoryId->setCategorySlug($catSlug);
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_blog_category', array('categoryId' => $categoryId->getCategoryId()));
        }

        return $this->render('admin/blog/category/edit-category.html.twig', array(
            'form' => $editForm->createView(),
            'categoy_id'=>$categoryId->getCategoryId(),
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'role' => $role
        ));
    }

    public function deleteCategoryAction($categoryId, Request $request)
    {
        $blogCat = $this->getDoctrine()
            ->getRepository(BlogCategories::class)
            ->find($categoryId);
        $deleteForm = $this->createCategoryDeleteForm($blogCat);
        $deleteForm->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        if ($deleteForm->isSubmitted() && $deleteForm->isValid()) {
            $em->remove($blogCat);
            $em->flush();
        } else {
            $em->remove($blogCat);
            $em->flush();
        }

        return $this->redirectToRoute('admin_blog_category');
    }
    private function createCategoryDeleteForm(BlogCategories $blogCat)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('blog_delete', array('postId' => $blogCat->getCategoryId())))
            ->setMethod('DELETE')
            ->getForm();
    }


    public function blogSettingsAction(Request $request)
    {
        $usr= $this->get('security.token_storage')->getToken()->getUser();
        $role = $usr->getRoles()['0'];
        $alias = $this->getUser()->getUsername();
        if ($role != "ROLE_SUPER_ADMIN") {
            $operatorId = $this->get('OulalaiFrame.repository.config')->getClientByAlias($alias);
        } else {
            $operatorId = null;
        }

        $postsCount = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('blog_pages_count', $operatorId);

        $form = $this->createFormBuilder()
            ->add('postCount', ChoiceType::class, array('label' => 'Select posts count to paginate:',
                'choices'=>array('5'=>'5','10'=>'10','25'=>'25','50'=>'50'),
                'data'=>$postsCount))
            ->getForm();

        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            // data is an array with "name", "email", and "message" keys
            $data = $form->getData();

            if ($this->get('OulalaiFrame.repository.config')->updateConfigPostCount($operatorId, $data['postCount'])) {
                $this->addFlash("success", "Successfully updated");
            } else {
                $this->addFlash("error", "Can not update data");
            }
        }

        return $this->render('admin/blog/settings/settings.html.twig', array(
            'post_count'=>$postsCount,
            'form'=>$form->createView(),
            'role' => $role
        ));
    }
}
