<?php
/**
 * Created by PhpStorm.
 * User: conta
 * Date: 03/10/2017
 * Time: 9:08 AM
 */

namespace OulalaiFrameBundle\Controller;

use OulalaiFrameBundle\Entity\Leagues;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class AdminLeaguesController extends Controller
{
    public function getClientLeaguesAction()
    {
        $usr= $this->get('security.token_storage')->getToken()->getUser();
        $role = $usr->getRoles()['0'];
        $alias = $this->getUser()->getUsername();
        $operatorId = $this->get('OulalaiFrame.repository.config')->getClientByAlias($alias);
        $leagues = $this->get('OulalaiFrame.repository.leagues')->getClientLeagues($operatorId);


        return $this->render('admin/leagues/leagues.html.twig', array('leagues' => $leagues,'role' => $role));
    }


    public function editLeaguesAction(Request $request, Leagues $leagueId)
    {
        $editForm = $this->createForm('OulalaiFrameBundle\Form\LeaguesType', $leagueId);
        $editForm->handleRequest($request);
        $alias = $this->getUser()->getUsername();
        $operatorId = $this->get('OulalaiFrame.repository.config')->getClientByAlias($alias);
        $clientName = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('client_name', $operatorId);
        $clientToken = $this->get('OulalaiFrame.repository.token')->getClientToken($operatorId)->access_token;
        $clientWebcite = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('iframe_toke_url', $operatorId);
        $iframe_response=$this->get("OulalaiFrame.apiservice.api")->APIRequest('POST', $clientWebcite, false, "asdsdadsxzc");
        $nodeUrl=$this->getParameter('node_url');
        $access_token = $this->get('OulalaiFrame.repository.token')->getClientToken($operatorId)->access_token;
        if ($clientToken) {
            $oulalaApi = true;
        } else {
            $oulalaApi = false;
        }
        if (isset($iframe_response->ok)) {
            $clientStatus = true;
        } else {
            $clientStatus = false;
        }

        $status = ['oulala' => $oulalaApi, 'client' => $clientStatus];
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $data = $editForm->getData();
            $isGprizeFixed=$data->getIsGprizeFixed();
            $gPrize=$data->getGprize();
            $params=["leagueId"=>$leagueId->getLeaguesId(),
                      "gprize"=>$gPrize,
                      "is_gprize_fixed"=>$isGprizeFixed];

            if ($isGprizeFixed=='1' && $gPrize <=0) {
                $this->addFlash("error", "If gprize fixed is 1 then then gprize should be greater 0");
            } else {
                $leagueUpdate=$this->get("OulalaiFrame.apiservice.api")->APIRequest('PUT', $this->container->getParameter('api_base_url') . 'leagues/update', $access_token, $params);
                if (isset($leagueUpdate->status) && $leagueUpdate->status=="success") {
                    $this->get('OulalaiFrame.repository.leagues')->updateOneLeagueData($access_token, $leagueId->getLeaguesId(), 1);
                }
//                $this->getDoctrine()->getManager()->flush();
                return $this->redirectToRoute('admin_leagues');
            }
        }

        return $this->render('admin/leagues/edit.html.twig', array(
            'form' => $editForm->createView(),
            'league_id'=>$leagueId->getLeaguesId(),
            'edit_form' => $editForm->createView(),
            'statuses' => $status,
            'clientName' => $clientName,
            'nodeUrl' => $nodeUrl
        ));
    }

    public function createLeaguesAction(Request $request)
    {
        $alias = $this->getUser()->getUsername();
        $operatorId = $this->get('OulalaiFrame.repository.config')->getClientByAlias($alias);

        $clientName = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('client_name', $operatorId);
        $clientToken = $this->get('OulalaiFrame.repository.token')->getClientToken($operatorId)->access_token;
        $clientWebcite = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('iframe_toke_url', $operatorId);
        $minCost=$this->get('OulalaiFrame.repository.config')->getConfigByKeyName('min_join_cost', $operatorId);
        $user_ref=$this->get('OulalaiFrame.repository.config')->getConfigByKeyName('client_ref', $operatorId);
        $iframe_response=$this->get("OulalaiFrame.apiservice.api")->APIRequest('POST', $clientWebcite, false, "asdsdadsxzc");

        $nodeUrl=$this->getParameter('node_url');
        if ($clientToken) {
            $oulalaApi = true;
        } else {
            $oulalaApi = false;
        }
        if (isset($iframe_response->ok)) {
            $clientStatus = true;
        } else {
            $clientStatus = false;
        }

        $status = ['oulala' => $oulalaApi, 'client' => $clientStatus];
        $date = new \DateTime();
        $gameDate = $request->request->get('date');
        $time = $date->format('H:i:s');
        $leaguesList = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('client_leagues', $operatorId);
        $gameDate = $gameDate ? $gameDate : $date->format('Y-m-d');
        $confDate = '00:30:00';

        if ($gameDate == $date->format('Y-m-d')) {
            $matches = $this->get('OulalaiFrame.repository.matches')->getFromDate($gameDate . ' ' . $time, $confDate);
            $country_leagues = $this->get('OulalaiFrame.repository.leagues')->getLeagues($gameDate . ' ' . $time, $leaguesList);
        } else {
            $matches = $this->get('OulalaiFrame.repository.matches')->getFromDate($gameDate . ' 00:00:00', $confDate);
            $country_leagues = $this->get('OulalaiFrame.repository.leagues')->getLeagues($gameDate . ' 00:00:00', $leaguesList);
        }

        $availableMatchDates = $this->get('OulalaiFrame.repository.matches')->getMatchesDate(3, $confDate);
        if ($request->isXmlHttpRequest()) {
            return $this->render('admin/leagues/league-matches-list.html.twig', array(
                    'matches' => $matches,
                    'dates' => $availableMatchDates,
                    'realLeague' => $country_leagues)
            );
        }
        $method = $request->getRealMethod();
        if ($method == "POST") {
            $leaguetypeid = $request->request->get('league_type');
            $leagueName = $request->request->get('leagueName');
            $entryFee = $request->request->get('entry_fee') ;
            $maxParticipants = ($leaguetypeid == 1) ? $request->request->get('maxParticipants') : 2;
            if ($maxParticipants == -1) {
                $maxParticipants = 0;
            }
            if ($entryFee == -1) {
                $entryFee = 0;
            }
            $prize = $request->request->get('prize_type');
            $gPrize = $request->request->get('guaranted_prize');
            $gPrizeFixed = $request->request->get('guaranted_prize_fixed');
            $matches_list = $request->request->get('matches');
            if (is_array($matches_list)) {
                $form_params = [
                    'user' => $user_ref, //test user ref param
                    'leagueName' => $leagueName,
                    'entryFee' => (float)$entryFee,
                    'maxParticipants' => (int)$maxParticipants,
                    'leaguetypeid' => $leaguetypeid,
                    'prize' => (int)$prize,
                    'gPrize' => (float)$gPrize,
                    'gPrizeFixed' => (int)$gPrizeFixed,
                    'matches' => array_map(create_function('$value', 'return (int)$value;'), $matches_list),
                    'is_admin'=>1
                ];
                $params = $form_params;
                $league = $this->get("OulalaiFrame.apiservice.api")->APIRequest('POST', $this->container->getParameter('api_base_url') . 'leagues', $clientToken, $params);
                
                if ($league->status == 'success') {
                    $this->get('OulalaiFrame.repository.matches')->updateOneLeagueMatchesAndUserData(
                        $clientToken,
                        $league->data->leagueId
                    );
                    $this->get('OulalaiFrame.repository.leagues')->updateOneLeagueData(
                        $clientToken,
                        $league->data->leagueId,
                        0
                    );
                    return new RedirectResponse($this->generateUrl('admin_leagues'));
                }
                if ($league->status == 'fail') {
                    foreach ($league->data->violations as $value) {
                        $errors[] = $value;
                    }
                    $errors['status'] = $league->status;
                    return $this->render('admin/leagues/create.html.twig',
                        array('error' => $errors,'statuses' => $status,
                            'clientName' => $clientName, 'nodeUrl' => $nodeUrl,
                            'matches' => $matches,
                            'dates' => $availableMatchDates,
                            'realLeague' => $country_leagues,
                            'mincost'=>$minCost
                        )
                    );
                }

                if ($league->status == 'failed') {
                    $errors[] = $league->data;
                    $errors['status'] = $league->status;
                    return $this->render('admin/leagues/create.html.twig',
                        array('error' => $errors,'statuses' => $status,
                            'clientName' => $clientName, 'nodeUrl' => $nodeUrl,
                            'matches' => $matches,
                            'dates' => $availableMatchDates,
                            'realLeague' => $country_leagues,
                            'mincost'=>$minCost
                        ));
                }
            }
        }

        return $this->render('admin/leagues/create.html.twig', array( 'statuses' => $status,
            'clientName' => $clientName, 'nodeUrl' => $nodeUrl,
            'matches' => $matches,
            'dates' => $availableMatchDates,
            'realLeague' => $country_leagues,
            'mincost'=>$minCost));
    }
}
