<?php
/**
 * Created by PhpStorm.
 * User: conta
 * Date: 03/10/2017
 * Time: 9:08 AM
 */

namespace OulalaiFrameBundle\Controller;

use OulalaiFrameBundle\Entity\Leagues;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class AdminController extends Controller
{
    public function indexAction(Request $request)
    {
        $usr= $this->get('security.token_storage')->getToken()->getUser();
        $role = $usr->getRoles()['0'];
        $alias = $this->getUser()->getUsername();

        if ($role != "ROLE_SUPER_ADMIN") {
            $operatorId = $this->get('OulalaiFrame.repository.config')->getClientByAlias($alias);
        } else {
            $operatorId = null;
        }
        $transactions = $this->get('repository.transaction')->getTransaction($operatorId);

        return $this->render('admin/index.html.twig',
            array('transactions' => $transactions));
    }

    public function getStatusAction()
    {
        $usr= $this->get('security.token_storage')->getToken()->getUser();
        $role = $usr->getRoles()['0'];
        $session = new Session();
        $alias = $this->getUser()->getUsername();
        if ($role != "ROLE_SUPER_ADMIN") {
            $operatorId = $this->get('OulalaiFrame.repository.config')->getClientByAlias($alias);
            $session->set('admin_operator_id', $operatorId);
        } else {
            $operatorId = null;
        }
        $clientNames= $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('client_name', $operatorId);
        if (!is_array($clientNames)) {
            $clientName[0]['config_value']=$clientNames;
            $clientName[0]['client_id']=$operatorId;
        } else {
            $clientName=$clientNames;
        }
        $clientTokens = $this->get('OulalaiFrame.repository.token')->getClientToken($operatorId);
        $api = [];
        if (is_array($clientTokens)) {
            foreach ($clientTokens as $key => $value) {
                if ($value->access_token) {
                    $api[$key]['status'] = true;
                    $api[$key]['client_id'] = $value->client_id;
                } else {
                    $api[$key]['status'] = false;
                    $api[$key]['client_id'] = $value->client_id;
                }
            }
        } else {
            if (isset($clientTokens->access_token)) {
                $api[0]['status'] = true;
                $api[0]['client_id'] = $clientTokens->client_id;
            } else {
                $api[0]['status'] = false;
                $api[0]['client_id'] = $clientTokens->client_id;
            }
        }

        $clientWebcites = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('iframe_toke_url', $operatorId);
        if (!is_array($clientWebcites)) {
            $clientWebcite[0]['config_value'] =  $clientWebcites;
            $clientWebcite[0]['client_id'] =  $operatorId;
        } else {
            $clientWebcite=$clientWebcites;
        }


        foreach ($clientWebcite as $key => $value) {
            $iframe_response[$key]=$this->get("OulalaiFrame.apiservice.api")->APIRequest('POST', $clientWebcite[$key]['config_value'], false, "asdsdadsxzc");

            $iframe_response[$key]->webcite = $value['config_value'];
            if (isset($iframe_response[$key]->ok)) {
                $clientStatus[$key]['status'] = true;
                $clientStatus[$key]['client_id'] = $value['client_id'];
                $clientStatus[$key]['webcite'] = $iframe_response[$key]->webcite;
            } else {
                $clientStatus[$key]['status'] = false;
                $clientStatus[$key]['client_id'] = $value['client_id'];
                $clientStatus[$key]['webcite'] = $iframe_response[$key]->webcite;
            }
        }

        $nodeUrl=$this->getParameter('node_url');
        $status = ['api' => $api, 'client' => $clientStatus];

        return $this->render('admin/status.html.twig', array( 'statuses' => $status,
            'clientName' => $clientName, 'nodeUrl' => $nodeUrl,'role' => $role));
    }

    public function getBlogPostsAction()
    {
        $usr= $this->get('security.token_storage')->getToken()->getUser();
        $role = $usr->getRoles()['0'];
        $session = new Session();
        $alias = $this->getUser()->getUsername();

        if ($role != "ROLE_SUPER_ADMIN") {
            $operatorId = $this->get('OulalaiFrame.repository.config')->getClientByAlias($alias);
            $session->set('admin_operator_id', $operatorId);
        } else {
            $operatorId = null;
        }

        $blogPosts = $this->get('OulalaiFrame.repository.blog')->getBlogPosts($operatorId);

        return $this->render('admin/blog/blog.html.twig', array('blogPosts' => $blogPosts,
            'role' => $role));
    }

    public function getBlogCategoriesAction()
    {
        $usr= $this->get('security.token_storage')->getToken()->getUser();
        $role = $usr->getRoles()['0'];
        $alias = $this->getUser()->getUsername();

        if ($role != "ROLE_SUPER_ADMIN") {
            $operatorId = $this->get('OulalaiFrame.repository.config')->getClientByAlias($alias);
        } else {
            $operatorId = null;
        }

        $blogCats= $this->get('OulalaiFrame.repository.blog')->getBlogCategories($operatorId);

        return $this->render('admin/blog/blog-category.html.twig', array('blogCats' => $blogCats,
            'role' => $role));
    }

    public function getForumAction()
    {
        $usr= $this->get('security.token_storage')->getToken()->getUser();
        $role = $usr->getRoles()['0'];

        $alias = $this->getUser()->getUsername();
        if ($role != "ROLE_SUPER_ADMIN") {
            $operatorId = $this->get('OulalaiFrame.repository.config')->getClientByAlias($alias);
        } else {
            $operatorId = null;
        }

        $topics = $this->get('OulalaiFrame.repository.forum')->getForums($operatorId);

        return $this->render('admin/topic.html.twig', array('topics' => $topics,
            'role' => $role));
    }

    public function getReplyAction()
    {
        $usr= $this->get('security.token_storage')->getToken()->getUser();
        $role = $usr->getRoles()['0'];

        $alias = $this->getUser()->getUsername();
        if ($role != "ROLE_SUPER_ADMIN") {
            $operatorId = $this->get('OulalaiFrame.repository.config')->getClientByAlias($alias);
        } else {
            $operatorId = null;
        }

        $replies = $this->get('OulalaiFrame.repository.forum')->getReplies($operatorId);

        return $this->render('admin/reply.html.twig', array('replies' => $replies,
            'role' => $role));
    }
}
