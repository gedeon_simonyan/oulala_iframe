<?php

namespace OulalaiFrameBundle\Controller;

use OulalaiFrameBundle\ApiManager\ApiUrls;
use OulalaiFrameBundle\OulalaiFrameBundle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use OulalaiFrameBundle\ApiManager\Manager;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Psr\Log\LoggerInterface;

class DefaultController extends Controller
{
    public function oulalaTokenAction(Request $request)
    {
        $token = json_decode($request->getContent());
        if ($token && isset($token->token) && $token->token =='1234') {
            return new JsonResponse(['ok'=>true,'username'=>'bob']);
        };
        return new JsonResponse($token);
    }

    public function oulalaBetAction(Request $request)
    {
        $bet = json_decode($request->getContent());
        if ($bet && isset($bet->amount) && isset($bet->token) && $bet->amount == 1) {
            return new JsonResponse(
                [
                    "balance" => "1.00",
                    "transaction_id"=> $bet->bet_transaction,
                    "ok" => true
                ]);
        }
        if ($bet && isset($bet->amount) && isset($bet->token) && $bet->amount == 2) {
            return new JsonResponse(
                [
                    "errorCode"=> 3,
                    "errorDescription" => "INSUFFICIENT FUNDS",
                    "ok" => false
                ]);
        }

        return new JsonResponse($bet);
    }

    /**
     * @param Request $request
     * @return Response
     */
    //    public function indexAction(Request $request)
    //    {
    //        $user_ref = $request->request->get('ref');
    //        $access_token = $request->request->get('token');
    //        $username = $request->request->get('uname');
    //        $slt = $request->request->get('slt');
    //        $clientId = $request->request->get('client');
    //
    //        if ($request->isXmlHttpRequest()) {
    //            $tmp = $this->setUserData($user_ref, $access_token, $username, $slt, $clientId);
    //            return new Response($tmp);
    //        }
    //    }

    private function setUserData($user_ref, $access_token, $username, $slt, $clientId)
    {
        if ($user_ref && $access_token) {
            $a_token  = $this->get('OulalaiFrame.repository.token')->getClientToken($clientId);
            $cleanRef = $this->get('OulalaiFrame.repository.token')->cleanUserRef($user_ref);

            if (isset($a_token->access_token)) {
                $user_exist = $this->get("OulalaiFrame.apiservice.api")->APIRequest('GET', $this->container->getParameter('api_base_url') . 'users' .'/'.$cleanRef, $a_token->access_token);

                if ($user_exist && $user_exist->status=='not found') {
                    $user_data=[
                        'username'=>$username,
                        'user_ref'=>$cleanRef
                    ];
                    $this->get("OulalaiFrame.apiservice.api")->APIRequest('POST', $this->container->getParameter('api_base_url') . 'users', $a_token->access_token, json_encode($user_data));
                }
            }

            $response = new Response();
            $cookie = new Cookie(md5('access_token'), $access_token, time() + $slt);
            $ref_coockie = new Cookie(md5('user_ref'), $user_ref, time() + $slt);
            $uname_coockie = new Cookie(md5('username'), $username, time() + $slt);
            $client_coockie = new Cookie(md5('client'), $clientId, time() + $slt);
            $response->headers->setCookie($cookie);
            $response->headers->setCookie($ref_coockie);
            $response->headers->setCookie($uname_coockie);
            $response->headers->setCookie($client_coockie);
            $response->send();
        }
    }

    public function logoutAction(Request $request)
    {
        $response = new Response();
        $response->headers->clearCookie(md5('access_token'));
        $response->headers->clearCookie(md5('user_ref'));
        $response->send();
        return $this->redirectToRoute('oulalai_frame', array(), 301);
    }
}
