<?php
namespace OulalaiFrameBundle\Controller;

use OulalaiFrameBundle\ApiManager\ApiUrls;
use OulalaiFrameBundle\OulalaiFrameBundle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use OulalaiFrameBundle\ApiManager\Manager;

class TeamsController extends Controller
{
    public function updateTeamsData()
    {
        $token = new TokenController();
        $connection = OulalaiFrameBundle::getContainer()->get('doctrine')->getManager()->getConnection();
        $connection->exec('DROP TABLE IF EXISTS `teams`');
        $connection->exec('CREATE TABLE IF NOT EXISTS `teams` (
                          `id` int(11) NOT NULL,
                          `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                          `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                          `league` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                          `client_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                          PRIMARY KEY (`id`)
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci');

        $access_token = $token->getToken();

        $teams = $this->get("OulalaiFrame.apiservice.api")->APIRequest('GET', $this->container->getParameter('api_base_url') . 'teams', $access_token);

        if (!empty($teams->data)) {
            foreach ($teams->data as $team) {
                $em = $connection
                    ->prepare(
                        'INSERT INTO teams
                    (
                    `id`,
                    `name`,
                    `image`,
                    `league`
                    )
                    VALUES
                    (
                    :id,
                    :name,
                    :image,
                    :league
                    );'
                    );
                $em->bindValue('id', intval($team->id));
                $em->bindValue('name', $team->name);
                $em->bindValue('image', $team->image);
                $em->bindValue('league', $team->league);
                $em->execute();
            }
        }
    }
}
