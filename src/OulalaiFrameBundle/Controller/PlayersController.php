<?php
namespace OulalaiFrameBundle\Controller;

use Buzz\Message\Response;
use OulalaiFrameBundle\ApiManager\ApiUrls;
use OulalaiFrameBundle\OulalaiFrameBundle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use OulalaiFrameBundle\ApiManager\Manager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class PlayersController extends Controller
{
    public function indexAction()
    {
        $session=new Session();
        $request = Request::createFromGlobals();
        if (! $session->has('user_ref')) {
            return $this->redirectToRoute('oulalai_frame', array(), 301);
        }

        $clientId =$session->get('client');
        $style = $this->get('OulalaiFrame.repository.config')->getUserStyle($clientId);

        $css_version = $this->getParameter('css_version');
        $js_version = $this->getParameter('js_version');

        $menu = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('show_menu', $clientId);

        $iPlayerID = $request->request->get('playerid');
        $upcomingfixtures = $this->get('OulalaiFrame.repository.players')->getPlayersStates($iPlayerID, true, true);
        $nextfixture = '';
        if (! empty($upcomingfixtures)) {
            $nextfixture = $upcomingfixtures[0];
        }
        $pointspergame = '';
        $playerdetails = $this->get('OulalaiFrame.repository.players')->getPlayerProfileDetails($iPlayerID);
        $aFullStats = $this->get('OulalaiFrame.repository.players')->getPlayersStates($iPlayerID);
        $aRecentStats = array_reverse(array_slice((array)$aFullStats, 0, 5));
        $aChartLabels = array();
        $aChartData = array();
        foreach (array_reverse(array_slice((array)$aFullStats, 0, 6)) as $aRecord) {
            $aChartLabels[] = "'".date("d-m-Y", strtotime(explode(" ", $aRecord['matchDate'])[0]))."'";
            $aChartData[] = $aRecord['playerScore'];
        }

        $aRecentStats = array_reverse(array_slice((array)$aFullStats, 0, 5));
        // convert to comma seperated string for use in the chart
        $aChartLabels = implode(',', $aChartLabels);
        $aChartData = implode(',', $aChartData);

        if ($request->isXmlHttpRequest()) {
            $iPlayerIDFrom = $request->request->get('from');
            $bStarting = $request->request->get('starting');
            $bStartingSub = 0;
            $bStartingField = 1;

            if ($iPlayerIDFrom == 'from-result') {
                $iPlayerIDFrom = 'addPlayer';
                $bStarting = 1;
            } else {
                $iPlayerIDFrom = 'swapPlayer';
                if ($bStarting == 'from-sub') {
                    $bStartingField = 1;
                    $bStartingSub = 0;
                } elseif ($bStarting == 'from-field') {
                    $bStartingField = 0;
                    $bStartingSub = 1;
                }
            }

            return $this->render('templates/controls/player-profile-dialog.html.twig',
                array("playerdetails"    => $playerdetails,
                    'pastmatches'        => $aFullStats,
                    'upcomingfixtures'    => $upcomingfixtures,
                    'recentstats'        => $aRecentStats,
                    'chartlabels'        => $aChartLabels,
                    'chartdata'        => $aChartData,
                    'nextfixture'        =>    $nextfixture,
                    'from'                => $iPlayerIDFrom,
                    'bStartingField'    => $bStartingField,
                    'bStartingSub'        => $bStartingSub,
                    'pointspergame'    =>  $pointspergame,
                    'style'            => $style.$css_version,
                    'css_version' => $css_version,
                    'js_version' => $js_version,
                    'menu'              => $menu
                ));
        }
    }
    public function playerDetailsAction()
    {
        $request = Request::createFromGlobals();
        $clientId =1;
        $style = $this->get('OulalaiFrame.repository.config')->getUserStyle($clientId);

        $css_version = $this->getParameter('css_version');
        $js_version = $this->getParameter('js_version');

        $menu = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('show_menu', $clientId);

        $iPlayerID = $request->request->get('playerid');
        $upcomingfixtures = $this->get('OulalaiFrame.repository.players')->getPlayersStates($iPlayerID, true, true);
        $nextfixture = '';
        if (! empty($upcomingfixtures)) {
            $nextfixture = $upcomingfixtures[0];
        }
        $pointspergame = '';
        $playerdetails = $this->get('OulalaiFrame.repository.players')->getPlayerProfileDetails($iPlayerID);
        $aFullStats = $this->get('OulalaiFrame.repository.players')->getPlayersStates($iPlayerID);
        $aRecentStats = array_reverse(array_slice((array)$aFullStats, 0, 5));
        $aChartLabels = array();
        $aChartData = array();
        foreach (array_reverse(array_slice((array)$aFullStats, 0, 6)) as $aRecord) {
            $aChartLabels[] = "'".date("d-m-Y", strtotime(explode(" ", $aRecord['matchDate'])[0]))."'";
            $aChartData[] = $aRecord['playerScore'];
        }

        $aRecentStats = array_reverse(array_slice((array)$aFullStats, 0, 5));
        // convert to comma seperated string for use in the chart
        $aChartLabels = implode(',', $aChartLabels);
        $aChartData = implode(',', $aChartData);

        if ($request->isXmlHttpRequest()) {
            $iPlayerIDFrom = $request->request->get('from');
            $bStarting = $request->request->get('starting');
            $bStartingSub = 0;
            $bStartingField = 1;

            if ($iPlayerIDFrom == 'from-result') {
                $iPlayerIDFrom = 'addPlayer';
                $bStarting = 1;
            } else {
                $iPlayerIDFrom = 'swapPlayer';
                if ($bStarting == 'from-sub') {
                    $bStartingField = 1;
                    $bStartingSub = 0;
                } elseif ($bStarting == 'from-field') {
                    $bStartingField = 0;
                    $bStartingSub = 1;
                }
            }
//            return new \Symfony\Component\HttpFoundation\Response('success');
            return $this->render('templates/homepage/player-profile.html.twig',
                array("playerdetails"    => $playerdetails,
                    'pastmatches'        => $aFullStats,
                    'upcomingfixtures'    => $upcomingfixtures,
                    'recentstats'        => $aRecentStats,
                    'chartlabels'        => $aChartLabels,
                    'chartdata'        => $aChartData,
                    'nextfixture'        =>    $nextfixture,
                    'from'                => $iPlayerIDFrom,
                    'bStartingField'    => $bStartingField,
                    'bStartingSub'        => $bStartingSub,
                    'pointspergame'    =>  $pointspergame,
                    'style'            => $style.$css_version,
                    'css_version' => $css_version,
                    'js_version' => $js_version,
                    'menu'              => $menu
                ));
        }
    }

    public function assignPlayerDetailsAction(Request $request)
    {
        $session=new Session();
        $request = Request::createFromGlobals();
        if (! $session->has('user_ref')) {
            return $this->redirectToRoute('oulalai_frame', array(), 301);
        }
        $userRef = $session->get('user_ref');

        $iPlayerID = (int)$request->get("pid");
        $iMatchDayID = (int)$request->get('matchday');
        $iWeekID = (int)$request->get('week');

        if ($request->isXmlHttpRequest()) {
            if ($iPlayerID && $iMatchDayID):
                $aPlayerDetails = $this->get('OulalaiFrame.repository.players')->getPlayerProfileDetails($iPlayerID);

            if ($aPlayerDetails) {
                $stats = $this->get('OulalaiFrame.repository.players')->getPlayerMatchBreakdown($iPlayerID, $iMatchDayID);
                $hasStats = false;
                foreach ($stats as $stat) {
                    if ((int)$stat['occurences'] > 0) {
                        $hasStats = true;
                        break;
                    }
                }
                $params = array(
                        "playerdetails"    => $aPlayerDetails,
                        'player_id'    => $iPlayerID,
                        'matchday_id'    => $iMatchDayID,
                        "matchstatistics" => $stats,
                        "hasmatchstatistics" => $hasStats,
                        'matchdetails'  => $this->get('OulalaiFrame.repository.players')->getFixtureByMatchDayAndTeamID($iMatchDayID, $aPlayerDetails['team_id'])
                    );
                if ($iWeekID) {
                    $params['memberplayer'] = $this->get('OulalaiFrame.repository.players')->getPlayerDetailsByCompetitionWeek($iWeekID, $iPlayerID, $userRef, $iMatchDayID);
                }

                return $this->render('templates/controls/player-profile-dialog.html.twig', $params);
            }
            endif;
        }
    }
}
