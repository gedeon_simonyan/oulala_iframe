<?php

namespace OulalaiFrameBundle\Controller;

use OulalaiFrameBundle\ApiManager\ApiUrls;
use OulalaiFrameBundle\OulalaiFrameBundle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use OulalaiFrameBundle\ApiManager\Manager;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\RedirectResponse;

class LeaguesController extends Controller
{
    private $sRef = null;
    private $bLeagueMember = false;
    private $iLang = 1;

    /**
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $session = new Session();
        $client_data = $this->get('OulalaiFrame.repository.config')->getClientData($request->query->all());
        $pager = 1;
        $row_count = 100;
        $clientId = $session->get('client');
        $style = $this->get('OulalaiFrame.repository.config')->getUserStyle($clientId);
  
        $css_version = $this->getParameter('css_version');
        $js_version = $this->getParameter('js_version');
        $menu = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('show_menu', $clientId);
        $termsUrl = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('terms_url', $clientId);
        $showCurrnecy = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('show_currency', $clientId);
        $leaguesList = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('client_leagues', $clientId);
        $loginUrl = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('login_url', $clientId);
        $allowRedirect = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('allow_redirect', $clientId);
        $currencies = $this->get('OulalaiFrame.repository.currency')->getCurrencies();
        $savedTeamId=isset($request->cookies->all()['teamId'])?$request->cookies->all()['teamId']:0;
        $user_ref=$session->get('user_ref');
        if($savedTeamId && $user_ref){
            $this->get('OulalaiFrame.repository.savedSquad')->updateSquadUserRef($savedTeamId, $user_ref);
        }

        $country_leagues = $this->get('OulalaiFrame.repository.leagues')->getCountryLeague($leaguesList?$leaguesList:'0');
        $sOrdering = "ORDER BY fromDate ASC";
        $sSorting = "";
        $sWhere = "WHERE lm.`match_id`=m.`match_id`";

        $minCost=$this->get('OulalaiFrame.repository.config')->getConfigByKeyName('min_join_cost', $clientId);
        $leagues = $this->get('OulalaiFrame.repository.leagues')->availableLeagues($sSorting, $sWhere, $sOrdering, $minCost?$minCost:1, $leaguesList?$leaguesList:'0');

        $paginate = null;//ceil($leagues_count / $row_count);

        if ($request->isXmlHttpRequest()) {
            $league_ids = $request->request->get('league');
            $pager = $request->request->get('pager');
            $orderBy = $request->request->get('orderby');
            $leagueName = htmlspecialchars(trim($request->request->get('field')));
            $entryFee_from = $request->request->get('entryFee_from');
            $entryFee_to = $request->request->get('entryFee_to');
            $contestTypes = $request->request->get('contestTypes');
            $mobileFilterByType = $request->request->get('mobileFilterByType');
            $orderStarts = $orderBy;
            $offset = ($pager - 1) * $row_count;
            if (!$pager) {
                $pager = 1;
            }

            if (!empty($orderBy) || !empty($mobileFilterByType)) {
                if (!empty($orderBy)) {
                    $sort = explode("-", $orderBy);
                }
                if (!empty($mobileFilterByType)) {
                    //$sort = explode("-", $mobileFilterByType);
                }
                $orderBy = empty($sort[0]) ? "timeToStart" : $sort[0];
                $order = empty($sort[1]) ? "ASC" : $sort[1];
                $sOrdering = " ORDER BY " . $orderBy . " " . $order;
            }
            if (!empty($leagueName)) {
                $sWhere .= " AND leagues.name LIKE '%{$leagueName}%' ";
            }
            if (!empty($entryFee_from) && !empty($entryFee_to)) {
                $sWhere .= " AND leagues.entryFee BETWEEN {$entryFee_from} AND {$entryFee_to} ";
            }
            if ($contestTypes == 'head-to-head') {
                $sWhere .= " AND leagues.maxParticipants = 2 ";
            } elseif ($contestTypes == 'leagues') {
                $sWhere .= " AND leagues.maxParticipants != 2 ";
            }
            if (!empty($league_ids)) {
                $list = implode(',', $league_ids);
                $sWhere .= ' AND m.`league_id` IN (' . $list . ')';
            }

            if ($pager == 1) {
                $sSorting .= $sOrdering . ' LIMIT ' . $row_count;
            } else {
                $sSorting .= $sOrdering . ' LIMIT ' . $offset . ', ' . $row_count;
            }

            $leagues = $this->get('OulalaiFrame.repository.leagues')->availableLeagues($sSorting, $sWhere, null, $minCost, $leaguesList?$leaguesList:'0');
            $cookies = $request->cookies;
            $source=$cookies->get('currency');
            if (!$source) {
                $source='EUR';
            }
            if (!empty($source)) {
                $exchangeValue = $this->get('OulalaiFrame.repository.currency')->exchangeRate($source);
                $entryPrice = round($exchangeValue['currency_value'], 2);
                $prize = round($exchangeValue['currency_value'], 3);
            }


            foreach ($leagues as $key => $value) {
                $leagues[$key]['class'] = strtolower($exchangeValue['currency_key']);
                if ($value['entryFee'] != 0 && $value['prize'] != 0) {
                    $leagues[$key]['entryFee'] = $value['entryFee'] * $entryPrice;
                    $leagues[$key]['prize'] = $value['prize'] * $prize;
                }
            }
            $maxValue = 200 * $prize;
            return $this->render('templates/controls/avilable-leagues-list.html.twig',
                array('leagues' => $leagues,
                    'paginate' => $paginate,
                    'showJoinButton' => true,
                    'orderStarts' => $orderStarts,
                    'maxValue' => $maxValue,
                    'currency_class'=>strtolower($exchangeValue['currency_key']),
                    'loginUrl'=>$loginUrl,
                    'allowRedirect' => $allowRedirect
                ));
        }
        $cookies = $request->cookies;
        $source=$cookies->get('currency');
        if (!$source) {
            $source='EUR';
        }
        if (!empty($source)) {
            $exchangeValue = $this->get('OulalaiFrame.repository.currency')->exchangeRate($source);
            $entryPrice = round($exchangeValue['currency_value'], 2);
            $prize = round($exchangeValue['currency_value'], 3);
        }
        $maxValue = 200 * $prize;
        if ($maxValue == 0) {
            $maxValue = null;
        }
        foreach ($leagues as $key => $value) {
            $leagues[$key]['class'] = strtolower($exchangeValue['currency_key']);
            if ($value['entryFee'] != 0 && $value['prize'] != 0) {
                $leagues[$key]['entryFee'] = $value['entryFee'] * $entryPrice;
                $leagues[$key]['prize'] = $value['prize'] * $prize;
            }
        }

        if (!isset($client_data['error'])) {
            return $this->render('templates/availableleagues.html.twig',
                array('leagues' => $leagues,
                    'countries' => $country_leagues,
                    'paginate' => $paginate,
                    'style' => $style . $css_version,
                    'css_version' => $css_version,
                    'js_version' => $js_version,
                    'menu' => $menu,
                    'orderStarts'=>'fromDate-ASC',
                    'currencies' => $currencies,
                    'source'=>$source?$source:'EUR',
                    'maxValue' => $maxValue,
                    'terms_url' => $termsUrl,
                    'currency_class'=>strtolower($exchangeValue['currency_key']),
                    'prize' => $prize,
                    'showCurrency'=>$showCurrnecy,
                    'loginUrl'=>$loginUrl,
                    'allowRedirect' => $allowRedirect,
                ));
        }
        if (isset($client_data['error'])) {
            $error = $client_data['error'];
        } else {
            $error = 'Invalid client';
        }
        return $this->render('templates/login.html.twig', ['error' => $error]);
    }

    public function createLeagueAction($iSavedTeamId = null)
    {
        $session = new Session();
        $request = Request::createFromGlobals();

        if (!$session->has('user_ref')) {
            $referer = $request->headers->get('referer');
            return new RedirectResponse($referer);
        }
        $this->sRef = $session->get('user_ref');

        $lang = explode('/', trim($request->getPathInfo(), '/'));
        if ($lang[0] == 'fr') {
            $this->iLang = 2;
        }
        $prizes = $this->get('OulalaiFrame.repository.prizes')->getPrizes($this->iLang);

        $clientId = $session->get('client');
        $style = $this->get('OulalaiFrame.repository.config')->getUserStyle($clientId);

        $css_version = $this->getParameter('css_version');
        $js_version = $this->getParameter('js_version');

        $menu = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('show_menu', $clientId);
        $minCost=$this->get('OulalaiFrame.repository.config')->getConfigByKeyName('min_join_cost', $clientId);
        $showCurrnecy = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('show_currency', $clientId);
        $access_token = $this->get('OulalaiFrame.repository.token')->getClientToken($clientId)->access_token;

        $leaguetypeid = $request->request->get('leaguetypeid');
        $leagueName = $request->request->get('leagueName');
        $entryFee = ($leaguetypeid == 1) ? $request->request->get('leagueentryfee') : $request->request->get('h2hentryfee');
        $maxParticipants = ($leaguetypeid == 1) ? $request->request->get('maxParticipants') : 2;
        if ($maxParticipants == -1) {
            $maxParticipants = 0;
        }
        if ($entryFee == -1) {
            $entryFee = 0;
        }
        $prize = $request->request->get('prize');
        $matches_list = $request->request->get('matches');
        $gameDate = $request->request->get('date');

        $date = new \DateTime();

        $time = $date->format('H:i:s');
        $leaguesList = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('client_leagues', $clientId);
        $gameDate = $gameDate ? $gameDate : $date->format('Y-m-d');
        $confDate = '00:30:00';

        //$confDate = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('league_date', 0);
        if ($gameDate == $date->format('Y-m-d')) {
            $matches = $this->get('OulalaiFrame.repository.matches')->getFromDate($gameDate . ' ' . $time, $confDate);
            $country_leagues = $this->get('OulalaiFrame.repository.leagues')->getLeagues($gameDate . ' ' . $time, $leaguesList);
        } else {
            $matches = $this->get('OulalaiFrame.repository.matches')->getFromDate($gameDate . ' 00:00:00', $confDate);
            $country_leagues = $this->get('OulalaiFrame.repository.leagues')->getLeagues($gameDate . ' 00:00:00', $leaguesList);
        }

        $method = $request->getRealMethod();
        $availableMatchDates = $this->get('OulalaiFrame.repository.matches')->getMatchesDate(3, $confDate);

        if ($request->isXmlHttpRequest()) {
            return $this->render('templates/controls/league-matches-list.html.twig', array(
                    'matches' => $matches,
                    'dates' => $availableMatchDates,
                    'realLeague' => $country_leagues)
            );
        }
        $cookies = $request->cookies;
        $source=$cookies->get('currency');
        if (!$source) {
            $source="EUR";
        }
        if (!empty($source)) {
            $exchangeValue = $this->get('OulalaiFrame.repository.currency')->exchangeRate($source);
            $currency = round($exchangeValue['currency_value'], 2);
        }

        $currencyClass = strtolower($exchangeValue['currency_key']);
        if ($method == "POST") {
            $form_params = [
                'user' => $this->sRef, //test user ref param
                'leagueName' => $leagueName,
                'entryFee' => (float)$entryFee,
                'maxParticipants' => (int)$maxParticipants,
                'leaguetypeid' => $leaguetypeid,
                'prize' => (int)$prize,
                'matches' => array_map(create_function('$value', 'return (int)$value;'), $matches_list),
                'is_admin'=>0
            ];

            $params = $form_params;

            if ($entryFee!=0 && $entryFee<$minCost) {
                $errors[0] = "Minimum league entry fee should not be less than %mincost% &euro;";
                $errors['status'] = 'fail';
                return $this->render('templates/createLeagues.html.twig',
                    array('error' => $errors,
                        'matches' => $matches,
                        'mincost' => $minCost,
                        'params' => $form_params,
                        'dates' => $availableMatchDates,
                        'prizes' => $prizes,
                        'realLeague' => $country_leagues,
                        'style' => $style.$css_version,
                        'css_version' => $css_version,
                        'js_version' => $js_version,
                        'menu' => $menu,
                        'currency' => $currency,
                        'currency_class' => $currencyClass
                    ));
            }
            $league = $this->get("OulalaiFrame.apiservice.api")->APIRequest('POST', $this->container->getParameter('api_base_url') . 'leagues', $access_token, $params);

            if ($league->status == 'fail') {
                foreach ($league->data->violations as $value) {
                    $errors[] = $value;
                }
                $errors['status'] = $league->status;
                return $this->render('templates/createLeagues.html.twig',
                    array('error' => $errors,
                        'matches' => $matches,
                        'params' => $form_params,
                        'dates' => $availableMatchDates,
                        'prizes' => $prizes,
                        'realLeague' => $country_leagues,
                        'style' => $style . $css_version,
                        'css_version' => $css_version,
                        'js_version' => $js_version,
                        'menu' => $menu,
                        'currency' => $currency,
                        'currency_class' => $currencyClass,
                        'mincost'=>$minCost
                        )
                );
            }

            if ($league->status == 'failed') {
                $errors[] = $league->data;
                $errors['status'] = $league->status;
                return $this->render('templates/createLeagues.html.twig',
                    array('error' => $errors,
                        'matches' => $matches,
                        'params' => $form_params,
                        'dates' => $availableMatchDates,
                        'prizes' => $prizes,
                        'realLeague' => $country_leagues,
                        'style' => $style . $css_version,
                        'css_version' => $css_version,
                        'js_version' => $js_version,
                        'menu' => $menu,
                        'mincost'=>$minCost,
                        'currency' => $currency,
                    ));
            }

            if ($league->status == 'success') {
                $this->get('OulalaiFrame.repository.matches')->updateOneLeagueMatchesAndUserData($access_token, $league->data->leagueId);
                $this->get('OulalaiFrame.repository.leagues')->updateOneLeagueData($access_token, $league->data->leagueId, 0);

                $this->sendInvitationEmailToUsersAction($request);
                if (!empty($iSavedTeamId)) {
                    $iLeagueId = $league->data->leagueId;
//                    $iJoinedTeamId = $this->get('OulalaiFrame.repository.squad')->joinToLeagueWithSavedTeam(intval($iSavedTeamId), $iLeagueId);
                    $iJoinedTeamId = $this->get('OulalaiFrame.repository.savedsquad')->joinLeagueWithSavedTeam(intval($iSavedTeamId), $iLeagueId);

                    if (isset($iJoinedTeamId['teamId'])) {
                        echo "ok";
                        return $this->redirect($this->generateUrl('edit_squads',
                            array('leagueId' => $iLeagueId,
                                'iCurrentTeamId' => $iJoinedTeamId['teamId'])));
                    } elseif (isset($iJoinedTeamId['error'])) {
                        $error=[0 => 'You have some players from another league, please edit your team. <br>'.$iJoinedTeamId['error']['errors']];
                        return $this->render('templates/createLeagues.html.twig',
                            array('matches' => $matches,
                                'dates' => $availableMatchDates,
                                'prizes' => $prizes,
                                'realLeague' => $country_leagues,
                                'error' => $error,
                                'params' => '',
                                'style' => $style . $css_version,
                                'css_version' => $css_version,
                                'js_version' => $js_version,
                                'menu' => $menu,
                                'mincost'=>$minCost,
                                'currency' => $currency,
                            ));
                    }
                } else {
                    $tmpTeam=$request->request->get('tmpTeam');
                    return $this->redirect($this->generateUrl('create_squads',
                    array('leagueId' => $league->data->leagueId)).'?operator_id='.$session->get('alias').'&token='.$session->get('token').'&tmpteam='.$tmpTeam);
                }
            }
        }

        return $this->render('templates/createLeagues.html.twig',
            array('matches' => $matches,
                'dates' => $availableMatchDates,
                'prizes' => $prizes,
                'realLeague' => $country_leagues,
                'params' => '',
                'mincost' => $minCost,
                'style' => $style.$css_version,
                'css_version' => $css_version,
                'js_version' => $js_version,
                'menu' => $menu,
                'currency' => $currency,
                'currency_class' => $currencyClass,
                'showCurrency'=>$showCurrnecy
            ));
    }

    public function detailsAction(Request $request)
    {
        $leaguesId = $request->request->get('leagueId');
        $search = $request->request->get('search');
        $access_token = $this->get('OulalaiFrame.repository.token')->getClientToken(1)->access_token;
        $this->get('OulalaiFrame.repository.matches')->updateOneLeagueMatchesAndUserData($access_token, $leaguesId);
        $lang = explode('/', trim($request->getPathInfo(), '/'));
        if ($lang[0] == 'fr') {
            $this->iLang = 2;
        }
        $showJoinButton = $request->request->get('showJoinButton');
        $activetab = $request->request->get('activetab');
        $matches = $this->get('OulalaiFrame.repository.leagues')->matchLeaguesDetails($leaguesId);
        $render = array();

        if (!empty($matches)) {
            foreach ($matches as $ml) {
                $ml['home_image'] = str_replace('//', '/', $ml['home_image']);
                $ml['home_image'] = str_replace(':/', '://', $ml['home_image']);
                $ml['away_image'] = str_replace('//', '/', $ml['away_image']);
                $ml['away_image'] = str_replace(':/', '://', $ml['away_image']);
                array_push($render, $ml);
            }
        }
        if ($request->isXmlHttpRequest() && isset($search)) {
            $users = $this->get('OulalaiFrame.repository.leagues')->leagueUserDetails($leaguesId, $search);
            return new Response(json_encode($users));
        }
        $matcheDates = $this->get('OulalaiFrame.repository.leagues')->getLeagueMatchDates($leaguesId);
        $users = $this->get('OulalaiFrame.repository.leagues')->leagueUserDetails($leaguesId);

        $leagues = $this->get('OulalaiFrame.repository.leagues')->getLeaguesById($leaguesId);
        $aRewardOption = $this->get('OulalaiFrame.repository.prizes')->getRewardDetails($leaguesId, $this->iLang);
        $aResults = null;

        //If the reward is first second third, calculate the amounts
        if ($aRewardOption['fk_member_league_template_id'] == 2) {
            $fPrizePool = $this->get('OulalaiFrame.repository.prizes')->getEntriesAndPrizes($leaguesId)['prizes'];
            $aResults = $this->get('OulalaiFrame.repository.prizes')->retrievePaymentOptionAmounts($aRewardOption['fk_member_league_template_id']);

            foreach ($aResults as &$aResult) {
                $aResult['winnings'] = bcmul($aResult['amount'], $fPrizePool, 2);
            }
        }

        return $this->render('templates/controls/dialog-league-fixtures.html.twig',
            array('matches' => $render,
                'leagues' => $leagues,
                'leagueId' => $leaguesId,
                'users' => $users,
                'matcheDates' => $matcheDates,
                'activetab' => $activetab,
                'showJoinButton' => $showJoinButton,
                'rewardOption' => $aRewardOption,
                'rewardOptionWinnings' => $aResults));
    }

    public function leagueDetailPageAction($leagueId, $sMemberName = '')
    {
        $request = Request::createFromGlobals();
        $session = new Session();
        $this->sRef = $session->get('user_ref');
        $iLeagueID = ($leagueId ? intval($leagueId) : false);

        if (!$iLeagueID) {
            return new RedirectResponse('/' . $request->getLocale());
        }
        $aLeague = $this->get('OulalaiFrame.repository.leagues')->getMemberLeague($iLeagueID);
        $aLeaguesList = array();
        if (empty($aLeague) || !$aLeague[0]['is_public']) {
            return new RedirectResponse('/' . $request->getLocale());
        }
        $clientId = $session->get('client');
        $style = $this->get('OulalaiFrame.repository.config')->getUserStyle($clientId);
        $css_version = $this->getParameter('css_version');
        $js_version = $this->getParameter('js_version');
        $menu = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('show_menu', $clientId);

        $this->setIsLeagueMember($this->get('OulalaiFrame.repository.leagues')->isMemberPartOfLeague($iLeagueID, $this->sRef));
        $showJoinButton = $this->get('OulalaiFrame.repository.leagues')->showJoinButton($iLeagueID, $this->sRef);
        if ($request->isXmlHttpRequest()) {
            $sMemberName = $request->request->get('member');
            $iLeagueID = $request->request->get('member_league_id');
            $aLeague = $this->get('OulalaiFrame.repository.leagues')->getMemberLeague($iLeagueID, $sMemberName);
            foreach ($aLeague as $league) {
                if ($this->get('OulalaiFrame.repository.profile')->hasPreference($this->sRef, 13)) {
                    $league['team_link'] = "/squads/create/" . $league->member_league_id . "/" . $league->member_team_id;
                }
                array_push($aLeaguesList, $league);
            }
            $result = array(
                'memberleague' => $aLeaguesList,
                'leagueid' => $iLeagueID,
                'ispublic' => (empty($aLeague[0]) ? 0 : $aLeague[0]['is_public']),
                'isLeagueMember' => $this->getIsLeagueMember(),
                'show_join_button' => $showJoinButton
            );
            return $this->render('templates/controls/league-details-list.html.twig', $result);
        }

        foreach ($aLeague as $league) {
            if ($this->get('OulalaiFrame.repository.profile')->hasPreference($this->sRef, 13)) {
                $league['team_link'] = "/squads/create/" . $league['member_league_id'] . "/" . $league['member_team_id'];
            }
            array_push($aLeaguesList, $league);
        }
        $cookies = $request->cookies;
        $source=$cookies->get('currency');
        if (!$source) {
            $source="EUR";
        }
        if (!empty($source)) {
            $exchangeValue = $this->get('OulalaiFrame.repository.currency')->exchangeRate($source);
            $entryPrice = round($exchangeValue['currency_value'], 2);
            $prize = round($exchangeValue['currency_value'], 3);
        }

        foreach ($aLeaguesList as $key => $value) {
            $aLeaguesList[$key]['class'] = strtolower($exchangeValue['currency_key']);
            if ($value['entry_fee'] != 0 && $value['prize'] != 0) {
                $aLeaguesList[$key]['entry_fee'] = $value['entry_fee'] * $entryPrice;
                $aLeaguesList[$key]['prize'] = $value['prize'] * $prize;
            }
        }

        $result = array(
            'style' => $style . $css_version,
            'css_version' => $css_version,
            'js_version' => $js_version,
            'memberleague' => $aLeaguesList,
            'leagueid' => $iLeagueID,
            'pagecount' => $this->get('OulalaiFrame.repository.leagues')->getPageCount($iLeagueID),
            'ispublic' => (empty($aLeague[0]) ? 0 : $aLeague[0]['is_public']),
            'isLeagueMember' => $this->getIsLeagueMember(),
            'client_allow_chat' => $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('client_allow_chat', $clientId),
            'show_join_button' => $showJoinButton,
            'member' => $this->sRef,
            'client_id' => $clientId,
            'menu' => $menu
        );

        return $this->render('templates/league-detailpage.html.twig', $result);
    }

    public function sendInvitationEmailToUsersAction(Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            $session = new Session();
            $clientId = $session->get('client');
            $access_token = $this->get('OulalaiFrame.repository.token')->getClientToken($clientId)->access_token;

            $client_email_url = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('client_email_url', $clientId);
            $refs = "";
            if (empty($request->get('users'))) {
                $response = new Response(json_encode(array('error' => 'Please select users to send emails')));
                $response->headers->set('Content-Type', 'application/json');
                return $response;
            }

            $users = explode(';', $request->get('users'));
            foreach ($users as $user) {
                $ref = $this->get('OulalaiFrame.repository.profile')->getUserRefByUsername($user, $clientId);
                $refs[] = $ref['userref'];
            }

            $refs = implode(';', $refs);

            $leagues = $request->get('leagues');
            $emailtext = $request->get('emailtext');
            $sUserRef = $session->get('user_ref');

            $sPostFields = "refs=$refs&leagues=$leagues&emailtext=$emailtext&LanguageID=1&sUserRef=$sUserRef";

            $data = $this->get("OulalaiFrame.apiservice.apiguzzle")->APIRequest('POST', $client_email_url, '', $sPostFields);
            if (!empty($data[0]->successful_invites) && ($data[0]->successful_invites > 0)) {
                $json = json_encode($data);
                $response = new Response($json, 200);
                $response->headers->set('Content-Type', 'application/json');
                return $response;
            }

            $response = new Response(json_encode(array('error' => 'Something was wrong while sending data.Please try again later.')));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
    }

    public function inviteMemberAction(Request $request)
    {
        $session = new Session();
        $userRef = $session->get('user_ref');
        $client_id = $session->get('client');
        $access_token = $this->get('OulalaiFrame.repository.token')->getClientToken($client_id)->access_token;
        $userId = $this->get('OulalaiFrame.repository.notification')->getUidByRef($userRef);
        $inviteUrl = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('invite_member', $client_id);

        $members = $request->request->get("members");
        $leagueName = $request->request->get("leagueName") ? $request->request->get("leagueName") : 'null';

        if (is_array($members) && count($members) >= 1) {
            $sendData = ['leagueName' => $leagueName, 'uid' => $userId];
            $sendData['emails'] = $members;
            $data = $this->get("OulalaiFrame.apiservice.apiguzzle")->APIRequest('POST', $inviteUrl, '', json_encode($sendData));

            if ($data->sent == "ok") {
                return new Response(json_encode("Success"));
            } else {
                return new Response(json_encode("Error"));
            }
        }
        return new Response("Error");
    }

    public function testEmailAction(Request $request)
    {
        $data = json_decode($request->getContent());

        if (!empty($data[0]) && isset($data[0]->uid) && isset($data[0]->emails)) {
            return new Response(json_encode(['success' => 'ok']));
        }
        return new Response(json_encode(['success' => 'error']));
    }


    public function leagueFixturesAction(Request $request)
    {
        $session = new Session();
        if (!$request->query->get('day')) {
            $date = new \DateTime();
            $dMatchesDay = $date->format('Y-m-d');
            $aSelectedDay = $this->get('OulalaiFrame.repository.leagues')->getMatchesDayDetailByDate($dMatchesDay, 'next', true);
            $nextDay = $this->get('OulalaiFrame.repository.leagues')->getMatchesDayDetailByDate($aSelectedDay['match_date'], 'next');
            $previewDay = $this->get('OulalaiFrame.repository.leagues')->getMatchesDayDetailByDate($aSelectedDay['match_date'], 'preview');
        } else {
            $aSelectedDay = $this->get('OulalaiFrame.repository.leagues')->getMatchesDayDetailByDate($request->query->get('day'));
            $nextDay = $this->get('OulalaiFrame.repository.leagues')->getMatchesDayDetailByDate($aSelectedDay['match_date'], 'next');
            $previewDay = $this->get('OulalaiFrame.repository.leagues')->getMatchesDayDetailByDate($aSelectedDay['match_date'], 'preview');
        }
        $clientId = $session->get('client');
        $style = $this->get('OulalaiFrame.repository.config')->getUserStyle($clientId);
        $css_version = $this->getParameter('css_version');
        $js_version = $this->getParameter('js_version');

        $menu = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('show_menu', $clientId);

        if ($request->isXmlHttpRequest()) {
//            if ($request->get('finalised')) {
//            $aSelectedDay = $this->get('OulalaiFrame.repository.leagues')->getMatchesDayDetailByDate($request->query->get('day'));
//                $params = $this->assignCurrentFixtureAndPlayersData($request->get('matchid'));

//            } else {
            $playersperteam=$this->get('OulalaiFrame.repository.leagues')->getPlayersPerTeams($request->get('matchid'));
            $currentFixtures=$this->get('OulalaiFrame.repository.leagues')->getOulalaFixturesByMatchID($request->get('matchid'));

            $nodeUrl = $this->getParameter('node_url');
            $params = array(
                    'currentfixture' => $currentFixtures,
                    'playersperteam'=>$playersperteam,
                    'val'=>$request->get('index'),
                    'nodeUrl' => $nodeUrl
                );

//            }

            return $this->render('templates/controls/livefixtures-live-dropdown.html.twig', $params);
        }
        $nodeUrl = $this->getParameter('node_fixtures_url');
        $eventTypes=$this->get('OulalaiFrame.repository.inplay')->getEventTypes();
        $params = array(
            'style' => $style . $css_version,
            'css_version' => $css_version,
            'js_version' => $js_version,
            'menu' => $menu,
            'selectedday' => $aSelectedDay,
            'nextday' => $nextDay,
            'eventtypes' => $eventTypes,
            'previewday' => $previewDay,
            'fixtures' => $this->get('OulalaiFrame.repository.leagues')->getOulalaFixturesByDate($aSelectedDay['match_date']),
            'playerPositions' => $this->get('OulalaiFrame.repository.players')->getAllPlayerPositions(),
            'nodeUrl' => $nodeUrl
        );
        return $this->render('templates/fixtures.html.twig', $params);
    }

    private function assignCurrentFixtureAndPlayersData($iMatchID)
    {
        $session = new Session();
        $iMatchID = (int)$iMatchID;

        $aDummyPlayerRecord = array(
            'starting_11' => 1
        );

        $aResult = $this->get('OulalaiFrame.repository.leagues')->getOulalaFixturesByMatchID($iMatchID);

        $aHomeTeamIDs = array();
        $aAwayTeamIDs = array();
        foreach ($aResult['home_team_players'] as $aPlayer) {
            array_push($aHomeTeamIDs, $aPlayer['player_id']);
        }
        foreach ($aResult['away_team_players'] as $aPlayer) {
            array_push($aAwayTeamIDs, $aPlayer['player_id']);
        }
        if ((count($aAwayTeamIDs) > 0) && (count($aHomeTeamIDs) > 0)) {
            $aHomeMatchEvents = array();
            $aAwayMatchEvents = array();
            $aFullMatchEvents = array();

            $iStarting11 = 0;
            foreach ($aResult['home_team_players'] as $key => $value) {
                $aPlayerEvents = $this->get('OulalaiFrame.repository.players')->getPlayerEvents($value['player_id'], $iMatchID, 0, 1000);
                $aResult['home_team_players'][$key]['events'] = $aPlayerEvents;
                if ($value['starting_11']) {
                    $iStarting11++;
                }
                foreach ($aPlayerEvents as $aEvent) {
                    if ($aEvent['event_type_id'] != 7) {
                        $aEvent = array_merge($aEvent, array('player_name' => $value['player_name']));
                        $aEvent = array_merge($aEvent, array('team' => 'home'));
                        array_push($aHomeMatchEvents, $aEvent);
                        array_push($aFullMatchEvents, $aEvent);
                    }
                }
            }
            if ($iStarting11 < 11) {
                array_splice($aResult['home_team_players'], $iStarting11, 0, array_fill(0, 11 - $iStarting11, $aDummyPlayerRecord));
            }
            $iStarting11 = 0;
            foreach ($aResult['away_team_players'] as $key => $value) {
                $aPlayerEvents = $this->get('OulalaiFrame.repository.players')->getPlayerEvents($value['player_id'], $iMatchID, 0, 1000);
                $aResult['away_team_players'][$key]['events'] = $aPlayerEvents;
                if ($value['starting_11']) {
                    $iStarting11++;
                }
                foreach ($aPlayerEvents as $aEvent) {
                    if ($aEvent['event_type_id'] != 7) {
                        $aEvent = array_merge($aEvent, array('player_name' => $value['player_name']));
                        $aEvent = array_merge($aEvent, array('team' => 'away'));
                        array_push($aAwayMatchEvents, $aEvent);
                        array_push($aFullMatchEvents, $aEvent);
                    }
                }
            }
            if ($iStarting11 < 11) {
                array_splice($aResult['away_team_players'], $iStarting11, 0, array_fill(0, 11 - $iStarting11, $aDummyPlayerRecord));
            }

            $aDummyPlayerRecord['starting_11'] = 0;
            if (count($aResult['away_team_players']) != count($aResult['home_team_players'])) {
                $iTotalPlayers = max(count($aResult['away_team_players']), count($aResult['home_team_players']));
                if ($iTotalPlayers > count($aResult['home_team_players'])) {
                    array_splice($aResult['home_team_players'], count($aResult['home_team_players']), 0, array_fill(0, $iTotalPlayers - count($aResult['home_team_players']), $aDummyPlayerRecord));
                }
                if ($iTotalPlayers > count($aResult['away_team_players'])) {
                    array_splice($aResult['away_team_players'], count($aResult['away_team_players']), 0, array_fill(0, $iTotalPlayers - count($aResult['away_team_players']), $aDummyPlayerRecord));
                }
            }

            $aFullMatchEvents = $this->subval_sort($aFullMatchEvents, 'minute');
            $aParamsResult["HomeMatchEvents"] = $aHomeMatchEvents;
            $aParamsResult["AwayMatchEvents"] = $aAwayMatchEvents;
            $aParamsResult["FullMatchEvents"] = $aFullMatchEvents;
        }

        $aParamsResult["currentfixture"] = $aResult;

        if ($session->has('user_ref')) {
            $aParamsResult["playersperteam"] = $this->get('OulalaiFrame.repository.leagues')->getPlayersPerTeams($iMatchID);
        }

        return $aParamsResult;
    }

    private function subval_sort($a, $subkey)
    {
        foreach ($a as $k => $v) {
            $b[$k] = strtolower($v[$subkey]);
        }
        asort($b);
        foreach ($b as $key => $val) {
            $c[] = $a[$key];
        }
        return $c;
    }

    private function getIsLeagueMember()
    {
        return (int)$this->bLeagueMember;
    }

    private function setIsLeagueMember($bLeagueMember)
    {
        $this->bLeagueMember = $bLeagueMember;
    }

    public function getCurrencyRateAction($currencyKey)
    {
        $exchangeValue = $this->get('OulalaiFrame.repository.currency')->exchangeRate($currencyKey);
        $rate = round($exchangeValue['currency_value'], 3);
        return new Response($rate);
    }
}
