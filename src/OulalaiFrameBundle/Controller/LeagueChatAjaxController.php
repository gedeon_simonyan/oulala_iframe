<?php
namespace OulalaiFrameBundle\Controller;

use OulalaiFrameBundle\ApiManager\ApiUrls;
use OulalaiFrameBundle\OulalaiFrameBundle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use OulalaiFrameBundle\ApiManager\Manager;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class LeagueChatAjaxController extends Controller
{
    private $iPage = 1;
    private $iLimit = 10;
    private $currentpage = 1;

    /**
     * Inserts a chat reply
     *
     */
    public function insertChatReplyAction(Request $request)
    {
        $iLeagueID = $request->get('member_league_id');
        $session = new Session();
        $sRef = $session->get('user_ref');
        $clientId = $session->get('client');
        $access_token = $this->get('OulalaiFrame.repository.token')->getClientToken($clientId)->access_token;

        if ($request->isXmlHttpRequest()) {
            $iMemberID = $this->get('OulalaiFrame.repository.token')->cleanUserRef($sRef);
            $sItemText = $request->get('item_text');
            $iItemParentID = $request->get('item_parent_id');

            $sItemText = trim(strip_tags($sItemText));

            $params = ['user' => $sRef, 'text' => $sItemText];
            $result = $this->get("OulalaiFrame.apiservice.api")->APIRequest('POST', $this->container->getParameter('api_base_url')."chats/".$iLeagueID."/".$iItemParentID, $access_token, $params);

            if ($result->status == "success") {
                /* get data from Api , update `member_league_chat_replies` table */
                $this->get('OulalaiFrame.repository.leagues')->updateLeagueChat($access_token, $iLeagueID);
            }

            return $this->render(
                'templates/controls/league-chat.html.twig',
                $this->loadMemberLeagueChats($request, $iMemberID)
            );
        }
    }

    /**
     * Inserts a chat item
     */
    public function insertChatPostAction(Request $request)
    {
        $iLeagueID = $request->get('member_league_id');
        $session = new Session();
        $sRef = $session->get('user_ref');
        $clientId = $session->get('client');
        $access_token = $this->get('OulalaiFrame.repository.token')->getClientToken($clientId)->access_token;

        if ($request->isXmlHttpRequest()) {
            $iMemberID = $this->get('OulalaiFrame.repository.token')->cleanUserRef($sRef);
            $sMessage = $request->get('message');
            $sMessage = trim(strip_tags($sMessage));
            $params = ['user' => $sRef, 'text' => $sMessage];
            $result = $this->get("OulalaiFrame.apiservice.api")->APIRequest('POST', $this->container->getParameter('api_base_url')."chats/".$iLeagueID, $access_token, $params);

            if ($result->status == "success") {
                $this->get("OulalaiFrame.repository.leagues")->updateLeagueChat($access_token, $iLeagueID);
            } else {
                $error = $result->data;
                return $this->render(
                    'templates/controls/league-chat.html.twig',
                    ['error' => $error]
                );
            }

            return $this->render(
                'templates/controls/league-chat.html.twig',
                $this->loadMemberLeagueChats($request, $iMemberID)
            );
        }
    }

    public function showLeagueReplyBoxAction(Request $request)
    {
        $is_league_member = $request->get('is_league_member');
        $iLeagueID = $request->get('member_league_id');
        $limit = $request->get('limit');
        $page = $request->get('p');
        $chat_id = $request->get('chat_id');

        if ($request->isXmlHttpRequest()) {
            $result = array(
                'id' => $chat_id,
                'limit' => $limit,
                'currentpage' => $page==0?$page=1:$page,
                'member_league_id' => $iLeagueID,
                'isLeagueMember' => $is_league_member,
            );

            return $this->render('templates/controls/dialog-league-chat-reply.html.twig', $result);
        }
    }

    public function loadChatAction(Request $request)
    {
        $session = new Session();
        $iMemberID = $session->get('user_ref');

        if ($request->isXmlHttpRequest()) {
            $result = $this->loadMemberLeagueChats($request, $iMemberID);

            return $this->render('templates/controls/league-chat.html.twig', $result);
        }

        return $this->render('templates/controls/league-chat.html.twig', array());
    }

    /**
     * Loads chat items for a member league
     */
    private function loadMemberLeagueChats($request, $iMemberID)
    {
        $is_league_member = $request->get('is_league_member');
        $iLeagueID = intval($request->request->get('member_league_id'));
        $bIsPublic = $request->request->get('is_public');
        $limit = $request->request->get('limit');
        $page = $request->request->get('p');
        $session = new Session();

        $clientId = $session->get('client');
        $access_token = $this->get('OulalaiFrame.repository.token')->getClientToken($clientId)->access_token;

        // show chat messages if the user is a member of the league, or if it is a public league
        if ($is_league_member || $bIsPublic) {
            if (is_numeric($iLeagueID)) {
                if ($page==0 || $page==1) {
                    $newLimit=0;
                } else {
                    $newLimit=($page-1)*$this->iLimit;
                }
                $this->get("OulalaiFrame.repository.leagues")->updateLeagueChat($access_token, $iLeagueID);

                $leaguechats = $this->searchMemberLeagueChatItemsWithReplies(
                    $iLeagueID,
                    $iFound,
                    $newLimit,
                     $this->iLimit
                );
            }

            $result = array(
                'pagelimit' => $this->iLimit,
                'currentpagecount' => $iFound,
                'currentpage' => $page==0?$page=1:$page,
                'pagecount' => $this->get('OulalaiFrame.repository.leagues')->getPageCount($iLeagueID),
                'leaguechats' => $leaguechats,
                'leaguId' => $iLeagueID,
                'isLeagueMember' => $this->get('OulalaiFrame.repository.leagues')->isMemberPartOfLeague(
                    $iLeagueID,
                    $iMemberID
                ),
            );

            return $result;
        }
    }

    /**
     * Gets all chat items for this member league that have replies
     *
     * @param int $iLeagueID Member league ID
     * @param int $iFoundRows Number of found rows
     * @param int $iOffset
     * @param int $iLimit Number of items on a page
     * @return array
     */
    private function searchMemberLeagueChatItemsWithReplies($iLeagueID, &$iFoundRows, $iOffset, $iLimit)
    {
        $iLimit = is_numeric($iLimit) ? $iLimit : $this->iLimit;

        $chatItem = $this->get('OulalaiFrame.repository.leagues')->leagueChatItemsWithReplies(
            $iLeagueID,
            $iFoundRows,
            $iOffset,
            $iLimit
        );
        foreach ($chatItem as $key => $value) {
            $chatItem[$key]['replies'] = $this->get('OulalaiFrame.repository.leagues')->getMemberLeagueChatItemsReplies(
                $value['id']
            );
        }

        return $chatItem;
    }
}
