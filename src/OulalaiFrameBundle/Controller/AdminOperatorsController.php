<?php

namespace OulalaiFrameBundle\Controller;

use OulalaiFrameBundle\Entity\Leagues;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class AdminOperatorsController extends Controller
{
    public function showOperatorsAction()
    {
        $usr= $this->get('security.token_storage')->getToken()->getUser();
        $role = $usr->getRoles()['0'];
        if ($role != "ROLE_SUPER_ADMIN") {
            return new RedirectResponse('/admin');
        }
        $operators = $this->get('OulalaiFrame.repository.config')->getOperators();


        return $this->render('admin/operators/show.html.twig',
            ['operators' => $operators,
                'role' => $role]);
    }

    public function createAction(Request $request)
    {
        $access_token = $this->get('OulalaiFrame.repository.token')->getClientToken(1)->access_token;
        $usr= $this->get('security.token_storage')->getToken()->getUser();
        $role = $usr->getRoles()['0'];

        if ($request->isXmlHttpRequest()) {
            $username=$request->get('username');
            $website=$request->get('website');
            $email=$request->get('email');
            if ($username && $website && $email) {
                $data=[
                    'website'=>$website,
                    'username'=>$username,
                    'email'=>$email,
                ];

                $client = $this->get("OulalaiFrame.apiservice.api")->APIRequest('POST', $this->container->getParameter('api_base_url') . 'customer/create', $access_token, $data);
                if (isset($client->status) && $client->status=="success") {
                    $alisa=md5($username);
                    $websiteId=$client->data->WebsiteId;
                    $add_alias=$this->get("OulalaiFrame.repository.admin")->addClientAlias($websiteId, $alisa);
                    if ($add_alias) {
                        $add_client_name = $this->get("OulalaiFrame.repository.config")->addConfigByKey($websiteId, 'client_name', $username);
                        $this->get("OulalaiFrame.repository.config")->addConfigByKey($websiteId, 'api_client_secret', $client->data->client_secret);
                        $this->get("OulalaiFrame.repository.config")->addConfigByKey($websiteId, 'api_client_id', $client->data->client_id);
                        $this->get("OulalaiFrame.repository.config")->addConfigByKey($websiteId, 'api_password', $client->data->password);
                        $this->get("OulalaiFrame.repository.config")->addConfigByKey($websiteId, 'api_username', $client->data->username);
                        if ($add_client_name) {
                            return new Response(json_encode(['success'=>$client->data]));
                        }
                    }
                } elseif (isset($client->status) && $client->status=="failed") {
                    return new Response(json_encode(['error'=>$client->data]));
                }
            }
        }

        return $this->render('admin/operators/create.html.twig', array(
            'role' => $role
        ));
    }

    public function editOperatorsAction($operatorId, Request $request)
    {
        $access_token = $this->get('OulalaiFrame.repository.token')->getClientToken(1)->access_token;
        $usr= $this->get('security.token_storage')->getToken()->getUser();
        $role = $usr->getRoles()['0'];
        $configs = $this->get("OulalaiFrame.repository.config")->getClientConfigs($operatorId);
        $config_keys = $this->get("OulalaiFrame.repository.config")->getConfigKeys();

        foreach ($config_keys as $key=>$configKey) {
            foreach ($configs as $config) {
                if ($configKey['config_key']==$config['config_key'] && isset($config['config_value'])) {
                    $config_keys[$key]['config_value']=$config['config_value'];
                }
            }
        }
        if ($request->getMethod()=='POST') {
            $datas = $request->request->all();
            $setData=false;
            foreach ($datas as $key=>$data) {
                $data_exists=$this->get("OulalaiFrame.repository.config")->getConfigByKeyName($key, $operatorId);
                if ($data_exists!==false) {
                    $setData=$this->get("OulalaiFrame.repository.config")->updateConfigByKey($operatorId, $key, $data);
                } else {
                    $setData=$this->get("OulalaiFrame.repository.config")->addConfigByKey($operatorId, $key, $data);
                }
            }
            if ($setData) {
                return new RedirectResponse($this->generateUrl('admin_operators'));
            }
        }

        return $this->render('admin/operators/edit.html.twig', array(
            'role' => $role,
            'operatorId'=>$operatorId,
            'config_keys'=>$config_keys
        ));
    }
}
