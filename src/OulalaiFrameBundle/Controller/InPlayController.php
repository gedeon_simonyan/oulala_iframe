<?php
namespace OulalaiFrameBundle\Controller;

use OulalaiFrameBundle\ApiManager\ApiUrls;
use Doctrine\ORM\EntityRepository;
use OulalaiFrameBundle\OulalaiFrameBundle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use OulalaiFrameBundle\ApiManager\Manager;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class InPlayController extends Controller
{
    private $aEventtypes = array();
    private $aSchedulePlayers = array();
    private $fAmountSpent = 0;

    public function indexAction($leagueId, $teamId=null)
    {
        $session=new Session();
        $request = Request::createFromGlobals();
        if (! $session->has('user_ref')) {
            $referer = $request->headers->get('referer');
            return new RedirectResponse($referer);
        }
        $sUserRef = $session->get('user_ref');

        $clientId =$session->get('client');
        $menu = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('show_menu', $clientId);
        $style = $this->get('OulalaiFrame.repository.config')->getUserStyle($clientId);
        $allowSubs = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('allow_subs', $clientId);
        $shareFb = $this->get('OulalaiFrame.repository.config')->getConfigByKeyName('fb_share', $clientId);
        $css_version = $this->getParameter('css_version');
        $js_version = $this->getParameter('js_version');

        $bIsTeamPast = false;
        $isTeamPartOfLeague = $this->get('OulalaiFrame.repository.inplay')->isTeamPartOfLeague($leagueId, $teamId);

        if (!$leagueId || !$teamId || !$isTeamPartOfLeague) {
            return new RedirectResponse('/'.$request->getLocale()."/squads-list/past", 301);
        }

        $squadFormation = $this->get('OulalaiFrame.repository.squad')->getSquadDetailsById($teamId);

        if ($squadFormation) {
            $squadFormation = $squadFormation['formation'];
        } else {
            //redirect to squads list if no squad with the requested squad ID
            return new RedirectResponse('/'.$request->getLocale()."/squads-list/past", 301);
        }
        $formation = $request->request->get('formation');

        $fID = ($squadFormation) ? $squadFormation : (($formation) ? $formation : 1);
        $aFormationSlots = $this->get('OulalaiFrame.repository.teams')->assignFormationDetails($fID);
        $bTeamPast = $this->get('OulalaiFrame.repository.squad')->isTeamPast($teamId, $sUserRef);
        $isSubsLimitFinished = $this->get('OulalaiFrame.repository.inplay')->getSubstitutionsPerformed($teamId, $sUserRef);
        $eventtypes = $this->get('OulalaiFrame.repository.inplay')->getEventTypes();
        $teamPlayers = $this->get('OulalaiFrame.repository.squad')->getSquadePlayers($teamId);
        $players=array();
        if (! empty($teamPlayers)) {
            foreach ($teamPlayers as $player) {
                $playerDetails = $this->get('OulalaiFrame.repository.inplay')->getPlayersLiveData($player['player_id'], $sUserRef, $leagueId, $teamId);

                if (! empty($playerDetails)) {
                    $playerDetails['matchDay']=$this->get('OulalaiFrame.repository.matches')->getMatchdayId($player['player_id'], $teamId);
                    $playerDetails['events'] = $this->get('OulalaiFrame.repository.players')->getPlayerEvents(
                        $player['player_id'],
                        $playerDetails['matchDay'],
                        $playerDetails['player_played_from'],
                        $playerDetails['player_played_to']
                    );
                }
                $players[$player['player_id']]=$playerDetails;
            }
        }

        if ($bTeamPast || !$isSubsLimitFinished) {
            $bIsTeamPast = true;
        }
        $aLeagueDetails = $this->get('OulalaiFrame.repository.leagues')->getMemberLeagueDetailsByID($leagueId);
        $firstDate = $this->get('OulalaiFrame.repository.leagues')->getEarliestMatchDate($leagueId);
        $memberteamschedule = $this->getSubstitutionGuide(true, $leagueId, $teamId);
        $positions = $this->get('OulalaiFrame.repository.players')->getAllPlayerPositions();
        $aPlayers = $this->assignPlayers($teamId, $leagueId, $sUserRef);
        $teamdetails = $this->get('OulalaiFrame.repository.squad')->getSquadDetailsById($teamId);
        $allFormations=$this->get('OulalaiFrame.repository.teams')->getFormation();
        if(!file_exists($this->getParameter('team_image_path').'/image-'.$teamId.'.jpg')){
            $this->get('OulalaiFrame.repository.players')->generateTeamImage($teamId,$aPlayers,$squadFormation,$allFormations);
        }
        if ($request->isXmlHttpRequest()) {
            $teamPlayers = $this->get('OulalaiFrame.repository.squad')->getSquadePlayers($teamId);
            $access_token = $this->get('OulalaiFrame.repository.token')->getToken();
            $this->get('OulalaiFrame.repository.players')->updateEvents($access_token);
            if (! empty($teamPlayers)) {
                foreach ($teamPlayers as $player) {
                    $this->get('OulalaiFrame.repository.players')->updatePlayerStats($access_token, $player['player_id']);
                }

                $aPlayers = $this->assignPlayers($teamId, $leagueId, $sUserRef);
                return $this->render('templates/controls/live-matches-content.html.twig',
                    array(
                        'teamdetails'    => $teamdetails,
                        'eventtypes'    => $eventtypes,
                        'players'    =>    $players,
                        'positions'    => $positions,
                        'leagueid'    => $leagueId,
                        'teamid'    => $teamId,
                        'formation'    => $this->get('OulalaiFrame.repository.inplay')->getFormationDetails($teamdetails['formation']),
                        'fieldFormation'=> $aFormationSlots,
                        'playerslist'    => $aPlayers,
                        'league'    => $aLeagueDetails,
                        'firstday'    => $firstDate,
                        'memberteamschedule' => $memberteamschedule,
                        'style'    => $style.$css_version,
                        'css_version' => $css_version,
                        'js_version' => $js_version,
                        'isPast'    => $bIsTeamPast,
                        'menu' => $menu,
                        'allowSubs'=>$allowSubs,
                        'share_fb'=>$shareFb,
                    )
                );
            }
        }
        $nodeUrl=$this->getParameter('node_url');
        return $this->render('templates/inplay.html.twig',
            array(
                'teamdetails'    => $teamdetails,
                'eventtypes'    => $eventtypes,
                'players'    =>    $players,
                'positions'    => $positions,
                'leagueid'    => $leagueId,
                'teamid'    => $teamId,
                'formation'    => $this->get('OulalaiFrame.repository.inplay')->getFormationDetails($teamdetails['formation']),
                'fieldFormation'=> $aFormationSlots,
                'playerslist'    => $aPlayers,
                'league'    => $aLeagueDetails,
                'firstday'    => $firstDate,
                'memberteamschedule' => $memberteamschedule,
                'style'    => $style.$css_version,
                'css_version' => $css_version,
                'js_version' => $js_version,
                'isPast'    => $bIsTeamPast,
                'menu' => $menu,
                'nodeUrl'=>$nodeUrl,
                'allowSubs'=>$allowSubs,
                'shareFb'=>$shareFb,
            )
        );
    }

    /**
     * Assings linup and bench players
     *
     * @param int iTeamId
     * @param int iLeagueId
     * @param string sRef
     */
    private function assignPlayers($iTeamId, $iLeagueId, $sRef)
    {
        $aPlayers['lineup'] = array();
        $aPlayers['bench'] = array();
        $aPlayers['lineup']['Midfielder'] = array();
        $aPlayers['lineup']['Defender'] = array();
        $aPlayers['lineup']['Striker'] = array();
        $aPlayers['lineup']['Goalkeeper'] = array();
        $aPlayers['timeline'] = array();

        $teamPlayers = $this->get('OulalaiFrame.repository.squad')->getSquadePlayers($iTeamId);

        if (! empty($teamPlayers)) {
            foreach ($teamPlayers as $player) {
                $playerDetails = $this->get('OulalaiFrame.repository.inplay')->getPlayersLiveData(
                    $player['player_id'],
                    $sRef,
                    $iLeagueId,
                    $iTeamId
                );
                if ($playerDetails) {
                    $playerDetails['matchDay'] = $this->get('OulalaiFrame.repository.matches')->getMatchdayId(
                        $player['player_id'],
                        $iTeamId
                    );

                    if (!empty($playerDetails)) {
                        $playerDetails['events'] = $this->get('OulalaiFrame.repository.players')->getPlayerEvents(
                            $player['player_id'],
                            $playerDetails['matchDay'],
                            $playerDetails['player_played_from'],
                            $playerDetails['player_played_to']
                        );

                        if ($playerDetails['substituted_with_id']) {
                            $playerDetails['is_playing'] = 0;
                            $playerDetails['completed_match'] = 1;
                        }
                        if ($player['starting_11'] == 1) {
                            $pos = $playerDetails['position'];

                            $playerDetails['starting_11'] = $player['starting_11'];
                            array_push($aPlayers['lineup'][$pos], $playerDetails);
                        } else {
                            if (count($aPlayers['bench']) < 7) {
                                $playerDetails['starting_11'] = $player['starting_11'];
                                array_push($aPlayers['bench'], $playerDetails);
                            }
                        }
                    }
                }
            }
        }

        return $aPlayers;
    }

    /**
     * Returns the amount of money a member has left to spend.
     *
     * @return int
     */
    private function getBudgetLeft()
    {
        return 100 - $this->fAmountSpent;
    }

    /**
     * Creates a table-like array representing the times of players' matches and marks substitute players as well as
     * players that play after substitute players and thus cannot be substituted.
     *
     * @param bool $bUseFillers whether to include a dummy element after every time group
     *
     * @return array|null table of values
     */
    private function getSubstitutionGuide($bUseFillers = true, $iMemberLeagueID, $iSquadId)
    {
        $aPlayerTimes = $this->getPlayersWithTimes($iMemberLeagueID, $iSquadId);
        if (!is_array($aPlayerTimes) || count($aPlayerTimes) < 1) {
            return null;
        }

        $aPlayerTimes = $this->groupPlayersByMatchDatetime($aPlayerTimes);

        $aOutput = array(
            'days' => array(),
            'times' => array(),
        );
        $aSubstitueFlags = array();

        // Get all the positions
        $aPositions = $this->get('OulalaiFrame.repository.players')->getAllPlayerPositions();
        $aPositionIndex = array();

        foreach ($aPositions as $iKey => $aValue) {
            $aPositionIndex[$aValue['player_position_id']] = $iKey;
            $aOutput['position' . $iKey] = array();
            $aSubstitueFlags['position' . $iKey] = false;
        }

        // Process each day
        foreach ($aPlayerTimes as $iDayStamp => $aDay) {
            $aOutput['days'][$iDayStamp] = array(
                'value' => date('l, M j', $iDayStamp),
                'day_colspan' => count($aDay),
            );

            // Process time of the day
            $iTotalPlayerPositionsInGroup = 0;
            $iPointer = 1;

            foreach ($aDay as $iTimestamp => $aTimeGroup) {
                $aMarkers = array();
                if ($iPointer == 1) {
                    $aMarkers['first'] = 'first';
                }
                if ($iPointer == count($aDay)) {
                    $aMarkers['last'] = 'last';
                }
                if ($iPointer != count($aDay)) {
                    $aTimeGroupKeys = array_keys($aDay);
                    $aCurrentKey = array_search($iTimestamp, $aTimeGroupKeys);
                    $iNextTimestamp = $aTimeGroupKeys[($aCurrentKey + 1)];

                    $aMarkers['hour-diff'] = 'hour-diff-' . round(($iNextTimestamp - $iTimestamp) / 3600);
                }

                $aOutput['times'][$iTimestamp] = array(
                    'value' => date('G:i', $iTimestamp),
                    'column_type' => 'time',
                    'markers' => implode(' ', $aMarkers),
                );

                $aPlayerPositions = array();
                $aPlayerPositionsCount = array();
                foreach ($aPositions as $iKey => $aValue) {
                    $aPlayerPositions['position' . $iKey] = array();
                    $aPlayerPositionsCount['position' . $iKey] = 0;
                }

                foreach ($aTimeGroup as $iPlayerKey => $aPlayer) {
                    $aPlayerMarkers = array();
                    $sPosition = 'position' . $aPositionIndex[$aPlayer['player_position_id']];
                    $aPlayerPositionsCount[$sPosition]++;
                    $aPlayerPositions[$sPosition][$iPlayerKey] = $aPlayer;
                    $aPlayerPositions[$sPosition][$iPlayerKey]['column_type'] = 'player';

                    if ($aPlayer['starting_11'] == 0) {
                        $aSubstitueFlags[$sPosition] = true;
                        $aPlayerMarkers[] = 'substitute';
                    } elseif ($aSubstitueFlags[$sPosition] == true) {
                        $aPlayerMarkers[] = 'plays-after-substitute';
                    }

                    $aPlayerPositions[$sPosition][$iPlayerKey]['markers'] = implode(' ', $aPlayerMarkers);
                }
                $iPlayerPositions = max($aPlayerPositionsCount);
                $iTotalPlayerPositionsInGroup += $iPlayerPositions;

                // Pad each group so that the amount of player columns is the same in each group
                foreach ($aPositions as $iKey => $aValue) {
                    $aPlayerPositions['position' . $iKey] = array_pad($aPlayerPositions['position' . $iKey], $iPlayerPositions, array('column_type' => 'empty_player'));
                }

                // Add fillers at the end of the time group
                if ($bUseFillers) {
                    $aFillerMarkers = $aMarkers;
                    unset($aFillerMarkers['first']);

                    $aOutput['times'][] = array('column_type' => 'filler', 'markers' => implode(' ', $aFillerMarkers));

                    foreach ($aPositions as $iKey => $aValue) {
                        $aPlayerPositions['position' . $iKey][] = array('column_type' => 'filler', 'markers' => implode(' ', $aFillerMarkers));
                    }
                }

                // Set how many columns the time group spans
                $aOutput['times'][$iTimestamp]['colspan'] = $iPlayerPositions;

                // Merge time groups
                foreach ($aPositions as $iKey => $aValue) {
                    $aOutput['position' . $iKey] = array_merge($aOutput['position' . $iKey], $aPlayerPositions['position' . $iKey]);
                }

                $iPointer++;
            }

            // Set how many columns the day spans
            if ($bUseFillers) {
                $aOutput['days'][$iDayStamp]['day_colspan'] = (count($aDay) * 2);
                $aOutput['days'][$iDayStamp]['total_colspan'] = $iTotalPlayerPositionsInGroup + count($aDay);
            } else {
                $aOutput['days'][$iDayStamp]['day_colspan'] = count($aDay);
                $aOutput['days'][$iDayStamp]['total_colspan'] = $iTotalPlayerPositionsInGroup;
            }
        }

        return $aOutput;
    }

    /**
     * Returns players data together with the date & time of the first match
     * of the current competition week for each player of the member's team.
     *
     * @return array|null players' times
     * @todo It would be better if this method could be avoided and we could always rely on $this->aSchedulePlayers containing
     * the current competition week match time.
     */
    private function getPlayersWithTimes($iMemberLeagueID, $iTeamID)
    {
        $aSchedulePlayers = array();
        $aSquadPlayers = $this->get('OulalaiFrame.repository.squad')->getSquadePlayers($iTeamID);
        if (!is_array($aSquadPlayers) || count($aSquadPlayers) < 1) {
            return null;
        }
        foreach ($aSquadPlayers as $player) {
            $aSchedulePlayers[$player['player_id']] = $player;
        }

        $aResult = $this->get('OulalaiFrame.repository.squad')->getPlayersWithTimes($iMemberLeagueID, $aSchedulePlayers);

        //$this->assignPlayers($iTeamID);
        // Add players' match dates
        $aOutput = $aSchedulePlayers;
        foreach ($aResult as $aValue) {
            $aOutput[$aValue['player_id']]['match_date'] = $aValue['match_date'];
        }

        // Sort players so that the substitutes come last
        $aSortFlags = array();
        foreach ($aSchedulePlayers as $iPlayerKey => $aPlayer) {
            $aSortFlags[$iPlayerKey] = $aPlayer['starting_11'];
        }

        array_multisort($aSortFlags, SORT_DESC, $aOutput);

        return $aOutput;
    }

    /**
     * Groups players into a multidimensional array of days and times depending on the 'match_date' value.
     *
     * @param array $aPlayerTimes array of players together with their match times
     * @return array players grouped by day (represented by timestamp) and match time (represented by timestamp)
     */
    private function groupPlayersByMatchDatetime($aPlayerTimes)
    {
        $aOutput = array();

        foreach ($aPlayerTimes as $iPlayerId => $aPlayer) {
            if (!array_key_exists('match_date', $aPlayer)) {
                continue;
            }

            $iTimestamp = strtotime($aPlayer['match_date']);
            $iDay = mktime(0, 0, 0, date('m', $iTimestamp), date('d', $iTimestamp), date('Y', $iTimestamp));
            $iTime = mktime(date('H', $iTimestamp), date('i', $iTimestamp), 0, date('m', $iTimestamp), date('d', $iTimestamp), date('Y', $iTimestamp));

            if (!array_key_exists($iDay, $aOutput)) {
                $aOutput[$iDay] = array();
                ksort($aOutput);
            }

            if (!array_key_exists($iTimestamp, $aOutput[$iDay])) {
                $aOutput[$iDay][$iTime] = array();
                ksort($aOutput[$iDay]);
            }

            $aOutput[$iDay][$iTime][$iPlayerId] = $aPlayer;
        }

        return $aOutput;
    }


    public function assingChooseSubstituteAction(Request $request)
    {
        $leaguesId = $request->request->get('league');
        $teamId = $request->request->get('teamid');
        $playerid = $request->request->get('playerid');
        $hasSubs=$this->get('OulalaiFrame.repository.inplay')->checkSubsLimitFinished($teamId);

        if ($hasSubs) {
            $available_subs=$this->get('OulalaiFrame.repository.inplay')->getAvailableSubstitutes($playerid, $teamId);
            if ($available_subs) {
                $fromPlayer=$this->get('OulalaiFrame.repository.players')->getPlayerProfileDetails($playerid);

                return $this->render('templates/controls/dialog-start-player-sub.html.twig', array(
                    'leagueid'=>$leaguesId,
                    'substitutes'=>$available_subs,
                    'fromplayerid'=>$playerid,
                    'fromplayer'=>$fromPlayer,
                    'teamid'=>$teamId
                ));
            } else {
                return $this->render('templates/controls/dialog-error.html.twig', array(
                    'errors'=>['error'=>'This player has no valid substitutes']
                ));
            }
        }
        return $this->render('templates/controls/dialog-error.html.twig', array(
            'errors'=>['error'=>'You have exhausted the allowed 3 substitutions.']
        ));
    }

    public function assingSubstitutesAction(Request $request)
    {
        $leaguesId = $request->request->get('league');
        $teamId = $request->request->get('teamid');
        $fromPlayerID = $request->request->get('fromplayerid');
        $toPlayerID = $request->request->get('toplayerid');
        $fromPlayer=$this->get('OulalaiFrame.repository.players')->getPlayerProfileDetails($fromPlayerID);
        $toPlayer=$this->get('OulalaiFrame.repository.players')->getPlayerProfileDetails($toPlayerID);
        return $this->render('templates/controls/dialog-confirm-player-sub.html.twig', array(
            'leagueid'=>$leaguesId,
            'teamid'=>$teamId,
            'toplayer'=>$toPlayer,
            'fromplayer'=>$fromPlayer
        ));
    }

    public function completeSubstitutePlayerAction(Request $request)
    {
        $session= new Session();
        $userRef= $session->get('user_ref');
        $clientId =$session->get('client');
        $access_token = $this->get('OulalaiFrame.repository.token')->getClientToken($clientId)->access_token;
        $leaguesId = $request->request->get('league');
        $teamId = $request->request->get('teamid');
        $fromPlayerID = $request->request->get('fromplayerid');
        $toPlayerID = $request->request->get('toplayerid');

        if ($leaguesId && $teamId && $fromPlayerID &&$toPlayerID) {
            $players=$this->get('OulalaiFrame.repository.inplay')->getSquadPlayers($teamId, $toPlayerID, $fromPlayerID);
            $squadData=$this->get('OulalaiFrame.repository.inplay')->getSquadData($teamId);
            if ($squadData && isset($squadData[0]) && !empty($squadData[0]) && $players) {
                $form_params = [
                    'user'        => $userRef, //test user param
                    'league'    => (int)$squadData[0]['league'],
                    'name'        => $squadData[0]['name'],
                    'squadId'        => (int)$teamId,
                    'formation' => (int)$squadData[0]['formation']
                ];
                $form_params['players']=$players;
                $subs = new \stdClass();
                $subs->in=(int)$toPlayerID;
                $subs->out=(int)$fromPlayerID;
                $form_params['substitutions']=[$subs];

                $squad = $this->get("OulalaiFrame.apiservice.api")->APIRequest('PUT', $this->container->getParameter('api_base_url') . 'squads', $access_token, $form_params);

                if (!empty($squad) && isset($squad->status) && $squad->status=='success') {
                    $this->get('OulalaiFrame.repository.squad')->updateOneSquadData($access_token, $teamId);
                    $this->get('OulalaiFrame.repository.leagues')->updateOneLeagueData($access_token, (int)$squadData[0]['league'], 1);
                    $this->get('OulalaiFrame.repository.matches')->updateOneLeagueMatchesAndUserData($access_token, (int)$squadData[0]['league']);
                    return new Response('true', 200);
//                    return new Response('/'.$request->getLocale()."/live/".$leaguesId."/".$teamId, 301);
                } else {
                    if (!empty($squad) &&  isset($squad->status) && $squad->status=='failed') {
                        if (isset($squad->data) && isset($squad->data->reason) && isset($squad->data->violations) && is_array($squad->data->violations)) {
                            return $this->render('templates/controls/dialog-error.html.twig', array(
                                'errors'=>$squad->data->violations
                            ));
                        }
                    } else {
                        return $this->render('templates/controls/dialog-error.html.twig', array(
                            'errors'=>['error'=>'Erro while update']
                        ));
                    }
                }
            }
        } else {
            return $this->render('templates/controls/dialog-error.html.twig', array(
                'errors'=>['error'=>'No player data found']
            ));
        }
    }
    public function playerStatsAction(Request $request)
    {
        $playerId=$request->query->all()['pid'];
        $squadId=$request->query->all()['week'];
        $matchDay=$request->query->all()['matchday'];
        $pDetails = $this->get('OulalaiFrame.repository.inplay')->getPlayerProfileDetails($squadId, $playerId);
        $memberDetails = $this->get('OulalaiFrame.repository.inplay')->getPlayerDetailsByCompetitionWeek($squadId, $playerId);
        $matchDetails = $this->get('OulalaiFrame.repository.inplay')->getFixtureByMatchDayAndTeamID($playerId);
        $matchStats = $this->get('OulalaiFrame.repository.players')->getPlayerMatchBreakdown($playerId, $matchDay);
        $hasStats=false;
        if (!empty($matchStats)) {
            $hasStats =true;
        }
        if (isset($memberDetails[0])) {
            $memberDetails=$memberDetails[0];
        }

        return $this->render('templates/player-stats-popup.html.twig',
            [
                'playerdetails'=>$pDetails,
                'memberplayer'=>$memberDetails,
                'matchdetails'=>$matchDetails,
                'hasmatchstatistics'=>$hasStats,
                'matchstatistics'=>$matchStats,
                'player_id'=>$playerId,
                'matchday_id'=>$matchDay
            ]);
    }

    public function inPlayChangeFormationAction(Request $request)
    {
        $squadId = $request->request->get('teamid');
        $leagueId = $request->request->get('league');

        $formations = $this->get('OulalaiFrame.repository.inplay')->getFormations();
        $squadFormation = $this->get('OulalaiFrame.repository.inplay')->getSquadFormation($squadId);

        return $this->render(
            'templates/controls/dialog-choose-formation.html.twig',
            [
                'formation' => $squadFormation,
                'formations' => $formations,
                'leagueId' => $leagueId,
                'teamId' => $squadId,
            ]
        );
    }

    public function choosePlayersToRemoveAction(Request $request)
    {
        $session = new Session();
        $userRef = $session->get('user_ref');
        $squadId = $request->request->get('teamid');
        $leagueId = $request->request->get('league');
        $formationId = $request->request->get('formationid');

        $squadFormation = $this->get('OulalaiFrame.repository.inplay')->getSquadFormation($squadId);
        $aNewFormation = $this->get('OulalaiFrame.repository.inplay')->getFormations($formationId);

        $formations = $this->get('OulalaiFrame.repository.inplay')->inPlayChangeFormation(
            $squadFormation['formation_id'],
            $formationId,
            $squadId,
            $leagueId,
            $userRef
        );

        return $this->render(
            'templates/controls/dialog-choose-players-to-remove.html.twig',
            [
                'formationchanges' => $formations,
                'newformation' => $aNewFormation,
                'leagueId' => $leagueId,
                'teamId' => $squadId,
            ]
        );
    }

    public function assignChooseSubstitutesAction(Request $request)
    {
        $session = new Session();
        $userRef = $session->get('user_ref');
        $squadId = $request->request->get('teamid');
        $leagueId = $request->request->get('league');
        $formationId = $request->request->get('formationid');
        $aPlayers = $request->request->get('players');

        $squadFormation = $this->get('OulalaiFrame.repository.inplay')->getSquadFormation($squadId);
        $aNewFormation = $this->get('OulalaiFrame.repository.inplay')->getFormations($formationId);
        $aChanges = $this->get('OulalaiFrame.repository.inplay')->inPlayChangeFormationChooseSubs(
            $squadFormation['formation_id'],
            $formationId,
            $aPlayers,
            $squadId,
            $leagueId,
            $userRef
        );
        $aPlayerDetails = array();
        foreach ($aPlayers as $key => $APlayer) {
            $aPlayerDetails[$key] = $this->get('OulalaiFrame.repository.inplay')->getPlayersLiveData(
                $APlayer,
                $userRef,
                $leagueId,
                $squadId
            );
        }


        return $this->render(
            'templates/controls/dialog-choose-substitutes.html.twig',
            [
                'formationchanges' => $aChanges,
                'newformation' => $aNewFormation,
                'playersout' => $aPlayerDetails,
                'leagueId' => $leagueId,
                'teamId' => $squadId,
            ]
        );
    }

    public function confirmFormationAction(Request $request)
    {
        $session = new Session();
        $userRef = $session->get('user_ref');
        $squadId = $request->request->get('teamid');
        $leagueId = $request->request->get('league');
        $formationId = $request->request->get('formationid');
        $aPlayers = $request->request->get('players');


        $aChanges = array();
        $aNewFormation = $this->get('OulalaiFrame.repository.inplay')->getFormations($formationId);
        foreach ($aPlayers as $key => $APlayer) {
            $aChanges[$key]['fromplayerid'] = $this->get('OulalaiFrame.repository.inplay')->getPlayersLiveData(
                $APlayer['fromplayerid'],
                $userRef,
                $leagueId,
                $squadId
            );
            $aChanges[$key]['toplayerid'] = $this->get('OulalaiFrame.repository.inplay')->getPlayersLiveData(
                $APlayer['toplayerid'],
                $userRef,
                $leagueId,
                $squadId
            );
        }

        return $this->render(
            'templates/controls/dialog-confirm-formation.html.twig',
            [
                'formationschanges' => $aChanges,
                'newformation' => $aNewFormation,
                'leagueId' => $leagueId,
                'teamId' => $squadId,
            ]
        );
    }

    public function completeFormationChangeAction(Request $request)
    {
        $session = new Session();
        $userRef = $session->get('user_ref');
        $clientId = $session->get('client');
        $access_token = $this->get('OulalaiFrame.repository.token')->getClientToken($clientId)->access_token;
        $squadId = $request->request->get('teamid');
        $leagueId = $request->request->get('league');
        $formationId = $request->request->get('formationid');
        $aPlayers = $request->request->get('players');

        if ($leagueId && $squadId && $aPlayers && $formationId) {
            $players = $this->get('OulalaiFrame.repository.inplay')->getPlayers($squadId, $aPlayers);

            $squadData = $this->get('OulalaiFrame.repository.inplay')->getSquadData($squadId);
            if ($squadData && isset($squadData[0]) && !empty($squadData[0]) && $players) {
                $form_params = [
                    'user' => $userRef, //test user param
                    'league' => (int)$squadData[0]['league'],
                    'name' => $squadData[0]['name'],
                    'squadId' => (int)$squadId,
                    'formation' => (int)$formationId,
                ];
                $form_params['players'] = $players;

                $subs = array();
                foreach ($aPlayers as $key => $APlayer) {
                    $subs[$key] = new \stdClass();
                    $subs[$key]->in = (int)$APlayer['toplayerid'];
                    $subs[$key]->out = (int)$APlayer['fromplayerid'];
                }

                $form_params['substitutions'] = $subs;
                $squad = $this->get("OulalaiFrame.apiservice.api")->APIRequest('PUT', $this->container->getParameter('api_base_url') . 'squads', $access_token,$form_params);

                if (!empty($squad) && isset($squad->status) && $squad->status == 'success') {
                    $this->get('OulalaiFrame.repository.squad')->updateOneSquadData($access_token, $squadId);
                    $this->get('OulalaiFrame.repository.leagues')->updateOneLeagueData(
                        $access_token,
                        (int)$squadData[0]['league'],
                        1
                    );
                    $this->get('OulalaiFrame.repository.matches')->updateOneLeagueMatchesAndUserData(
                        $access_token,
                        (int)$squadData[0]['league']
                    );

                    return $this->render('templates/controls/dialog-refresh-page.html.twig');
                } else {
                    if (!empty($squad) && isset($squad->status) && $squad->status == 'failed') {
                        if (isset($squad->data) && isset($squad->data->reason) && isset($squad->data->violations) && is_array(
                                $squad->data->violations
                            )) {
                            return $this->render(
                                'templates/controls/dialog-error.html.twig',
                                array(
                                    'errors' => $squad->data->violations,
                                )
                            );
                        }
                    } else {
                        return $this->render(
                            'templates/controls/dialog-error.html.twig',
                            array(
                                'errors' => ['error' => 'Erro while update'],
                            )
                        );
                    }
                }
            }
        } else {
            return $this->render(
                'templates/controls/dialog-error.html.twig',
                array(
                    'errors' => ['error' => 'No player data found'],
                )
            );
        }
    }
}
