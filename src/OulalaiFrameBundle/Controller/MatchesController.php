<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 15-Dec-16
 * Time: 14:21
 */

namespace OulalaiFrameBundle\Controller;

use Doctrine\ORM\Tools\Pagination\Paginator;
use OulalaiFrameBundle\ApiManager\ApiUrls;

use OulalaiFrameBundle\OulalaiFrameBundle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use OulalaiFrameBundle\ApiManager\Manager;

class MatchesController extends Controller
{
    public function detailsAction($matchesId)
    {
        $id = filter_var($matchesId, FILTER_SANITIZE_NUMBER_INT);
        $match_info = $this->get()->matchInfo($id);
        
        return $this->render('templates/matchDetails.html.twig', array("match" => $match_info));
    }
}
