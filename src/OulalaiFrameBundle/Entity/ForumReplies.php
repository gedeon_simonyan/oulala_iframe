<?php

namespace OulalaiFrameBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ForumReplies
 *
 * @ORM\Table(name="forum_replies")
 * @ORM\Entity
 */
class ForumReplies
{
    /**
     * @var boolean
     *
     * @ORM\Column(name="has_reply", type="boolean", nullable=true)
     */
    private $hasReply = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="reply_content", type="text", length=65535, nullable=true)
     */
    private $replyContent;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_time", type="datetime", nullable=true)
     */
    private $createTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="update_time", type="datetime", nullable=true)
     */
    private $updateTime;

    /**
     * @var integer
     *
     * @ORM\Column(name="reply_by", type="integer", nullable=true)
     */
    private $replyBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="reply_topic", type="integer", nullable=true)
     */
    private $replyTopic;

    /**
     * @var integer
     *
     * @ORM\Column(name="reply_to", type="integer", nullable=true)
     */
    private $replyTo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_admin", type="boolean", nullable=true)
     */
    private $isAdmin;

    /**
     * @var integer
     *
     * @ORM\Column(name="forum_level", type="integer", nullable=true)
     */
    private $forumLevel;

    /**
     * @var integer
     *
     * @ORM\Column(name="reply_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $replyId;



    /**
     * Set hasReply
     *
     * @param boolean $hasReply
     *
     * @return ForumReplies
     */
    public function setHasReply($hasReply)
    {
        $this->hasReply = $hasReply;

        return $this;
    }

    /**
     * Get hasReply
     *
     * @return boolean
     */
    public function getHasReply()
    {
        return $this->hasReply;
    }

    /**
     * Set replyContent
     *
     * @param string $replyContent
     *
     * @return ForumReplies
     */
    public function setReplyContent($replyContent)
    {
        $this->replyContent = $replyContent;

        return $this;
    }

    /**
     * Get replyContent
     *
     * @return string
     */
    public function getReplyContent()
    {
        return $this->replyContent;
    }

    /**
     * Set createTime
     *
     * @param \DateTime $createTime
     *
     * @return ForumReplies
     */
    public function setCreateTime($createTime)
    {
        $this->createTime = $createTime;

        return $this;
    }

    /**
     * Get createTime
     *
     * @return \DateTime
     */
    public function getCreateTime()
    {
        return $this->createTime;
    }

    /**
     * Set updateTime
     *
     * @param \DateTime $updateTime
     *
     * @return ForumReplies
     */
    public function setUpdateTime($updateTime)
    {
        $this->updateTime = $updateTime;

        return $this;
    }

    /**
     * Get updateTime
     *
     * @return \DateTime
     */
    public function getUpdateTime()
    {
        return $this->updateTime;
    }

    /**
     * Set replyBy
     *
     * @param integer $replyBy
     *
     * @return ForumReplies
     */
    public function setReplyBy($replyBy)
    {
        $this->replyBy = $replyBy;

        return $this;
    }

    /**
     * Get replyBy
     *
     * @return integer
     */
    public function getReplyBy()
    {
        return $this->replyBy;
    }

    /**
     * Set replyTopic
     *
     * @param integer $replyTopic
     *
     * @return ForumReplies
     */
    public function setReplyTopic($replyTopic)
    {
        $this->replyTopic = $replyTopic;

        return $this;
    }

    /**
     * Get replyTopic
     *
     * @return integer
     */
    public function getReplyTopic()
    {
        return $this->replyTopic;
    }

    /**
     * Set replyTo
     *
     * @param integer $replyTo
     *
     * @return ForumReplies
     */
    public function setReplyTo($replyTo)
    {
        $this->replyTo = $replyTo;

        return $this;
    }

    /**
     * Get replyTo
     *
     * @return integer
     */
    public function getReplyTo()
    {
        return $this->replyTo;
    }

    /**
     * Set isAdmin
     *
     * @param boolean $isAdmin
     *
     * @return ForumReplies
     */
    public function setIsAdmin($isAdmin)
    {
        $this->isAdmin = $isAdmin;

        return $this;
    }

    /**
     * Get isAdmin
     *
     * @return boolean
     */
    public function getIsAdmin()
    {
        return $this->isAdmin;
    }

    /**
     * Set forumLevel
     *
     * @param integer $forumLevel
     *
     * @return ForumReplies
     */
    public function setForumLevel($forumLevel)
    {
        $this->forumLevel = $forumLevel;

        return $this;
    }

    /**
     * Get forumLevel
     *
     * @return integer
     */
    public function getForumLevel()
    {
        return $this->forumLevel;
    }

    /**
     * Get replyId
     *
     * @return integer
     */
    public function getReplyId()
    {
        return $this->replyId;
    }
}
