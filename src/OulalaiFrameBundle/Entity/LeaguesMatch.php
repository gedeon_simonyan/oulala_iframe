<?php
namespace OulalaiFrameBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * leagues matches
 *
 * @ORM\Table(name="leagues_matches")
 * @ORM\Entity(repositoryClass="OulalaiFrameBundle\Repository\LeaguesMatchRepository")
 */
class LeaguesMatch
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="league_id", type="integer")
     */
    private $league_id;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer")
     */
    private $user_id;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getLeagueId()
    {
        return $this->league_id;
    }

    /**
     * @param int $league_id
     */
    public function setLeagueId($league_id)
    {
        $this->league_id = $league_id;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }
}
