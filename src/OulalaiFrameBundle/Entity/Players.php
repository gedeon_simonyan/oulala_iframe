<?php

namespace OulalaiFrameBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Players
 *
 * @ORM\Table(name="players")
 * @ORM\Entity(repositoryClass="OulalaiFrameBundle\Repository\PlayersRepository")
 */
class Players
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="firstName", type="string", length=255)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="lastName", type="string", length=255)
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="knownName", type="string", length=255)
     */
    private $knownName;

    /**
     * @var string
     *
     * @ORM\Column(name="position", type="string", length=255)
     */
    private $position;

    /**
     * @var float
     *
     * @ORM\Column(name="value", type="float")
     */
    private $value;

    /**
     * @var int
     *
     * @ORM\Column(name="team_id", type="integer")
     */
    private $teamId;

    /**
     * @var string
     *
     * @ORM\Column(name="team_name", type="string", length=255)
     */
    private $teamName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateOfBirth", type="date")
     */
    private $dateOfBirth;

    /**
     * @var float
     *
     * @ORM\Column(name="weight", type="float")
     */
    private $weight;

    /**
     * @var float
     *
     * @ORM\Column(name="height", type="float")
     */
    private $height;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255)
     */
    private $image;

    /**
     * @var int
     *
     * @ORM\Column(name="seasonScore", type="integer")
     */
    private $seasonScore;

    /**
     * @var int
     *
     * @ORM\Column(name="positionId", type="integer")
     */
    private $positionId;

    /**
     * @var int
     *
     * @ORM\Column(name="leagueId", type="integer")
     */
    private $leagueId;

    /**
     * @var string
     *
     * @ORM\Column(name="stats", type="string", length=255)
     */
    private $stats;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Players
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return Players
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return Players
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set knownName
     *
     * @param string $knownName
     *
     * @return Players
     */
    public function setKnownName($knownName)
    {
        $this->knownName = $knownName;

        return $this;
    }

    /**
     * Get knownName
     *
     * @return string
     */
    public function getKnownName()
    {
        return $this->knownName;
    }

    /**
     * Set position
     *
     * @param string $position
     *
     * @return Players
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set value
     *
     * @param float $value
     *
     * @return Players
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set teamId
     *
     * @param integer $teamId
     *
     * @return Players
     */
    public function setTeamId($teamId)
    {
        $this->teamId = $teamId;

        return $this;
    }

    /**
     * Get teamId
     *
     * @return int
     */
    public function getTeamId()
    {
        return $this->teamId;
    }

    /**
     * Set teamName
     *
     * @param string $teamName
     *
     * @return Players
     */
    public function setTeamName($teamName)
    {
        $this->teamName = $teamName;

        return $this;
    }

    /**
     * Get teamName
     *
     * @return string
     */
    public function getTeamName()
    {
        return $this->teamName;
    }

    /**
     * Set dateOfBirth
     *
     * @param \DateTime $dateOfBirth
     *
     * @return Players
     */
    public function setDateOfBirth($dateOfBirth)
    {
        $this->dateOfBirth = $dateOfBirth;

        return $this;
    }

    /**
     * Get dateOfBirth
     *
     * @return \DateTime
     */
    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }

    /**
     * Set weight
     *
     * @param float $weight
     *
     * @return Players
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return float
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set height
     *
     * @param float $height
     *
     * @return Players
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get height
     *
     * @return float
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Players
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set seasonScore
     *
     * @param integer $seasonScore
     *
     * @return Players
     */
    public function setSeasonScore($seasonScore)
    {
        $this->seasonScore = $seasonScore;

        return $this;
    }

    /**
     * Get seasonScore
     *
     * @return int
     */
    public function getSeasonScore()
    {
        return $this->seasonScore;
    }

    /**
     * Set positionId
     *
     * @param integer $positionId
     *
     * @return Players
     */
    public function setPositionId($positionId)
    {
        $this->positionId = $positionId;

        return $this;
    }

    /**
     * Get positionId
     *
     * @return int
     */
    public function getPositionId()
    {
        return $this->positionId;
    }

    /**
     * Set leagueId
     *
     * @param integer $leagueId
     *
     * @return Players
     */
    public function setLeagueId($leagueId)
    {
        $this->leagueId = $leagueId;

        return $this;
    }

    /**
     * Get leagueId
     *
     * @return int
     */
    public function getLeagueId()
    {
        return $this->leagueId;
    }

    /**
     * Set stats
     *
     * @param string $stats
     *
     * @return Players
     */
    public function setStats($stats)
    {
        $this->stats = $stats;

        return $this;
    }

    /**
     * Get stats
     *
     * @return string
     */
    public function getStats()
    {
        return $this->stats;
    }
    /**
     * @var string
     */
    private $firstname;

    /**
     * @var string
     */
    private $lastname;

    /**
     * @var string
     */
    private $knownname;

    /**
     * @var \DateTime
     */
    private $dateofbirth;

    /**
     * @var \DateTime
     */
    private $fromDate;

    /**
     * @var \DateTime
     */
    private $toDate;

    /**
     * @var integer
     */
    private $seasonscore;

    /**
     * @var integer
     */
    private $positionid;

    /**
     * @var integer
     */
    private $leagueid;

    /**
     * @var \DateTime
     */
    private $clientDate = 'CURRENT_TIMESTAMP';


    /**
     * Set fromDate
     *
     * @param \DateTime $fromDate
     *
     * @return Players
     */
    public function setFromDate($fromDate)
    {
        $this->fromDate = $fromDate;

        return $this;
    }

    /**
     * Get fromDate
     *
     * @return \DateTime
     */
    public function getFromDate()
    {
        return $this->fromDate;
    }

    /**
     * Set toDate
     *
     * @param \DateTime $toDate
     *
     * @return Players
     */
    public function setToDate($toDate)
    {
        $this->toDate = $toDate;

        return $this;
    }

    /**
     * Get toDate
     *
     * @return \DateTime
     */
    public function getToDate()
    {
        return $this->toDate;
    }

    /**
     * Set clientDate
     *
     * @param \DateTime $clientDate
     *
     * @return Players
     */
    public function setClientDate($clientDate)
    {
        $this->clientDate = $clientDate;

        return $this;
    }

    /**
     * Get clientDate
     *
     * @return \DateTime
     */
    public function getClientDate()
    {
        return $this->clientDate;
    }
}
