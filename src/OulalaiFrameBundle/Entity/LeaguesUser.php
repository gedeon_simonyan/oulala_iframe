<?php
namespace OulalaiFrameBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * leagues_user
 *
 * @ORM\Table(name="leagues_user")
 * @ORM\Entity(repositoryClass="OulalaiFrameBundle\Repository\LeaguesUserRepository")
 */
class LeaguesUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="leagues_id", type="integer")
     */
    private $leagues_id;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer")
     */
    private $user_id;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getLeaguesId()
    {
        return $this->leagues_id;
    }

    /**
     * @param int $leagues_id
     */
    public function setLeaguesId($leagues_id)
    {
        $this->leagues_id = $leagues_id;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }
}
