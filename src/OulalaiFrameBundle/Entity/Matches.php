<?php

namespace OulalaiFrameBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Matches
 *
 * @ORM\Table(name="matches")
 * @ORM\Entity(repositoryClass="OulalaiFrameBundle\Repository\MatchesRepository")
 */
class Matches
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255)
     */
    private $status;

    /**
     * @var int
     *
     * @ORM\Column(name="home", type="integer")
     */
    private $home;

    /**
     * @var int
     *
     * @ORM\Column(name="away", type="integer")
     */
    private $away;

    /**
     * @var int
     *
     * @ORM\Column(name="matchday", type="integer")
     */
    private $matchday;

    /**
     * @var int
     *
     * @ORM\Column(name="home_golas", type="integer")
     */
    private $homeGolas;

    /**
     * @var int
     *
     * @ORM\Column(name="away_golas", type="integer")
     */
    private $awayGolas;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var int
     *
     * @ORM\Column(name="finalised", type="integer")
     */
    private $finalised;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="client_date", type="datetime")
     */
    private $clientDate;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getHome()
    {
        return $this->home;
    }

    /**
     * @param int $home
     */
    public function setHome($home)
    {
        $this->home = $home;
    }

    /**
     * @return int
     */
    public function getAway()
    {
        return $this->away;
    }

    /**
     * @param int $away
     */
    public function setAway($away)
    {
        $this->away = $away;
    }

    /**
     * @return int
     */
    public function getMatchday()
    {
        return $this->matchday;
    }

    /**
     * @param int $matchday
     */
    public function setMatchday($matchday)
    {
        $this->matchday = $matchday;
    }

    /**
     * @return int
     */
    public function getHomeGolas()
    {
        return $this->homeGolas;
    }

    /**
     * @param int $homeGolas
     */
    public function setHomeGolas($homeGolas)
    {
        $this->homeGolas = $homeGolas;
    }

    /**
     * @return int
     */
    public function getAwayGolas()
    {
        return $this->awayGolas;
    }

    /**
     * @param int $awayGolas
     */
    public function setAwayGolas($awayGolas)
    {
        $this->awayGolas = $awayGolas;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return int
     */
    public function getFinalised()
    {
        return $this->finalised;
    }

    /**
     * @param int $finalised
     */
    public function setFinalised($finalised)
    {
        $this->finalised = $finalised;
    }

    /**
     * @return \DateTime
     */
    public function getClientDate()
    {
        return $this->clientDate;
    }

    /**
     * @param \DateTime $clientDate
     */
    public function setClientDate($clientDate)
    {
        $this->clientDate = $clientDate;
    }
    /**
     * @var integer
     */
    private $matchdayId;

    /**
     * @var integer
     */
    private $homeGoals;

    /**
     * @var integer
     */
    private $awayGoals;

    /**
     * @var string
     */
    private $homeName;

    /**
     * @var string
     */
    private $awayName;

    /**
     * @var string
     */
    private $homeImage;

    /**
     * @var string
     */
    private $awayImage;

    /**
     * @var string
     */
    private $nationThumb;

    /**
     * @var integer
     */
    private $leagueId;

    /**
     * @var string
     */
    private $leagueName;

    /**
     * @var integer
     */
    private $matchId;


    /**
     * Set matchdayId
     *
     * @param integer $matchdayId
     *
     * @return Matches
     */
    public function setMatchdayId($matchdayId)
    {
        $this->matchdayId = $matchdayId;

        return $this;
    }

    /**
     * Get matchdayId
     *
     * @return integer
     */
    public function getMatchdayId()
    {
        return $this->matchdayId;
    }

    /**
     * Set homeGoals
     *
     * @param integer $homeGoals
     *
     * @return Matches
     */
    public function setHomeGoals($homeGoals)
    {
        $this->homeGoals = $homeGoals;

        return $this;
    }

    /**
     * Get homeGoals
     *
     * @return integer
     */
    public function getHomeGoals()
    {
        return $this->homeGoals;
    }

    /**
     * Set awayGoals
     *
     * @param integer $awayGoals
     *
     * @return Matches
     */
    public function setAwayGoals($awayGoals)
    {
        $this->awayGoals = $awayGoals;

        return $this;
    }

    /**
     * Get awayGoals
     *
     * @return integer
     */
    public function getAwayGoals()
    {
        return $this->awayGoals;
    }

    /**
     * Set homeName
     *
     * @param string $homeName
     *
     * @return Matches
     */
    public function setHomeName($homeName)
    {
        $this->homeName = $homeName;

        return $this;
    }

    /**
     * Get homeName
     *
     * @return string
     */
    public function getHomeName()
    {
        return $this->homeName;
    }

    /**
     * Set awayName
     *
     * @param string $awayName
     *
     * @return Matches
     */
    public function setAwayName($awayName)
    {
        $this->awayName = $awayName;

        return $this;
    }

    /**
     * Get awayName
     *
     * @return string
     */
    public function getAwayName()
    {
        return $this->awayName;
    }

    /**
     * Set homeImage
     *
     * @param string $homeImage
     *
     * @return Matches
     */
    public function setHomeImage($homeImage)
    {
        $this->homeImage = $homeImage;

        return $this;
    }

    /**
     * Get homeImage
     *
     * @return string
     */
    public function getHomeImage()
    {
        return $this->homeImage;
    }

    /**
     * Set awayImage
     *
     * @param string $awayImage
     *
     * @return Matches
     */
    public function setAwayImage($awayImage)
    {
        $this->awayImage = $awayImage;

        return $this;
    }

    /**
     * Get awayImage
     *
     * @return string
     */
    public function getAwayImage()
    {
        return $this->awayImage;
    }

    /**
     * Set nationThumb
     *
     * @param string $nationThumb
     *
     * @return Matches
     */
    public function setNationThumb($nationThumb)
    {
        $this->nationThumb = $nationThumb;

        return $this;
    }

    /**
     * Get nationThumb
     *
     * @return string
     */
    public function getNationThumb()
    {
        return $this->nationThumb;
    }

    /**
     * Set leagueId
     *
     * @param integer $leagueId
     *
     * @return Matches
     */
    public function setLeagueId($leagueId)
    {
        $this->leagueId = $leagueId;

        return $this;
    }

    /**
     * Get leagueId
     *
     * @return integer
     */
    public function getLeagueId()
    {
        return $this->leagueId;
    }

    /**
     * Set leagueName
     *
     * @param string $leagueName
     *
     * @return Matches
     */
    public function setLeagueName($leagueName)
    {
        $this->leagueName = $leagueName;

        return $this;
    }

    /**
     * Get leagueName
     *
     * @return string
     */
    public function getLeagueName()
    {
        return $this->leagueName;
    }

    /**
     * Get matchId
     *
     * @return integer
     */
    public function getMatchId()
    {
        return $this->matchId;
    }
}
