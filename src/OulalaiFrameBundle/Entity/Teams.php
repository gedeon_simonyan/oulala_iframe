<?php

namespace OulalaiFrameBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Teams
 *
 * @ORM\Table(name="teams")
 * @ORM\Entity(repositoryClass="OulalaiFrameBundle\Repository\TeamsRepository")
 */
class Teams
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255)
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="league", type="string", length=255)
     */
    private $league;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Teams
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Teams
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set league
     *
     * @param string $league
     *
     * @return Teams
     */
    public function setLeague($league)
    {
        $this->league = $league;

        return $this;
    }

    /**
     * Get league
     *
     * @return string
     */
    public function getLeague()
    {
        return $this->league;
    }
    /**
     * @var integer
     */
    private $fkLeagueId = '0';

    /**
     * @var \DateTime
     */
    private $clientDate = 'CURRENT_TIMESTAMP';


    /**
     * Set fkLeagueId
     *
     * @param integer $fkLeagueId
     *
     * @return Teams
     */
    public function setFkLeagueId($fkLeagueId)
    {
        $this->fkLeagueId = $fkLeagueId;

        return $this;
    }

    /**
     * Get fkLeagueId
     *
     * @return integer
     */
    public function getFkLeagueId()
    {
        return $this->fkLeagueId;
    }

    /**
     * Set clientDate
     *
     * @param \DateTime $clientDate
     *
     * @return Teams
     */
    public function setClientDate($clientDate)
    {
        $this->clientDate = $clientDate;

        return $this;
    }

    /**
     * Get clientDate
     *
     * @return \DateTime
     */
    public function getClientDate()
    {
        return $this->clientDate;
    }
}
