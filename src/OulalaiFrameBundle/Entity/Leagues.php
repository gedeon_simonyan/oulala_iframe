<?php

namespace OulalaiFrameBundle\Entity;

/**
 * Leagues
 */
class Leagues
{
    /**
     * @var integer
     */
    private $leaguesId = '0';

    /**
     * @var string
     */
    private $user;

    /**
     * @var integer
     */
    private $entry;

    /**
     * @var string
     */
    private $entryfee;

    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $maxparticipants = '0';

    /**
     * @var integer
     */
    private $timetostart;

    /**
     * @var \DateTime
     */
    private $fromdate;

    /**
     * @var \DateTime
     */
    private $todate;

    /**
     * @var string
     */
    private $prize;

    /**
     * @var string
     */
    private $gprize = '0.00';

    /**
     * @var integer
     */
    private $isGprizeFixed = '0';

    /**
     * @var integer
     */
    private $isPublic = '1';

    /**
     * @var integer
     */
    private $isOpen = '1';

    /**
     * @var integer
     */
    private $isFinalized = '0';

    /**
     * @var string
     */
    private $slug;

    /**
     * @var \DateTime
     */
    private $clientDate = 'CURRENT_TIMESTAMP';

    /**
     * @var integer
     */
    private $templatePrizeId;


    /**
     * Get leaguesId
     *
     * @return integer
     */
    public function getLeaguesId()
    {
        return $this->leaguesId;
    }

    /**
     * Set user
     *
     * @param string $user
     *
     * @return Leagues
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set entry
     *
     * @param integer $entry
     *
     * @return Leagues
     */
    public function setEntry($entry)
    {
        $this->entry = $entry;

        return $this;
    }

    /**
     * Get entry
     *
     * @return integer
     */
    public function getEntry()
    {
        return $this->entry;
    }

    /**
     * Set entryfee
     *
     * @param string $entryfee
     *
     * @return Leagues
     */
    public function setEntryfee($entryfee)
    {
        $this->entryfee = $entryfee;

        return $this;
    }

    /**
     * Get entryfee
     *
     * @return string
     */
    public function getEntryfee()
    {
        return $this->entryfee;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Leagues
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set maxparticipants
     *
     * @param integer $maxparticipants
     *
     * @return Leagues
     */
    public function setMaxparticipants($maxparticipants)
    {
        $this->maxparticipants = $maxparticipants;

        return $this;
    }

    /**
     * Get maxparticipants
     *
     * @return integer
     */
    public function getMaxparticipants()
    {
        return $this->maxparticipants;
    }

    /**
     * Set timetostart
     *
     * @param integer $timetostart
     *
     * @return Leagues
     */
    public function setTimetostart($timetostart)
    {
        $this->timetostart = $timetostart;

        return $this;
    }

    /**
     * Get timetostart
     *
     * @return integer
     */
    public function getTimetostart()
    {
        return $this->timetostart;
    }

    /**
     * Set fromdate
     *
     * @param \DateTime $fromdate
     *
     * @return Leagues
     */
    public function setFromdate($fromdate)
    {
        $this->fromdate = $fromdate;

        return $this;
    }

    /**
     * Get fromdate
     *
     * @return \DateTime
     */
    public function getFromdate()
    {
        return $this->fromdate;
    }

    /**
     * Set todate
     *
     * @param \DateTime $todate
     *
     * @return Leagues
     */
    public function setTodate($todate)
    {
        $this->todate = $todate;

        return $this;
    }

    /**
     * Get todate
     *
     * @return \DateTime
     */
    public function getTodate()
    {
        return $this->todate;
    }

    /**
     * Set prize
     *
     * @param string $prize
     *
     * @return Leagues
     */
    public function setPrize($prize)
    {
        $this->prize = $prize;

        return $this;
    }

    /**
     * Get prize
     *
     * @return string
     */
    public function getPrize()
    {
        return $this->prize;
    }

    /**
     * Set gprize
     *
     * @param string $gprize
     *
     * @return Leagues
     */
    public function setGprize($gprize)
    {
        $this->gprize = $gprize;

        return $this;
    }

    /**
     * Get gprize
     *
     * @return string
     */
    public function getGprize()
    {
        return $this->gprize;
    }

    /**
     * Set isGprizeFixed
     *
     * @param integer $isGprizeFixed
     *
     * @return Leagues
     */
    public function setIsGprizeFixed($isGprizeFixed)
    {
        $this->isGprizeFixed = $isGprizeFixed;

        return $this;
    }

    /**
     * Get isGprizeFixed
     *
     * @return integer
     */
    public function getIsGprizeFixed()
    {
        return $this->isGprizeFixed;
    }

    /**
     * Set isPublic
     *
     * @param integer $isPublic
     *
     * @return Leagues
     */
    public function setIsPublic($isPublic)
    {
        $this->isPublic = $isPublic;

        return $this;
    }

    /**
     * Get isPublic
     *
     * @return integer
     */
    public function getIsPublic()
    {
        return $this->isPublic;
    }

    /**
     * Set isOpen
     *
     * @param integer $isOpen
     *
     * @return Leagues
     */
    public function setIsOpen($isOpen)
    {
        $this->isOpen = $isOpen;

        return $this;
    }

    /**
     * Get isOpen
     *
     * @return integer
     */
    public function getIsOpen()
    {
        return $this->isOpen;
    }

    /**
     * Set isFinalized
     *
     * @param integer $isFinalized
     *
     * @return Leagues
     */
    public function setIsFinalized($isFinalized)
    {
        $this->isFinalized = $isFinalized;

        return $this;
    }

    /**
     * Get isFinalized
     *
     * @return integer
     */
    public function getIsFinalized()
    {
        return $this->isFinalized;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Leagues
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set clientDate
     *
     * @param \DateTime $clientDate
     *
     * @return Leagues
     */
    public function setClientDate($clientDate)
    {
        $this->clientDate = $clientDate;

        return $this;
    }

    /**
     * Get clientDate
     *
     * @return \DateTime
     */
    public function getClientDate()
    {
        return $this->clientDate;
    }

    /**
     * Set templatePrizeId
     *
     * @param integer $templatePrizeId
     *
     * @return Leagues
     */
    public function setTemplatePrizeId($templatePrizeId)
    {
        $this->templatePrizeId = $templatePrizeId;

        return $this;
    }

    /**
     * Get templatePrizeId
     *
     * @return integer
     */
    public function getTemplatePrizeId()
    {
        return $this->templatePrizeId;
    }
}
