<?php

namespace OulalaiFrameBundle\Entity;

/**
 * OperatorAlias
 */
class OperatorAlias
{
    /**
     * @var integer
     */
    private $clientId;

    /**
     * @var string
     */
    private $clientAlias;


    /**
     * Set clientId
     *
     * @param integer $clientId
     *
     * @return OperatorAlias
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;

        return $this;
    }

    /**
     * Get clientId
     *
     * @return integer
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * Set clientAlias
     *
     * @param string $clientAlias
     *
     * @return OperatorAlias
     */
    public function setClientAlias($clientAlias)
    {
        $this->clientAlias = $clientAlias;

        return $this;
    }

    /**
     * Get clientAlias
     *
     * @return string
     */
    public function getClientAlias()
    {
        return $this->clientAlias;
    }
}
