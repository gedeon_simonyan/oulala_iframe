<?php

namespace OulalaiFrameBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * BlogCategories
 *
 * @ORM\Table(name="blog_categories")
 * @ORM\Entity
 */
class BlogCategories
{

    /**
     * @var integer
     *
     * @ORM\Column(name="category_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $categoryId;

    /**
     * @var integer
     *
     * @ORM\Column(name="operator_id", type="integer", nullable=true)
     */
    private $operatorId;

    /**
     * @var string
     *
     * @ORM\Column(name="category_local", type="string", length=5, nullable=true)
     */
    private $categoryLocal;

    /**
     * @var string
     *
     * @ORM\Column(name="category_title", type="string", length=255, nullable=true)
     */
    private $categoryTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="category_slug", type="string", length=255, nullable=true)
     */
    private $categorySlug;

    public function __construct()
    {
        $this->posts = new ArrayCollection();
    }


    /**
     * Get categoryId
     *
     * @return integer
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }

    /**
     * Set operatorId
     *
     * @param integer $operatorId
     *
     * @return BlogCategories
     */
    public function setOperatorId($operatorId)
    {
        $this->operatorId = $operatorId;

        return $this;
    }

    /**
     * Get operatorId
     *
     * @return integer
     */
    public function getOperatorId()
    {
        return $this->operatorId;
    }

    /**
     * Set categoryLocal
     *
     * @param string $categoryLocal
     *
     * @return BlogCategories
     */
    public function setCategoryLocal($categoryLocal)
    {
        $this->categoryLocal = $categoryLocal;

        return $this;
    }

    /**
     * Get categoryLocal
     *
     * @return string
     */
    public function getCategoryLocal()
    {
        return $this->categoryLocal;
    }

    /**
     * Set categoryTitle
     *
     * @param string $categoryTitle
     *
     * @return BlogCategories
     */
    public function setCategoryTitle($categoryTitle)
    {
        $this->categoryTitle = $categoryTitle;

        return $this;
    }

    /**
     * Get categoryTitle
     *
     * @return string
     */
    public function getCategoryTitle()
    {
        return $this->categoryTitle;
    }

    /**
     * Set categorySlug
     *
     * @param string $categorySlug
     *
     * @return BlogCategories
     */
    public function setCategorySlug($categorySlug)
    {
        $this->categorySlug = $categorySlug;

        return $this;
    }

    /**
     * Get categorySlug
     *
     * @return string
     */
    public function getCategorySlug()
    {
        return $this->categorySlug;
    }

    /**
     * Add post
     *
     * @param \OulalaiFrameBundle\Entity\BlogPosts $post
     *
     * @return BlogCategories
     */
    public function addPost(\OulalaiFrameBundle\Entity\BlogPosts $post)
    {
        $this->posts[] = $post;

        return $this;
    }

    /**
     * Remove post
     *
     * @param \OulalaiFrameBundle\Entity\BlogPosts $post
     */
    public function removePost(\OulalaiFrameBundle\Entity\BlogPosts $post)
    {
        $this->posts->removeElement($post);
    }

    /**
     * Get posts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPosts()
    {
        return $this->posts;
    }
}
