<?php

namespace OulalaiFrameBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * BlogPosts
 *
 * @ORM\Table(name="blog_posts")
 * @ORM\Entity
 */
class BlogPosts
{
    /**
     * @var integer
     *
     * @ORM\Column(name="post_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $postId;

    /**
     * @var string
     *
     * @ORM\Column(name="post_subject", type="string", length=255, nullable=true)
     */
    private $postSubject;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_date", type="datetime", nullable=true)
     */
    private $createDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="update_date", type="datetime", nullable=false)
     */
    private $updateDate = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     *
     * @ORM\Column(name="post_locale", type="string", length=5, nullable=true)
     */
    private $postLocale;

    /**
     * @var integer
     *
     * @ORM\Column(name="post_operator", type="integer", nullable=true)
     */
    private $postOperator;

    /**
     * @var string
     *
     * @ORM\Column(name="post_illustration_link", type="string", length=255, nullable=true)
     *@Assert\Image()
     * @Assert\File(maxSize = "4M")
     *
     */
    private $postIllustrationLink;

    /**
     * @var string
     *
     * @ORM\Column(name="post_content", type="text", nullable=false)
     */
    private $postContent;

    /**
     * @var integer
     *
     * @ORM\Column(name="category_id", type="integer", nullable=true)
     */
    private $categoryId;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_featured", type="integer", nullable=true)
     */
    private $isFeatured;

    /**
     * Get postId
     *
     * @return integer
     */
    public function getPostId()
    {
        return $this->postId;
    }

    /**
     * Set postSubject
     *
     * @param string $postSubject
     *
     * @return BlogPosts
     */
    public function setPostSubject($postSubject)
    {
        $this->postSubject = $postSubject;

        return $this;
    }

    /**
     * Get postSubject
     *
     * @return string
     */
    public function getPostSubject()
    {
        return $this->postSubject;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     *
     * @return BlogPosts
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Set updateDate
     *
     * @param \DateTime $updateDate
     *
     * @return BlogPosts
     */
    public function setUpdateDate($updateDate)
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    /**
     * Get updateDate
     *
     * @return \DateTime
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * Set postLocale
     *
     * @param string $postLocale
     *
     * @return BlogPosts
     */
    public function setPostLocale($postLocale)
    {
        $this->postLocale = $postLocale;

        return $this;
    }

    /**
     * Get postLocale
     *
     * @return string
     */
    public function getPostLocale()
    {
        return $this->postLocale;
    }

    /**
     * Set postOperator
     *
     * @param integer $postOperator
     *
     * @return BlogPosts
     */
    public function setPostOperator($postOperator)
    {
        $this->postOperator = $postOperator;

        return $this;
    }

    /**
     * Get postOperator
     *
     * @return integer
     */
    public function getPostOperator()
    {
        return $this->postOperator;
    }


    public function setPostIllustrationLink($postIllustrationLink)
    {
        $this->postIllustrationLink = $postIllustrationLink;

        return $this;
    }


    public function getPostIllustrationLink()
    {
        return $this->postIllustrationLink;
    }

    /**
     * Set postContent
     *
     * @param string $postContent
     *
     * @return BlogPosts
     */
    public function setPostContent($postContent)
    {
        $this->postContent = $postContent;

        return $this;
    }

    /**
     * Get postContent
     *
     * @return string
     */
    public function getPostContent()
    {
        return $this->postContent;
    }

    /**
     * Set categoryId
     *
     * @param integer $categoryId
     *
     * @return BlogPosts
     */
    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;

        return $this;
    }

    /**
     * Get categoryId
     *
     * @return integer
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }

    /**
     * Set isFeatured
     *
     * @param integer $isFeatured
     *
     * @return BlogPosts
     */
    public function setIsFeatured($isFeatured)
    {
        $this->isFeatured = $isFeatured;

        return $this;
    }

    /**
     * Get isFeatured
     *
     * @return integer
     */
    public function getIsFeatured()
    {
        return $this->isFeatured;
    }
}
