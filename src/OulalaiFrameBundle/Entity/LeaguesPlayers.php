<?php

namespace OulalaiFrameBundle\Entity;

/**
 * LeaguesPlayers
 */
class LeaguesPlayers
{
    /**
     * @var integer
     */
    private $leaguesId = '0';

    /**
     * @var integer
     */
    private $playerId = '0';

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $clientDate = 'CURRENT_TIMESTAMP';


    /**
     * Set leaguesId
     *
     * @param integer $leaguesId
     *
     * @return LeaguesPlayers
     */
    public function setLeaguesId($leaguesId)
    {
        $this->leaguesId = $leaguesId;

        return $this;
    }

    /**
     * Get leaguesId
     *
     * @return integer
     */
    public function getLeaguesId()
    {
        return $this->leaguesId;
    }

    /**
     * Set playerId
     *
     * @param integer $playerId
     *
     * @return LeaguesPlayers
     */
    public function setPlayerId($playerId)
    {
        $this->playerId = $playerId;

        return $this;
    }

    /**
     * Get playerId
     *
     * @return integer
     */
    public function getPlayerId()
    {
        return $this->playerId;
    }

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return LeaguesPlayers
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set clientDate
     *
     * @param \DateTime $clientDate
     *
     * @return LeaguesPlayers
     */
    public function setClientDate($clientDate)
    {
        $this->clientDate = $clientDate;

        return $this;
    }

    /**
     * Get clientDate
     *
     * @return \DateTime
     */
    public function getClientDate()
    {
        return $this->clientDate;
    }
}
