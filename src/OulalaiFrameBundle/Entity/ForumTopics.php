<?php

namespace OulalaiFrameBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ForumTopics
 *
 * @ORM\Table(name="forum_topics")
 * @ORM\Entity
 */
class ForumTopics
{
    /**
     * @var string
     *
     * @ORM\Column(name="topic_subject", type="string", length=255, nullable=true)
     */
    private $topicSubject;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_time", type="datetime", nullable=true)
     */
    private $createTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="update_time", type="datetime", nullable=true)
     */
    private $updateTime;

    /**
     * @var integer
     *
     * @ORM\Column(name="topic_by", type="integer", nullable=true)
     */
    private $topicBy;

    /**
     * @var string
     *
     * @ORM\Column(name="topic_locale", type="string", length=2, nullable=true)
     */
    private $topicLocale;

    /**
     * @var string
     *
     * @ORM\Column(name="topic_operator", type="string", length=45, nullable=true)
     */
    private $topicOperator;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_admin", type="boolean", nullable=true)
     */
    private $isAdmin = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="topic_level", type="integer", nullable=true)
     */
    private $topicLevel;

    /**
     * @var integer
     *
     * @ORM\Column(name="topic_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $topicId;



    /**
     * Set topicSubject
     *
     * @param string $topicSubject
     *
     * @return ForumTopics
     */
    public function setTopicSubject($topicSubject)
    {
        $this->topicSubject = $topicSubject;

        return $this;
    }

    /**
     * Get topicSubject
     *
     * @return string
     */
    public function getTopicSubject()
    {
        return $this->topicSubject;
    }

    /**
     * Set createTime
     *
     * @param \DateTime $createTime
     *
     * @return ForumTopics
     */
    public function setCreateTime($createTime)
    {
        $this->createTime = $createTime;

        return $this;
    }

    /**
     * Get createTime
     *
     * @return \DateTime
     */
    public function getCreateTime()
    {
        return $this->createTime;
    }

    /**
     * Set updateTime
     *
     * @param \DateTime $updateTime
     *
     * @return ForumTopics
     */
    public function setUpdateTime($updateTime)
    {
        $this->updateTime = $updateTime;

        return $this;
    }

    /**
     * Get updateTime
     *
     * @return \DateTime
     */
    public function getUpdateTime()
    {
        return $this->updateTime;
    }

    /**
     * Set topicBy
     *
     * @param integer $topicBy
     *
     * @return ForumTopics
     */
    public function setTopicBy($topicBy)
    {
        $this->topicBy = $topicBy;

        return $this;
    }

    /**
     * Get topicBy
     *
     * @return integer
     */
    public function getTopicBy()
    {
        return $this->topicBy;
    }

    /**
     * Set topicLocale
     *
     * @param string $topicLocale
     *
     * @return ForumTopics
     */
    public function setTopicLocale($topicLocale)
    {
        $this->topicLocale = $topicLocale;

        return $this;
    }

    /**
     * Get topicLocale
     *
     * @return string
     */
    public function getTopicLocale()
    {
        return $this->topicLocale;
    }

    /**
     * Set topicOperator
     *
     * @param string $topicOperator
     *
     * @return ForumTopics
     */
    public function setTopicOperator($topicOperator)
    {
        $this->topicOperator = $topicOperator;

        return $this;
    }

    /**
     * Get topicOperator
     *
     * @return string
     */
    public function getTopicOperator()
    {
        return $this->topicOperator;
    }

    /**
     * Set isAdmin
     *
     * @param boolean $isAdmin
     *
     * @return ForumTopics
     */
    public function setIsAdmin($isAdmin)
    {
        $this->isAdmin = $isAdmin;

        return $this;
    }

    /**
     * Get isAdmin
     *
     * @return boolean
     */
    public function getIsAdmin()
    {
        return $this->isAdmin;
    }

    /**
     * Set topicLevel
     *
     * @param integer $topicLevel
     *
     * @return ForumTopics
     */
    public function setTopicLevel($topicLevel)
    {
        $this->topicLevel = $topicLevel;

        return $this;
    }

    /**
     * Get topicLevel
     *
     * @return integer
     */
    public function getTopicLevel()
    {
        return $this->topicLevel;
    }

    /**
     * Get topicId
     *
     * @return integer
     */
    public function getTopicId()
    {
        return $this->topicId;
    }
}
