<?php

namespace OulalaiFrameBundle\Form;

use Doctrine\ORM\EntityRepository;
use OulalaiFrameBundle\Entity\BlogCategories;
use OulalaiFrameBundle\Entity\BlogPosts;
use OulalaiFrameBundle\Repository\BlogRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BlogPostsType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $options['entity_manager'];
        $postCatId = $builder->getData()->getCategoryId();


//        $builder->add('postSubject')->add('createDate')->add('updateDate')->add('postLocale')->add('postOperator')->add('postIllustrationLink')->add('postContent')->add('categoryId');
        $builder->add('postSubject', TextType::class, array('label' => 'Title'))
            ->add(
                'postLocale',
                ChoiceType::class,
                array(
                    'label' => 'Post language',
                    'choices' => array('EN' => 'EN', 'FR' => 'FR', 'GR' => 'GR', 'TR' => 'TR', 'PT' => 'PT'),
                )
            )
            ->add('postContent', TextareaType::class, array('label' => 'Post Content'))
            ->add('isFeatured',ChoiceType::class,array('label' => 'Featured Post','choices' => array('1' => '1', '0' => '0'),))
            ->add('categoryId', null, array('label' => 'Category'));
        $builder->add(
            'postIllustrationLink',
            FileType::class,
            [
                'required' => false,
                'label' => 'Upload image',
                'attr' => [
                    'class' => 'form-control hidden',
                    'id' => 'imgInp',
                    'data-iconName' => 'glyphicon glyphicon-camera',
                    'data-buttonText' => null,
                    'accept' => 'image/*',
                ],
                'label_attr' => [
                    'class' => ' btn btn-primary btn-block btn-outlined',
                ],
                'data_class' => null,
            ]
        );

        $formModifier = function (FormInterface $form, $locale = 'EN') {
            $form->add('categoryId', EntityType::class, array(
                'class'=>'OulalaiFrameBundle\Entity\BlogCategories',
                'query_builder' => function (EntityRepository $repo) use ($locale) {
                    $session=new Session();
                    $clientId=$session->get('admin_operator_id');
                    if($clientId){
                        return $repo->createQueryBuilder('b')
                            ->where('b.operatorId='.$clientId.' AND b.categoryLocal=:locale')
                            ->setParameter('locale', $locale);
                    } else {
                        return $repo->createQueryBuilder('b');
                    }

                },
                'choice_label' => 'category_title',
                'choice_value' => 'category_id',
                'expanded' => false,
                'multiple' => false,
                'label'=>'Post Category',
            ));
        };

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($formModifier) {
                $data = $event->getData();
                $locale = $data->getPostLocale();
                if (!$locale) {
                    $locale="EN";
                }
                $formModifier($event->getForm(), $locale);
            }
        );
        $builder->get('postLocale')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($formModifier) {
                // It's important here to fetch $event->getForm()->getData(), as
                // $event->getData() will get you the client data (that is, the ID)
                $locale = $event->getForm()->getData();

                // since we've added the listener to the child, we'll have to pass on
                // the parent to the callback functions!
                $formModifier($event->getForm()->getParent(), $locale);
            }
        );
    }

    public function getName()
    {
        return 'blogPosts';
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'OulalaiFrameBundle\Entity\BlogPosts',
            )
        );
        $resolver->setRequired('entity_manager');
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'oulalaiframebundle_blogposts';
    }
}
