<?php

namespace OulalaiFrameBundle\Repository;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManager;
use OulalaiFrameBundle\ApiManager\ApiUrls;
use OulalaiFrameBundle\ApiManager\Manager;
use OulalaiFrameBundle\ApiService\Api;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Created by Oulala.
 * User: JUL
 * Date: 11/05/2017
 * Time: 14:09
 */
class PrizesRepository
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var Api
     */
    private $apiService;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var
     */
    private $prizesUrl;

    /**
     * PrizesRepository constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em, Api $apiService, ContainerInterface $container)
    {
        $this->em = $em;
        $this->apiService = $apiService;
        $this->container=$container;
        $this->prizesUrl = $container->getParameter('api_base_url') . 'prizes';
    }

    public static function calculatePrizes($fTotalFees)
    {
        $fCommissionRatio = 0.1;
        $fCommission = bcmul($fTotalFees, $fCommissionRatio, 2);
        $fOutput = bcsub($fTotalFees, $fCommission, 2);

        return $fOutput;
    }

    public function getRewardDetails($leagueId, $lang)
    {
        $sSQL = "SELECT
				tl.title, tl.description, tc.css_class, tl.fk_member_league_template_id
				FROM member_league_template_cmn tc
				INNER JOIN leagues ml ON ml.template_prize_id = tc.member_league_template_id
				INNER JOIN member_league_template_lang tl ON tl.fk_member_league_template_id = tc.member_league_template_id

				AND tc.is_active = 1
				AND tl.is_active = 1
				WHERE ml.leagues_id = :member_league_id
				AND tl.fk_language_id = {$lang}
				ORDER BY tc.rank ASC";

        $em = $this->em->getConnection()->prepare($sSQL);
        $em->bindValue('member_league_id', $leagueId);
        $em->execute();
        $rewardOption = $em->fetch();
        return $rewardOption;
    }

    /**
     * returns the payout ratios according to a member league template prize id
     *
     * @param $iMemberLeaguePayoutID
     * @return array
     */
    public function retrievePaymentOptionAmounts($iMemberLeaguePayoutID)
    {
        if ($iMemberLeaguePayoutID && is_numeric($iMemberLeaguePayoutID)) {
            /* Retrieving the payout options */
            $sSQL = "SELECT mltpm.amount,  mltpm.rank
				FROM member_league_template_prizes mltp
				INNER JOIN member_league_template_prizes_members mltpm ON mltpm.fk_member_league_template_prize_id = mltp.member_league_template_prize_id
				WHERE mltp.member_league_template_prize_id = :member_league_payout_id";

            $em = $this->em->getConnection()->prepare($sSQL);
            $em->bindValue('member_league_payout_id', $iMemberLeaguePayoutID);
            $em->execute();
            return $em->fetchAll();
        }
    }

    /**
    * Gets the total entries and the total prize money.
    *
    * @param int $iMemberLeagueID
    * @param int $iLanguageID
    *
    * @return array
    */
    public function getEntriesAndPrizes($leagueId)
    {
        $sSQL = "
			SELECT
				ml.entryFee as entry_fee,
				ml.maxParticipants as max_participants,
				COUNT(mt.member_team_id) as entries,
				(CASE WHEN (ml.`is_gprize_fixed` = 1) 
                 THEN
                     ml.`gprize`
                 ELSE
                      ml.`gprize`+ml.`prize`
                 END) AS prizes 
			FROM leagues ml
			INNER JOIN squads mt ON ml.leagues_id = mt.league
			WHERE ml.leagues_id = :member_league_id
					AND mt.valid_team = 1
					AND mt.banned_team = 0
					AND mt.is_blocked = 0";

        $em = $this->em->getConnection()->prepare($sSQL);
        $em->bindValue('member_league_id', $leagueId);
        $em->execute();
        $aSearchResults = $em->fetch();

        // Prizes
        if (is_array($aSearchResults) && array_key_exists('prizes', $aSearchResults)) {
            $aSearchResults['prizes'] = static::calculatePrizes($aSearchResults['prizes']);
        }

        return $aSearchResults;
    }

    public function getPrizes($lang)
    {
        $em = $this->em->getConnection()
            ->prepare(
                " SELECT p.`css_class`,p.id,mltl.* FROM prizes p
					INNER JOIN member_league_template_lang mltl ON mltl.`fk_member_league_template_id` = p.`id`
					WHERE mltl.`fk_language_id` = {$lang}"
            );
        $em->execute();

        $prizes = $em->fetchAll();

        return $prizes;
    }

    public function updatePrizesData($access_token)
    {
        $prizes = $this->apiService->APIRequest('GET', $this->prizesUrl, $access_token);
        if (!empty($prizes->data)) {
            foreach ($prizes->data as $prize) {
                $em = $this->em->getConnection()
                    ->prepare(
                        'INSERT IGNORE INTO member_league_prizes
                    (
                    `member_league_prize_id`,
                    `fk_member_league_team_id`,
                    `amount`,
                    `rank`,
                    `date_awarded`
                    )
                    VALUES
                    (
                    :member_league_prize_id,
                    :fk_member_league_team_id,
                    :amount,
                    :rank,
                    :date_awarded);'
                    );

                $em->bindValue('member_league_prize_id', intval($prize->member_league_prize_id));
                $em->bindValue('fk_member_league_team_id', intval($prize->fk_member_league_team_id));
                $em->bindValue('amount', $prize->amount);
                $em->bindValue('rank', intval($prize->rank));
                $em->bindValue('date_awarded', $prize->date_awarded->date);
                $em->execute();
            }
        }
    }
}
