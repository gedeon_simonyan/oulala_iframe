<?php

namespace OulalaiFrameBundle\Repository;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManager;
use OulalaiFrameBundle\ApiManager\ApiUrls;
use OulalaiFrameBundle\ApiManager\Manager;
use OulalaiFrameBundle\ApiService\Api;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Created by Oulala.
 * User: JUL
 * Date: 11/05/2017
 * Time: 14:09
 */
class DashboardRepository
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var Api
     */
    private $apiService;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var
     */
    private $usersUrl;

    /**
     *
     * @var
     */
    private $levelUrl;

    /**
     *
     * @var
     */
    private $rewardsUrl;

    /**
     *
     * @var
     */
    private $followersUrl;

    /**
     *
     * @var
     */
    private $AllFollowersUrl;


    /**
     * DashboardRepository constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em, Api $apiService, ContainerInterface $container)
    {
        $this->em = $em;
        $this->apiService = $apiService;
        $this->container=$container;
        $this->usersUrl = $container->getParameter('api_base_url') . 'matches';
        $this->levelUrl = $container->getParameter('api_base_url') . 'levelinfo';
        $this->rewardsUrl = $container->getParameter('api_base_url') . 'rewards';
        $this->followersUrl = $container->getParameter('api_base_url') . 'followers';
        $this->AllFollowersUrl = $container->getParameter('api_base_url') . 'allfollowers';
    }

    public function getUserRefByUsername($sUsername, $clientId)
    {
        $sSQL = 'SELECT userref FROM member_level_info WHERE username = :username AND `website_id`=:client';
        $em = $this->em->getConnection()->prepare($sSQL);
        $em->bindValue('username', $sUsername);
        $em->bindValue('client', $clientId);
        $em->execute();

        return $em->fetch();
    }

    public function hasPreference($userRef, $iPref)
    {
        $sSQL = 'SELECT `value` FROM member_preferences WHERE user_ref = :ref AND preference_id = :pref_id';
        $em = $this->em->getConnection()->prepare($sSQL);
        $em->bindValue('ref', $userRef);
        $em->bindValue('pref_id', $iPref);
        $em->execute();

        return $em->fetch();
    }

    /**
     * Get followers for a profile ID
     * @param  int $sRef ref of Member Profile being viewed
     * @return array List of followers
     */
    public function getMemberFollowers($sRef)
    {
        $sSQL = 'SELECT mf.*,mli.`username` FROM `member_follows` mf
			INNER JOIN `member_level_info` mli ON mli.`userref` = mf.`follower`
			 WHERE mf.`following` = :user_ref';

        $em = $this->em->getConnection()->prepare($sSQL);
        $em->bindValue('user_ref', $sRef);
        $em->execute();

        return $em->fetchAll();
    }

    /**
     * Get following members for a profile ID
     * @param  int $sRef ref of Member Profile being viewed
     * @return array List of following
     */
    public function getMemberFollowings($sRef)
    {
        $sSQL = 'SELECT mf.*,mli.`username` FROM `member_follows` mf
			INNER JOIN `member_level_info` mli ON mli.`userref` = mf.`following`
			 WHERE mf.`follower` = :user_ref';
        $em = $this->em->getConnection()->prepare($sSQL);
        $em->bindValue('user_ref', $sRef);
        $em->execute();

        return $em->fetchAll();
    }

    public function getMemberLevelInfo($sRef)
    {
        $sSQL = 'SELECT * FROM `member_level_info` WHERE `userref` = :user_ref';
        $em = $this->em->getConnection()->prepare($sSQL);
        $em->bindValue('user_ref', $sRef);
        $em->execute();

        return $em->fetch();
    }

    public function getMemberBadges($sRef)
    {
        $sSQL = 'SELECT * FROM member_badges WHERE userref = :user_ref GROUP BY `reward_rule_id` ';
        $em = $this->em->getConnection()->prepare($sSQL);
        $em->bindValue('user_ref', $sRef);
        $em->execute();

        return $em->fetchAll();
    }

    public function getAllLevels()
    {
        $sSQL = 'SELECT * FROM `member_levels_cmn`';
        $em = $this->em->getConnection()->prepare($sSQL);
        $em->execute();

        return $em->fetchAll();
    }

    public function getAllRewards()
    {
        $sSQL = "SELECT fk_reward_id AS reward_id, title, description FROM member_badges GROUP BY `fk_reward_id` ORDER BY reward_id";
        $em = $this->em->getConnection()->prepare($sSQL);
        $em->execute();

        return $em->fetchAll();
    }

    public function getMemberAllAchievedRewardsIds($sRef)
    {
        $sSQL = "SELECT fk_reward_id AS reward_id FROM member_badges
    		WHERE userref=:user_ref
    			AND is_active = 1
    			GROUP BY `fk_reward_id` ORDER BY reward_id";
        $em = $this->em->getConnection()->prepare($sSQL);
        $em->bindValue('user_ref', $sRef);
        $em->execute();
        $result = $em->fetchAll();

        return array_column($result, "reward_id");
    }


    public function getMemberNotifications($sRef)
    {
        $sSQL = "SELECT n.`note_name`,n.`rank`,l.`username`,n.`note_type` 
                 FROM member_notifications AS n 
                 INNER JOIN member_follows AS mf ON mf.`following` = n.`user_ref` 
                 INNER JOIN member_level_info AS l ON l.`userref` = n.`user_ref` 
                 WHERE mf.`follower` = :user_ref 
                 ORDER BY n.`note_date` DESC ";
        $em = $this->em->getConnection()->prepare($sSQL);
        $em->bindValue('user_ref', $sRef);
        $em->execute();
        $result = $em->fetchAll();

        return $result;
    }


    public function getRewardsDataByRule($rule_id)
    {
        $sSQL = "SELECT fk_reward_id AS reward_id, `title`, `description`, `badge_type` FROM member_badges
    		WHERE reward_rule_id = :rule_id GROUP BY `fk_reward_id` ORDER BY rank DESC";
        $em = $this->em->getConnection()->prepare($sSQL);
        $em->bindValue('rule_id', $rule_id);
        $em->execute();

        return $em->fetchAll();
    }

    /**
     * Checks whether entry exists
     * @param  int $iMemberID ref of Member choosing to follow other Member
     * @param  int $iMemberFollowID ref of Member being followed
     * @return bool                    Follow Exists
     */
    public function follwingExists($iMemberID, $iMemberFollowID)
    {
        $sSQL = "SELECT `follower`
			FROM member_follows
			WHERE `follower` = :iMemberID
			AND `following` = :iMemberFollowID";

        $em = $this->em->getConnection()->prepare($sSQL);
        $em->bindValue('iMemberID', $iMemberID);
        $em->bindValue('iMemberFollowID', $iMemberFollowID);
        $em->execute();
        $result = $em->fetch();
        if (empty($result)) {
            return false;
        }

        return true;
    }

    /**
     * Insert a new follow for the logged in member
     * @param  int $iMemberID Ref of Member following
     * @param  int $iMemberFollowID Ref of Member being followed
     */
    public function follow($iMemberID, $iMemberFollowID)
    {
        if (!$this->follwingExists($iMemberID, $iMemberFollowID)) {
            $sSQL = "INSERT INTO `member_follows` (`follower`, `following`)
							VALUES
						( :follower, :following )";
            $em = $this->em->getConnection()->prepare($sSQL);
            $em->bindValue('iMemberID', $iMemberID);
            $em->bindValue('iMemberFollowID', $iMemberFollowID);
            $em->execute();
        }
    }

    /**
     * Removes a follow for the logged in member.
     *
     * @param  int $iMemberID Ref of Member unfollowing
     * @param  int $iMemberFollowID Ref of Member being unfollowed
     */
    public function unfollow($iMemberID, $iMemberFollowID)
    {
        $sSQL = "DELETE FROM member_follows
		         WHERE `follower` = :memberID
		         AND `follwoing` = :memberFollowID";
        $em = $this->em->getConnection()->prepare($sSQL);
        $em->bindValue('iMemberID', $iMemberID);
        $em->bindValue('iMemberFollowID', $iMemberFollowID);
        $em->execute();
    }

    public function getFollowerIDs($iMemberID)
    {
        $sSQL = "SELECT mf.follower
			FROM member_follows mf
			WHERE mf.following = :ref AND mf.follower != :ref";
        $em = $this->em->getConnection()->prepare($sSQL);
        $em->bindValue('ref', $iMemberID);
        $em->execute();
        $result = $em->fetchAll();

        return array_column($result, "follower");
    }

    public function getFollowingIDs($iMemberID)
    {
        $sSQL = "SELECT mf.following
			FROM member_follows mf
			WHERE  mf.follower = :ref  AND mf.`following` != :ref";
        $em = $this->em->getConnection()->prepare($sSQL);
        $em->bindValue('ref', $iMemberID);
        $em->execute();
        $result = $em->fetchAll();

        return array_column($result, "following");
    }

    public function getMembersToFollow($sRef, $search, $website_id, $is_invite = false)
    {
        $sSQL = " SELECT `username`, `userref` FROM member_level_info
			WHERE `username` LIKE :search
				AND `userref` != :ref
				AND website_id = :website_id
				";

        if (!$is_invite) {
            $followings = $this->getFollowingIDs($sRef);
            $followings = '\''.implode('\', \'', $followings).'\'';
            $sSQL .= "AND `userref` NOT IN ({$followings})";
        }
        $em = $this->em->getConnection()->prepare($sSQL);
        $em->bindValue('ref', $sRef);
        $em->bindValue('search', $search."%");
        $em->bindValue('website_id', $website_id);
        $em->execute();
        $result = $em->fetchAll();

        return $result;
    }

    public function updateMemberBio($access_token, $user_ref)
    {
        $user_bio = $this->apiService->APIRequest('GET', $this->container->getParameter('api_base_url') . 'bio' . '/' .$user_ref, $access_token);
        if (isset($user_bio->status) && $user_bio->status == 'success') {
            if (!empty($user_bio->data)) {
                foreach ($user_bio->data as $key => $bio) {
                    $em = $this->em->getConnection()->prepare(
                        "UPDATE `member_level_info` SET `bio`=:bio, `team_id`=:team_id
                                                      WHERE `userref`=:user_ref "
                    );
                    $em->bindValue('bio', $bio->bio);
                    $em->bindValue('team_id', $bio->team_id);
                    $em->bindValue('user_ref', $key);
                    $em->execute();
                }
            }
        }
    }

    public function updateOneUserInfo($access_token, $userId)
    {
        $user_info = $this->apiService->APIRequest(
            'GET',
            $this->levelUrl.'/'.$userId,
            $access_token
        );

        if (!empty($user_info->data)) {
            $em = $this->em->getConnection()->prepare(
                "INSERT INTO `member_level_info`
                        	(`userref`, `username`, `points`, `level`, `memberexperience`, `membertrophy`, `website_id`, `bio`, `team_id`)
							VALUES
							(:user_ref, :u_name, :points, :level, :m_exp, :m_tr, :website_id, :bio, :team_id);"
            );

            $em2 = $this->em->getConnection()->prepare(
                "INSERT INTO `member_preferences` (`user_ref`, `preference`, `value`, `preference_id`)
							VALUES (:user_ref, :preference, :val, :preference_id);"
            );

            foreach ($user_info->data as $key => $info) {
                if (isset($user_info->status) && $user_info->status == 'success') {
                    $this->em->getConnection()->exec(
                        "DELETE FROM  `member_level_info` WHERE `userref`='{$key}'"
                    );
                    $this->em->getConnection()->exec(
                        "DELETE FROM  `member_preferences` WHERE `user_ref`='{$key}'"
                    );
                }
                $em->bindValue('user_ref', $key);
                $em->bindValue('u_name', $info->username);
                $em->bindValue('points', $info->memberpoints);
                $em->bindValue('level', $info->memberlevel);
                $em->bindValue('m_exp', $info->memberexperience->value);
                $em->bindValue('m_tr', $info->membertrophy);
                $em->bindValue('website_id', $info->website_id);
                $em->bindValue('bio', $info->bio);
                $em->bindValue('team_id', $info->team_id);
                if (!empty($info->preferences)) {
                    foreach ($info->preferences as $pref) {
                        $em2->bindValue('user_ref', $key);
                        $em2->bindValue('preference', $pref->preference);
                        $em2->bindValue('val', $pref->preference_value);
                        $em2->bindValue('preference_id', $pref->preference_id);
                        $em2->execute();
                    }
                }
                $em->execute();
            }
        }
    }
    public function updateUserInfo($access_token)
    {
        $userIds = $this->apiService->APIRequest('GET', $this->usersUrl, $access_token);
        if ($userIds && !empty($userIds) && !empty($userIds->data)) {
            foreach ($userIds->data as $userId) {
                if (isset($userId->external_ref)) {
                    $this->updateOneUserInfo($access_token, $userId->external_ref);
                }
            }
        }
    }

    public function updateUserFollowers($access_token, $user_ref = null)
    {
        $connection = $this->em->getConnection();
        if ($user_ref != null) {
            $userIds = array();
            $userObj = new \stdClass();
            $userObj->external_ref = new \stdClass();
            $userObj->external_ref = $user_ref;
            $userIds[0] = $userObj;
            $connection->exec("DELETE FROM `member_follows` WHERE `follower`='{$user_ref}'");
        } else {
            $userRefs = $this->apiService->APIRequest('GET', $this->usersUrl, $access_token);
            $connection->exec('TRUNCATE TABLE `member_follows`');
            if (isset($userRefs->status) && $userRefs->status == 'success') {
                $userIds = $userRefs->data;
            } else {
                exit('error');
            }
        }

        if ($userIds && !empty($userIds)) {
            foreach ($userIds as $userId) {
                if (isset($userId->external_ref)) {
                    $user_follows = $this->apiService->APIRequest(
                        'GET',
                        $this->followersUrl.'/'.$userId->external_ref,
                        $access_token
                    );

                    if (!empty($user_follows->data)) {
                        $em = $connection->prepare(
                            "INSERT INTO `member_follows`
					(`follower`,`following`)
					VALUES
					(:follower, :following);"
                        );
                        foreach ($user_follows->data as $key => $info) {
                            if (!empty($info->following)) {
                                foreach ($info->following as $item) {
                                    $em->bindValue('follower', $key);
                                    $em->bindValue('following', $item->external_ref);
                                    $em->execute();
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public function updateAllFollowers($access_token)
    {
        $connection = $this->em->getConnection();
        $user_follows = Manager::APIRequest('GET', $this->AllFollowersUrl, $access_token);

        if (!empty($user_follows->status) && $user_follows->status == 'success') {
            $connection->exec('DELETE FROM `member_follows` WHERE 1');
        }
        if (isset($user_follows->data) && !empty($user_follows->data)) {
            $em = $connection->prepare(
                "INSERT INTO `member_follows`
					(`follower`,`following`)
					VALUES
					(:follower, :following);"
            );
            foreach ($user_follows->data as $key => $info) {
                if (isset($info->following) && !empty($info->following)) {
                    foreach ($info->following[0] as $item) {
                        $em->bindValue('follower', $key);
                        $em->bindValue('following', $item->external_ref);
                        $em->execute();
                    }
                }
            }
        }
    }

    public function updateUserRewards($access_token)
    {
        $userIds = $this->apiService->APIRequest('GET', $this->usersUrl, $access_token);

        if ($userIds && !empty($userIds) && !empty($userIds->data)) {
            foreach ($userIds->data as $userId) {
                if (isset($userId->external_ref)) {
                    $this->updateOneUserReward($userId->external_ref, 'Football Manager', $access_token);
                }
            }
        }
    }

    public function updateOneUserReward($user_ref, $client_name, $access_token)
    {
        if ($user_ref) {
            $rewards = $this->apiService->APIRequest(
                'GET',
                $this->rewardsUrl.'/'.$user_ref,
                $access_token
            );
            $connection = $this->em->getConnection();

            if (!empty($rewards->data)) {
                foreach ($rewards->data as $key => $info) {
                    if (isset($rewards->status) && $rewards->status == 'success') {
                        $connection->exec("DELETE FROM  `member_badges` WHERE `userref`='{$key}'");
                    }
                    if (!empty($info->budges)) {
                        $em = $this->em->getConnection()->prepare(
                            "INSERT INTO `member_badges` (
											`userref`,
											`fk_reward_id`,
											`title`,
											`description`,
											`is_active`,
											`memberLastReward`,
											`memberNextReward`,
											`badge_type`,
											`rank`,
											`reward_rule_id`)
										VALUES
											(:user_ref,
											:reward_id,
											:title,
											:descrip,
											:is_active,
											:memberLastReward,
											:memberNextReward,
											:badge_type,
											:rank,
											:reward_rule_id);"
                        );
                        foreach ($info->budges as $badge) {
                            if (isset($badge->memberNextReward->description)) {
                                $badge->memberNextReward->description=str_replace('Oulala', $client_name, $badge->memberNextReward->description);
                            }
                            if (isset($badge->memberLastReward->description)) {
                                $badge->memberLastReward->description=str_replace('Oulala', $client_name, $badge->memberLastReward->description);
                            }
                            $memberLastReward = serialize($badge->memberLastReward);
                            $memberNextReward = serialize($badge->memberNextReward);
                            $reward_rule_id = $badge->rule->reward_rule_id;
                            if (!empty($badge->reward)) {
                                foreach ($badge->reward as $reward) {
                                    $reward_is_active = 0;
                                    if (!empty($info->rewardIds)) {
                                        if (in_array($reward->reward_id, $info->rewardIds)) {
                                            $reward_is_active = 1;
                                        }
                                    }
                                    $rank = 3;
                                    switch ($reward->type) {
                                        case 'Silver':
                                            $rank = 2;
                                            break;
                                        case 'Gold':
                                            $rank = 1;
                                            break;
                                    }
                                    $description=str_replace('Oulala', $client_name, $reward->description);
                                    $em->bindValue('user_ref', $key);
                                    $em->bindValue('reward_id', $reward->reward_id);
                                    $em->bindValue('title', $reward->title);
                                    $em->bindValue('descrip', $description);
                                    $em->bindValue('is_active', $reward_is_active);
                                    $em->bindValue('memberLastReward', $memberLastReward);
                                    $em->bindValue('memberNextReward', $memberNextReward);
                                    $em->bindValue('badge_type', $reward->type);
                                    $em->bindValue('rank', $rank);
                                    $em->bindValue('reward_rule_id', $reward_rule_id);

                                    $em->execute();
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
