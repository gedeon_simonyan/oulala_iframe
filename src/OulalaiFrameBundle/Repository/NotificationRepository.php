<?php

namespace OulalaiFrameBundle\Repository;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManager;
use OulalaiFrameBundle\ApiManager\ApiUrls;
use OulalaiFrameBundle\ApiManager\Manager;
use OulalaiFrameBundle\ApiService\Api;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Created by Oulala.
 * User: JUL
 * Date: 11/05/2017
 * Time: 14:09
 */
class NotificationRepository
{
    /**
     * @var EntityManager
     */
    private $em;
    private $content;

    /**
     * @var LeaguesRepository
     */
    private $leaguesRepository;

    /**
     * @var SquadRepository
     */
    private $squadRepository;

    /**
     * @var TokenrRepository
     */
    private $tokenRepository;

    /**
     * @var TokenrRepository
     */
    private $configRepository;

    /**
     * @var Api
     */
    private $apiService;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var
     */
    private $refundUrl;

    /**
     * @var
     */
    private $payoutUrl;

    /**
     * NitificationRepository constructor.
     * @param TokenrRepository $tokenRepository
     * @param LeaguesRepository $leaguesRepository
     * @param SquadRepository $squadRepository
     * @param EntityManager $em
     */
    public function __construct(
        EntityManager $em,
        LeaguesRepository $leaguesRepository,
        SquadRepository $squadRepository,
        TokenrRepository $tokenRepository,
        ConfigRepository $configRepository,
        Api $apiService,
        ContainerInterface $container
    ) {
        $this->em = $em;
        $this->leaguesRepository = $leaguesRepository;
        $this->tokenRepository = $tokenRepository;
        $this->squadRepository = $squadRepository;
        $this->configRepository = $configRepository;
        $this->apiService = $apiService;
        $this->container = $container;
        $this->refundUrl = $container->getParameter('api_base_url').'refunds';
        $this->payoutUrl = $container->getParameter('api_base_url').'payouts';
    }


    public function sendPayouts()
    {
        $access_token = $this->tokenRepository->getAllTokens();
        foreach ($access_token as $token) {
            $this->checkPayouts($token['token'], $token['client_id']);
        }
    }

    private function checkPayouts($access_token, $client_id)
    {
        $date = new \DateTime();
        $dates = [];
        for ($i = 0; $i <= 1; $i++) {
            $date->modify('-'.$i.' day');
            $now = $date->format('Y-m-d');
            $dates[] = $now;
        }

        foreach ($dates as $now) {
            $payouts = $this->apiService->APIRequest('GET', $this->payoutUrl.'/'.$now, $access_token);//$now /2017-04-19
            $connection = $this->em->getConnection();
            if (isset($payouts->status) && $payouts->status == 'success') {
                if (isset($payouts->data) && is_array($payouts->data[0])) {
                    $clients_payouts = $payouts->data[0];

                    foreach ($clients_payouts as $clients_payout) {
                        $trans_id = $clients_payout->id;
                        $user_ref = $clients_payout->userId;
                        $username = $clients_payout->userName;
                        $amount = $clients_payout->cashIn;
                        $league_id = $clients_payout->leagueId;
                        $team_id = $clients_payout->member_team_id;
                        $message = $clients_payout->agentMessage;
                        $send_date = $clients_payout->createdAt;
                        $event_type = $clients_payout->agentMessageTrans;
                        $event_date = date('Y-m-d H:i:s', strtotime('-1 hour', strtotime($send_date)));
                        $trans = $connection
                            ->prepare(
                                'SELECT `id` FROM `member_transactions` WHERE
                          `transaction_id`=:trans_id'
                            );
                        $trans->bindValue('trans_id', $trans_id);
                        $trans->execute();

                        if (!$trans->fetchColumn()) {
                            $em = $connection
                                ->prepare(
                                    'INSERT INTO `member_transactions` ( `transaction_id`, `user_ref`, `username`, `fk_league_id`, `fk_member_team_id`, `amount`, `message`, `event_date`, `event_type_id`, `is_send`) VALUES
                                ( :trans_id, :user_ref, :username, :fk_league_id, :fk_member_team_id, :amount, :message, :event_date, :event_type_id, 0);'
                                );
                            $em->bindValue('trans_id', $trans_id);
                            $em->bindValue('user_ref', $user_ref);
                            $em->bindValue('username', $username);
                            $em->bindValue('fk_league_id', $league_id);
                            $em->bindValue('fk_member_team_id', $team_id);
                            $em->bindValue('amount', $amount);
                            $em->bindValue('message', $message);
                            $em->bindValue('event_date', $event_date);
                            $em->bindValue('event_type_id', $event_type);
                            $em->execute();

                            $league_name = $this->leaguesRepository->getLeaguesById(
                                $league_id
                            )['name'];//$league_id /4012

                            $team_name = $this->squadRepository->getSquadDetailsById($team_id)['name'];//$team_id /200
                            $tid = $connection
                                ->prepare(
                                    'SELECT `tid`.`tid` FROM `tid` WHERE `type` LIKE \'join\' AND `teamid` = :teamid;'
                                );
                            $tid->bindValue('teamid', $team_id);
                            $tid->execute();
                            $tidwin = uniqid();
                            $tidbet = $tid->fetch()['tid'];
                            $params = [
                                'type' => 'winning',
                                'league_name' => $league_name,
                                'league_id' => $league_id,
                                'team_name' => $team_name,
                                'team_id' => $team_id,
                                'winning' => $amount,
                                'user_ref' => $user_ref,
                            ];
                            if ($this->getUidByRef($user_ref)) {
                                $paramsclient = [
                                    'bet_transaction' => $tidbet,
                                    'transaction_id' => $tidwin,
                                    'uid' => $this->getUidByRef($user_ref),
                                    'amount' => $amount,
                                    'extra_params1' => null,
                                    'extra_params2' => null,
                                ];
                            } else {
                                $paramsclient = [
                                    'bet_transaction' => $tidbet,
                                    'transaction_id' => $tidwin,
                                    'token' => $user_ref,
                                    'amount' => $amount,
                                    'extra_params1' => null,
                                    'extra_params2' => null,
                                ];
                            }

                            if ($user_ref) {
                                $client_website = $this->configRepository->getConfigByKeyName(
                                    'client_money_in',
                                    $client_id
                                );

                                echo "email note send from payouts";

                                $result = $this->apiService->APIRequest(
                                    'POST',
                                    $client_website,
                                    false,
                                    json_encode($paramsclient)
                                );

                                $em = $connection
                                    ->prepare(
                                        'INSERT INTO `tid` 
                                    (`id`, `tid`, `tid_oulala`, `error_client`, `error_oulala`, `type`, `created_at`, `updated_at`, 
                                    `teamid`, `amount`,`username`, `operator_id`) 
                                    VALUES (
                                    NULL, 
                                    :tid, 
                                    :tidoulala, 
                                    :errorclient, 
                                    :erroroulala, 
                                    :type,
                                    CONVERT_TZ(NOW(),\'SYSTEM\',\'CET\'), 
                                    CONVERT_TZ(NOW(),\'SYSTEM\',\'CET\'),
                                    :teamid,
                                    :amount,
                                    :username,
                                    :operator_id)'
                                    );
                                $em->bindValue('tid', $tidwin);
                                $em->bindValue('tidoulala', $trans_id);
                                if ($result && $result->ok != true) {
                                    $errorClient = 'Code: '.$result->errorCode.' - '.$result->errorDescription;
                                } else {
                                    $errorClient = null;
                                }
                                $em->bindValue('errorclient', $errorClient);
                                if (!$result) {
                                    $errorOulala = "API not reachable";
                                } else {
                                    $errorOulala = null;
                                }

                                $em->bindValue('erroroulala', $errorOulala);
                                $em->bindValue('type', "win");
                                $em->bindValue('teamid', $team_id);
                                $em->bindValue('amount', $amount);
                                $em->bindValue('username', $username);
                                $em->bindValue('operator_id', $client_id);

                                $em->execute();
                            }
                        }
                    }
                }
            }

            $refunds = $this->apiService->APIRequest('GET', $this->refundUrl.'/'.$now, $access_token);

            if (isset($refunds->status) && $refunds->status == 'success') {
                if (isset($refunds->data) && is_array($refunds->data[0])) {
                    $clients_refunds = $refunds->data[0];

                    foreach ($clients_refunds as $clients_refund) {
                        $trans_id = $clients_refund->id;
                        $user_ref = $clients_refund->userId;
                        $username = $clients_refund->userName;
                        $amount = $clients_refund->cashIn;
                        $league_id = $clients_refund->leagueId;
                        $team_id = $clients_refund->member_team_id;
                        $message = $clients_refund->agentMessage;
                        $send_date = $clients_refund->createdAt;
                        $event_type = $clients_refund->agentMessageTrans;
                        $event_date = date('Y-m-d H:i:s', strtotime('-1 hour', strtotime($send_date)));
                        $trans = $connection
                            ->prepare(
                                'SELECT `id` FROM `member_transactions` WHERE
                          `transaction_id`=:trans_id'
                            );
                        $trans->bindValue('trans_id', $trans_id);
                        $trans->execute();

                        if (!$trans->fetchColumn()) {
                            $em = $connection
                                ->prepare(
                                    'INSERT INTO `member_transactions` ( `transaction_id`, `user_ref`, `username`, `fk_league_id`, `fk_member_team_id`, `amount`, `message`, `event_date`, `event_type_id`, `is_send`) VALUES
                                ( :trans_id, :user_ref, :username, :fk_league_id, :fk_member_team_id, :amount, :message, :event_date, :event_type_id, 0);'
                                );
                            $em->bindValue('trans_id', $trans_id);
                            $em->bindValue('user_ref', $user_ref);
                            $em->bindValue('username', $username);
                            $em->bindValue('fk_league_id', $league_id);
                            $em->bindValue('fk_member_team_id', $team_id);
                            $em->bindValue('amount', $amount);
                            $em->bindValue('message', $message);
                            $em->bindValue('event_date', $event_date);
                            $em->bindValue('event_type_id', $event_type);
                            $em->execute();
                            $league_name = $this->leaguesRepository->getLeaguesById($league_id)[0]['name'];
                            $team_name = $this->squadRepository->getSquadDetailsById($team_id)['name'];
                            $tid = $connection
                                ->prepare(
                                    'SELECT `tid`.`tid` FROM `tid` WHERE `type` LIKE \'join\' AND `teamid` = :teamid;'
                                );
                            $tid->bindValue('teamid', $team_id);
                            $tid->execute();
                            $tidrefund = uniqid();
                            $tidbet = $tid->fetch()['tid'];

                            $params = [
                                'type' => 'refund',
                                'league_name' => $league_name,
                                'league_id' => $league_id,
                                'team_name' => $team_name,
                                'team_id' => $team_id,
                                'winning' => $amount,
                                'user_ref' => $user_ref,
                            ];
                            if ($this->getUidByRef($user_ref)) {
                                $paramsclient = [
                                    'bet_transaction' => $tidbet,
                                    'uid' => $this->getUidByRef($user_ref),
                                    'amount' => $amount,
                                    'extra_params1' => null,
                                    'extra_params2' => null,
                                ];
                            } else {
                                $paramsclient = [
                                    'bet_transaction' => $tidbet,
                                    'transaction_id' => $tidrefund,
                                    'token' => $user_ref,
                                    'amount' => $amount,
                                    'extra_params1' => null,
                                    'extra_params2' => null,
                                ];
                            }

                            if ($user_ref) {
                                $client_website = $this->configRepository->getConfigByKeyName(
                                    'client_money_in',
                                    $client_id
                                );
                                echo "email nots send fro refunds";
                                $result = $this->apiService->APIRequest(
                                    'POST',
                                    $client_website,
                                    false,
                                    json_encode($paramsclient)
                                );
                                $em = $connection
                                    ->prepare(
                                        'INSERT INTO `tid` 
                                    (`id`, `tid`, `tid_oulala`, `error_client`, `error_oulala`, `type`, `created_at`, `updated_at`,
                                     `teamid`, `amount`,`username`, `operator_id`) 
                                    VALUES (
                                    NULL, 
                                    :tid, 
                                    :tidoulala, 
                                    :errorclient, 
                                    :erroroulala, 
                                    :type,
                                    CONVERT_TZ(NOW(),\'SYSTEM\',\'CET\'), 
                                    CONVERT_TZ(NOW(),\'SYSTEM\',\'CET\'),
                                    :teamid,
                                    :amount,
                                    :username,
                                    :operator_id
                                    );'
                                    );
                                $em->bindValue('tid', $tidrefund);
                                $em->bindValue('tidoulala', $trans_id);
                                $em->bindValue('amount', $amount);
                                $em->bindValue('username', $username);
                                $em->bindValue('operator_id', $client_id);
                                if (!$result) {
                                    $errorOulala = "API not reachable";
                                } else {
                                    $errorOulala = null;
                                }

                                if ($result && $result->ok != true) {
                                    $errorClient = 'Code: '.$result->errorCode.' - '.$result->errorDescription;
                                } else {
                                    $errorClient = null;
                                }
                                $em->bindValue('errorclient', $errorClient);
                                $em->bindValue('erroroulala', $errorOulala);
                                $em->bindValue('type', "refund");
                                $em->bindValue('teamid', $team_id);
                                $em->execute();
                            }
                        }
                    }
                }
            }
        }
    }

    public function getUidByRef($userRef)
    {
        $em = $this->em->getConnection()->prepare(
            "SELECT `uid` FROM `client_users` where `user_ref`=:ref "
        );
        $em->bindValue('ref', $userRef);
        $em->execute();
        $uid = $em->fetchColumn();

        return $uid;
    }
}
