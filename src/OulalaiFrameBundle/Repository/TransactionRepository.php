<?php
/**
 * Created by PhpStorm.
 * User: conta
 * Date: 03/10/2017
 * Time: 10:13 AM
 */

namespace OulalaiFrameBundle\Repository;

use Doctrine\ORM\EntityManager;

class TransactionRepository
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * DashboardRepository constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getTransaction($operatorId)
    {
        if(!$operatorId){
            $where = "";
        } else {
            $where =  "WHERE `tid`.`operator_id` = :operator";
        }

        $em = $this->em->getConnection()
        ->prepare(
                "SELECT tid.*, s.`league`, l.`name`,l.`leagues_id`,l.`timeToStart`
                         FROM `tid`
                         LEFT JOIN squads AS s ON s.`member_team_id` = tid.`teamid`
                         LEFT JOIN leagues AS l ON l.`leagues_id` = s.`league`". $where ."
                         ORDER BY `tid`.`created_at` DESC"
            );
        $em->bindValue("operator", $operatorId);
        $em->execute();
        return $em->fetchAll();
    }
}
