<?php

namespace OulalaiFrameBundle\Repository;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManager;
use OulalaiFrameBundle\ApiManager\ApiUrls;
use OulalaiFrameBundle\ApiManager\Manager;
use OulalaiFrameBundle\ApiService\Api;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Created by Oulala.
 * User: JUL
 * Date: 11/05/2017
 * Time: 14:09
 */
class SquadRepository
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var Api
     */
    private $apiService;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var
     */
    private $squadsUrl;

    /**
     * SquadRepository constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em, Api $apiService, ContainerInterface $container)
    {
        $this->em = $em;
        $this->apiService = $apiService;
        $this->container=$container;
        $this->squadsUrl = $container->getParameter('api_base_url') . 'squads';
    }

    public function squadsList($sUserRef, $sOrderBy = 'fromDate', $bPast = false, $bStarted = false, $bWon = false)
    {
        $sJoin = "";
        $sSelect = "";
        if ($bPast) {
            $sWhere = "AND ml.is_finalized = 1";
        } elseif ($bWon) {
            $sJoin = "LEFT JOIN member_league_prizes mlp ON mlp.fk_member_league_team_id = mt.member_team_id";
            $sWhere = "AND ml.is_finalized = 1 AND mt.is_winner = 1 AND mt.valid_team = 1 AND ml.is_open = 1 ";
            $sSelect = " ,IF(mlp.amount > 0, mlp.amount,0) as mywinnings";
        } else {
            $sWhere = "AND ml.is_finalized = 0";
            if ($bStarted) {
                $sWhere .= " AND (CONVERT_TZ(NOW(),'SYSTEM','CET') BETWEEN ml.fromDate and ml.toDate)
								AND mt.valid_team = 1 AND ml.is_open = 1 ";
            } else {
                $sWhere .= " AND CONVERT_TZ(NOW(),'SYSTEM','CET') < ml.fromDate AND ml.is_open = 1 ";
            }
        }

        $sSQL = "SELECT ml.leagues_id as leagues_id,
						mt.member_team_id as team_id,
						mt.`name` as team_name,
						ml.`name` as `name`,
						ml.entry,
						ml.prize,
						ml.slug as slug,
						ml.maxParticipants as maxParticipants,
						ml.entryFee as entryFee,
						CONVERT_TZ(ml.fromDate,'SYSTEM','CET') as fromDate,
						CONVERT_TZ(ml.toDate,'SYSTEM','CET') as toDate,
						mt.ranking as rank,
						mt.valid_team as valid_team,
						TIME_TO_SEC(TIMEDIFF(ml.fromDate, CONVERT_TZ(NOW(), 'SYSTEM', 'CET'))) as timetostart
						{$sSelect}
				FROM leagues ml
				INNER JOIN squads mt ON mt.league = ml.leagues_id AND mt.user = :iMemberID
				{$sJoin}
				WHERE mt.banned_team = 0
				AND ml.is_public = 1
				AND mt.valid_team = 1
				AND mt.is_blocked = 0
				{$sWhere}
				GROUP BY mt.member_team_id
				ORDER BY {$sOrderBy}";

        $em = $this->em->getConnection()->prepare($sSQL);
        $em->bindValue('iMemberID', $sUserRef);
        $em->execute();
        $squads = $em->fetchAll();

        return $squads;
    }

    /*
    * update squad formation id
    *
    * @param $fID pormation id
    * @param iSquadId
    */
    public function updateSquadFromation($fID, $iSquadId)
    {
        $em = $this->em->getConnection()
                ->prepare(
                    'UPDATE squads
				   SET formation = :fid
					WHERE `member_team_id` = :team_id'
                );
        $em->bindValue('fid', $fID);
        $em->bindValue('team_id', $iSquadId);
        $em->execute();
    }


    public function isTeamPast($teamId, $ref)
    {
        $sSQL = "SELECT mt.`member_team_id`

				FROM leagues ml
				INNER JOIN squads mt ON mt.league = ml.leagues_id AND mt.user = :user_ref

				WHERE mt.banned_team = 0
				AND ml.is_public = 1
				AND mt.valid_team = 1
				AND mt.is_blocked = 0
				AND ml.is_finalized = 1
				AND mt.`member_team_id` = :team_id
				GROUP BY mt.member_team_id";
        $em = $this->em->getConnection()->prepare($sSQL);
        $em->bindValue('user_ref', $ref);
        $em->bindValue('team_id', $teamId);
        $em->execute();
        $result = $em->fetch();
        if (! empty($result)) {
            return true;
        }
        return false;
    }

    /*
    * retrun squads detail by id
    *
    * @param $iSquadId
    */
    public function getSquadDetailsById($iSquadId)
    {
        $em = $this->em->getConnection()
            ->prepare(
                'SELECT s.*,f.name AS formation_name,m.`date` AS match_date FROM `squads` s
                INNER JOIN `formation` f ON f.id = s.formation
                INNER JOIN league_matches lm ON lm.`league_id` = s.`league`
                INNER JOIN matches m ON m.`match_id` = lm.`match_id`
                   WHERE s.member_team_id = :member_team_id GROUP BY s.`league`
                 '
            );

        $em->bindValue('member_team_id', $iSquadId);
        $em->execute();
        return $em->fetch();
    }

    public function getPlayersWithTimes($iMemberLeagueID, $schedulePlayers)
    {
        $aSchedulePlayers = array_keys($schedulePlayers);
        $aSchedulePlayers = implode(',', $aSchedulePlayers);

        $sSQL = "SELECT pd.id as player_id, ma.date as match_date
		          FROM players as pd,league_matches as mlm
                          INNER JOIN matches ma ON mlm.match_id = ma.match_id
			  WHERE  pd.id IN ({$aSchedulePlayers})
		          AND (((CONVERT_TZ( NOW(), 'SYSTEM', 'CET')) BETWEEN pd.from_date AND pd.to_date
		          AND (CONVERT_TZ( NOW(), 'SYSTEM', 'CET')) < pd.to_date) OR ((CONVERT_TZ( NOW(), 'SYSTEM', 'CET')) >= pd.from_date AND pd.to_date IS NULL))
		          AND ((pd.team_id = ma.`home`) OR (pd.team_id = ma.`away`))
		          ORDER BY ma.date ASC";

        $em = $this->em->getConnection()->prepare($sSQL);
//        $em->bindValue('iMemberLeagueID', $iMemberLeagueID);
        $em->execute();
        return $em->fetchAll();
    }

    /*
    * retrun player position title by position id
    *
    * @param $pId position id
    */
    public function getPositionById($pId)
    {
        $em = $this->em->getConnection()
            ->prepare(
                'SELECT `comment`
                 FROM `player_positions_cmn` WHERE player_position_id = :player_position_id'
            );

        $em->bindValue('player_position_id', $pId);
        $em->execute();
        return $em->fetch();
    }

    /**
     * return squade players for current team
     *
     * @param int iSquadeId
     */
    public function getSquadePlayers($iSquadeId, $is_draft = false)
    {
        $draft = 0;
        if ($is_draft) {
            $draft = 1;
        }
        $em = $this->em->getConnection()
            ->prepare(
                'SELECT sp.*,p.image,p.`firstName`,p.`lastName`,t.`name` AS team_name FROM `squads_players` sp
					INNER JOIN players p ON p.`id` = sp.`player_id`
					INNER JOIN teams t ON t.`id` = p.`team_id`
				   	INNER JOIN squads s ON s.`member_team_id` = sp.`squad_id`
					   WHERE sp.`squad_id` = :squad_id AND sp.is_draft = :draft;'
            );
        $em->bindValue('squad_id', $iSquadeId);
        $em->bindValue('draft', $draft);
        $em->execute();
        return $em->fetchAll();
    }

    /**
     * return temp players for current team
     *
     * @param int iTeamId
     */
    public function getTeamPlayers($iTeamId)
    {
        $em = $this->em->getConnection()
            ->prepare(
                'SELECT * FROM `temp_squad_players`
                WHERE `teamid` = :team_id;'
            );
        $em->bindValue('team_id', $iTeamId);
        $em->execute();
        return $em->fetchAll();
    }

    /**
     * remove players from temp table when reloading page or after joining to league
     *
     * @param int iTeamId
     */
    public function clearTeamQuery($iTeamId, $bIsSquadeEdit = false)
    {
        $sTable = " `temp_squad_players` ";
        $sParam = " teamid ";
        if ($bIsSquadeEdit) {
            $sTable = " `squads_players` ";
            $sParam = " squad_id ";
        }
        $em = $this->em->getConnection()
                ->prepare(
                    'DELETE FROM '.$sTable.'
					WHERE '.$sParam.' = :team_id;'
                );
        $em->bindValue('team_id', $iTeamId);
        $em->execute();
    }

    /**
     * remove add player
     *
     * @param int iPlayerId
     * @param int iTeamId
     * @param bollean bIsSquadeEdit to check if editing existing squade or creating new one
     */
    public function removePlayerQuery($iPlayerId, $iTeamId, $bIsSquadeEdit = false)
    {
        $sTable = " `temp_squad_players` ";
        $sParam = " teamid ";
        if ($bIsSquadeEdit) {
            $sTable = " `squads_players` ";
            $sParam = " squad_id ";
        }
        $em = $this->em->getConnection()
                ->prepare(
                    'DELETE FROM '.$sTable.'
					WHERE '.$sParam.' = :team_id AND player_id = :player_id;'
                );
        $em->bindValue('team_id', $iTeamId);
        $em->bindValue('player_id', $iPlayerId);
        $em->execute();
    }

    /**
     * change player from lineup to bench and from bench to lineup
     *
     * @param int iPlayerId
     * @param int iTeamId
     * @param int starting get 0 or 1 to check if player is for lineup or bench
     */
    public function swapPlayerQuery($iPlayerId, $iTeamId, $starting, $bIsSquadeEdit = false, $swapLastPlayer = false)
    {
        $sTable = " `temp_squad_players` ";
        $lastPlayer = null;
        $sParam = " teamid ";
        if ($bIsSquadeEdit) {
            $sTable = " `squads_players` ";
            $sParam = " squad_id ";
        }

        if ($swapLastPlayer) {
            $em = $this->em->getConnection()->prepare("SELECT * FROM {$sTable} WHERE `player_position_id` = :player_position_id LIMIT 1 ");
            $em->bindValue('player_position_id', $swapLastPlayer);
            $em->execute();
            $lastPlayer = $em->fetch();
            if ($lastPlayer) {
                $sSQL = 'UPDATE '.$sTable.'
				   SET starting_11 = :switch_starting_11
					WHERE '.$sParam.' = :team_id AND player_id = :switch_player_id;
					UPDATE '.$sTable.'
				   SET starting_11 = :starting_11
					WHERE '.$sParam.' = :team_id AND player_id = :player_id;';
                $em = $this->em->getConnection()->prepare($sSQL);
                $em->bindValue('team_id', $iTeamId);
                $em->bindValue('switch_player_id', $lastPlayer['player_id']);
                $em->bindValue('player_id', $iPlayerId);
                $em->bindValue('starting_11', (($starting)? 1 : 0));
                $em->bindValue('switch_starting_11', (($starting)? 0 : 1));
                $em->execute();
            }
        } else {
            $sSQL = 'UPDATE '.$sTable.'
					   SET starting_11 = :starting_11
						WHERE '.$sParam.' = :team_id AND player_id = :player_id;';
            $em = $this->em->getConnection()->prepare($sSQL);
            $em->bindValue('team_id', $iTeamId);
            $em->bindValue('player_id', $iPlayerId);
            $em->bindValue('starting_11', $starting);
            $em->execute();
        }
    }

    public function insertTempPlayers($iTeamId, $iPlayerId, $starting, $iPositionId)
    {
        $em = $this->em->getConnection()
                ->prepare(
                    'INSERT INTO `temp_squad_players`
				   (
					`teamid`,
					`player_id`,
					`starting_11`,
					`player_position_id`
				   )
					VALUES
					(
					:team_id,
					:player_id,
					:starting_11,
					:player_position_id
					);'
                );
        $em->bindValue('team_id', $iTeamId);
        $em->bindValue('player_id', $iPlayerId);
        $em->bindValue('starting_11', $starting);
        $em->bindValue('player_position_id', $iPositionId);
        $em->execute();
    }

    public function insertSquadeplayers($iSquadId, $iPlayerId, $starting, $sPositionTitle, $positionId)
    {
        $em = $this->em->getConnection()
                ->prepare(
                    'INSERT INTO `squads_players`
				   (
					`squad_id`,
					`player_id`,
					`starting_11`,
					`position_title`,
					`player_position_id`
				   )
					VALUES
					(
					:squad_id,
					:player_id,
					:starting_11,
					:position_title,
					:positionId
					);'
                );
        $em->bindValue('squad_id', $iSquadId);
        $em->bindValue('player_id', $iPlayerId);
        $em->bindValue('starting_11', $starting);
        $em->bindValue('position_title', $sPositionTitle);
        $em->bindValue('positionId', $positionId);
        $em->execute();
    }

    public function changePlayersDraftStatus($iTeamID, $status, $beforeDelete = 0)
    {
        if (!empty($iTeamID)) {
            $sSQL = "SELECT sp.*
			 FROM squads_players sp
			 WHERE squad_id = :team_id AND sp.`is_draft` = 1";
            $em = $this->em->getConnection()->prepare($sSQL);
            $em->bindvalue('team_id', $iTeamID);
            $em->execute();
            $hasDraftPlayers = $em->fetchAll();

            if (! empty($hasDraftPlayers)) {
                $em = $this->em->getConnection()->prepare("DELETE FROM squads_players WHERE squad_id = :teamID AND is_draft = 1");
                $em->bindvalue('teamID', $iTeamID);
                $em->execute();
            }
            $em = $this->em->getConnection()->prepare("UPDATE squads_players SET is_draft = '{$status}' WHERE squad_id = :teamID");
            $em->bindvalue('teamID', $iTeamID);
            $updated = $em->execute();
            if ($beforeDelete) {
                $em = $this->em->getConnection()->prepare("DELETE FROM squads_players WHERE squad_id = :teamID AND (is_draft = 0 OR is_draft IS NULL)");
                $em->bindvalue('teamID', $iTeamID);
                $em->execute();
            }
            return true;
        }
    }

    public function updateSquadFormation($access_token)
    {
        $formation = $this->apiService->APIRequest('GET', $this->squadsUrl . '/formation', $access_token);
        $connection = $this->em->getConnection();

        if (!empty($formation->data)) {
            foreach ($formation->data as $format) {
                $em = $connection
                    ->prepare(
                        'INSERT IGNORE INTO `formation`
						   (
							`id`,
							`name`
						   )
							VALUES
							(
							:id,
							:name
							);'
                    );
                $em->bindValue('id', $format->formation_id);
                $em->bindValue('name', $format->comment);
                $em->execute();
            }
        }
    }

    /**
    * Updates Squads data from API
    */
    public function updateSquadsData($access_token, $current=false)
    {
        if ($current) {
            $squads = $this->apiService->APIRequest('GET', $this->squadsUrl.'/current', $access_token);
        } else {
            $squads = $this->apiService->APIRequest('GET', $this->squadsUrl, $access_token);
        }

        $connection = $this->em->getConnection();

        if (!empty($squads->data) && !isset($squads->data->error)) {
//            if (!$current) {
//                $connection->exec("TRUNCATE TABLE `squads`");
//                $connection->exec("TRUNCATE TABLE `squads_players`");
//            }
            foreach ($squads->data as $squad) {
                if ($squad) {
                    $c_At = date_create($squad->createAt);
                    $u_At = date_create($squad->updateAt);
                    $createAt =date_format($c_At, "Y-m-d H:i:s")!="1970-01-01 01:00:00"?date_format($c_At, "Y-m-d H:i:s"):date("Y-m-d H:i:s");
                    $updateAt = date_format($u_At, "Y-m-d H:i:s")!="1970-01-01 01:00:00"?date_format($u_At, "Y-m-d H:i:s"):date("Y-m-d H:i:s");
//                    if ($current) {
                        $connection->exec("DELETE FROM `squads` WHERE `member_team_id`={$squad->teamId}");
//                    }
                    $em = $connection
                    ->prepare(
                       'INSERT  INTO `squads`
                       (
                        `member_team_id`,
                        `name`,
                        `formation`,
                        `user`,
                        `league`,
                        `score`,
                        `createAt`,
                        `updateAt`,
                        `is_winner`,
                        `valid_team`,
                        `is_blocked`,
                        `ranking`,
                        `banned_team`
                       )
                        VALUES
                        (
                        :member_team_id,
                        :name,
                        :formation,
                        :user,
                        :league,
                        :score,
                        :createAt,
                        :updateAt,
                        :is_winner,
                        :valid_team,
                        :is_blocked,
                        :ranking,
                        :banned_team
                        );'
                    );
                    $em->bindValue('member_team_id', $squad->teamId);
                    $em->bindValue('name', $squad->name);
                    $em->bindValue('formation', $squad->formation);
                    $em->bindValue('user', $squad->user);
                    $em->bindValue('league', $squad->league);
                    $em->bindValue('score', $squad->score);
                    $em->bindValue('createAt', $createAt);
                    $em->bindValue('updateAt', $updateAt);
                    $em->bindValue('is_winner', $squad->is_winner);
                    $em->bindValue('valid_team', $squad->valid_team);
                    $em->bindValue('is_blocked', $squad->is_blocked);
                    $em->bindValue('ranking', $squad->ranking);
                    $em->bindValue('banned_team', $squad->banned_team);
                    $em->execute();
                    $squads_by_id = $this->apiService->APIRequest('GET', $this->squadsUrl.'/'.$squad->teamId, $access_token);
                    if (isset($squads_by_id->data) && !empty($squads_by_id->data->players)) {
//                        if ($current) {
                            $connection->exec("DELETE FROM `squads_players` WHERE `squad_id`={$squad->teamId}");
//                        }
                        if (!empty($squads_by_id->data->players->lineup)) {
                            foreach ($squads_by_id->data->players->lineup as $l_key => $l_value) {
                                foreach ($l_value as $l_p_id => $l_score) {
                                    if (! empty($l_score)) {
                                        $em = $connection
                                            ->prepare(
                                                'INSERT INTO `squads_players`
                                                     (
                                                      `squad_id`,
                                                      `player_id`,
                                                      `position_title`,
                                                      `player_position_id`,
                                                      `score`,
                                                      `starting_11`,
                                                      `subbed_in_for_id`,
                                                      `from_minute`,
                                                      `to_minute`
                                                     )
                                                      VALUES
                                                      (
                                                      :squad_id,
                                                      :player_id,
                                                      :position_title,
                                                      :player_position_id,
                                                      :score,
                                                      :is_active,
                                                      :subbed_in_for_id,
                                                      :from_minute,
                                                      :to_minute
                                                      );');
                                        $em->bindValue('squad_id', $squad->teamId);
                                        $em->bindValue('player_id', $l_p_id);
                                        $em->bindValue('position_title', $l_key);
                                        $em->bindValue('player_position_id', $l_score->position);
                                        $em->bindValue('score', $l_score->score);
                                        $em->bindValue('is_active', 1);
                                        $em->bindValue('subbed_in_for_id', $l_score->subbed_in_for_id);
                                        $em->bindValue('from_minute', $l_score->from_minute);
                                        $em->bindValue('to_minute', $l_score->to_minute);
                                        $em->execute();
                                    }
                                }
                            }
                        }
                        if (!empty($squads_by_id->data->players->bench)) {
                            foreach ($squads_by_id->data->players->bench as $b_key => $b_value) {
                                foreach ($b_value as $b_p_id => $b_score) {
                                    if (! empty($b_score)) {
                                        $em = $connection
                                            ->prepare(
                                                'INSERT INTO `squads_players`
                                         (
                                          `squad_id`,
                                          `player_id`,
                                          `position_title`,
                                          `player_position_id`,
                                          `score`,
                                          `starting_11`,
                                          `subbed_in_for_id`,
                                          `from_minute`,
                                          `to_minute`
                                         )
                                          VALUES
                                          (
                                          :squad_id,
                                          :player_id,
                                          :position_title,
                                          :player_position_id,
                                          :score,
                                          :is_active,
                                          :subbed_in_for_id,
                                          :from_minute,
                                          :to_minute
                                          );'
                                            );
                                        $em->bindValue('squad_id', $squad->teamId);
                                        $em->bindValue('player_id', $b_p_id);
                                        $em->bindValue('position_title', $b_key);
                                        $em->bindValue('score', $b_score->score);
                                        $em->bindValue('player_position_id', $b_score->position);
                                        $em->bindValue('is_active', 0);
                                        $em->bindValue('subbed_in_for_id', $b_score->subbed_in_for_id);
                                        $em->bindValue('from_minute', $b_score->from_minute);
                                        $em->bindValue('to_minute', $b_score->to_minute);
                                        $em->execute();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public function updateOneSquadData($access_token, $squad_id)
    {
        $squads = $this->apiService->APIRequest('GET', $this->squadsUrl.'/'.$squad_id, $access_token);
        $connection = $this->em->getConnection();

        if (!empty($squads->data) && !isset($squads->data->error)) {
            $squad=$squads->data ;

            if ($squad) {
                $connection->exec("DELETE FROM `squads` WHERE `member_team_id`={$squad_id}");

                $c_At = date_create($squad->createAt);
                $u_At = date_create($squad->updateAt);
                $createAt =date_format($c_At, "Y-m-d H:i:s")!="1970-01-01 01:00:00"?date_format($c_At, "Y-m-d H:i:s"):date("Y-m-d H:i:s");
                $updateAt = date_format($u_At, "Y-m-d H:i:s")!="1970-01-01 01:00:00"?date_format($u_At, "Y-m-d H:i:s"):date("Y-m-d H:i:s");

                $em = $connection
                    ->prepare(
                        'INSERT  INTO `squads`
                       (
                        `member_team_id`,
                        `name`,
                        `formation`,
                        `user`,
                        `league`,
                        `score`,
                        `createAt`,
                        `updateAt`,
                        `is_winner`,
                        `valid_team`,
                        `is_blocked`,
                        `ranking`,
                        `banned_team`
                       )
                        VALUES
                        (
                        :member_team_id,
                        :name,
                        :formation,
                        :user,
                        :league,
                        :score,
                        :createAt,
                        :updateAt,
                        :is_winner,
                        :valid_team,
                        :is_blocked,
                        :ranking,
                        :banned_team
                        );'
                    );
                $em->bindValue('member_team_id', $squad->teamId);
                $em->bindValue('name', $squad->name);
                $em->bindValue('formation', $squad->formation);
                $em->bindValue('user', $squad->user);
                $em->bindValue('league', $squad->league);
                $em->bindValue('score', $squad->score);
                $em->bindValue('createAt', $createAt);
                $em->bindValue('updateAt', $updateAt);
                $em->bindValue('is_winner', $squad->is_winner);
                $em->bindValue('valid_team', $squad->valid_team);
                $em->bindValue('is_blocked', $squad->is_blocked);
                $em->bindValue('ranking', $squad->ranking);
                $em->bindValue('banned_team', $squad->banned_team);
                $em->execute();
                if (!empty($squad->players)) {
                    $connection->exec("DELETE FROM `squads_players` WHERE `squad_id`={$squad_id}");
                    if (!empty($squad->players->lineup)) {
                        foreach ($squad->players->lineup as $l_key => $l_value) {
                            foreach ($l_value as $l_p_id => $l_score) {
                                if (! empty($l_score)) {
                                    $em = $connection
                                        ->prepare(
                                            'INSERT INTO `squads_players`
                                 (
                                  `squad_id`,
                                  `player_id`,
                                  `position_title`,
                                  `player_position_id`,
                                  `score`,
                                  `starting_11`,
                                  `subbed_in_for_id`,
                                  `from_minute`,
                                  `to_minute`
                                 )
                                  VALUES
                                  (
                                  :squad_id,
                                  :player_id,
                                  :position_title,
                                  :player_position_id,
                                  :score,
                                  :is_active,
                                  :subbed_in_for_id,
                                  :from_minute,
                                  :to_minute
                                  );');
                                    $em->bindValue('squad_id', $squad->teamId);
                                    $em->bindValue('player_id', $l_p_id);
                                    $em->bindValue('position_title', $l_key);
                                    $em->bindValue('player_position_id', $l_score->position);
                                    $em->bindValue('score', $l_score->score);
                                    $em->bindValue('is_active', 1);
                                    $em->bindValue('subbed_in_for_id', $l_score->subbed_in_for_id);
                                    $em->bindValue('from_minute', $l_score->from_minute);
                                    $em->bindValue('to_minute', $l_score->to_minute);
                                    $em->execute();
                                }
                            }
                        }
                    }
                    if (!empty($squad->players->bench)) {
                        foreach ($squad->players->bench as $b_key => $b_value) {
                            foreach ($b_value as $b_p_id => $b_score) {
                                if (! empty($b_score)) {
                                    $em = $connection
                                        ->prepare(
                                            'INSERT INTO `squads_players`
                                         (
                                          `squad_id`,
                                          `player_id`,
                                          `position_title`,
                                          `player_position_id`,
                                          `score`,
                                          `starting_11`,
                                          `subbed_in_for_id`,
                                          `from_minute`,
                                          `to_minute`
                                         )
                                          VALUES
                                          (
                                          :squad_id,
                                          :player_id,
                                          :position_title,
                                          :player_position_id,
                                          :score,
                                          :is_active,
                                          :subbed_in_for_id,
                                          :from_minute,
                                          :to_minute
                                          );'
                                        );
                                    $em->bindValue('squad_id', $squad->teamId);
                                    $em->bindValue('player_id', $b_p_id);
                                    $em->bindValue('position_title', $b_key);
                                    $em->bindValue('score', $b_score->score);
                                    $em->bindValue('player_position_id', $b_score->position);
                                    $em->bindValue('is_active', 0);
                                    $em->bindValue('subbed_in_for_id', $b_score->subbed_in_for_id);
                                    $em->bindValue('from_minute', $b_score->from_minute);
                                    $em->bindValue('to_minute', $b_score->to_minute);
                                    $em->execute();
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public function getTeamName($teamId)
    {
        $sSQL = "SELECT s.`name` FROM squads AS s
                 WHERE s.`member_team_id` = :team_id";

        $em = $this->em->getConnection()->prepare($sSQL);
        $em->bindvalue('team_id', $teamId);
        $em->execute();
        return $result = $em->fetchAll();
    }

    public function updateTeamName($teamId, $teamName)
    {
        $sSQL = "UPDATE squads AS s
                 SET s.`name` = :team_name
                 WHERE s.`member_team_id` = :team_id";

        $em = $this->em->getConnection()->prepare($sSQL);
        $em->bindvalue('team_id', $teamId);
        $em->bindvalue('team_name', $teamName);
        $em->execute();

        return true;
    }
}
