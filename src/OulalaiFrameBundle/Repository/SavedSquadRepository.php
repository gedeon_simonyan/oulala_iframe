<?php

namespace OulalaiFrameBundle\Repository;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManager;
use OulalaiFrameBundle\ApiManager\ApiUrls;
use OulalaiFrameBundle\ApiManager\Manager;
use OulalaiFrameBundle\ApiService\Api;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Created by Oulala.
 * User: JUL
 * Date: 11/05/2017
 * Time: 14:09
 */
class SavedSquadRepository
{
    /**
     * @var EntityManager
     */
    public $em;
    private $bIsValid= false;
    private $sRef;

    /**
     * @var LeaguesRepository
     */
    private $leaguesRepository;

    /**
     * @var SquadRepository
     */
    private $squadRepository;

    /**
     * @var TokenrRepository
     */
    private $tokenRepository;
    /**
     * @var PrizesRepository
     */
    private $prizeRepository;

    /**
     * @var ConfigRepository
     */
    private $configRepository;

    /**
     * @var MatchesRepository
     */
    private $matchRepository;

    /**
     * @var Api
     */
    private $apiService;

    /**
     * @var ContainerInterface
     */

    private $container;

    /**
     * @var
     */
    private $squadsUrl;

    /**
     * SavedSquadRepository constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em, TokenrRepository $tokenrRepository, LeaguesRepository $leaguesRepository,
                                ConfigRepository $configRepository, SquadRepository $squadRepository,
        PrizesRepository $prizesRepository, MatchesRepository $matchesRepository, Api $apiService, ContainerInterface $container)
    {
        $this->em = $em;
        $this->matchRepository=$matchesRepository;
        $this->squadRepository=$squadRepository;
        $this->prizeRepository=$prizesRepository;
        $this->configRepository=$configRepository;
        $this->tokenRepository=$tokenrRepository;
        $this->leaguesRepository=$leaguesRepository;
        $this->apiService = $apiService;
        $this->container=$container;
        $this->squadsUrl = $container->getParameter('api_base_url') . 'squads';
    }

    public function changePlayersDraftStatus($iTeamID, $status, $beforeDelete = 0)
    {
        if (!empty($iTeamID)) {
            $sSQL = "SELECT mt.*
			 FROM member_draftteam_players mt
			 WHERE member_draftteam_id = :team_id AND mt.`is_draft` = 1";
         
            $em = $this->em->getConnection()->prepare($sSQL);
            $em->bindvalue('team_id', $iTeamID);
            $em->execute();
            $hasDraftPlayers = $em->fetchAll();
        
            if (! empty($hasDraftPlayers)) {
                if ($beforeDelete) {
                    $em = $this->em->getConnection()->prepare("DELETE FROM member_draftteam_players WHERE member_draftteam_id = :teamID AND (is_draft = 0 OR is_draft IS NULL)");
                    $em->bindvalue('teamID', $iTeamID);
                    $em->execute();
                }
                if (!$status) {
                    $em = $this->em->getConnection()->prepare("UPDATE member_draftteam_players SET is_draft = '{$status}' WHERE member_draftteam_id = :teamID");
                    $em->bindvalue('teamID', $iTeamID);
                    $updated = $em->execute();
                    if (! $updated) {
                        return false;
                    }
                }
            } else {
                if ($status) {
                    $em = $this->em->getConnection()->prepare("UPDATE member_draftteam_players SET is_draft = '{$status}' WHERE member_draftteam_id = :teamID");
                    $em->bindvalue('teamID', $iTeamID);
                    $updated = $em->execute();
                    if (! $updated) {
                        return false;
                    }
                }
            }
        
            return true;
        }
    }

    public function getNotDraftPlayers($iTeamID)
    {
        $sSQL = "SELECT mt.*,p.value as `value`
			 FROM member_draftteam_players mt
				INNER JOIN players p ON p.id = mt.fk_player_id
			 WHERE member_draftteam_id = :teamId
				 AND ( mt.`is_draft` IS NULL OR mt.`is_draft` !=1) 
					GROUP BY mt.`fk_player_id`";
         
        $em = $this->em->getConnection()->prepare($sSQL);
        $em->bindValue("teamId", $iTeamID);
        $em->execute();
        return $em->fetchAll();
    }
    
    /**
     * change player from lineup to bench and from bench to lineup
     *
     */
    public function swapPlayerQuery($iPlayerId, $iTeamId, $starting, $bIsSquadeEdit = false, $swapLastPlayer = false)
    {
        $sTable = " `member_draftteam_players` ";
        $lastPlayer = null;
        $sParam = " `member_draftteam_id` ";
        
        if ($swapLastPlayer) {
            $em = $this->em->getConnection()->prepare("SELECT * FROM {$sTable} WHERE `fk_player_position_id` = :player_position_id LIMIT 1 ");
            $em->bindValue('player_position_id', $swapLastPlayer);
            $em->execute();
            $lastPlayer = $em->fetch();
            if ($lastPlayer) {
                $sSQL = 'UPDATE '.$sTable.'
				   SET starting_11 = :switch_starting_11  
					WHERE '.$sParam.' = :team_id AND fk_player_id = :switch_player_id;
					UPDATE '.$sTable.'
				   SET starting_11 = :starting_11  
					WHERE '.$sParam.' = :team_id AND fk_player_id = :player_id;';
                $em = $this->em->getConnection()->prepare($sSQL);
                $em->bindValue('team_id', $iTeamId);
                $em->bindValue('switch_player_id', $lastPlayer['fk_player_id']);
                $em->bindValue('player_id', $iPlayerId);
                $em->bindValue('starting_11', (($starting)? 1 : 0));
                $em->bindValue('switch_starting_11', (($starting)? 0 : 1));
                $em->execute();
            }
        } else {
            $sSQL = 'UPDATE '.$sTable.'
					   SET starting_11 = :starting_11  
						WHERE '.$sParam.' = :team_id AND fk_player_id = :player_id;';
            $em = $this->em->getConnection()->prepare($sSQL);
            $em->bindValue('team_id', $iTeamId);
            $em->bindValue('player_id', $iPlayerId);
            $em->bindValue('starting_11', $starting);
            $em->execute();
        }
    }
    
    /**
     * remove add player
     *
     */
    public function removePlayerQuery($iPlayerId, $iTeamId, $bIsSquadeEdit = false)
    {
        $em = $this->em->getConnection()
                ->prepare(
                    'DELETE FROM `member_draftteam_players` 
						WHERE `member_draftteam_id` = :team_id AND `fk_player_id` = :player_id;'
                );
        $em->bindValue('team_id', $iTeamId);
        $em->bindValue('player_id', $iPlayerId);
        $em->execute();
    }
    
    /**
     * remove players from temp table when reloading page or after joining to league
     *
     */
    public function clearTeamQuery($iTeamId, $bIsSquadeEdit = false)
    {
        //if ( $bIsSquadeEdit ){
        $em = $this->em->getConnection()
                ->prepare(
                    'DELETE FROM `member_draftteam_players`  
						WHERE member_draftteam_id = :team_id;
					DELETE FROM `member_draftteams`  
						WHERE member_draftteam_id = :team_id;'
                );
        $em->bindValue('team_id', $iTeamId);
        $em->execute();
        //}
    }
    
    /**
     * return squade players for current team
     *
     */
    public function getDraftTeamPlayers($iSquadeId, $iPlayerPosition = null)
    {
        $sWhere = "";
        if ($iPlayerPosition  == 'sub') {
            $sWhere = " AND mdtp.`starting_11` = 0 ";
        } elseif (! empty($iPlayerPosition)) {
            $sWhere = " AND mdtp.`fk_player_position_id` = {$iPlayerPosition} AND mdtp.`starting_11` = 1 ";
        }
        $em = $this->em->getConnection()
            ->prepare(
                "SELECT mdtp.*,mdtp.fk_player_id AS player_id,p.image,p.name AS player_name,p.value AS score FROM `member_draftteam_players` mdtp
					INNER JOIN `member_draftteams` mdt ON mdt.`member_draftteam_id` = mdtp.`member_draftteam_id`
					INNER JOIN players p ON p.`id` = mdtp.`fk_player_id`
					WHERE mdt.`member_draftteam_id` = :squad_id {$sWhere} AND ( mdtp.is_draft = 0 OR mdtp.is_draft IS NULL );"
            );
        $em->bindValue('squad_id', $iSquadeId);
        $em->execute();
        return $em->fetchAll();
    }
    
    /**
     * Get member saved teams
     *
     */
    public function searchMySavedTeams($sRef, $iTeamID = 0)
    {
        $sSQL = "SELECT mdt.*,rl.nation_thumb AS nation_thumb
				FROM member_draftteams mdt
				 INNER JOIN real_league rl ON rl.`id` IN (mdt.`team_leagues`)
					WHERE mdt.fk_member_id = '{$sRef}' 
				";
        if (! empty($iTeamID)) {
            $sSQL .=" AND mdt.member_draftteam_id != {$iTeamID}";
        }
        $sSQL .=" GROUP BY mdt.member_draftteam_id
					ORDER BY mdt.last_modified DESC";
        $em = $this->em->getConnection()->prepare($sSQL);

        $em->execute();
        return $em->fetchAll();
    }
    
    /**
     * Remove member saved team by id
     *
     */
    public function removeMySavedTeams($sRef, $iTeamID)
    {
        $sSQL = "";
        if (! empty($iTeamID)) {
            $sSQL ="DELETE FROM member_draftteams WHERE member_draftteam_id = :teamID  
						AND `fk_member_id` = '{$sRef}'; 
				DELETE FROM member_draftteam_players WHERE member_draftteam_id = :teamID";
        }
        $em = $this->em->getConnection()->prepare($sSQL);
        $em->bindValue('teamID', $iTeamID);
        $result = $em->execute();
        if ($result) {
            return true;
        }
        return false;
    }
    
    /**
     * Returns the team info.
     *
     */
    public function loadTeamInfo($sRef, $iMemberTeamID, $iTeamID = 0)
    {
        $sSQL = "SELECT
						  mt.member_draftteam_id as member_team_id,
						  mt.comment as `name`,
						  mt.fk_formation_id as formation_id,
						  mt.valid_team as valid_team,
						  mt.team_leagues,
						  mt.team_matches_days,
						  mt.team_matches
					 FROM member_draftteams mt
					 WHERE
						  mt.fk_member_id = '{$sRef}'";
        if (empty($iTeamID)) {
            $sSQL .= " AND mt.member_draftteam_id = {$iMemberTeamID}";
        } else {
            $sSQL .= " AND mt.member_draftteam_id = {$iTeamID}";
        }
        
        $em = $this->em->getConnection()->prepare($sSQL);
        $em->execute();
        return $em->fetch();
    }
    
    /*
    * update squad formation id
    *
    */
    public function updateDraftTeamFromation($fID, $iSquadId)
    {
        $em = $this->em->getConnection()
            ->prepare(
                'UPDATE member_draftteams
					 SET fk_formation_id = :fid  
						WHERE `member_draftteam_id` = :team_id'
            );
        $em->bindValue('fid', $fID);
        $em->bindValue('team_id', $iSquadId);
        $em->execute();
    }
    
    /*
    * retrun squads detail by id
    *
    * @param $iSquadId
    */
    public function getDraftTeamDetailsById($iSquadId)
    {
        $em = $this->em->getConnection()
            ->prepare(
                'SELECT * FROM `member_draftteams` WHERE member_draftteam_id = :member_team_id'
            );
       
        $em->bindValue('member_team_id', $iSquadId);
        $em->execute();
        return $em->fetch();
    }
    
    public function getTeamJoinedLeaguesCount($iMemberTeamID)
    {
        $sSQL = "SELECT count(`member_team_id`) AS join_count FROM squads WHERE `fk_saved_team_id` = :teamID";
        $em = $this->em->getConnection()->prepare($sSQL);
        $em->bindValue('teamID', $iMemberTeamID);
        $em->execute();
        return $em->fetchAll();
    }

    public function updateSquadUserRef($iMemberTeamID, $memberRef){
        $sSQL = 'UPDATE member_draftteams
				   SET fk_member_id = :member_ref 
					WHERE member_draftteam_id = :memberTeamID';
        $em = $this->em->getConnection()->prepare($sSQL);
        $em->bindValue('memberTeamID', $iMemberTeamID);
        $em->bindValue('member_ref', $memberRef);
        if($em->execute()){
            return true;
        }
        return false;
    }
    
    public function updateDraftTeamData($iMemberTeamID, $aSlots, $sTeamName, $sMatches, $sDays, $sSaveLeagues)
    {
        if (!empty($sMatches) && ! empty($sDays) && ! empty($sSaveLeagues)) {
            $sSQL = 'UPDATE member_draftteams
				   SET team_matches = :team_matches ,
				   team_matches_days = :team_matches_days ,
				   team_leagues = :team_leagues ,
				   `comment` = :team_name,
				   `fk_formation_id` = :formation_id,
				   last_modified = "'. date("Y-m-d H:i:s").'",
				   valid_team = 1
					WHERE member_draftteam_id = :memberTeamID';
            $em = $this->em->getConnection()->prepare($sSQL);
            $em->bindValue('memberTeamID', $iMemberTeamID);
            $em->bindValue('formation_id', $aSlots);
            $em->bindValue('team_matches', $sMatches);
            $em->bindValue('team_matches_days', $sDays);
            $em->bindValue('team_leagues', $sSaveLeagues);
            $em->bindValue('team_name', $sTeamName);
            if($em->execute()){
                return true;
            }
            return false;
        }
        return false;
    }
    
    public function setCurrentDraftTeam($iMemberTeamID, $sRef, $aSlots, $sTeamName, $bIsValid, $sMatches, $sDays, $sSaveLeagues)
    {
        $sSQL = 'SELECT * FROM member_draftteams 
					WHERE member_draftteam_id = :memberTeamID';
        $em = $this->em->getConnection()->prepare($sSQL);
        $em->bindValue('memberTeamID', $iMemberTeamID);
        $em->execute();
        $result = $em->fetch();
        if (empty($result)) {
            $em = $this->em->getConnection()->prepare(
                        'INSERT INTO `member_draftteams`
					   (     
						`member_draftteam_id`,
						`fk_member_id`,       
						`fk_formation_id`,
						`comment`,
						`valid_team`,
						`team_matches`,
						`team_matches_days`,
						`team_leagues`,
						`date_created`
					   )
						VALUES
						(
						:member_draftteam_id,
						:fk_member_id,
						:fk_formation_id,
						:team_name,
						:valid_team,
						:team_matches,
						:team_matches_days,
						:team_leagues,
						"'.date("Y-m-d H:i:s").'"
						);'
                    );
                
            $em->bindValue('member_draftteam_id', $iMemberTeamID);
            $em->bindValue('fk_member_id', $sRef);
            $em->bindValue('fk_formation_id', $aSlots);
            $em->bindValue('team_name', $sTeamName);
            $em->bindValue('valid_team', $bIsValid);
            $em->bindValue('team_matches', $sMatches);
            $em->bindValue('team_matches_days', $sDays);
            $em->bindValue('team_leagues', $sSaveLeagues);
            $em->execute();
        }
    }
    
    /*
    * retrun player position title by position id
    *
    * @param $pId position id
    */
    public function getPositionById($pId)
    {
        $em = $this->em->getConnection()
            ->prepare(
                'SELECT `comment` ,player_position_id
                 FROM `player_positions_cmn` WHERE player_position_id = :player_position_id'
            );
       
        $em->bindValue('player_position_id', $pId);
        $em->execute();
        return $em->fetch();
    }
    
    public function insertDraftTeamPlayers($iSquadId, $iPlayerId, $iPlayerTeamId, $starting, $sPositionId)
    {
        $em = $this->em->getConnection()
                ->prepare(
                    'INSERT INTO `member_draftteam_players`
				   (     
					`member_draftteam_id`,
					`fk_player_id`,       
					`starting_11`,
					`fk_player_position_id`,
					`fk_team_id`,
					`is_draft`
				   )
					VALUES
					(
					:member_draftteam_id,
					:fk_player_id,
					:starting_11,
					:fk_player_position_id,
					:team_id,
					0
					);'
                );
        $em->bindValue('member_draftteam_id', $iSquadId);
        $em->bindValue('fk_player_id', $iPlayerId);
        $em->bindValue('team_id', $iPlayerTeamId);
        $em->bindValue('starting_11', $starting);
        $em->bindValue('fk_player_position_id', $sPositionId);
        $em->execute();
    }
    
    public function getDraftTeamPlayersData($iSquadId)
    {
        $sSQL = "SELECT mdt.*,p.id AS player_id, p.value,p.position AS position_title,p.positionId AS player_position_id FROM `member_draftteam_players` mdt
        	INNER JOIN players p ON p.`id` = mdt.`fk_player_id`
        WHERE `member_draftteam_id` = :member_team_id GROUP BY mdt.`fk_player_id`";
        
        $em = $this->em->getConnection()->prepare($sSQL);
       
        $em->bindValue('member_team_id', $iSquadId);
        $em->execute();
        return $em->fetchAll();
    }
    
    public function getDraftPlayersByJoinedTeamIDs($iMemberTeamID, $joinedLeagues)
    {
        $aJoinedTeamsIDs = $this->em->getConnection()->prepare("SELECT `member_team_id` FROM squads WHERE `fk_saved_team_id` = :teamID AND `league` IN (:leagues)");
        $aJoinedTeamsIDs  ->bindValue('teamID', $iMemberTeamID);
        $aJoinedTeamsIDs ->bindValue('leagues', $joinedLeagues);
        $aJoinedTeamsIDs ->execute();
        $aJoinedTeamsIDs  ->fetchAll();
        
        $sSQL = "SELECT mdt.*, pd.value,pd.position AS position_title,p.positionId FROM `member_draftteam_players` mdt
        	INNER JOIN players p ON p.`id` = mdt.`fk_player_id`
        WHERE `member_draftteam_id` = :teamID GROUP BY mdt.`fk_player_id`";
        $aPlayers = $this->em->getConnection()->prepare($sSQL);
        $aPlayers->bindValue('teamID', $iMemberTeamID);
        $aPlayers->execute();
        $aPlayers->fetchAll();
        return $aPlayers;
    }
    
    public function deleteSquadPlayers($joinedTeamsIDs)
    {
        $sSQL = "DELETE FROM squads_players WHERE squad_id IN ({$joinedTeamsIDs}) AND (is_draft = 0 OR is_draft IS NULL)";
        return $this->em->getConnection()->prepare($sSQL)->execute();
    }
    
    public function deleteDraftTeamPlayers($iMemberTeamID)
    {
        $sSQL = "DELETE FROM member_draftteam_players WHERE member_draftteam_id = :teamID AND is_draft = 1";
        return $this->em->getConnection()->prepare($sSQL)->bindValue('teamID', $iMemberTeamID)->execute();
    }

    public function joinLeagueWithSavedTeam($iSquadId, $iLeagueId)
    {
        $request = Request::createFromGlobals();
        $session=new Session();
        $this->sRef = $session->get('user_ref');
        $clientId = $session->get('client');
        $username = $session->get('username');
        $join = false;

        if (! empty($iLeagueId)) {
            $aTeam = $this->getDraftTeamDetailsById($iSquadId);
            $aPlayers = $this->getDraftTeamPlayersData($iSquadId);

            $form_params = [
                'user'        => $this->sRef,//test user param
                'league'    => (int)$iLeagueId,
                'name'        => $aTeam['comment'],
                'formation' => (int)$aTeam['fk_formation_id'],

            ];
            $join = $this->sendDataToApiToJoin($aPlayers, $form_params);
        }

        $league = $this->leaguesRepository->getLeaguesById((int)$iLeagueId);
        $fEntryFee = $league['entryFee'] ? $league['entryFee'] : '0';
        if (isset($join['teamid'])) {
            $this->configRepository
                ->tid(
                    "insert",
                    "join",
                    $join['tid'],
                    $clientId,
                    $fEntryFee,
                    $username,
                    null,
                    $join['oulalaTid'],
                    null,
                    $join['teamid']
                );

            return ['teamId'=>$join['teamid']];
        } else {
            return ['error'=>$join['errors']];
        }
    }
    public function sendDataToApiToJoin($teamPlayers, $form_params)
    {
        $session=new Session();
        $tid = uniqid();
        $oulalaTid = null;
        $clientId =$session->get('client');
        $username =$session->get('username');
        $access_token = $this->tokenRepository->getClientToken($clientId)->access_token;
        $aPlayersCommon = new \stdClass();
        $aPlayersCommon->lineup = new \stdClass();
        $aPlayersCommon->bench = new \stdClass();

        //adding the keywords in bench with empty list
        $aPlayersCommon->bench->goalkeeper = [];
        $aPlayersCommon->bench->defenders = [];
        $aPlayersCommon->bench->midfielders = [];
        $aPlayersCommon->bench->strikers = [];
        //adding the keywords in linup with empty list
        $aPlayersCommon->lineup->goalkeeper = [];
        $aPlayersCommon->lineup->defenders = [];
        $aPlayersCommon->lineup->midfielders = [];
        $aPlayersCommon->lineup->strikers = [];
        $linup_count = 0;

        if (! empty($teamPlayers)) {
            foreach ($teamPlayers as $key => $value) {
                switch ($value['player_position_id']) {
                    case 1:
                        if ($value['starting_11']) {
                            $aPlayersCommon->lineup->goalkeeper[] = intval($value['player_id']);
                        } else {
                            $aPlayersCommon->bench->goalkeeper[] = intval($value['player_id']);
                        }
                        break;
                    case 2:
                        if ($value['starting_11']) {
                            $aPlayersCommon->lineup->defenders[] =intval($value['player_id']);
                        } else {
                            $aPlayersCommon->bench->defenders[] = intval($value['player_id']);
                        }
                        break;
                    case 3:
                        if ($value['starting_11']) {
                            $aPlayersCommon->lineup->midfielders[] = intval($value['player_id']);
                        } else {
                            $aPlayersCommon->bench->midfielders[] = intval($value['player_id']);
                        }
                        break;
                    case 4:
                        if ($value['starting_11']) {
                            $aPlayersCommon->lineup->strikers[] = intval($value['player_id']);
                        } else {
                            $aPlayersCommon->bench->strikers[] = intval($value['player_id']);
                        }
                        break;
                }
                if ($value['starting_11']) {
                    $linup_count++;
                }
            }
        }

        if ($linup_count >= 11) {
            $this->bIsValid = true;
        }
        $date=new \DateTime();
        $curdate=$date->format('Y-m-d h:m:i');
        if ($this->bIsValid) {
            $form_params['players']    = $aPlayersCommon;

            $league = $this->leaguesRepository->getLeaguesById((int)$form_params['league']);

            $fEntryFee = $league['entryFee'] ? $league['entryFee'] : '0';

            $squad = $this->apiService->APIRequest('POST', $this->squadsUrl, $access_token, $form_params);

            if (isset($squad->status) && $squad->status == 'success') {
                $oulalaTid = $squad->data->transaction;
                $bPayed = true;
                $teamId = (!empty($squad->data->teamId))?$squad->data->teamId:0;

                $sLeaguePayUrl = $this->configRepository->getConfigByKeyName('client_money_out', $clientId);
                /* league is NOT free */
                if (bccomp($fEntryFee, 0) == 1) {
//                    if ($this->getParameter('oulalatest')) {
//                        $sLeaguePayUrl = "http://deviframejulien.oulala.com/bet";
//                    }
                    $res = $this->leaguesRepository->moneyOut($sLeaguePayUrl, $fEntryFee, $teamId, (int)$form_params['league'], $tid);
                    if (isset($res->error)) {
                        $bPayed = false;
                    }
                } else {
                    $res = $this->leaguesRepository->moneyOut($sLeaguePayUrl, $fEntryFee, $teamId, (int)$form_params['league'], $tid);
                }

                if ($bPayed) {
                    $league_id = $form_params['league'];
                    $leagues=$this->leaguesRepository->getLeaguesById($league_id);
                    $updateParams=["wager_state"=>3];
                    $updateSquad = $this->apiService->APIRequest('PUT', $this->squadsUrl.'/update/'.$squad->data->teamId, $access_token, $updateParams);
                    $aRewardOption = $this->prizeRepository->getRewardDetails($form_params['league'], 1);
                    $this->squadRepository->updateOneSquadData($access_token, $squad->data->teamId);
                    $this->leaguesRepository->updateOneLeagueData($access_token, $form_params['league'], 1);
                    $this->matchRepository->updateOneLeagueMatchesAndUserData($access_token, ($form_params['league']));
                    $params = [
                        'league_name' => $leagues['name'],
                        'league_id' =>$form_params['league'],
                        'entry_fee' => $leagues['entryFee'],
                        'date_and_time' => $curdate,
                        'prize_distribution' => $aRewardOption['title'],
                        'team_id' =>  $squad->data->teamId,
                        'team_name' => $form_params['name'],
                        'league_starting_date_and_time' => $leagues['fromDate'],
                        'league_settlement_date_and_time' => $leagues['toDate'],
                        'user_ref'=> $form_params['user'],
                    ];

//                    $result = $this->apiService->APIRequest('POST', ApiUrls::URL_LEAGUES, $access_token, json_encode($params));
                    $this->configRepository
                        ->tid("insert", "join", $tid, $clientId, $fEntryFee, $username, null, $oulalaTid, null, $teamId);
                    return array(
                        'leagueId'        => $form_params['league'],
                        'teamid'        => $teamId,
                        'formation'        => $form_params['formation'],
                        'isSaved'        => 0,
                        'loadingteam'    => 0,
                        'tid'    => $tid,
                        'oulalaTid' => $oulalaTid
                    );
                } else {
                    if (isset($res)) {
                        $errors['errors'] = "code " .$res->errorCode . " - " . $res->errorDescription;
                        $this->configRepository
                            ->tid("insert", "join", $tid, $clientId, $fEntryFee, $username,
                                "code " .$res->errorCode . " - " . $res->errorDescription,
                                null,
                                null,
                                $teamId);
                    } else {
                        $errors['errors'] = "Client API UNREACHABLE";
                        $this->configRepository
                            ->tid("insert", "join", $tid, $clientId, $fEntryFee, $username,
                                "Client API UNREACHABLE",
                                null,
                                null,
                                $teamId);
                    }
                    return array('errors' => $errors);
                }
            } else {
                $errors['errors'] = implode(',<br>',$squad->data->violations);
                $errors['status'] = $squad->status;
                $this->configRepository
                    ->tid("insert", "join", $tid, $clientId, $fEntryFee, $username, json_encode($errors));
                return array('errors' => $errors);
            }
//            } else {
//                $errors['errors'] = "code ".$moneyExist->errorCode." - ".$moneyExist->errorDescription;
//                $this->configRepository
//                    ->tid("insert", "join", $tid, $clientId, $fEntryFee, $username,
//                        "code ".$moneyExist->errorCode." - ".$moneyExist->errorDescription);
//                return array('errors' => $errors);
//            }
        }
    }

    public function updateDraftTeamName($sTeamId, $sTeamName)
    {
        $sSQL = "UPDATE member_draftteams SET `comment`=:teamName WHERE member_draftteam_id = :teamID";
        $sUpdateSquad=$this->em->getConnection()->prepare($sSQL);
        $sUpdateSquad->bindValue('teamName', $sTeamName);
        $sUpdateSquad->bindValue('teamID', $sTeamId);
        $sUpdateSquad->execute();
        try {
            $sUpdateSquad->execute();
            $output=true;
        } catch (\PDOException $e) {
            $output=false;
        }
        return $output;
    }
}
