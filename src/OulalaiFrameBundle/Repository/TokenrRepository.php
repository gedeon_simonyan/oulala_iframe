<?php

namespace OulalaiFrameBundle\Repository;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManager;
use OulalaiFrameBundle\ApiManager\ApiUrls;
use OulalaiFrameBundle\ApiManager\Manager;
use OulalaiFrameBundle\ApiService\Api;
use OulalaiFrameBundle\ApiService\ApiGuzzle;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Created by Oulala.
 * User: JUL
 * Date: 11/05/2017
 * Time: 14:09
 */
class TokenrRepository
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var ContainerInterface
     */
    private $container;

    private $content;

    /**
     * @var Api
     */
    private $apiService;

    /**
     * @var
     */
    private $accessTokenUrl;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em, ContainerInterface $container, Api $apiService)
    {
        $this->em = $em;
        $this->container=$container;
        $this->accessTokenUrl = $this->container->getParameter('api_auth_url');
        $this->apiService = $apiService;
    }

    public function getToken()
    {
        $this->getClientData('api', 1);

        $access_token = $this->apiService->APIRequest("POST", $this->accessTokenUrl, '', $this->content);
        if (isset($access_token->error)) {
            return "Invalid token";
        }
        return $access_token->access_token;
    }

    public function getAllTokens()
    {
        $access_token=[];
        $contents=$this->getClientData('api');

        foreach ($contents as $key=>$content) {
            $token=$this->apiService->APIRequest("POST", $this->accessTokenUrl, '', $content);

            if (!isset($token->error) && isset($token->access_token)) {
                $access_token[$key]['token'] = $token->access_token;
                $access_token[$key]['client_id'] = $content['form_params']['client_id'];
            }
        }
        return $access_token;
    }

    public function getClientToken($client_id)
    {
        if(!$client_id){
            $client_id = false;
        }
        $contents=$this->getClientData('api', $client_id);

        $token=array();
            foreach ($contents as $key => $value){

                $token[$key] = $this->apiService->APIRequest("POST", $this->accessTokenUrl, '', $contents[$key]);
                $token[$key]->client_id = $value['form_params']['client'];
            }

            if(count($token)==1){
                $token=$token[0];
            }
        return $token;
    }

    public function getIframeTokens($client_id)
    {
        $access_token=[];
        $contents=$this->getClientData('iframe', $client_id);

        foreach ($contents as $key=>$content) {
            //            $url=$this->configRepository->getConfigByKeyName('iframe_toke_url',$client_id);

            $url=$this->container->get('OulalaiFrame.repository.config')->getConfigByKeyName('iframe_toke_url', $client_id);
            $client_website=$this->container->get('OulalaiFrame.repository.config')->getConfigByKeyName('client_money_in', $client_id);
            $token=$this->apiService->APIRequest("POST", $url, '', $content['content']);

            if (!isset($token->error)) {
                $access_token['token'] = $token->access_token;
                $access_token['url_in'] = $client_website;
            }
        }

        return $access_token;
    }

    private function getClientData($type, $id=0)
    {
        $content = [];
        $where='';
        if ($id) {
            $where=' AND `client_id`='.$id;
        } else {
            $where = '';
        }

        $connection = $this->em->getConnection();
        $trans = $connection->prepare('SELECT `config_key` ,`config_value`  FROM `config` WHERE 
                          `config_key` IN ("'.$type.'_client_secret", "'.$type.'_client_id") '.$where);
        $trans->execute();
        $secrets = $trans->fetchAll();

        if ($secrets) {
            foreach ($secrets as $secret) {
                if ($secret['config_key'] == $type.'_client_secret') {
                    $client_secret = $secret['config_value'];
                } elseif ($secret['config_key'] == $type.'_client_id') {
                    $client_id = $secret['config_value'];
                }
            }
        }

        $em1 = $connection->prepare('SELECT `client_id` FROM `config` 
                      WHERE `config_key`="'.$type.'_username"'.$where.' GROUP BY `client_id`');
        $em1->execute();
        $clients = $em1->fetchALL();

        if ($clients) {
            foreach ($clients as $key=>$client) {
                $em2 = $connection->prepare('SELECT `config_key`, `config_value`  FROM `config` 
                 WHERE `config_key` IN("'.$type.'_username","'.$type.'_password", "'.$type.'_client_secret", "'.$type.'_client_id")
                  AND `client_id`=:id ORDER BY `config_key` ASC');
                $em2->bindValue(':id', $client['client_id']);
                $em2->execute();
                $client_datas = $em2->fetchALL();

                if ($client_datas) {
                    foreach ($client_datas as $client_data) {
                        if ($client_data['config_key'] == $type.'_password') {
                            $client_password = $client_data['config_value'];
                        } elseif ($client_data['config_key'] == $type.'_username') {
                            $client_username = $client_data['config_value'];
                        } elseif ($client_data['config_key'] == $type.'_client_secret') {
                            $client_secret = $client_data['config_value'];
                        } elseif ($client_data['config_key'] == $type.'_client_id') {
                            $client_id = $client_data['config_value'];
                        }
                    }
                    if ($client_id && $client_secret && $client_username && $client_password) {
                        $content[$key]['form_params']=[
                           'grant_type'=> 'password',
                           'client_id'=> $client_id,
                           'client_secret'=> $client_secret,
                           'username'=> $client_username,
                           'password'=> $client_password,
                            'client'=>$client['client_id']

                        ];

//                        $content[$key]['content'] = 'grant_type=password&client_id=' . $client_id . '&client_secret=' . $client_secret . '&username=' . $client_username . '&password=' . $client_password;
//                        $content[$key]['client_id'] = $client['client_id'];
                        $this->content=$content[0];
                    } else {
                        $content=[];
                    }
                }
            }
        }

        return $content;
    }


    public function cleanUserRef($ref)
    {
        $tmp_ref_1=str_replace('91ad6fe19f4832f96794a37650224dd2', '', $ref);
        $user_ref=str_replace('20f17eff19ca2d84f143e70cf7ba2c51', '', $tmp_ref_1);

        return $user_ref;
    }
}
