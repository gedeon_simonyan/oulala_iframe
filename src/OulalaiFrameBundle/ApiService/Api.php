<?php
namespace OulalaiFrameBundle\ApiService;

use Buzz\Client\Curl;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\HttpFoundation\Request;
use Buzz\Browser;

/**
 * Class Manager
 * This class implements API calls.
 */
class Api
{
    protected $logger;

    public function __construct($logger)
    {
        $this->logger = $logger;
    }
    /**
     * Method: POST, PUT, GET etc, default GET
     * Data: array("param" => "value") ==> index.php?param=value
     * @param string $method
     * @param string $url
     * @param string $access_token
     * @param array $data
     * @return mixed
     */
    public function APIRequest($method = 'GET', $url = '', $access_token = '', $sendData = '', $log = true)
    {
        if ($log) {
            $this->logger->info('[' . $method . ']' . $url . ' data: ' . json_encode($sendData));
        }

        $browser = new Client();
        $response = array();
//        $data = array();
        if ($access_token) {
            $data['headers'] = ['Authorization'=> 'Bearer ' . $access_token];
//            $headers[] = 'cache-control: no-cache';
//            $headers[] = 'content-type: application/json';
        }
        if(isset($sendData['form_params'])){
            $data=$sendData;
        }elseif($sendData){
            $data['json']=$sendData;
        }
        try{
            switch ($method) {

                case "POST":
                    if ($data) {
                        $response = $browser->post($url,  $data);
                    }
                    break;
                case "PUT":
                    $response = $browser->put($url, $data);
                    break;
                case "GET":

                    $response = $browser->get($url, $data);
                    break;
            }

//            if (empty($response->getBody()->getContents())) {
//                echo "API is not available. ";
//                $this->logger->debug('API is not available for [' . $method . ']' . $url);
//
//            }
//            if ($log) {
//                $this->logger->debug('Response: ' . $response->getBody()->getContents());
//            }

            return json_decode($response->getBody()->getContents());
        }catch(RequestException $e){
//            return ['error'=>$e->getCode(),'message'=>json_decode($e->getResponse()->getBody()->getContents())];
            return json_decode($e->getResponse()->getBody()->getContents());
        }

    }
}
