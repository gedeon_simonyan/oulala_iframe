var MatchViewModel = new MatchViewModel(matches,positions);
ko.applyBindings({
    MatchViewModel: MatchViewModel
});

setUpSocket(function(socket) {
    for (var i = 0; i < matches.length; i++) {
        socket.emit('register_match', matches[i]);
        registerMatchHandler(matches[i], socket);
    }
});

function registerMatchHandler(match, socket) {
    socket.on("match_score_" + match.matchday_id + "_" + match.home_team_id + "_" + match.away_team_id, function(data) {
        if (data.result) {

            var id = data.result.match_id;
            var index = match_ids[id];
            if (index === parseInt(index) && data.result.minute !== undefined ) {
                //status

                if (data.result.status == "complete") {
                    matches[index].is_completed(true);
                } else if (data.result.status == "inplay") {
                    matches[index].is_inplay(true);
                }

                //minute
                if (data.result.minute == 1000) {
                    matches[index].minute(90);
                } else {
                    matches[index].minute(data.result.minute);
                }

                //score
                if (matches[index].home_team_goals() != data.result.home_goals){
                    matches[index].home_team_goals(data.result.home_goals);
                }
                if (matches[index].away_team_goals() != data.result.away_goals){
                    matches[index].away_team_goals(data.result.away_goals);
                }

            }
        }
    });

}
