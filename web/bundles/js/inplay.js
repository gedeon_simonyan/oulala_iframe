var timeline = new Timeline();
var inplayRow = 0;

for (var i=players.length-1; i >= 0 ; i--) {
	for (var j=0; j < players.length; j++) {
		if (players[i].sub_of == players[j].player_id) {
			players[i].is_sub = true;
			players[i].row = players[j].row;
			
			break;
		}
	}
	
	if (players[i].is_playing) {

		if (players[i].is_sub) {
			timeline.inplay.splice(players[i].row, 0, ko.observableArray( [ players[i] ] ));
		} else{

			timeline.inplay.push( ko.observableArray([ players[i] ]) );
			players[i].row = inplayRow;
			inplayRow++;
		}
	} else {
		if (players[i].is_sub) {
			if (players[i].row != undefined) {
				timeline.inplay()[players[i].row]().splice(1,0,players[i]);
			}
		} else {
	  		timeline.subs.push(ko.observableArray([ players[i] ]));
		}
	}
}

var tempScore = ko.observable(0);

ko.applyBindings({
	players: players,
	formations: formation,
	totalScore: ko.computed(function () {
		var score = 0;
		for (var i = 0; i < players.length; i++) {
			if (players[i].member_played_to()() > 0) {
				score += players[i].score();
			}
		}

		return score;
	})
});


setUpSocket(function(socket) {

    for (var i = 0; i < players.length; i++) {

		bindPlayerHover(players[i].player_id);

        // if(!players[i].is_offline() ){
            var player = players[i];
            player.minute_to  = (player.minute_to == 0?1000:player.minute_to);
            socket.emit('register_player', player);
        // } else {
        //     resetTips(players[i].player_id);
        // }
		socket.on("score_data_" + players[i].player_id + "_" + players[i].matchday_id + "_" + players[i].member_played_from() +  "_" + (players[i].member_played_to()() > 0?players[i].member_played_to()():1000), function(data) {

			if (data.result) {
				var id = data.result.player_id;
				// console.log(data.result);
				var index = $.inArray(parseInt(id), player_ids);
			
				if (index != -1) {
					if ( players[index].is_completed() != true ) {
                        if (data.result.status == 'noscore') {
                            players[index].score(null);
                        } else {
                            players[index].score(data.result.score);
                        }
                    }

					players[index].is_completed((data.result.status == "complete")?true:false);
                    players[index].is_offline((data.result.status == "complete")?true:false);
					players[index].minute(parseInt(data.result.minute));

					if (players[index].events != undefined) {

						players[index].events(data.result.events);
                        players[index].status();
                        
                        if (data.result.events) {
                            for (var k = 0; k < data.result.events.length; k++) {
                                if (data.result.events[k].ends_match == "1" &&
                                    data.result.events[k].event_type_id == "3") {
                                    // here be dragons
                                    for (var x = 0; x < timeline.inplay().length; x++) {
                                        for (var o = 0; o < timeline.inplay()[x]().length; o++) {
                                            if (id == timeline.inplay()[x]()[o].player_id) {
                                                var red_card_minute = timeline.inplay()[x]()[o].red_card_at()();
                                                for (var y = 0; y < timeline.inplay()[x]().length; y++) {
                                                    if ( timeline.inplay()[x]()[y].member_played_from() >= red_card_minute ) {

                                                        var subindex = $.inArray(timeline.inplay()[x]()[y].player_id, player_ids);
                                                        if ( subindex > -1 )
                                                        {
                                                            players[subindex].score(0);
                                                            players[subindex].is_completed(true);
                                                            players[subindex].is_offline(true);
                                                        }
                                                    }
                                                }
                                                timeline.inplay()[x].valueHasMutated();

                                            }
                                        }
                                    }
                                }
                            }
                        }

                        resetTips(id);
					}

					if ($(".inplay-sidepanel.table tr.p" + id + " .score").text() != data.result.score) {

						$('.field .player.p' + id).css("position", "relative").animate({ opacity:'0.5' }, 500, function() {
							$(this).animate({ opacity:'1' }, 500);
						});
					}
				}

				bindPlayerHover(id);

				$(".side-board .p" + id + " .score").text(data.result.score);

				$(".p" + id).fadeIn();
            }
		});
	}
});

function resetTips(player_id) {
    // reset tips
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || $(window).width() < 768 ) {
		console.log('tooltip not availabel on mobile devices');
	}else {
		$('.timeline-bg .timeline .player.p' + player_id + ' .event').qtip({
			content: {
				text: function(api) {
					return $(this).find('.tip').clone();
				}
			}, position: {
				my: 'bottom center',
				at: 'top center'
			}, style: {
				classes: 'qtip-shadow qtip-oulala'
			}
		});
	}	
}

$(document).ready(function() {
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || $(window).width() < 768 ) {
	}else {
		$('.legend-box').qtip({
			content: {
				text: function(api) {
					return $(this).find('.legend-tip').clone();
				}
			}, position: {
				my: 'bottom center',
				at: 'top center'
			}, style: {
				classes: 'qtip-shadow qtip-oulala'
			}
		});
	}	
});